//
// Created by Ghufran on 19/07/2022.
//

#ifndef OUR_IDEA_SHAREDLIB_H
#define OUR_IDEA_SHAREDLIB_H

#include <algorithm>
//#define totalSpace 50 * (1024 * 1024 * 1024)
#include <utility>  //declarations of unique_ptr
#include <atomic>
#include <random>
#include <chrono>
#include <ctime>
#include<vector>

#include <memory>
#include <stdlib.h>
#include "ThirdParty/oneTBB-2020.2/include/tbb/concurrent_hash_map.h"

using namespace std::chrono;
typedef uint64_t vertex_t;
typedef uint64_t timestamp_t;
typedef int64_t artKeyType;
typedef int32_t counter_t;
//typedef *reinterpret_cast<uint64_t*> INTMEM;
typedef unsigned char stalb_type;
typedef uint8_t *uint8_ptr;
typedef uint16_t *uint16_ptr;
typedef uint32_t *uint32_ptr;
typedef uint64_t *uint64_ptr;
typedef tbb::concurrent_hash_map<vertex_t, vertex_t> l_t_p_table;

extern void *EMPTY_SLOT;
extern void *FLAG_DELETE;
#define PAGE_SIZE 8
#define MEMORY_VECTOR_SIZE 16
#define MEMORY_QUEUE_SIZE 1024
#define fixedSize_queue 16
#define START_BLOCK_UPDATE_SIZE 1
#define END_BLOCK_UPDATE_SIZE 1024
#define StartFixedBlockThreshold 11
#define EndFixedBlockThreshold 11

#define BLOCKSIZEFIXED 4096

// optimal block size for power degree distribution
/* ########## Debug puposes ######### */
//#define DBUGFLAGPERSOURCE
#define DEBUG_BFS
//#define DBUGFLAGG
#define LOGFILEPATH  "/local/user/LOGFILE/log.txt"
#define COUT_DEBUG_FORCE(msg) { std::scoped_lock<std::mutex> lock{::gfe::_log_mutex}; std::cout << "[Interface::" << __FUNCTION__ << "] " << msg << std::endl; }
#if defined(DEBUG)
#define COUT_DEBUG(msg) COUT_DEBUG_FORCE(msg)
#else
#define COUT_DEBUG(msg)
#endif

#if defined(DEBUG_BFS)
#define COUT_DEBUG_BFS(msg) COUT_DEBUG(msg)
#else
#define COUT_DEBUG_BFS(msg)
#endif
//#define DBUGFLAGPERSOURCE
//#define DBUGFLAGG

#define SRC 5236192 // source to debug
#define DST 3133953 // destination node to debug

/* ########## end ######### */

#define bucketPerIndexLimit 10
#define blockUpdateSize 64


#define FILEPATH_SORTED_BY_GRAPH500_24 "/local/user/sorted-graph500-24/"
#define FILEPATH  "/local/user/graph500-24-workload1/graph500-24-workload/"
#define FILEPATH_OOO_UPDATES  "/local/user/ooo-folder/graph500-24/txtFile/"
#define FILEPATH_OOO_UPDATES_BINARY  "/local/user/ooo-folder/graph500-24/binaryFile/"

#define TRANS_SIZE 4
#define fixedBlockSize 8
// (address & 0x1)
#define THRESHOLDINVALID 20
#define fixedSize 1
#define datasetSize 64155735ul //260379520ul
#define vertexArraySize 20
#define VertexArrayIndr  8
#define MutexLockSize 16777216
#define threadSize 1
#define IS_EDGE_BLOCK 0x1
//const vertex_t PRIMETABLE[9] = {4ul,16ul,128ul,512ul,1024ul,8192ul,12288ul,102400ul, 737280ul};
const vertex_t PRIMETABLE[9] = {4ul, 16ul, 128ul, 512ul, 1024ul, 8192ul, 16384ul, 131072ul, 524288ul};
const u_int64_t FLAG_EMPTY_SLOT = 0xFFFFFFFFFFFFFFFF;
const u_int64_t FLAG_TOMB_STONE = 0xFFFFFFFFFFFFFFFE;
const int64_t MaxValue = 9223372036854775807;

// took from sortledton
inline uint64_t get_pointer(uint64_t pointer_number)
{
    return pointer_number &= ~0x1;;
}

//const vertex_t PRIMETABLE[8] = {16u,128u,1024u,12289u,98317ul, 196613ul,393241ul};
#define FOURMB 4L*(1024L)*(1024L)
#define EIGHTMB 8L*(1024L)*(1024L)
#define SIXTEENMB 16L*(1024L)*(1024L)


#define tot_no_edges 63497050
#define cirEdgeBuffer 17
#define start_time 1612134000 //1262300400
#define LENGTH (MB_TO_RESERVE*1024*1024*1024)
#define MAP_HUGE_1GB (30 << MAP_HUGE_SHIFT)
#define startBlockSize 100;

// took from sortledton
#define INITIAL_VECTOR_SIZE 16777216

#define thread_prefix 1023

typedef double property_type;

// for bitmaskting distribution ranges
// start index out-of-order updates
const int64_t L1IndexStart = 0;
const int64_t L1SizeStart = 32;
const int64_t L2IndexStart = 37;
const int64_t L2SizeStart = 48;
const int64_t DNPStart = 52;
const int64_t IsSTALStart = 62;
const int64_t IsInOrderStart = 63;
// each index bits required inorderupdate
const int64_t L1IndexBits = 32;
const int64_t L1SizeBits = 5;
const int64_t L2IndexBits = 11;
const int64_t L2SizeBits = 4;
const int64_t DNPBits = 10;
const int64_t IsSTALBits = 1;
const int64_t IsInOrderBits = 1;

// out-of-order update start
const int64_t t_s_start = 0;
const int64_t DNPStart_OOO = 48;
const int64_t IsOutOrderStart = 63;

// out-of-order update end bits
const int64_t t_s_bits = 48;
const int64_t DNP_ooo_bits = 15;
const int64_t IsOutOrderBits = 1;

// version for dest id
#define thread_prefix_OOO 32767 /// 15 bits
#define dest_id_MASK (1L << 63)
#define add_version(e) (e | dest_id_MASK)
#define remove_version(e) (e & ~dest_id_MASK)
#define check_version(e) (e & dest_id_MASK)

#define dest_id_read_MASK (1L << 62)
#define add_version_read(e) (e | dest_id_read_MASK)
#define remove_version_read(e) (e & ~dest_id_read_MASK)
#define check_version_read(e) (e & dest_id_read_MASK)

#define dest_id_ooo_MASK (1L << 61)

#define add_version_ooo(e) (e | dest_id_ooo_MASK)
#define remove_version_ooo(e) (e & ~dest_id_ooo_MASK)
#define check_version_ooo(e) (e & dest_id_ooo_MASK)


//// end version dest id
#define NOTFOUNDKEY (1L << 61)
const vertex_t totalSpace = 50L * (1024L) * (1024L) * (1024L);

u_int64_t setBitMask(u_int64_t index, u_int64_t val, u_int64_t pos, u_int64_t bitsNeeded);

u_int64_t getBitMask(u_int64_t index, u_int64_t pos, u_int64_t bitsNeeded);

std::size_t CalculatePadding(std::size_t baseAddress, const std::size_t alignment);

int getCurSizeTable(int curSize);

uint64_t rdtsc();

void initbyte();

u_int64_t setBitMasktest(u_int64_t index, u_int64_t val, u_int64_t pos, u_int64_t bitsNeeded);

int getIndrVertexArrayIndex(vertex_t srcVertex);

int getVertexArrayIndex(vertex_t srcVertex, int divSrcVertexId);

const char *getArtKey(std::string keyStr);

std::string getArtKeyTime(int64_t key);

const char *getArtKeyByMonth(std::string keyStr);

timestamp_t getCurrTimeStamp();

timestamp_t getNextSecond(timestamp_t cur_time);

time_t getArtIndex(time_t des_t);

std::string addZero(int mon_v);

artKeyType getHourT(std::string keyStr);

int random1(int range_from, int range_to);

artKeyType extractHour(timestamp_t time_u);

const char *extractKeyStr(timestamp_t time_u);

const char *extractKeyStr1(time_t time_u);

const char *extractKeyStrbymonth(time_t time_u);

std::vector<artKeyType> getCheckpointKeys(timestamp_t start_c, timestamp_t end_c);

#endif //OUR_IDEA_SHAREDLIB_H
