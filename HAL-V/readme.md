## HAL dynamic graph

The source code to the HAL-V a variant of HAL dynamic graph system which store per leaf single out-of-order updates. The results you can see in the Figure 9.

---
### Prerequisite

- Intel Threading Building Blocks 2 (version 2020.1-2)
- A compiler compliant with C++17 standards and equipped with OpenMP support. We employed GCC 10.
---

### Building
To build the static HAL-V
<kbd>mkdir build && cd build && cmake .. &&  make</kbd>
