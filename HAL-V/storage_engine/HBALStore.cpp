//
// Created by Ghufran on 25/01/2023.
//
#include "HBALStore.h"

/* HBALStore defination */
PerSourceVertexIndr ***HBALStore::vertexIndrList;
vertex_t HBALStore::curIndVertexSize;
int64_t HBALStore::numEdges = 0;
vertex_t HBALStore::traverse_count_t = 0;
std::atomic<vertex_t>  HBALStore::maxSourceVertex;
//spinlock HBALStore::querylock{0};

bool HBALStore::isReadActive = false;
bool HBALStore::isDegreeActive = false;

std::mutex HBALStore::vertexTableResizeLock;
double HBALStore::loadingTime = 0.0;
vertex_t HBALStore::outOfOrderCout = 0;

// ReadTable classes
inline int HBALStore::findIndexForIndr(stalb_type **arr, int start, int n, timestamp_t K)
{
    // Lower and upper bounds
    int end = n - 1;
    // Traverse the search space
    timestamp_t eih_cur_time = 0;
    timestamp_t eih_end_time = 0;
    while (start <= end)
    {
        int mid = (start + end) / 2;
        if (!check_version(reinterpret_cast<vertex_t>(arr[mid])))
        {
            u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(arr[mid]);
            EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(arr[mid] + ((eb_block_size / 2) * 8) + ((*reinterpret_cast<uint16_ptr>(arr[mid] + 1) * 8) - 8));
            EdgeInfoHal *eih_end = *reinterpret_cast<EdgeInfoHal **>(arr[mid] + ((eb_block_size / 2) * 8) + ((((eb_block_size / 2) - 1) * 8) - 8));
            eih_cur_time = eih_cur->getSTime();
            eih_end_time = eih_end->getSTime();
        }
        else
        {
            //
            eih_cur_time = remove_version(reinterpret_cast<vertex_t>(arr[mid]));
            eih_end_time = eih_cur_time;
            std::cout << "verion deleted" << std::endl;
        }
        //
        if (eih_cur_time > K && eih_end_time < K)
            return mid;
        else if (eih_cur_time > K)
            start = mid + 1;
        else
            end = mid - 1;
    }
    // Return insert position
    return end;
}

inline int HBALStore::findIndexForBlockArr(stalb_type *arr, int start, int eb_block_size, timestamp_t K)
{
    // Lower and upper bounds
    int end = eb_block_size - 1;
    // Traverse the search space
    while (start <= end)
    {
        int mid = (start + end) / 2;
        // If K is found
        EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(arr + ((eb_block_size) * 8) + ((mid * 8) - 8)); // %%%% start here

        if (eih_cur->getSTime() == K)
            return mid;
        else if (eih_cur->getSTime() > K)
            start = mid + 1;
        else
            end = mid - 1;
    }
    // Return insert position

    return end;
}

bool HBALStore::InsertSrcVertex(vertex_t vertex_id, MemoryAllocator *me, int thread_id)
{

    vertex_t p_id;
    l_t_p_table::accessor w;
    if (logical_to_physical.insert(w, vertex_id))
    {
        p_id = high_water_mark.fetch_add(1);

        grow_vector_if_smaller(index, p_id, PerSourceVertexIndr());  // TODO Should I do this earlier and assynchronous
        grow_vector_if_smaller(physical_to_logical, p_id, FLAG_EMPTY_SLOT);


        w->second = p_id;
        // aquire_vertex_lock_p(p_id);
        w.release();

        // Update index
        //  assert(index[p_id].adjacency_set & VERTEX_NOT_USED_MASK);
        index[p_id].edgePtr = NULL;
        index[p_id].degree = 0;
        index[p_id].latestSrcTime = 0;
        index[p_id].hashPtr = NULL;
        index[p_id].perVertexLock.initLock();
        //  index[p_id].lock.init(); // = perVertexLock.initLock();
        // Update logical mapping
        // assert(physical_to_logical[p_id] & VERTEX_NOT_USED_MASK);

        physical_to_logical[p_id] = vertex_id;

        // Update vertex count
        vertex_count.fetch_add(1);

        return true;
    }
    else
    {
        p_id = w->second;
        w.release();
        // aquire_vertex_lock_p(p_id);
        return false;
    }
    //
    /* vertex_t divSrcVertexId = getIndrVertexArrayIndex(vertex_id);
     if (vertexIndrList[divSrcVertexId] == NULL) {
         // global lock on vertex table for resize
         std::scoped_lock<std::mutex> l(vertexTableResizeLock);
         if (vertexIndrList[divSrcVertexId] == NULL) {
             resizeVertexArrayHal(divSrcVertexId, me, thread_id);
         }
     }*/
    /* if (maxSourceVertex < vertex_id)
     {
         // would be criticle in case frequent request for changing maxSourceVertex id
         // remove in that case the condition and atomic part and think about per thread max_SourceVertex solution
         maxSourceVertex = vertex_id;
     }*/
    // return divSrcVertexId;
}

// took from sortledton
vertex_t HBALStore::logical_id(vertex_t v)
{
    assert(v < high_water_mark);
    return physical_to_logical[v];
}

vertex_t HBALStore::physical_id(vertex_t v)
{
    l_t_p_table::const_accessor a;
    logical_to_physical.find(a, v);
    return a->second;
}

PerSourceVertexIndr const &HBALStore::operator[](size_t v) const
{
    return index[v];
}

PerSourceVertexIndr &HBALStore::operator[](size_t v)
{
    return index[v];
}

size_t HBALStore::get_high_water_mark()
{
    return high_water_mark.load();
}

size_t HBALStore::get_vertex_count(vertex_t version)
{
    return vertex_count.load();
}


// resize block upward double STAL
UPBlockSizePointer *HBALStore::UpEdgeBlock(stalb_type *stalb, uint8_t cur_block_size, MemoryAllocator *me, int thread_id)
{
    uint8_t prev_size = cur_block_size;
    uint8_t new_size = ++cur_block_size;
    uint32_t new_size_power = ((int16_t) 1 << new_size);

    stalb_type *temp = static_cast<stalb_type *>(me->Allocate((8 * new_size_power), thread_id));

    double *property_temp = static_cast<double *>(me->Allocate((8 * (new_size_power / 2)), thread_id));

    memset(temp, 0, (8 * new_size_power));
    memset(property_temp, 0, (8 * (new_size_power / 2)));

    ////////////////////////////

    if (*reinterpret_cast<uint8_ptr>(stalb + 3))
    {
        *reinterpret_cast<uint8_ptr>(temp + 3) = 1;
        *reinterpret_cast<uint8_ptr>(temp + 5) = *reinterpret_cast<uint16_ptr>(stalb + 5);
    }


    ///////////////////////////

//  FixedTimeIndexBlock **temp2;
// todo deletion mechanism ()create node in globle linklist and add the address of deleted index array and with time to replace
// the garbage collector will deallocate the memory than
    // u_int64_t k = 0;

    uint32_t prev_block_element_size = ((uint32_t) 1 << prev_size) / 2;

    memcpy(temp + ((prev_block_element_size * 8) + 8), stalb + 8, (prev_block_element_size - 1) * 8); // dest id

    memcpy(temp + ((prev_block_element_size * 8) + ((new_size_power / 2) * 8)), (stalb + (prev_block_element_size * 8)), ((prev_block_element_size - 1) * 8)); // edgeinfo

    double *property = *reinterpret_cast<double **>(stalb + ((((uint32_t) 1 << prev_size) * 8) - 8));

    memcpy(property_temp + prev_block_element_size + 1, property + 1, (prev_block_element_size - 1) * 8); // property block

    property_temp[0] = 0;

    *reinterpret_cast<double **>(temp + ((new_size_power * 8) - 8)) = property_temp; // update the address of property update

    *reinterpret_cast<uint8_ptr>(temp) = log2(new_size_power); // block size

    *reinterpret_cast<uint16_ptr>(temp + 1) = prev_block_element_size + 1; // cur_index

    // property pointer to free
    // stalb prev pointer to delete
    //  prev_block_size
    //  new stalb pointer

    UPBlockSizePointer *up_block = static_cast<UPBlockSizePointer *>(me->Allocate(sizeof(UPBlockSizePointer), thread_id));
    up_block->new_stalb = temp;
    up_block->prev_stalb = stalb;
    up_block->prev_property_arr = property;
    up_block->prev_block_size = (uint64_t) 1 << prev_size;

    return up_block;
}
// downsize the block

// resize block upward double STAL
stalb_type *HBALStore::DownEdgeBlock(uint8_t cur_block_size, MemoryAllocator *me, int thread_id)
{
    uint8_t new_size = --cur_block_size;
    uint32_t new_size_power = ((int32_t) 1 << new_size);

    stalb_type *temp = static_cast<stalb_type *>(me->Allocate((8 * new_size_power), thread_id));

    double *property_temp = static_cast<double *>(me->Allocate((8 * (new_size_power / 2)), thread_id));

    memset(temp, 0, (8 * new_size_power));
    memset(property_temp, 0, (8 * (new_size_power / 2)));

    *reinterpret_cast<double **>(temp + ((new_size_power * 8) - 8)) = property_temp; // update the address of property update

    *reinterpret_cast<uint8_ptr>(temp) = log2(new_size_power); // block size

    *reinterpret_cast<uint16_ptr>(temp + 1) = (new_size_power / 2); // cur_index

    // property pointer to free
    // stalb prev pointer to delete
    //  prev_block_size
    //  new stalb pointer


    return temp;
}
/// end

bool HBALStore::InsertDestVertex(vertex_t vertex_id, MemoryAllocator *me, int thread_id)
{
    vertex_t p_id;
    l_t_p_table::accessor w;
    if (logical_to_physical.insert(w, vertex_id))
    {
        p_id = high_water_mark.fetch_add(1);

        grow_vector_if_smaller(index, p_id, PerSourceVertexIndr());  // TODO Should I do this earlier and assynchronous
        grow_vector_if_smaller(physical_to_logical, p_id, FLAG_EMPTY_SLOT);


        w->second = p_id;
        // aquire_vertex_lock_p(p_id);
        w.release();

        // Update index
        //  assert(index[p_id].adjacency_set & VERTEX_NOT_USED_MASK);
        index[p_id].edgePtr = NULL;
        index[p_id].degree = 0;
        index[p_id].latestSrcTime = 0;
        index[p_id].hashPtr = NULL;
        index[p_id].perVertexLock.initLock(); // = perVertexLock.initLock();
        // Update logical mapping

        physical_to_logical[p_id] = vertex_id;

        // Update vertex count
        vertex_count.fetch_add(1);

        return true;
    }
    else
    {
        p_id = w->second;
        w.release();
        // aquire_vertex_lock_p(p_id);
        return false;
    }
    // return divSrcVertexId;
}

bool HBALStore::CheckOutOfOrder(timestamp_t src_timestamp, vertex_t phy_src)
{

    if (src_timestamp >= index[phy_src].getLatestSrcTime())
    {
        index[phy_src].setLatestSrcTime(src_timestamp);
    }
    else
    {
        return true;
    }

    return false;
}

void HBALStore::updateNewSTALB(vertex_d cur_edge_b, vertex_t new_edge_b, vertex_t phy_src_id, stalb_type *old_eb, stalb_type *new_eb, property_type *old_property, MemoryAllocator *me, int thread_id)
{
    property_type *new_property = *reinterpret_cast<property_type **>( new_eb + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(new_eb)) * 8) - 8)); // property
    int counter_block = 0;
    for (int l = ((cur_edge_b / 2) - 1); l >= *reinterpret_cast<uint16_ptr>(old_eb + 1); l--)
    {
        if (!check_version(*reinterpret_cast<uint64_ptr>(old_eb + (l * 8))))
        {
            uint16_t newEb_index = *reinterpret_cast<uint16_ptr>(new_eb + 1);

            *reinterpret_cast<uint64_ptr>(new_eb + ((newEb_index - 1) * 8)) = *reinterpret_cast<uint64_ptr>(old_eb + (l * 8)); // dest id

            *reinterpret_cast<EdgeInfoHal **>(new_eb + ((new_edge_b / 2) * 8) + (((newEb_index - 1) * 8) - 8)) = *reinterpret_cast<EdgeInfoHal **>(old_eb + ((cur_edge_b / 2) * 8) + ((l * 8) - 8)); //  IEM block

            new_property[newEb_index - 1] = old_property[l]; // property array

            *reinterpret_cast<uint16_ptr>(new_eb + 1) = *reinterpret_cast<uint16_ptr>(new_eb + 1) - 1; // update the current index in new stalb

            AdjacentPos apn; // = static_cast<AdjacentPos *>(me->Allocate(sizeof(AdjacentPos), thread_id));
            apn.indrIndex = -1;
#ifdef DBUGFLAGPERSOURCE
            if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                            {
                                                testLock.lock();
                                                foutput << numEdges<<" address 3 "<<eh<<std::endl;
                                                testLock.unlock();
                                            }
#endif
            AdjacentPos *ap_pos = index[phy_src_id].getDestNodeHashTableVal(*reinterpret_cast<uint64_ptr>(old_eb + (l * 8)), &apn);
            //                    testLock.lock();
#ifdef DBUGFLAGPERSOURCE
            if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                            {
                                                testLock.lock();
                                                foutput << numEdges << " address 4" << eh << std::endl;
                                                testLock.lock();
                                            }
#endif
            //testLock.unlock();
            u_int64_t adjac_posi = index[phy_src_id].getHashTable()->hashtable[ap_pos->hashTableIndex];

            adjac_posi = setBitMask(adjac_posi, *reinterpret_cast<uint16_ptr>(new_eb + 1), L2IndexStart, L2IndexBits);
            adjac_posi = setBitMask(adjac_posi, *reinterpret_cast<uint8_ptr>(new_eb), L2SizeStart, L2SizeBits);

            index[phy_src_id].getHashTable()->hashtable[ap_pos->hashTableIndex] = adjac_posi;
        }
        else
        {
            // ooo check imposed here

            //  counter_block++;
            EdgeInfoHal *edge_info = *reinterpret_cast<EdgeInfoHal **>(old_eb + ((cur_edge_b / 2) * 8) + ((l * 8) - 8));
            if (edge_info->getInvldTime()->getWts() < min_read_version)
            {
                if (edge_info->getOutOfOrderUpdt() == NULL)
                {
                    me->free(edge_info->getInvldTime(), sizeof(ITM), thread_id);
                    me->free(edge_info, sizeof(EdgeInfoHal), thread_id);
                }
                else
                {
                    std::cout << "hello not null" << std::endl;

                    if (edge_info->getOutOfOrderUpdt()->getCount() == 0)
                    {
                        // if art tree root is null then null edge info hal
                        me->free(edge_info->getInvldTime(), sizeof(ITM), thread_id);
                        me->free(edge_info, sizeof(EdgeInfoHal), thread_id);
                    }
                }
            }
            else
            {

                uint16_t newEb_index = *reinterpret_cast<uint16_ptr>(new_eb + 1);

                *reinterpret_cast<uint64_ptr>(new_eb + ((newEb_index - 1) * 8)) = *reinterpret_cast<uint64_ptr>(old_eb + (l * 8)); // dest id

                *reinterpret_cast<EdgeInfoHal **>(new_eb + ((new_edge_b / 2) * 8) + (((newEb_index - 1) * 8) - 8)) = edge_info;//  IEM block

                new_property[newEb_index - 1] = old_property[l]; // property array

                *reinterpret_cast<uint16_ptr>(new_eb + 1) = *reinterpret_cast<uint16_ptr>(new_eb + 1) - 1; // update the current index in new stalb
                /// empty space
                if (edge_info->getOutOfOrderUpdt() == NULL)
                {
                    *reinterpret_cast<uint16_ptr>(new_eb + 5) = *reinterpret_cast<uint16_ptr>(new_eb + 5) + 1;
                }
                else
                {
                    std::cout << "hello not null" << std::endl;
                    if (edge_info->getOutOfOrderUpdt()->getCount() == 0)
                    {
                        *reinterpret_cast<uint16_ptr>(new_eb + 5) = *reinterpret_cast<uint16_ptr>(new_eb + 5) + 1;
                    }
                }

                *reinterpret_cast<uint8_ptr>(new_eb + 3) = 1;
            }
        }
    }
    // if()

    ///
    //old_property[0] >
}

// get min version for degree
timestamp_t HBALStore::getMinReadDegreeVersion()
{
    return min_read_degree_version;
}
// get min version

timestamp_t HBALStore::getMinReadVersion()
{
    return min_read_version;//readTable->readQuery[0];
    timestamp_t min_version = MaxValue;
    //  std::cout<<"start :"<<this->readTable->lock.data<<std::endl;

    //  this->readTable->lock.lock();
    // HBALStore::querylock.lock();

    // std::cout<<"end 0"<<std::endl;

    if (readTable->readQuery[1] != std::numeric_limits<timestamp_t>::max())
    {
        //  std::cout<<"end 0"<<std::endl;

        for (int i = 0; i < readTable->len; i++)
        {
            if (readTable->readQuery[i] == std::numeric_limits<timestamp_t>::max())
            {
                break;
            }
            else
            {
                if ((readTable->readQuery[i] < min_version))
                {
                    min_version = readTable->readQuery[i];
                }
            }
        }
        //  readTable->lock.unlock();
        // HBALStore::querylock.unlock();

        //  std::cout<<"end"<<std::endl;

        return min_version;
    }
    else
    {
        min_version = readTable->readQuery[0];
        // std::cout<<"end"<<std::endl;

        //  readTable->lock.unlock();

        //std::cout<<"end"<<std::endl;

        return min_version;
    }

}


// %%%%%% gc step for invalid block if exist

void HBALStore::edgeBlockGC(vertex_t phy_src_id, bool is_single_block, MemoryAllocator *me, int thread_id)
{
    /// %%%%%%

    InvalidListVal *inval_chain = static_cast<InvalidListVal *>(index[phy_src_id].invalidBlock);
    if (inval_chain == NULL)
    {
        return;
        // std::cout<<"returning null";
    }
    InvalidLinkListNode *inval_node = inval_chain->head;
    InvalidLinkListNode *prev_node;
    timestamp_t rts = MaxValue;
    bool readFlag = false;

    if (HBALStore::isReadActive)
    {
        rts = getMinReadVersion();
        readFlag = true;
    }
    //  new_inval_node->edge_block_size = FLAG_EMPTY_SLOT;
    //  new_inval_node->latest_Del_Time = itm->getWts();
    //  new_inval_node->block_index = add_version(reinterpret_cast<vertex_t>(art_ln));;
    //  new_inval_node->indr_block_size = itm->getSrcTime();

    while (inval_node != NULL)
    {
        if (inval_node->latest_Del_Time > rts)
        {
            break;
        }
        if (inval_node->edge_block_size != FLAG_EMPTY_SLOT) // in-order update
        {
            // if
            if (!(index[phy_src_id].edgePtr & 0x1))
            {
                // 2.1
                PerSourceIndrPointer *ptr_ind = static_cast<PerSourceIndrPointer *>(index[phy_src_id].getEdgePtr());

                u_int64_t index_block = (((u_int64_t) 1 << ptr_ind->getBlockLen()) - ((u_int64_t) (inval_node->indr_block_size - inval_node->block_index)));


                stalb_type *eb = ptr_ind->getperVertexBlockArr()[index_block];

                int cur_edge_b = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

                if (cur_edge_b == inval_node->edge_block_size)
                {
                    int num_element_cap = ((cur_edge_b - 2) / 2);

                    // todo if block resize then need to check again 50% block is empty or not; if not 50%
#ifdef DBUGFLAGPERSOURCE
                    if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                    {
                             testLock.lock();
                             foutput << numEdges<<"cur_edge "<<cur_edge_b<<" empty space: "<<eb->getEmptySpace()<<" address 2.5"<<std::endl;
                             testLock.unlock();
                    }
#endif
                    bool flag_already_init_srcVertex = false;

                    u_int64_t new_edge_b = cur_edge_b / 2;
                    double *property = *reinterpret_cast<double **>( eb + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb)) * 8) - 8)); // property
                    if (property[0] == inval_node->latest_Del_Time) // check the eb latest time iss equal to when 50% was empty
                    {
                        if (new_edge_b != 2)
                        {
                            stalb_type *newEb = DownEdgeBlock(log2(cur_edge_b), me, thread_id);


                            u_int64_t counter_cur_index = new_edge_b;

#ifdef DBUGFLAGPERSOURCE
                            if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                                        {
                                                            foutput << numEdges << " address 2.7" << std::endl;
                                                       }
#endif
                            // edge block resize value store

                            updateNewSTALB(cur_edge_b, new_edge_b, phy_src_id, eb, newEb, property, me, thread_id); // update stalb when resize

                            // ptr_ind and newEb

                            // index[phy_src_id].setEdgePtr(newEb);
                            /////////
                            ptr_ind->getperVertexBlockArr()[index_block] = newEb;

                            me->free(eb, cur_edge_b * 8, thread_id); // free stalb

                            me->free(property, sizeof(property_type) * (cur_edge_b / 2), thread_id); // free property array
                        }
                        else
                        {
                            // the stalb need to null means after deletion no neighbour exist
                            EdgeInfoHal *eddgeInfo = *reinterpret_cast<EdgeInfoHal **>( eb + 16);
                            //

                            // new edge block siz 32 should be garbage collect
                            ptr_ind->getperVertexBlockArr()[index_block] = (stalb_type *) FLAG_EMPTY_SLOT; //add_version(eddgeInfo->getSTime());

                            ptr_ind->setEmptySpace();
                            //  testLock.lock();
                            //  std::cout<<"hello "<<ptr_ind->getCurPos()<<" "<<index_block<<" "<<(float) ((u_int64_t) 1 << ptr_ind->getBlockLen())<<" "<<(float) ptr_ind->getEmptySpace()<<std::endl;
                            //  testLock.unlock();

                            // *************** indr array resize start ***********

                            while (reinterpret_cast<u_int64_t>(ptr_ind->getperVertexBlockArr()[ptr_ind->getCurPos()]) == FLAG_EMPTY_SLOT)
                            {
                                ptr_ind->getperVertexBlockArr()[ptr_ind->getCurPos()] = NULL;
                                ptr_ind->setCurPos(ptr_ind->getCurPos() + 1);
                                ptr_ind->emptySpace--;
                            }

                            // testLock.lock();
                            //  std::cout<<"hello "<<ptr_ind->getperVertexBlockArr()[ptr_ind->getCurPos()]<<" "<<ptr_ind->getCurPos()<<" "<<index_block<<" "<<(float) ((u_int64_t) 1 << ptr_ind->getBlockLen())<<" "<<(float) ptr_ind->getEmptySpace()<<std::endl;
                            // testLock.unlock();

                            if (((float) ((u_int64_t) 1 << ptr_ind->getBlockLen()) / (float) ptr_ind->getEmptySpace()) <= 2.00 && ptr_ind->getEmptySpace() != 0)
                            {

                                if (ptr_ind->getBlockLen() == 1)
                                {

#ifdef DBUGFLAGPERSOURCE
                                    if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                                    {
                                                                        testLock.lock();
                                                                        foutput << "indr array size 1.1 " <<vertexIndrList[divSrcVertexId][SrcVertexId]->getBlockLen() <<" cur pos "<<vertexIndrList[divSrcVertexId][SrcVertexId]->getCurPos()<< " edge block index pos " <<index_block<< std::endl;
                                                                        testLock.unlock();
                                                    }
#endif
                                    index[phy_src_id].indirectionLock.lock();

                                    if (reinterpret_cast<u_int64_t>(ptr_ind->getperVertexBlockArr()[0]) == FLAG_EMPTY_SLOT)
                                    {
                                        index[phy_src_id].setEdgePtr(reinterpret_cast<vertex_t>(ptr_ind->getperVertexBlockArr()[1]), 1);
                                    }
                                    else
                                    {
                                        index[phy_src_id].setEdgePtr(reinterpret_cast<vertex_t>(ptr_ind->getperVertexBlockArr()[0]), 1);
                                    }
                                    //  isDegreeActive = false;
                                    index[phy_src_id].degreeBit = false;

                                    me->free(ptr_ind->getperVertexBlockArr(), ((u_int64_t) 1 << ptr_ind->getBlockLen()) * 8, thread_id);
                                    index[phy_src_id].indirectionLock.unlock();

                                    flag_already_init_srcVertex = true;

                                }
                                else
                                {
#ifdef DBUGFLAGPERSOURCE
                                    if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                                    {
                                                        testLock.lock();
                                                        foutput << "indr array size 1.2 " <<vertexIndrList[divSrcVertexId][SrcVertexId]->getBlockLen() <<" cur pos "<<vertexIndrList[divSrcVertexId][SrcVertexId]->getCurPos()<< " edge block index pos " <<index_block<< std::endl;
                                                        testLock.unlock();
                                                    }
#endif
                                    index[phy_src_id].indirectionLock.lock();

                                    //downsize the blockArr
                                    int indr_cur_s = (int) 1 << ptr_ind->getBlockLen();

                                    int new_indr_b = indr_cur_s / 2;

                                    stalb_type **tempBlock = static_cast<stalb_type **>(me->Allocate(8 * new_indr_b, thread_id));///static_cast<EdgeBlock **>(aligned_alloc(8,8 * (blockLen * fixedSize)));//static_cast<EdgeBlock **>(malloc(8 * (blockLen * fixedSize)));
                                    memset(tempBlock, 0, 8 * new_indr_b);

                                    // use temp array to store new downsize array indexes in previous size array for faster updates hash table
                                    int *tempArr = reinterpret_cast<int *>(me->Allocate(8 * indr_cur_s, thread_id));//reinterpret_cast<EdgeInfoHal **>(aligned_alloc(8,8 * START_BLOCK_UPDATE_SIZE));
                                    memset(tempArr, 0, 8 * indr_cur_s);

                                    int new_array_index_start = new_indr_b;

                                    bool flag = false; // when first non null index that will be the new indr array current size
                                    int new_array_cur_pos = -1;

#ifdef DBUGFLAGPERSOURCE
                                    if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                                    {
                                                        testLock.lock();
                                                        foutput << "indr array size 1.3 "
                                                                  << vertexIndrList[divSrcVertexId][SrcVertexId]->getBlockLen()
                                                                  << " cur pos "
                                                                  << vertexIndrList[divSrcVertexId][SrcVertexId]->getCurPos()
                                                                  << " edge block index pos " << index_block << " empty slots "
                                                                  << vertexIndrList[divSrcVertexId][SrcVertexId]->getEmptySpace()
                                                                  << std::endl;

                                                        for (int p = 0; p < ((u_int64_t) 1 << vertexIndrList[divSrcVertexId][SrcVertexId]->getBlockLen()); p++)
                                                        {
                                                            foutput<< vertexIndrList[divSrcVertexId][SrcVertexId]->PerVertexBlockArr()[p]<< std::endl;
                                                        }
                                                        testLock.unlock();
                                                    }
#endif
                                    bool IsValFlag = false;
                                    for (int k = (indr_cur_s - 1); k >= ptr_ind->getCurPos(); k--)
                                    {
#ifdef DBUGFLAGPERSOURC
                                        if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                                        {
                                                            testLock.lock();
                                                            foutput << "indr array size 1.3 " <<vertexIndrList[divSrcVertexId][SrcVertexId]->getBlockLen() <<" cur pos "<<vertexIndrList[divSrcVertexId][SrcVertexId]->getCurPos()<< " edge block index pos " <<index_block<< "index k "<<k<<" indr_cur_s "<<indr_cur_s<<"  block address "<<vertexIndrList[divSrcVertexId][SrcVertexId]->PerVertexBlockArr()[k]<<" new_array_cur_pos"<<new_array_cur_pos<<std::endl;
                                                            testLock.unlock();
                                                        }
#endif
                                        if (reinterpret_cast<u_int64_t>(ptr_ind->getperVertexBlockArr()[k]) != FLAG_EMPTY_SLOT)
                                        {
                                            //
                                            tempBlock[--new_array_index_start] = ptr_ind->getperVertexBlockArr()[k];
                                            tempArr[k] = new_array_index_start;
                                            new_array_cur_pos = new_array_index_start;
                                            IsValFlag = true;
                                        }
                                    }
#ifdef DBUGFLAGPERSOURCE
                                    if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                                    {
                                                        testLock.lock();
                                                        foutput << "indr array size 1.4 " <<vertexIndrList[divSrcVertexId][SrcVertexId]->getBlockLen() <<" cur pos "<<vertexIndrList[divSrcVertexId][SrcVertexId]->getCurPos()<< " edge block index pos " <<index_block<< std::endl;
                                                        testLock.unlock();
                                                    }
#endif
                                    // replace the new indr array
                                    if (IsValFlag)
                                    {

                                        void *ptr = ptr_ind->getperVertexBlockArr();
                                        ptr_ind->setPerVertexBlockArr(tempBlock);
                                        ptr_ind->setBlockLen(log2(new_indr_b));
                                        ptr_ind->setCurPos(new_array_cur_pos);
                                        // ptr_ind->initDownsizeVal();
                                        me->free(ptr, indr_cur_s * 8, thread_id);
#ifdef DBUGFLAGPERSOURCE
                                        if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                                        {
                                                            testLock.lock();
                                                            foutput << "indr array size 1.5 " <<vertexIndrList[divSrcVertexId][SrcVertexId]->getBlockLen() <<" cur pos "<<vertexIndrList[divSrcVertexId][SrcVertexId]->getCurPos()<< " edge block index pos " <<index_block<< std::endl;
                                                            testLock.unlock();
                                                        }
#endif
                                        // hash table update

                                        for (u_int64_t u = 0; u < index[phy_src_id].getHashTable()->hashBlockSize; u++)
                                        {
                                            if (index[phy_src_id].getHashTable()->hashtable[u] != FLAG_EMPTY_SLOT && index[phy_src_id].getHashTable()->hashtable[u] != FLAG_TOMB_STONE)
                                            {
                                                u_int64_t adj_po = index[phy_src_id].getHashTable()->hashtable[u];
                                                u_int64_t indr_index1 = getBitMask(adj_po, L1IndexStart, L1IndexBits);
                                                u_int64_t indr_size1 = (u_int64_t) 1 << getBitMask(adj_po, L1SizeStart, L1SizeBits);

                                                // lock on per source vertex to get the index_block edge block
                                                u_int64_t index_block_temp = ((indr_cur_s) - (indr_size1 - indr_index1));
                                                u_int64_t new_pos_l2 = tempArr[index_block_temp];

                                                adj_po = setBitMask(adj_po, new_pos_l2, L1IndexStart, L1IndexBits);
                                                adj_po = setBitMask(adj_po, log2(new_indr_b), L1SizeStart, L1SizeBits);

                                                index[phy_src_id].getHashTable()->hashtable[u] = adj_po;
                                            }
                                        }

                                        //%%%%%%%%%%% update invalid list
                                        InvalidLinkListNode *update_invl_node = inval_node;
                                        while (update_invl_node->next != NULL)
                                        {
                                            update_invl_node = update_invl_node->next;
                                            u_int64_t index_block_pr = ((indr_cur_s) - (update_invl_node->indr_block_size - update_invl_node->block_index));
                                            update_invl_node->block_index = tempArr[index_block_pr];
                                            update_invl_node->indr_block_size = new_indr_b;
                                        }

                                        // %%%%%%%%%% end

                                        me->free(tempArr, indr_cur_s * 8, thread_id);

                                    }
                                    else
                                    {

                                        index[phy_src_id].initVal();

                                        //  index[phy_src_id].degreeBit = false;

                                    }
                                    index[phy_src_id].indirectionLock.unlock();

                                }
#ifdef DBUGFLAGPERSOURCE
                                if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                                {
                                                    testLock.lock();
                                                    foutput << "indr array size 1.6 " <<vertexIndrList[divSrcVertexId][SrcVertexId]->getBlockLen() <<" cur pos "<<vertexIndrList[divSrcVertexId][SrcVertexId]->getCurPos()<< " edge block index pos " <<index_block<< std::endl;
                                                    testLock.unlock();
                                                }
#endif
                            }


                            // *************** indr array resize end   ***********

                        }
                    }
                }
            }
            else
            {
                stalb_type *eb = static_cast<stalb_type *>(index[phy_src_id].getEdgePtr());

                int cur_edge_b = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);
                //  int cur_edge_b1 = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(inval_node->ptr);
                int cur_edge_b2 = inval_node->indr_block_size;

                if (cur_edge_b == inval_node->edge_block_size)
                {
                    int num_element_cap = ((cur_edge_b - 2) / 2);

                    double *property = *reinterpret_cast<double **>(eb + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb)) * 8) - 8)); // property

                    if (property[0] == inval_node->latest_Del_Time) // check the eb latest time iss equal to when 50% was empty
                    {


                        /// %%%%%% single block
                        u_int64_t new_edge_b = cur_edge_b / 2;
                        index[phy_src_id].indirectionLock.lock();

                        if (new_edge_b != 2)
                        {
                            stalb_type *newEb = DownEdgeBlock(log2(cur_edge_b), me, thread_id);

                            u_int64_t counter_cur_index = new_edge_b;

#ifdef DBUGFLAGPERSOURCE
                            if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                                {
                                                    foutput << numEdges << " address 2.7" << std::endl;
                                               }
#endif
                            // edge block resize value store

                            updateNewSTALB(cur_edge_b, new_edge_b, phy_src_id, eb, newEb, property, me, thread_id); // update stalb when resize

                            // ptr_ind and newEb

                            index[phy_src_id].setEdgePtr(reinterpret_cast<vertex_t>(newEb), 1);

                            me->free(eb, cur_edge_b * 8, thread_id); // free stalb

                            me->free(property, sizeof(property_type) * (cur_edge_b / 2), thread_id); // free property array
                        }
                        else
                        {
                            // the stalb need to null means after deletion no neighbour exist
                            index[phy_src_id].initVal(); // init all the information of src vertex
                            index[phy_src_id].degreeBit = false;
                        }
                        index[phy_src_id].indirectionLock.unlock();
                    }
                }
            }
        }
        else
        {

            // out-of-order update gc
            //  new_inval_node->edge_block_size = FLAG_EMPTY_SLOT;
            //  new_inval_node->latest_Del_Time = itm->getWts();
            //  new_inval_node->block_index = add_version(reinterpret_cast<vertex_t>(art_ln));;
            //  new_inval_node->indr_block_size = ln->getSrcTime();
            ArtPerIndex *art_ln = reinterpret_cast<ArtPerIndex *> ((inval_node->block_index));
            LeafNode *ln = art_ln->getLeaf(inval_node->indr_block_size);
            art_ln->removeArtLeaf(inval_node->indr_block_size);
            art_ln->decCount();
            me->free(ln->getInvalEntry(), sizeof(ITM), thread_id);
            me->free(ln, sizeof(LeafNode), thread_id);
            // new_inval_node->block_index = remove_version(reinterpret_cast<vertex_t>(art_ln));
        }

        prev_node = inval_node;

        /// %%%%%%%%% delete inval_node

        inval_node = inval_node->next;

        me->free(prev_node, sizeof(InvalidLinkListNode), thread_id);
    }

    //%%% change the srcvertex.invalblock

    if (inval_node != NULL)
    {
        inval_chain->head = inval_node;
    }
    else
    {
        me->free(index[phy_src_id].invalidBlock, sizeof(InvalidListVal), thread_id);
        index[phy_src_id].invalidBlock = NULL;
    }

    /// %%%%%%
}


/// %%%%%%% gc step end

/// %%%%%%%% gc step for per source vertex %%%%%% ///

void HBALStore::gcDegreeList(vertex_t phy_src_id, MemoryAllocator *me, int thread_id)
{
    if (this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.try_lock())
    {

        InvalidNodeAddress *invl_node = this->invalidListDegree[phy_src_id % THRESHOLDINVALID];


        if (invl_node->ptr != NULL)
        {
            //   std::cout<<"hello"<<std::endl;
            InvalidDegree *id = static_cast<InvalidDegree *>(invl_node->ptr);
            invl_node->ptr = NULL;
            this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.unlock();

            InvalidDegree *prev_i = NULL;

            while (id != NULL)
            {
                // %%%%%%%%%% Degree node garbage collection %%%%%%%

                DegreeNode *dn = static_cast<DegreeNode *>(id->ptr);
                DegreeNode *prev = NULL;
                while (dn != NULL)
                {
                    prev = dn;
                    dn = dn->next;
                    me->free(prev, sizeof(DegreeNode), thread_id);
                }

                // %%%%%%%% end degreenode %%%%%%%%%
                prev_i = id;
                id = id->next;
                //me->free(prev_i,sizeof(InvalidDegree),thread_id);
                free(prev_i);
            }

            //invl_node->ptr = NULL;

        }
        else
        {
            this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.unlock();
        }
    }
    // }

    //testLock.lock();
    //std::cout<<"end gc degree"<<std::endl;
    // testLock.unlock();

}

/// %%%%%%%  gc step end %%%%%%%% ////

//remove edge
void HBALStore::removeEdge(InputEdge inputedge_info, MemoryAllocator *me, int thread_id)
{


    /*

     For InorderUpdate

     At the time of insertion we need indr_array_current_size + indr_cur_index (indirection array level)
     and in block size level we need current_index of the insrted_entry + the size of the fixed block

     For outoforder update
     1) we need indriection size + indr_cur_index
     2) address of leafNode


     Bucket

     has two possible value
     either a int_64 (position of insertion entry in two level vector) or leafNode address of Art
     We need lock to delete entry from EdgeHal

     Need to think on deletion part

     */

    bool flag = false;
    // vertex_t divSrcVertexId = getIndrVertexArrayIndex(inputedge_info.getSrcVertexId());
    // vertex_t SrcVertexId = getVertexArrayIndex(inputedge_info.getSrcVertexId(), divSrcVertexId);

    //aquire_vertex_lock(inputedge_info.getSrcVertexId()); //.lock();

    vertex_t phy_src_id = physical_id(inputedge_info.getSrcVertexId());
    vertex_t phy_dest_id = physical_id(inputedge_info.getDestVertexId());

    // hash table bucket deletion part

    index[phy_src_id].perVertexLock.lock();

    //std::cout<<"start deletion:"<<phy_src_id<<" "<<phy_dest_id<<std::endl;
    AdjacentPos ap;
    ap.edgeBlockIndex = NOTFOUNDKEY;
    ap.indrIndex = NOTFOUNDKEY;
    ap.hashTableIndex = NOTFOUNDKEY;
    AdjacentPos *ap_pos;

    // garbage collection call
    edgeBlockGC(phy_src_id, 0, me, thread_id);


    // 4394 dest vertex 600912

#ifdef DBUGFLAGG
    testLock.lock();
        foutput<< "deletion start: " << inputedge_info.getSrcVertexId() << " dest node "<< inputedge_info.getDestVertexId() << " getcurrentpos "<<vertexIndrList[divSrcVertexId][SrcVertexId]->degree<<std::endl;
    testLock.unlock();
#endif

    // testLock.lock();
    // std::cout << "deletion start "<<inputedge_info.getSrcVertexId()<<" dest node "<<inputedge_info.getDestVertexId()<<std::endl;
    //testLock.unlock();

#ifdef DBUGFLAGPERSOURCE
    if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
    {
        testLock.lock();
        foutput << "deletion start "<<inputedge_info.getSrcVertexId()<<" dest node "<<inputedge_info.getDestVertexId()<<std::endl;
       testLock.unlock();
    }
#endif

    //   std::cout << " number of edges deleted 2 "<<inputedge_info.getDestVertexId()<<std::endl;

    bool flagc = false;

    /// now check based on edge block indr index
    ap_pos = index[phy_src_id].getDestNodeHashTableVal(phy_dest_id, &ap);

#ifdef DBUGFLAGPERSOURCE
    if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
    {

         testLock.lock();
         foutput << numEdges<<" address 2"<<std::endl;
         testLock.unlock();

    }
#endif
    if (ap_pos->edgeBlockIndex != NOTFOUNDKEY)
    {
        if (!check_version(ap_pos->edgeBlockIndex)) // most significant bit shows either its in-order or out-of-order update
        {
#ifdef DBUGFLAGPERSOURCE
            if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
        {
            testLock.lock();
            foutput << numEdges << " address 2.1" << std::endl;
            testLock.unlock();
        }
#endif

            // std::cout<<ap_pos->indrIndex<<std::endl;
            EdgeInfoHal *eih;
            bool isInOrder = false;
            timestamp_t rts = MaxValue;
            bool readFlag = false;
            // ap_pos->edgeindex != -1

            // index[phy_src_id].hashPtr->hashtable[ap_pos->hashTableIndex] = FLAG_TOMB_STONE;
            index[phy_src_id].removeDestNodeHashTableVal(phy_dest_id);

            // due to resize of indr array the block_index size that store in hash table bucket could change there position for that we use formula
            // current block len (indr array) - (indrsize size at the time of insertion - the block index of insertion)

            // check if the deletion dest node entry in order or ooo

            if (!(index[phy_src_id].edgePtr & 0x1))
            {

                u_int64_t index_block = ap_pos->indrIndex;

                PerSourceIndrPointer *ptr_ind = static_cast<PerSourceIndrPointer *>(index[phy_src_id].getEdgePtr());

                stalb_type *eb = ptr_ind->getperVertexBlockArr()[index_block];    //->->()[index_block];

                u_int64_t index_edge_block = ap_pos->edgeBlockIndex;

                ITM *itm = (ITM *) (me->Allocate(sizeof(ITM), thread_id));
                itm->setSrcTime(inputedge_info.getUpdtSrcTime());

#ifdef DBUGFLAGPERSOURCE
                if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                {
                       testLock.lock();
                       foutput << "the index_edge_block > edge block size " << index_edge_block << std::endl;
                       testLock.unlock();
                }
#endif
                u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

                eih = *reinterpret_cast<EdgeInfoHal **>(eb + ((eb_block_size / 2) * 8) + ((index_edge_block * 8) - 8)); //->getEdgeUpdate()[index_edge_block];

#ifdef DBUGFLAGPERSOURCE
                if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                {
                    testLock.lock();
                    foutput << numEdges << " address 2.3 " << std::endl;
                    testLock.unlock();
               }
#endif

                //   timestamp_t read_time = getMinReadVersion();


                eih->setInvldTime(itm);

                *reinterpret_cast<uint8_ptr>(eb + 3) = 1; // isdeletion flag

                *reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8)) = add_version(*reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8))); // dest-id FLAGEMTPY
                // if no ooo exist in eih
                if (eih->getOutOfOrderUpdt() == NULL)
                {
                    *reinterpret_cast<uint16_ptr>(eb + 5) = *reinterpret_cast<uint16_ptr>(eb + 5) + 1; // set empty space
                }
                itm->setWts(getCurrTimeStamp());

                double *property = *reinterpret_cast<double **>( eb + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb)) * 8) - 8)); // property
                // deletion wts store in the unused index of the property array

                int cur_edge_b = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);


                int num_element_cap = ((cur_edge_b - 2) / 2);

#ifdef DBUGFLAGPERSOURCE
                if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                            {
                                     testLock.lock();
                                     foutput << numEdges<<"cur_edge "<<cur_edge_b<<" empty space: "<<eb->getEmptySpace()<<" address 2.5"<<std::endl;
                                     testLock.unlock();
                            }
#endif

                bool flag_already_init_srcVertex = false;
                if (!HBALStore::isReadActive)
                {
                    *reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8)) = add_version_read(*reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8))); /// no read-only queries are active
                }
                if ((((float) (num_element_cap + 1) / 2) == *reinterpret_cast<uint16_ptr>(eb + 5)) && *reinterpret_cast<uint16_ptr>(eb + 5) != 0) // half of the elements in the stalb is empty
                {


                    //    std::cout<<cur_edge_b<<" "<<num_element_cap<<" empty "<<*reinterpret_cast<uint16_ptr>(eb + 5)<<std::endl;

                    // ((float) (num_element_cap + 1) / 2) == *reinterpret_cast<uint16_ptr>(eb + 5)
#ifdef DBUGFLAGPERSOURCE
                    if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                {
                                    testLock.lock();
                                        foutput << numEdges << " address 2.6" << std::endl;
                                    testLock.unlock();

                                }
#endif

// %%%%%%%%%%%%%%%%%%%%%%%%%%%% store the block in invalid list
                    //  testLock.lock();
                    //  std::cout  << " debug 0" << std::endl;
                    //  testLock.unlock();
                    timestamp_t latest_deletion_time = eih->getInvldTime()->getWts();
                    property[0] = latest_deletion_time;
                    //  this->invalLock.lock();
                    if (index[phy_src_id].invalidBlock == NULL)
                    {

                        InvalidListVal *inv = (InvalidListVal *) (me->Allocate(sizeof(InvalidListVal), thread_id));

                        InvalidLinkListNode *new_inval_node = (InvalidLinkListNode *) (me->Allocate(sizeof(InvalidLinkListNode), thread_id));
                        new_inval_node->edge_block_size = cur_edge_b;
                        new_inval_node->latest_Del_Time = latest_deletion_time;
                        new_inval_node->block_index = index_block;
                        new_inval_node->indr_block_size = (u_int64_t) 1 << ptr_ind->getBlockLen(); //
                        new_inval_node->next = NULL;
                        inv->head = new_inval_node;
                        inv->tail = new_inval_node;

                        index[phy_src_id].invalidBlock = (inv);
                        // index[phy_src_id].invalidBlock = (index[phy_src_id].invalidBlock);
                    }
                    else
                    {
                        InvalidLinkListNode *newInval = (InvalidLinkListNode *) (me->Allocate(sizeof(InvalidLinkListNode), thread_id));
                        newInval->edge_block_size = cur_edge_b;
                        newInval->latest_Del_Time = latest_deletion_time;
                        newInval->block_index = index_block;
                        newInval->indr_block_size = (u_int64_t) 1 << ptr_ind->getBlockLen(); //

                        newInval->next = NULL;

                        InvalidListVal *inv_per_src = reinterpret_cast<InvalidListVal *>(index[phy_src_id].invalidBlock);

                        inv_per_src->tail->next = newInval;
                        inv_per_src->tail = newInval;
                    }

// %%%%%%%%%%%%%%% end %%%%%%%%%%%%%%

                }

// 2592222
            }
            else
            {
                // edge block direct read

                // %%%%%%%%%%%%%% gc


                //   edgeBlockGC(phy_src_id, 1, me, thread_id);


                //
                stalb_type *ptr_edge;
                // bool flagIsIndr = 0;

                ptr_edge = static_cast<stalb_type *>(index[phy_src_id].getEdgePtr());

                //   PerSourceIndrPointer *ptr_ind = static_cast<PerSourceIndrPointer *>(vertexIndrList[divSrcVertexId][SrcVertexId]->getEdgePtr());

                stalb_type *eb = ptr_edge;    //->->()[index_block];

                //  assert( eb==NULL && "edge block does not exist");

                u_int64_t index_edge_block = ap_pos->edgeBlockIndex;

                ITM *itm = (ITM *) (me->Allocate(sizeof(ITM), thread_id));
                itm->setSrcTime(inputedge_info.getUpdtSrcTime());

#ifdef DBUGFLAGPERSOURCE
                if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
            {
                   testLock.lock();
                   foutput << "the index_edge_block > edge block size " << index_edge_block << std::endl;
                   testLock.unlock();
            }
#endif

                u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

                eih = *reinterpret_cast<EdgeInfoHal **>(eb + ((eb_block_size / 2) * 8) + ((index_edge_block * 8) - 8)); //->getEdgeUpdate()[index_edge_block];

#ifdef DBUGFLAGPERSOURCE
                if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
            {
                testLock.lock();
                foutput << numEdges << " address 2.3 " << std::endl;
                testLock.unlock();
           }
#endif
                //  assert( eih==NULL && "edge info does not exist");


                eih->setInvldTime(itm);



                // itm->setWts(getCurrTimeStamp());
                *reinterpret_cast<uint8_ptr>(eb + 3) = 1; // deletion bit

                *reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8)) = add_version(*reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8))); // dest-id FLAGEMTPY


                //property[index_edge_block] = 0.0; // weight

                ///    me->free(eih, sizeof(EdgeInfoHal), thread_id);
                // if ooo is null then empty increment
                if (eih->getOutOfOrderUpdt() == NULL)
                {
                    *reinterpret_cast<uint16_ptr>(eb + 5) = *reinterpret_cast<uint16_ptr>(eb + 5) + 1; // set empty space
                }

                itm->setWts(getCurrTimeStamp());


                double *property = *reinterpret_cast<double **>( eb + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb)) * 8) - 8)); // property
                // property[0] = itm->getWts(); // deletion wts store in the unused index of the property array
                if (!HBALStore::isReadActive)
                {
                    *reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8)) = add_version_read(*reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8)));
                }



                // read table timestamps
                // Read Table wts comparison
                //  readTable->lock.lock_shared();
                int cur_edge_b = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

                int num_element_cap = ((cur_edge_b - 2) / 2);
#ifdef DBUGFLAGPERSOURCE
                if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                            {
                                     testLock.lock();
                                     foutput << numEdges<<"cur_edge "<<cur_edge_b<<" empty space: "<<eb->getEmptySpace()<<" address 2.5"<<std::endl;
                                     testLock.unlock();
                            }
#endif

                bool flag_already_init_srcVertex = false;


                if ((((float) (num_element_cap + 1) / 2) == *reinterpret_cast<uint16_ptr>(eb + 5)) && *reinterpret_cast<uint16_ptr>(eb + 5) != 0) // half of the elements in the stalb is empty
                {


#ifdef DBUGFLAGPERSOURCE
                    if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                {
                                    testLock.lock();
                                        foutput << numEdges << " address 2.6" << std::endl;
                                    testLock.unlock();

                                }
#endif
                    // eb per deletion lock

                    // whenever read-only transaction lock the eb, after lock statement first access again the eb for getting updated edge block
                    //  eb->lock();

                    // %%%%%%%%%%%%%%%%%%% move block to invalidListVal %%%%%%%%%%%%%

                    // testLock.lock();
                    // std::cout  << " debug 1.0" << std::endl;
                    // testLock.unlock();
                    timestamp_t latest_deletion_time = eih->getInvldTime()->getWts();
                    property[0] = latest_deletion_time;
                    if ((index[phy_src_id].invalidBlock) == NULL)
                    {

                        InvalidListVal *inv = (InvalidListVal *) (me->Allocate(sizeof(InvalidListVal), thread_id));

                        InvalidLinkListNode *new_inval_node = (InvalidLinkListNode *) (me->Allocate(sizeof(InvalidLinkListNode), thread_id));
                        new_inval_node->edge_block_size = cur_edge_b;
                        new_inval_node->latest_Del_Time = latest_deletion_time;
                        new_inval_node->block_index = 0;
                        new_inval_node->indr_block_size = 1;

                        new_inval_node->next = NULL;
                        inv->head = new_inval_node;
                        inv->tail = new_inval_node;

                        index[phy_src_id].invalidBlock = (inv);
                        // add_version(index[phy_src_id].invalidBlock);
                    }
                    else
                    {
                        InvalidLinkListNode *newInval = (InvalidLinkListNode *) (me->Allocate(sizeof(InvalidLinkListNode), thread_id));
                        newInval->edge_block_size = cur_edge_b;
                        newInval->latest_Del_Time = latest_deletion_time;
                        newInval->next = NULL;
                        newInval->block_index = 0;
                        newInval->indr_block_size = 1;

                        InvalidListVal *inv_per_src = reinterpret_cast<InvalidListVal *>(index[phy_src_id].invalidBlock);

                        inv_per_src->tail->next = newInval;
                        inv_per_src->tail = newInval;
                    }
                    // testLock.lock();
                    // std::cout  << " debug 1.1" << std::endl;
                    // testLock.unlock();
                    // %%%%%%%%%%%%%%%%%%% end block %%%%%%%%%%%%%

                }


            }


            //%%%%%%%%%%%% degree decrement %%%%%%%%

        }
        else
        {
            // out of order deletion part
            LeafNode *ln = reinterpret_cast<LeafNode *> (remove_version(ap_pos->edgeBlockIndex));
            ITM *itm = (ITM *) (me->Allocate(sizeof(ITM), thread_id));
            itm->setSrcTime(inputedge_info.getUpdtSrcTime());
            ln->setInvalEntry(itm);
            itm->setWts(getCurrTimeStamp());
            if (itm->getWts() > getMinReadVersion())
            {

                //%%%%%%%%%% store for garbage collection
                ArtPerIndex *art_ln = reinterpret_cast<ArtPerIndex *> (remove_version(ap_pos->indrIndex));

                if ((index[phy_src_id].invalidBlock) == NULL)
                {

                    InvalidListVal *inv = (InvalidListVal *) (me->Allocate(sizeof(InvalidListVal), thread_id));

                    InvalidLinkListNode *new_inval_node = (InvalidLinkListNode *) (me->Allocate(sizeof(InvalidLinkListNode), thread_id));
                    new_inval_node->edge_block_size = FLAG_EMPTY_SLOT;
                    new_inval_node->latest_Del_Time = itm->getWts();
                    new_inval_node->block_index = reinterpret_cast<vertex_t>(art_ln);
                    new_inval_node->indr_block_size = ln->getSrcTime();

                    new_inval_node->next = NULL;
                    inv->head = new_inval_node;
                    inv->tail = new_inval_node;

                    index[phy_src_id].invalidBlock = (inv);
                    // add_version(index[phy_src_id].invalidBlock);
                }
                else
                {
                    InvalidLinkListNode *newInval = (InvalidLinkListNode *) (me->Allocate(sizeof(InvalidLinkListNode), thread_id));
                    newInval->edge_block_size = FLAG_EMPTY_SLOT;
                    newInval->latest_Del_Time = itm->getWts();
                    newInval->block_index = (reinterpret_cast<vertex_t>(art_ln));;
                    newInval->indr_block_size = ln->getSrcTime();

                    InvalidListVal *inv_per_src = reinterpret_cast<InvalidListVal *>(index[phy_src_id].invalidBlock);

                    inv_per_src->tail->next = newInval;
                    inv_per_src->tail = newInval;
                }

                //%%%%%%%%% end garbage collection for out-fo-order update
            }
            else
            {
                ArtPerIndex *art_ln = reinterpret_cast<ArtPerIndex *> (remove_version(ap_pos->indrIndex));
                art_ln->removeArtLeaf(ln->getSrcTime());
                art_ln->decCount();
                me->free(itm, sizeof(ITM), thread_id);
                me->free(ln, sizeof(LeafNode), thread_id);
            }

        }
    }
    else
    {
        std::cout << "entry does not exist 1:" << ap_pos->indrIndex << " " << ap_pos->edgeBlockIndex << " src vertex " << phy_src_id << " dest vertex " << phy_dest_id << std::endl;
    }

    if (check_version(index[phy_src_id].degree))
    {
        // DegreeNode
        DegreeNode *cur_degree = reinterpret_cast<DegreeNode *>(remove_version(index[phy_src_id].degree));
        if (getMinReadDegreeVersion() > cur_degree->wts)
        {

            /// %%%%%%%%%%
            InvalidDegree *new_invl_node = static_cast<InvalidDegree *>(malloc(sizeof(InvalidDegree)));//static_cast<InvalidDegree *>(me->Allocate(sizeof(InvalidDegree), thread_id)); // InvalidDegree();

            index[phy_src_id].degree = remove_version(cur_degree->degree) - 1;

            new_invl_node->ptr = cur_degree;

            this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.lock();
            if (this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr == NULL)
            {
                this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr = new_invl_node;
                new_invl_node->next = NULL;
            }
            else
            {
                new_invl_node->next = static_cast<InvalidDegree *>(this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr);
                this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr = new_invl_node;
            }
            this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.unlock();

            flagc = true; // in case writer put node to InvalidListDegree
        }
        else
        {

            DegreeNode *new_degree;

            new_degree = (DegreeNode *) (me->Allocate(sizeof(DegreeNode), thread_id));

            new_degree->degree = remove_version(cur_degree->degree) - 1;

            new_degree->next = cur_degree;

            index[phy_src_id].degree = add_version(reinterpret_cast<vertex_t>(new_degree));
            new_degree->wts = getCurrTimeStamp();
        }

    }
    else
    {
        //
        if (getMinReadDegreeVersion() == MaxValue)
        {
            index[phy_src_id].degree = remove_version(index[phy_src_id].degree) - 1;
        }
        else
        {
            DegreeNode *cur_degree;
            DegreeNode *new_degree;

            cur_degree = (DegreeNode *) (me->Allocate(sizeof(DegreeNode), thread_id));
            new_degree = (DegreeNode *) (me->Allocate(sizeof(DegreeNode), thread_id));
            cur_degree->degree = remove_version(index[phy_src_id].degree);
            cur_degree->wts = 0;
            cur_degree->next = NULL;

            new_degree->degree = remove_version(index[phy_src_id].degree) - 1;
            new_degree->next = cur_degree;

            index[phy_src_id].degree = add_version(reinterpret_cast<vertex_t>(new_degree));
            new_degree->wts = getCurrTimeStamp();

        }
    }
    //%%%%%%%%%%% end decrement degree
    if (check_version(index[phy_src_id].degree))
    {
        flagc = true;
    }

    if (flagc)
    {
        gcDegreeList(phy_src_id, me, thread_id);
    }


    // index[phy_src_id].decDegree();
    //  std::cout<<"index src "<<phy_src_id<<"  dst id "<<phy_dest_id<<std::endl;

    //if(check_version(index[phy_src_id].degree))
    //{
    //  gcDegreeList(phy_src_id, me, thread_id);
    //}
    // bool flagc = false;
    // std::cout<<"end deletion:"<<phy_src_id<<" "<<phy_dest_id<<std::endl;

    index[phy_src_id].perVertexLock.unlock();


}

// insert edge
void HBALStore::InsertEdge(InputEdge inputedge_info, MemoryAllocator *me, int thread_id)
{



    //  double weight_w = inputedge_info.getUpdtWeight();
    double weight_w = 0;//inputedge_info.getUpdtWeight();

    // least significant 10 bits extract from destnode to store in hash table
    vertex_t phy_src_id = physical_id(inputedge_info.getSrcVertexId());
    vertex_t phy_dest_id = physical_id(inputedge_info.getDestVertexId());


    if (!CheckOutOfOrder(inputedge_info.getUpdtSrcTime(), phy_src_id))
    {

        //  EdgeBlock *eb;


        // phy_src_id phy_dest_id
        index[phy_src_id].perVertexLock.lock();



        //  bool flagc = false;

        /// now check based on edge block indr index
        //  ap_pos = index[phy_src_id].getDestNodeHashTableVal(phy_dest_id, &ap);

        // if (ap.edgeBlockIndex == NOTFOUNDKEY)
        //{
        EdgeInfoHal *ei = static_cast<EdgeInfoHal *>(me->Allocate(sizeof(EdgeInfoHal), thread_id));//(EdgeInfoHal *) (std::aligned_alloc(PAGE_SIZE,sizeof(EdgeInfoHal))); //new EdgeInfoHal();
        ei->initEdgeInfoHal();
        ei->setSTime(inputedge_info.getUpdtSrcTime());
        ei->setOutOfOrderUpdt(NULL);


        u_int64_t dest_prefix = (u_int64_t) phy_dest_id & thread_prefix;

        // int indr_arr_cur_pos = srcVertex->getCurPos();
#ifdef DBUGFLAGPERSOURCE
        if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                {
                                    testLock.lock();
                                    //foutput<<"block length 6 : "<<srcVertex->getBlockLen()<<" src time "<<srcVertex->getLatestSrcTime()<< " insertion start "<<inputedge_info.getSrcVertexId()<<" dest "<<inputedge_info.getDestVertexId()<<" indirection index "<<indr_arr_cur_pos<<std::endl;
                                    testLock.unlock();
                                }
#endif
        if (index[phy_src_id].getHashTable() == NULL)
        {

            // return edge block pointer after creation
            // EdgeBlock *eb = (EdgeBlock *) (me->Allocate(sizeof(EdgeBlock), thread_id));
            stalb_type *stalb = static_cast<stalb_type *>(me->Allocate(32, thread_id));

            double *property = static_cast<double *>(me->Allocate(16, thread_id));


            memset(stalb, 0, 32);
            memset(property, 0, 16);

            *reinterpret_cast<uint8_ptr>(stalb) = 2; // block size

            *reinterpret_cast<uint16_ptr>(stalb + 1) = 2; // cur_index


            // insertion steps

            *reinterpret_cast<uint64_ptr>(stalb + 8) = remove_version(phy_dest_id); // dest node

            *reinterpret_cast<EdgeInfoHal **>(stalb + 16) = ei; // edge info hal insertion

            property[1] = weight_w;

            *reinterpret_cast<double **>(stalb + 24) = property; // property insertion

            uint16_t *cur_ind = reinterpret_cast<uint16_ptr>(stalb + 1);

            *cur_ind = 1; // update cur index



            //   eb->initEdgeBlock(me, thread_id, 1);
            //  eb->setEdgeUpdt(ei, inputedge_info.getDestVertexId(), weight_w);

            // perThreadNumberOfEdges[thread_id]++;

            index[phy_src_id].initHashTable(me, thread_id);

            u_int64_t indr_index_bits_val = 0;
            u_int64_t indr_size_bits_val = 0;
            u_int64_t edge_block_index_bits_val = *cur_ind;
            u_int64_t edge_block_size_bits_val = 2;
            u_int64_t adjcent_pos_store = 0;


            // for indr array 0,32 for indr size 32 to 5, for edgeblock index 37 to 11 bit, for edge block size 48 to 4, for prefix 52 to 62
            // for either pointer for edge per source is edgeblock or STAL 62 to 1 and for ooo 63 to 1

            adjcent_pos_store = setBitMask(adjcent_pos_store, indr_index_bits_val, L1IndexStart, L1IndexBits);
            adjcent_pos_store = setBitMask(adjcent_pos_store, indr_size_bits_val, L1SizeStart, L1SizeBits);
            adjcent_pos_store = setBitMask(adjcent_pos_store, edge_block_index_bits_val, L2IndexStart, L2IndexBits);
            adjcent_pos_store = setBitMask(adjcent_pos_store, edge_block_size_bits_val, L2SizeStart, L2SizeBits);
            adjcent_pos_store = setBitMask(adjcent_pos_store, dest_prefix, DNPStart, DNPBits);
            // 0 means edge block 1 means STAL
            adjcent_pos_store = setBitMask(adjcent_pos_store, 0, IsSTALStart, IsSTALBits);
            // for ooo update 1 means no ooo 0 means ooo
            adjcent_pos_store = setBitMask(adjcent_pos_store, 0, IsInOrderStart, IsInOrderBits);


            index[phy_src_id].setDestNodeHashTableVal(phy_dest_id, adjcent_pos_store, me, thread_id);

            perThreadNumberOfvertices[thread_id]++;
            index[phy_src_id].indirectionLock.lock();
            index[phy_src_id].setEdgePtr(reinterpret_cast<vertex_t>(stalb), true);
            index[phy_src_id].indirectionLock.unlock();

            // index[phy_src_id].incrDegree();

            ei->setWTsEdge(getCurrTimeStamp());

            /*   for indrArrayIndex: pos 0 and bitsNeeded 32
               for indrArraySize: pos 32 and bitsNeeded 5
               for blockIndex: pos 37 and bitsNeeded 12
               for blockSize: pos 49 and bitsNeeded 4
               for prefix dest_node: pos 53 and bitsNeeded 10
               for updateFlage(in order,ooo) pos 63 and bitNeeded 1
              */


#ifdef DBUGFLAGPERSOURCE
            if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                    {
                                        testLock.lock();
                                        foutput << " insertion start 6.1 "<<inputedge_info.getSrcVertexId()<<" dest "<<inputedge_info.getDestVertexId()<<std::endl;
                                        testLock.unlock();
                                    }
#endif

        }
        else
        {


            u_int64_t indr_index_bits_val = 0;
            u_int64_t indr_size_bits_val = 0;
            u_int64_t edge_block_index_bits_val = 0;
            u_int64_t edge_block_size_bits_val = 0;
            u_int64_t isSTAL = 0;

            // %%%%%% start



            // %%%%% end

            //if ((remove_version(index[phy_src_id].getDegree()) == (((uint32_t) 1 << StartFixedBlockThreshold) - 1)) )

            if (index[phy_src_id].edgePtr & 0x1)
            {

                stalb_type *ebCur = static_cast<stalb_type *>(index[phy_src_id].getEdgePtr());
                if (((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(ebCur)) == BLOCKSIZEFIXED && (*reinterpret_cast<uint16_ptr>(ebCur + 1)) == 1)
                {
                    // newly inserted STAL block as the StartFixedBlockThreshold condition meet
                    PerSourceIndrPointer *per_source_stal = (PerSourceIndrPointer *) (me->Allocate(sizeof(PerSourceIndrPointer), thread_id));
                    per_source_stal->initVal();
                    per_source_stal->setIndexList(me, thread_id);

                    per_source_stal->addNewBlock(static_cast<stalb_type *>(index[phy_src_id].getEdgePtr()));

                    // create new edge block and insert value // init stalb

                    stalb_type *new_stalb = static_cast<stalb_type *>(me->Allocate(32, thread_id));

                    double *property = static_cast<double *>(me->Allocate(16, thread_id));


                    memset(new_stalb, 0, 32);
                    memset(property, 0, 16);

                    //  std::memcpy(stalb,&blockSize, sizeof())
                    *reinterpret_cast<uint8_ptr>(new_stalb) = 2; // block size

                    *reinterpret_cast<uint16_ptr>(new_stalb + 1) = 2; // cur_index

                    // insertion steps

                    *reinterpret_cast<uint64_ptr>(new_stalb + 8) = remove_version(phy_dest_id); // dest node

                    *reinterpret_cast<EdgeInfoHal **>(new_stalb + 16) = ei; // edge info hal insertion

                    property[1] = weight_w;

                    *reinterpret_cast<double **>(new_stalb + 24) = property; // property insertion

                    uint16_t *cur_ind = reinterpret_cast<uint16_ptr>(new_stalb + 1);

                    *cur_ind = 1; // update cur index

                    // perThreadNumberOfEdges[thread_id]++;

                    per_source_stal->addNewBlock(new_stalb);

                    // hash table related entries

                    indr_index_bits_val = per_source_stal->getCurPos();
                    indr_size_bits_val = per_source_stal->getBlockLen();

                    edge_block_index_bits_val = *cur_ind;
                    edge_block_size_bits_val = 2;

                    isSTAL = 1;

                    // replace edge block pointer with STAL in persource vertex array
                    index[phy_src_id].indirectionLock.lock();

                    index[phy_src_id].setEdgePtr(reinterpret_cast<vertex_t>(per_source_stal), false);

                    index[phy_src_id].indirectionLock.unlock();

                    ei->setWTsEdge(getCurrTimeStamp());
                }
                else
                {
                    // insert in the edge block

                    uint16_t *curPos = reinterpret_cast<uint16_ptr>(ebCur + 1);  //ebCur->getCurIndex(); // *(ebCur + 1);
                    if (*curPos > 1)
                    {
                        *reinterpret_cast<uint64_ptr>((ebCur) + ((*curPos - 1) * 8)) = remove_version(phy_dest_id); // dst id

                        *reinterpret_cast<EdgeInfoHal **>((ebCur) + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(ebCur)) / 2) * 8) + (((*curPos - 1) * 8) - 8)) = ei; // edgeinfohal

                        double *property = *reinterpret_cast<double **>( ebCur + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(ebCur)) * 8) - 8)); // blocksize

                        property[(*curPos - 1)] = weight_w; // weight

                        *curPos = *curPos - 1;

                        //  perThreadNumberOfEdges[thread_id]++;

                        ei->setWTsEdge(getCurrTimeStamp());
                        indr_index_bits_val = 0; //srcVertex->getCurPos();
                        indr_size_bits_val = 0; //srcVertex->getBlockLen();
                        edge_block_index_bits_val = *curPos;
                        edge_block_size_bits_val = *reinterpret_cast<uint8_ptr>(ebCur); // get block size
                        //  index[phy_src_id].incrDegree();
                        //
                    }
                    else
                    {
                        // allocate new stal block
                        index[phy_src_id].indirectionLock.lock();

                        UPBlockSizePointer *newblock_pointer = UpEdgeBlock(ebCur, *reinterpret_cast<uint8_ptr>(ebCur), me, thread_id);

                        stalb_type *new_ebCur = newblock_pointer->new_stalb;

                        index[phy_src_id].setEdgePtr(reinterpret_cast<vertex_t>(new_ebCur), true);

                        me->free(newblock_pointer->prev_stalb, newblock_pointer->prev_block_size * 8, thread_id); // free previous block after new block update

                        me->free(newblock_pointer->prev_property_arr, (newblock_pointer->prev_block_size / 2) * 8, thread_id); // free property array

                        // ebCur->UpEdgeBlock(me, thread_id, ebCur->getBlockSize());
                        uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(new_ebCur + 1);


                        // insert edge block update

                        *reinterpret_cast<uint64_ptr>((new_ebCur) + ((*new_curPos - 1) * 8)) = remove_version(phy_dest_id); // dst id

                        *reinterpret_cast<EdgeInfoHal **>((new_ebCur) + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(new_ebCur)) / 2) * 8) + (((*new_curPos - 1) * 8) - 8)) = ei; // edgeinfohal

                        double *property = *reinterpret_cast<double **>( new_ebCur + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(new_ebCur)) * 8) - 8)); // blocksize

                        property[(*new_curPos - 1)] = weight_w; // weight

                        *new_curPos = *new_curPos - 1;


                        //  ebCur->setEdgeUpdt(ei, inputedge_info.getDestVertexId(), weight_w);
                        //   perThreadNumberOfEdges[thread_id]++;

                        indr_index_bits_val = 0; //srcVertex->getCurPos();
                        indr_size_bits_val = 0; //srcVertex->getBlockLen();
                        edge_block_index_bits_val = *new_curPos;
                        edge_block_size_bits_val = (*reinterpret_cast<uint8_ptr>(new_ebCur));

                        index[phy_src_id].indirectionLock.unlock();

                        ei->setWTsEdge(getCurrTimeStamp());
                        //  index[phy_src_id].incrDegree();
                    }


                } /// %%%


            }
            else
            {


                // STAL exist and we need to insert edge entry there
                PerSourceIndrPointer *ptr_per_src = static_cast<PerSourceIndrPointer *>(index[phy_src_id].getEdgePtr());

                int indr_arr_cur_pos = ptr_per_src->getCurPos();


                stalb_type *ebCur = ptr_per_src->getperVertexBlockArr()[indr_arr_cur_pos];

                uint16_t *curPos = reinterpret_cast<uint16_ptr>(ebCur + 1);//ebCur->getCurIndex();


                if (*curPos > 1)
                {

                    *reinterpret_cast<uint64_ptr>((ebCur) + ((*curPos - 1) * 8)) = remove_version(phy_dest_id); // dst id

                    *reinterpret_cast<EdgeInfoHal **>((ebCur) + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(ebCur)) / 2) * 8) + (((*curPos - 1) * 8) - 8)) = ei; // edgeinfohal

                    double *property = *reinterpret_cast<double **>( ebCur + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(ebCur)) * 8) - 8)); // blocksize

                    property[(*curPos - 1)] = weight_w; // weight

                    *curPos = *curPos - 1;

                    // perThreadNumberOfEdges[thread_id]++;

                    indr_index_bits_val = ptr_per_src->getCurPos();
                    indr_size_bits_val = ptr_per_src->getBlockLen();
                    edge_block_index_bits_val = *curPos;
                    edge_block_size_bits_val = *reinterpret_cast<uint8_ptr>(ebCur);
                    isSTAL = 1;

                    // index[phy_src_id].incrDegree();
                    ei->setWTsEdge(getCurrTimeStamp());

                }
                else
                {
                    if (indr_arr_cur_pos == 0)
                    {

                        // if block is full than create new block and commit previous block
                        stalb_type *ebNew;
                        //      2^*StartFixedBlockThreshold 32, 64, 128, 256, 512, 1024, 2048
                        // srcVertex->blockLen - srcVertex->blockLen - 0
                        // if blockLen *  StartFixedBlockThreshold < endFixedBlockThreshold then checkThreshold is blockLen *  StartFixedBlockThreshold
                        // else checkThreshold = endFixedBlockThreshold
                        // block size 8
                        // 8 * 32 = 256
                        // 32 64 128 256 512 1024 2048
                        // compute max threashold block size


                        uint64_t checkThreshold = EndFixedBlockThreshold;


                        //

                        if ((*reinterpret_cast<uint8_ptr>(ebCur) - 1) < checkThreshold)
                        {
                            // allocate new stal block

                            index[phy_src_id].indirectionLock.lock();

                            UPBlockSizePointer *newblock_pointer = UpEdgeBlock(ebCur, *reinterpret_cast<uint8_ptr>(ebCur), me, thread_id);

                            stalb_type *new_ebCur = newblock_pointer->new_stalb;

                            ptr_per_src->getperVertexBlockArr()[indr_arr_cur_pos] = new_ebCur;

                            me->free(newblock_pointer->prev_stalb, newblock_pointer->prev_block_size * 8, thread_id); // free previous block after new block update

                            me->free(newblock_pointer->prev_property_arr, (newblock_pointer->prev_block_size / 2) * 8, thread_id); // free property array

                            // ebCur->UpEdgeBlock(me, thread_id, ebCur->getBlockSize());
                            uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(new_ebCur + 1);

                            // insert edge block update

                            *reinterpret_cast<uint64_ptr>((new_ebCur) + ((*new_curPos - 1) * 8)) = remove_version(phy_dest_id); // dst id

                            *reinterpret_cast<EdgeInfoHal **>((new_ebCur) + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(new_ebCur)) / 2) * 8) + (((*new_curPos - 1) * 8) - 8)) = ei; // edgeinfohal

                            double *property = *reinterpret_cast<double **>( new_ebCur + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(new_ebCur)) * 8) - 8)); // blocksize

                            property[(*new_curPos - 1)] = weight_w; // weight

                            *new_curPos = *new_curPos - 1;

                            //   perThreadNumberOfEdges[thread_id]++;

                            indr_index_bits_val = ptr_per_src->getCurPos();
                            indr_size_bits_val = ptr_per_src->getBlockLen();
                            edge_block_index_bits_val = *new_curPos;
                            edge_block_size_bits_val = *reinterpret_cast<uint8_ptr>(new_ebCur);


                            isSTAL = 1;
                            index[phy_src_id].indirectionLock.unlock();

                            //  index[phy_src_id].incrDegree();
                            ei->setWTsEdge(getCurrTimeStamp());

                        }
                        else
                        {

                            // init stalb

                            index[phy_src_id].indirectionLock.lock();

                            stalb_type *new_stalb = static_cast<stalb_type *>(me->Allocate(32, thread_id));

                            double *property = static_cast<double *>(me->Allocate(16, thread_id));


                            memset(new_stalb, 0, 32);
                            memset(property, 0, 16);

                            //  std::memcpy(stalb,&blockSize, sizeof())
                            *reinterpret_cast<uint8_ptr>(new_stalb) = 2; // block size log2

                            *reinterpret_cast<uint16_ptr>(new_stalb + 1) = 2; // cur_index


                            // insertion steps

                            *reinterpret_cast<uint64_ptr>(new_stalb + 8) = remove_version(phy_dest_id); // dest node

                            *reinterpret_cast<EdgeInfoHal **>(new_stalb + 16) = ei; // edge info hal insertion

                            property[1] = weight_w;

                            *reinterpret_cast<double **>(new_stalb + 24) = property; // property insertion

                            uint16_t *cur_ind = reinterpret_cast<uint16_ptr>(new_stalb + 1);

                            *cur_ind = 1; // update cur index


                            //  ebNew = (EdgeBlock *) ((me->Allocate(sizeof(EdgeBlock), thread_id)));
                            //   ebNew->initEdgeBlock(me, thread_id, 1);
                            //   ebNew->setEdgeUpdt(ei, inputedge_info.getDestVertexId(), weight_w);


                            //  perThreadNumberOfEdges[thread_id]++;

                            ptr_per_src->setIndexList(me, thread_id);

                            ptr_per_src->addNewBlock(new_stalb); // add in STAL

                            indr_index_bits_val = ptr_per_src->getCurPos();
                            indr_size_bits_val = ptr_per_src->getBlockLen();
                            edge_block_index_bits_val = *cur_ind;
                            edge_block_size_bits_val = 2;
                            // index[phy_src_id].incrDegree();
                            isSTAL = 1;
                            index[phy_src_id].indirectionLock.unlock();

                            ei->setWTsEdge(getCurrTimeStamp());

                        }

                    }
                    else
                    {


                        // blocklen - curpos = 4 and cur 2
                        // 4-2 = 2 = 3 - 2 = 1<<(1)
                        // (blocklen-1) - cur_index = 7 - 3 = (1<<4 *  StartFixedBlockThreshold
                        // 8-3 = 5
                        // 7 6 5 4 3

                        // 2^2 * 2^5
                        //

                        // compute max threashold block size

                        uint64_t checkThreshold = EndFixedBlockThreshold;

                        //

                        if ((*reinterpret_cast<uint8_ptr>(ebCur) - 1) < checkThreshold)
                        {
                            // allocate new stal block
                            index[phy_src_id].indirectionLock.lock();

                            UPBlockSizePointer *newblock_pointer = UpEdgeBlock(ebCur, *reinterpret_cast<uint8_ptr>(ebCur), me, thread_id);

                            stalb_type *new_ebCur = newblock_pointer->new_stalb;

                            ptr_per_src->getperVertexBlockArr()[indr_arr_cur_pos] = new_ebCur;

                            me->free(newblock_pointer->prev_stalb, newblock_pointer->prev_block_size * 8, thread_id); // free previous block after new block update

                            me->free(newblock_pointer->prev_property_arr, (newblock_pointer->prev_block_size / 2) * 8, thread_id); // free property array

                            // ebCur->UpEdgeBlock(me, thread_id, ebCur->getBlockSize());
                            uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(new_ebCur + 1);

                            // insert edge block update

                            *reinterpret_cast<uint64_ptr>((new_ebCur) + ((*new_curPos - 1) * 8)) = phy_dest_id; // dst id

                            *reinterpret_cast<EdgeInfoHal **>((new_ebCur) + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(new_ebCur)) / 2) * 8) + (((*new_curPos - 1) * 8) - 8)) = ei; // edgeinfohal

                            //  double *property = *reinterpret_cast<double **>( new_ebCur +
                            double *property = *reinterpret_cast<double **>( new_ebCur + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(new_ebCur)) * 8) - 8)); // blocksize

                            property[(*new_curPos - 1)] = weight_w; // weight

                            *new_curPos = *new_curPos - 1;


                            // ebCur->UpEdgeBlock(me, thread_id, ebCur->getBlockSize());
                            //  ebCur->setEdgeUpdt(ei, inputedge_info.getDestVertexId(), weight_w);

                            // perThreadNumberOfEdges[thread_id]++;

                            indr_index_bits_val = ptr_per_src->getCurPos();
                            indr_size_bits_val = ptr_per_src->getBlockLen();
                            edge_block_index_bits_val = *new_curPos;
                            edge_block_size_bits_val = *reinterpret_cast<uint8_ptr>(new_ebCur);

                            //  index[phy_src_id].incrDegree();
                            isSTAL = 1;
                            index[phy_src_id].indirectionLock.unlock();

                            ei->setWTsEdge(getCurrTimeStamp());
                        }
                        else
                        {
                            // if block is full than create new block and commit previous block
                            index[phy_src_id].indirectionLock.lock();

                            // init stalb
                            stalb_type *new_stalb = static_cast<stalb_type *>(me->Allocate(32, thread_id));

                            double *property = static_cast<double *>(me->Allocate(16, thread_id));


                            memset(new_stalb, 0, 32);
                            memset(property, 0, 16);

                            //  std::memcpy(stalb,&blockSize, sizeof())
                            *reinterpret_cast<uint8_ptr>(new_stalb) = 2; // block size log2

                            *reinterpret_cast<uint16_ptr>(new_stalb + 1) = 2; // cur_index


                            // insertion steps

                            *reinterpret_cast<uint64_ptr>(new_stalb + 8) = remove_version(phy_dest_id); // dest node

                            *reinterpret_cast<EdgeInfoHal **>(new_stalb + 16) = ei; // edge info hal insertion

                            property[1] = weight_w;

                            *reinterpret_cast<double **>(new_stalb + 24) = property; // property insertion

                            uint16_t *cur_ind = reinterpret_cast<uint16_ptr>(new_stalb + 1);

                            *cur_ind = 1; // update cur index



                            //   EdgeBlock *ebNew = (EdgeBlock *) ((me->Allocate(sizeof(EdgeBlock), thread_id)));

                            // ebNew->initEdgeBlock(me, thread_id, 1);
                            // ebNew->setEdgeUpdt(ei, inputedge_info.getDestVertexId(), weight_w);

                            //  perThreadNumberOfEdges[thread_id]++;

                            ptr_per_src->addNewBlock(new_stalb);
                            index[phy_src_id].indirectionLock.unlock();

                            indr_index_bits_val = ptr_per_src->getCurPos();
                            indr_size_bits_val = ptr_per_src->getBlockLen();
                            edge_block_index_bits_val = *cur_ind;
                            edge_block_size_bits_val = 2;
                            isSTAL = 1;

                            //  index[phy_src_id].incrDegree();

                            ei->setWTsEdge(getCurrTimeStamp());
                        }

                    }
                }

            }


#ifdef DBUGFLAGPERSOURCE
            if(inputedge_info.getSrcVertexId() == SRC && inputedge_info.getDestVertexId() == DST)
                                        {
                                            testLock.lock();
                                          //  foutput << " insertion start 6.5 "<<inputedge_info.getSrcVertexId()<<" dest "<<inputedge_info.getDestVertexId()<<" indrection size "<<srcVertex->getBlockLen()<<" indirection index "<<indr_arr_cur_pos<<std::endl;
                                            testLock.unlock();
                                        }
#endif

            //%%%%%%  }
            u_int64_t adjcent_pos_store = 0;


            adjcent_pos_store = setBitMask(adjcent_pos_store, indr_index_bits_val, L1IndexStart, L1IndexBits);
            adjcent_pos_store = setBitMask(adjcent_pos_store, indr_size_bits_val, L1SizeStart, L1SizeBits);
            adjcent_pos_store = setBitMask(adjcent_pos_store, edge_block_index_bits_val, L2IndexStart, L2IndexBits);
            adjcent_pos_store = setBitMask(adjcent_pos_store, edge_block_size_bits_val, L2SizeStart, L2SizeBits);
            adjcent_pos_store = setBitMask(adjcent_pos_store, dest_prefix, DNPStart, DNPBits);
            adjcent_pos_store = setBitMask(adjcent_pos_store, isSTAL, IsSTALStart, IsSTALBits);
            adjcent_pos_store = setBitMask(adjcent_pos_store, 0, IsInOrderStart, IsInOrderBits);

            index[phy_src_id].setDestNodeHashTableVal(phy_dest_id, adjcent_pos_store, me, thread_id);


#ifdef DBUGFLAGG
            testLock.lock();
                                    //foutput << " insertion end " << inputedge_info.getSrcVertexId() << " "<< inputedge_info.getDestVertexId() << std::endl;
                                    testLock.unlock();
#endif
            // index[phy_src_id].unsetPerVertexLock(); //.unlock();


        }
        //  testLock.lock();
        // std::cout << " insertion end " << phy_src_id << " "<< phy_dest_id << std::endl;
        // testLock.unlock();
        //   }

        ///%%%%%%%%% degree calculation %%%%%%%%%

        // if(check_version(index[phy_src_id].degree))
        //{
        //  gcDegreeList(phy_src_id, me, thread_id);
        //}

        bool flagc = false;
        if (check_version(index[phy_src_id].degree))
        {
            // DegreeNode
            DegreeNode *cur_degree = reinterpret_cast<DegreeNode *>(remove_version(index[phy_src_id].degree));
            if (getMinReadDegreeVersion() > cur_degree->wts)
            {

                /// %%%%%%%%%%
                InvalidDegree *new_invl_node = static_cast<InvalidDegree *>(malloc(sizeof(InvalidDegree)));//static_cast<InvalidDegree *>(me->Allocate(sizeof(InvalidDegree), thread_id)); // InvalidDegree();

                index[phy_src_id].degree = remove_version(cur_degree->degree) + 1;

                new_invl_node->ptr = cur_degree;

                this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.lock();
                if (this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr == NULL)
                {
                    this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr = new_invl_node;
                    new_invl_node->next = NULL;
                }
                else
                {
                    new_invl_node->next = static_cast<InvalidDegree *>(this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr);
                    this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr = new_invl_node;
                }
                this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.unlock();

                flagc = true; // in case writer put node to InvalidListDegree
            }
            else
            {

                DegreeNode *new_degree;

                new_degree = (DegreeNode *) (me->Allocate(sizeof(DegreeNode), thread_id));

                new_degree->degree = remove_version(cur_degree->degree) + 1;

                new_degree->next = cur_degree;

                index[phy_src_id].degree = add_version(reinterpret_cast<vertex_t>(new_degree));
                new_degree->wts = getCurrTimeStamp();
            }

        }
        else
        {
            //
            if (getMinReadDegreeVersion() == MaxValue)
            {

                index[phy_src_id].degree = remove_version(index[phy_src_id].degree) + 1;

            }
            else
            {

                DegreeNode *cur_degree;
                DegreeNode *new_degree;
                cur_degree = (DegreeNode *) (me->Allocate(sizeof(DegreeNode), thread_id));
                new_degree = (DegreeNode *) (me->Allocate(sizeof(DegreeNode), thread_id));
                cur_degree->degree = remove_version(index[phy_src_id].degree);
                cur_degree->wts = 0;
                cur_degree->next = NULL;

                new_degree->degree = remove_version(index[phy_src_id].degree) + 1;
                new_degree->next = cur_degree;

                index[phy_src_id].degree = add_version(reinterpret_cast<vertex_t>(new_degree));
                new_degree->wts = getCurrTimeStamp();

                // timestamp_t  wts_t = getMinReadVersion();
            }
        }


        ///%%%%%%%%% end degree %%%%%%%%%

        if (check_version(index[phy_src_id].degree))
        {
            flagc = true;
        }
        if (flagc)
        {
            gcDegreeList(phy_src_id, me, thread_id);
        }
        //std::cout<<"start insertion:"<<phy_src_id<<" "<<phy_dest_id<<std::endl;
        // invalLock.lock();

        // invalLock.unlock();
        index[phy_src_id].perVertexLock.unlock();
        //}
    }
    else
    {
        //std::cout<<"out-of-order update"<<std::endl;
        //outOfOrderCout++;
        LeafNode *ln = (LeafNode *) (me->Allocate(sizeof(LeafNode), thread_id)); //new EdgeInfoHal();
        ln->intLeafNode();
        ln->setDNode(phy_dest_id);
        ln->setSrcTime(inputedge_info.getUpdtSrcTime());

        // %%%%%%%%%%%%% hash table set value for out-of-order update %%%%%%%%%%%%

        u_int64_t adjcent_pos_store = 0;

        u_int64_t dest_prefix_ooo = (u_int64_t) phy_dest_id & thread_prefix_OOO;

        adjcent_pos_store = setBitMask(adjcent_pos_store, inputedge_info.getUpdtSrcTime(), t_s_start, t_s_bits);
        adjcent_pos_store = setBitMask(adjcent_pos_store, dest_prefix_ooo, DNPStart_OOO, DNP_ooo_bits);

        adjcent_pos_store = setBitMask(adjcent_pos_store, 1, IsOutOrderStart, IsOutOrderBits);

        index[phy_src_id].perVertexLock.lock();
        index[phy_src_id].setDestNodeHashTableVal(phy_dest_id, adjcent_pos_store, me, thread_id);


        // %%%%%%%%%%%% hash table end out-of-order update
        timestamp_t key = inputedge_info.getUpdtSrcTime();


        if (!(index[phy_src_id].edgePtr & 0x1))
        {
            PerSourceIndrPointer *ptr_per_src = static_cast<PerSourceIndrPointer *>(index[phy_src_id].getEdgePtr());

            int startPos = ptr_per_src->getCurPos();
            int endPos = ((u_int64_t) 1 << ptr_per_src->getBlockLen());
            int indexIndrArray = findIndexForIndr((ptr_per_src->getperVertexBlockArr()), startPos, endPos, key);

            ///
            u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray]);
            EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + ((eb_block_size / 2) * 8) + ((((u_int16_t) *reinterpret_cast<uint16_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + 1)) * 8) - 8));

            ///
            //     ae->blockIndex = indexIndrArray; // store for setting isdeletion bit in case of deletion
            if (eih_cur->getSTime() < key)
            {
                if (indexIndrArray != 0)
                {
                    indexIndrArray--;
                }
            }
            //fixed size block binary search
            int indexFixedBlock = findIndexForBlockArr(ptr_per_src->getperVertexBlockArr()[indexIndrArray], ((u_int16_t) *reinterpret_cast<uint16_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + 1)), (eb_block_size / 2), key);

            /// eb block
            EdgeInfoHal *eih_index = *reinterpret_cast<EdgeInfoHal **>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + ((eb_block_size / 2) * 8) + ((indexFixedBlock * 8) - 8));

            //

            if (eih_index->getSTime() < key)
            {
                indexFixedBlock--;
            }
            EdgeInfoHal *setOutOfOrder = *reinterpret_cast<EdgeInfoHal **>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + ((eb_block_size / 2) * 8) + ((indexFixedBlock * 8) - 8));


            // EdgeInfoHal *setOutOfOrder = *reinterpret_cast<EdgeInfoHal *>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] +((eb_block_size / 2) * 8) + ((indexFixedBlock * 8) - 8));
            if (setOutOfOrder->getOutOfOrderUpdt() == NULL)
            {
                // use art syncornized version for correctness

                ArtPerIndex *perArt = (ArtPerIndex *) (std::aligned_alloc(PAGE_SIZE, sizeof(ArtPerIndex)));
                perArt->initArtTree();
                perArt->insertArtLeaf(ln);
                setOutOfOrder->setOutOfOrderUpdt(perArt);
                *reinterpret_cast<uint64_ptr>((ptr_per_src->getperVertexBlockArr()[indexIndrArray]) + ((indexFixedBlock) * 8)) = add_version_ooo(*reinterpret_cast<uint64_ptr>((ptr_per_src->getperVertexBlockArr()[indexIndrArray]) + ((indexFixedBlock) * 8)));
                perArt->increCount();
                ln->setWts(getCurrTimeStamp());
            }
            else
            {
                ArtPerIndex *perArt = setOutOfOrder->getOutOfOrderUpdt();
                perArt->insertArtLeaf(ln);
                perArt->increCount();
                ln->setWts(getCurrTimeStamp());
            }
            *reinterpret_cast<uint8_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + 4) = 1; // out-of-order 1
        }
        else
        {
            //%%%%%%%%%%%%% direct edge block

            //fixed size block binary search
            stalb_type *eb = static_cast<stalb_type *>(index[phy_src_id].getEdgePtr());
            u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

            int indexFixedBlock = findIndexForBlockArr(eb, ((u_int16_t) *reinterpret_cast<uint16_ptr>(eb + 1)), (eb_block_size / 2), key);

            /// eb block
            EdgeInfoHal *eih_index = *reinterpret_cast<EdgeInfoHal **>(eb + ((eb_block_size / 2) * 8) + ((indexFixedBlock * 8) - 8));

            //

            if (eih_index->getSTime() < key)
            {
                indexFixedBlock--;
            }

            EdgeInfoHal *setOutOfOrder = *reinterpret_cast<EdgeInfoHal **>(eb + ((eb_block_size / 2) * 8) + ((indexFixedBlock * 8) - 8));
            if (setOutOfOrder->getOutOfOrderUpdt() == NULL)
            {
                // use art syncornized version for correctness
                ArtPerIndex *perArt = (ArtPerIndex *) (std::aligned_alloc(PAGE_SIZE, sizeof(ArtPerIndex)));
                perArt->initArtTree();
                perArt->insertArtLeaf(ln);
                perArt->increCount();
                *reinterpret_cast<uint64_ptr>(eb + ((indexFixedBlock) * 8)) = add_version_ooo(*reinterpret_cast<uint64_ptr>(eb + ((indexFixedBlock) * 8)));
                setOutOfOrder->setOutOfOrderUpdt(perArt);
                ln->setWts(getCurrTimeStamp());
            }
            else
            {
                ArtPerIndex *perArt = setOutOfOrder->getOutOfOrderUpdt();
                perArt->insertArtLeaf(ln);
                perArt->increCount();
                ln->setWts(getCurrTimeStamp());
            }
            *reinterpret_cast<uint8_ptr>(eb + 4) = 1; // out-of-order 1

            // %%%%%%% end
        }


        /*   bool flagc = false;
           if(check_version(index[phy_src_id].degree))
           {
               // DegreeNode
               DegreeNode *cur_degree = reinterpret_cast<DegreeNode *>(remove_version(index[phy_src_id].degree));
               if(getMinReadDegreeVersion() > cur_degree->wts)
               {

                   /// %%%%%%%%%%
                   InvalidDegree *new_invl_node = static_cast<InvalidDegree *>(malloc(sizeof(InvalidDegree)));//static_cast<InvalidDegree *>(me->Allocate(sizeof(InvalidDegree), thread_id)); // InvalidDegree();

                   index[phy_src_id].degree = remove_version(cur_degree->degree) + 1;

                   new_invl_node->ptr = cur_degree;

                   this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.lock();
                   if(this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr == NULL)
                   {
                       this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr = new_invl_node;
                       new_invl_node->next = NULL;
                   }
                   else
                   {
                       new_invl_node->next = static_cast<InvalidDegree *>(this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr);
                       this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr = new_invl_node;
                   }
                   this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.unlock();

                   flagc = true; // in case writer put node to InvalidListDegree
               }
               else
               {

                   DegreeNode *new_degree;

                   new_degree = (DegreeNode *) (me->Allocate(sizeof(DegreeNode), thread_id));

                   new_degree->degree = remove_version(cur_degree->degree) + 1;

                   new_degree->next = cur_degree;

                   index[phy_src_id].degree = add_version(reinterpret_cast<vertex_t>(new_degree));
                   new_degree->wts = getCurrTimeStamp();
               }

           }
           else
           {
               //
               if(getMinReadDegreeVersion() ==  MaxValue)
               {

                   index[phy_src_id].degree = remove_version(index[phy_src_id].degree) + 1;

               }
               else
               {
                   DegreeNode *cur_degree;
                   DegreeNode *new_degree;
                   cur_degree = (DegreeNode *) (me->Allocate(sizeof(DegreeNode), thread_id));
                   new_degree = (DegreeNode *) (me->Allocate(sizeof(DegreeNode), thread_id));
                   cur_degree->degree     = remove_version(index[phy_src_id].degree);
                   cur_degree->wts = 0;
                   cur_degree->next = NULL;

                   new_degree->degree     = remove_version(index[phy_src_id].degree) + 1;
                   new_degree->next = cur_degree;

                   index[phy_src_id].degree = add_version(reinterpret_cast<vertex_t>(new_degree));
                   new_degree->wts        = getCurrTimeStamp();

                   // timestamp_t  wts_t = getMinReadVersion();
               }
           }


           ///%%%%%%%%% end degree %%%%%%%%%

           if(check_version(index[phy_src_id].degree))
           {
               flagc = true;
           }
           if(flagc)
           {
             //  gcDegreeList(phy_src_id, me, thread_id);
           }*/
        index[phy_src_id].perVertexLock.unlock();
    }

    // todo end
}

////
void HBALStore::initVertexIndrList(MemoryAllocator *la)
{
    vertex_t curVertexSize = (vertex_t) 1 << vertexArraySize;
    //static vertex_t  curs = (vertex_t) 1 << vertexArraySize;
    PerSourceVertexIndr **vertexArray = reinterpret_cast<PerSourceVertexIndr **>((PerSourceVertexIndr *) malloc((curVertexSize * sizeof(PerSourceVertexIndr))));

    for (int i = 0; i < curVertexSize; i++)
    {
        PerSourceVertexIndr *perS = (PerSourceVertexIndr *) (malloc(sizeof(PerSourceVertexIndr)));
        // perS->perVertex = (PerSourceVertexIndr *) (std::aligned_alloc(PAGE_SIZE,sizeof(PerSourceVertexIndr)));
        // perS->initVal();
        // perS->initLock();

        //  psv->setBlockLen(1);
        //   psv->lock.init();

        vertexArray[i] = perS;//(PerSourceVertexIndr *) (std::aligned_alloc(PAGE_SIZE,sizeof(PerSourceVertexIndr)));
        // vertexArray[i]->initVal();
    }

    curIndVertexSize = VertexArrayIndr;
    vertexIndrList = reinterpret_cast<PerSourceVertexIndr ***>(reinterpret_cast<PerSourceVertexIndr **>((PerSourceVertexIndr *) malloc(((vertex_t) 1 << curIndVertexSize) * 8)));
    // memset(vertexIndrList, 0, ((vertex_t) 1 << curIndVertexSize) * sizeof(PerSourceVertexIndr));

    /*  for (int i = 0; i < ((vertex_t) 1 << curIndVertexSize); i++)
    {
        vertexIndrList[i] = NULL;
    }*/
    vertexIndrList[0] = vertexArray;

}

void HBALStore::resizeVertexArrayHal(vertex_t devVertexIndex, MemoryAllocator *ptr, int thread_id)
{
    if (devVertexIndex > (((vertex_t) 1 << curIndVertexSize) - 1))
    {
        // todo resize the indirection array (done)
        curIndVertexSize++;
        PerSourceVertexIndr ***tempArray = reinterpret_cast<PerSourceVertexIndr ***>(reinterpret_cast<PerSourceVertexIndr **>((PerSourceVertexIndr *) ptr->Allocate(((vertex_t) 1 << curIndVertexSize) * 8, thread_id)));
        for (int i = 0; i < ((vertex_t) 1 << curIndVertexSize); i++)
        {
            tempArray[i] = NULL;
        }
        // curIndVertexSize decremented becuase of neutralizing the increament effect few lines before
        std::copy(vertexIndrList, vertexIndrList + ((vertex_t) 1 << (curIndVertexSize - 1)), tempArray);
        //   delete [] vertexIndrList;
        vertexIndrList = tempArray;
    }

    // todo specfic vertex array block assignment (done)
    //  BlockProvider *ptr;
    vertex_t curVertexSize = (vertex_t) 1 << vertexArraySize;
    //static vertex_t  curs = (vertex_t) 1 << vertexArraySize;
    PerSourceVertexIndr **vertexArray = reinterpret_cast<PerSourceVertexIndr **>((PerSourceVertexIndr *) ptr->Allocate(curVertexSize * sizeof(PerSourceVertexIndr), thread_id));
    for (int i = 0; i < curVertexSize; i++)
    {
        PerSourceVertexIndr *perS = (PerSourceVertexIndr *) (ptr->Allocate(sizeof(PerSourceVertexIndr), thread_id));
        //  perS->perVertex = (PerSourceVertexIndr *) (std::aligned_alloc(PAGE_SIZE,sizeof(PerSourceVertexIndr)));
        //perS->initVal();
        //   perS->initLock();

        vertexArray[i] = perS;//(PerSourceVertexIndr *) (std::aligned_alloc(PAGE_SIZE,sizeof(PerSourceVertexIndr)));
        // vertexArray[i]->initVal();

    }
    // todo initialize new vertex array block(done)
    vertexIndrList[devVertexIndex] = vertexArray;
}

void HBALStore::initArt()
{
    //m = new KeyIndexStore();
}

PerSourceVertexIndr ***HBALStore::getVertexIndrList()
{
    return HBALStore::vertexIndrList;
}

vertex_t HBALStore::getMaxSourceVertex()
{
    return maxSourceVertex;
}

vertex_t HBALStore::getNumEdges()
{
    return numEdges;
}

void HBALStore::setNumEdges(vertex_t c)
{
    numEdges = 0;
}

vertex_t HBALStore::gettraverse_count_t()
{
    return traverse_count_t;
}

void HBALStore::settraverse_count_t(vertex_t c)
{
    traverse_count_t = 0;
}

void HBALStore::setLoadingTime(double loadTime)
{
    loadingTime = loadTime;
}

double HBALStore::getLoadingTime()
{
    return loadingTime;
}


void HBALStore::getLogClose()
{
    HBALStore::foutput.close();
}

bool HBALStore::has_vertex(vertex_t v)
{
    l_t_p_table::const_accessor a;
    return logical_to_physical.find(a, v);
}
/* common analytics functions */

// calculate number of vertices entries
u_int64_t HBALStore::CalculateNumberOfVertices()
{
    u_int64_t total_vertices = 0;

    for (int i = 0; i < numberOfThread; i++)
    {
        total_vertices += perThreadNumberOfvertices[i];
    }
    return total_vertices;
}

// calculate number of edges
uint64_t HBALStore::CalculateNumberOfEdges()
{
    uint64_t total_edge = 0;
    for (int i = 0; i < numberOfThread; i++)
    {
        total_edge += perThreadNumberOfEdges[i];
    }
    return total_edge;
}

// get per source vertex pointer to get adjacency list
PerSourceVertexIndr *HBALStore::getSourceVertexPointer(vertex_t src)
{
    return nullptr; //index[src];
}

//

/* Algorithm specfic analytics function for present query */
//// %%%%%%%%
// get degree



/// %%%%%%%
// get degree
vertex_t HBALStore::perSourceGetDegree(vertex_t v, int read_version, MemoryAllocator *me)
{
    //  std::cout<<me<<std::endl;
    //

    if (check_version(index[v].degree))
    {
        // DegreeNode
        DegreeNode *cur_degree = reinterpret_cast<DegreeNode *>(remove_version(index[v].degree));
        DegreeNode *prev = NULL;

        while (cur_degree != NULL)
        {
            // std::cout<<v<<std::endl;
            if (cur_degree->wts < min_read_degree_version)
            {
                // Add degree in garbage collection list
                // if(cur)
                if (cur_degree->next != NULL)
                {
                    // InvalidDegree *new_invl_node = new InvalidDegree(); // replace with own memory allocator

                    prev = cur_degree->next;
                    cur_degree->next = NULL;

                    InvalidDegree *new_invl_node = static_cast<InvalidDegree *>(malloc(sizeof(InvalidDegree)));//static_cast<InvalidDegree *>(me->Allocate(sizeof(InvalidDegree), (v % numberOfThread)));//static_cast<InvalidDegree *>(malloc(sizeof(InvalidDegree))); // InvalidDegree();
                    new_invl_node->ptr = prev;

                    this->invalidListDegree[v % THRESHOLDINVALID]->lock.lock();

                    if (this->invalidListDegree[v % THRESHOLDINVALID]->ptr == NULL)
                    {
                        this->invalidListDegree[v % THRESHOLDINVALID]->ptr = new_invl_node;
                        new_invl_node->next = NULL;
                    }
                    else
                    {
                        new_invl_node->next = static_cast<InvalidDegree *>(this->invalidListDegree[v % THRESHOLDINVALID]->ptr);
                        this->invalidListDegree[v % THRESHOLDINVALID]->ptr = new_invl_node;
                    }

                    this->invalidListDegree[v % THRESHOLDINVALID]->lock.unlock();

                    ///////// %%%%%%%%%%%%%%

                }
                // end degree garbage collection list
                return cur_degree->degree;
            }
            cur_degree = cur_degree->next;
        }
        std::cout << "No degree" << std::endl;
        return 0;
    }
    else
    {
        return remove_version(index[v].degree);
    }

    // index[v].indirectionLock.unlock();

    // return count_edge;*/
}
// BFS start

int64_t HBALStore::do_bfs_BUStep(uint64_t max_vertex_id, int64_t *distances, int64_t distance, gapbs::Bitmap &front, gapbs::Bitmap &next, int read_version)
{
    timestamp_t min_read_versions = min_read_version;
    int64_t awake_count = 0;
    next.reset();
    // std::cout<<"do_bfs_BUStep 0 :"<<std::endl;

#pragma omp parallel for schedule(dynamic, 1024) reduction(+ : awake_count)
    for (uint64_t u = 0; u < max_vertex_id; u++)
    {

        // if (distances[u] == std::numeric_limits<int64_t>::max()) continue; // the vertex does not exist


        // COUT_DEBUG_BFS("explore: " << u << ", distance: " << distances[u]);
        if (distances[u] < 0)
        { // the node has not been visited yet

            // #### per source edge reading
            bool IsWTS = false;
            index[u].indirectionLock.lock();
            //  index[u].perVertexLock.lock();
            if (index[u].edgePtr != 0)
            {

                if (!(index[u].edgePtr & 0x1))
                {

                    //  std::cout<<"do_bfs_BUStep 0.1 :"<<u<<std::endl;

                    PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[u].getEdgePtr());
                    int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                    bool flag_l1_delete = false;
                    flag_l1_delete = ind_ptr->isDeletionBlock();
                    //  std::cout<<"do_bfs_BUStep 0.2 :"<<u<<std::endl;

                    if (!flag_l1_delete)
                    {


                        // deletion bit per source-vertex not active direct read L2 vector
                        for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                        {

                            stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                            int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size


                            //// %%%%%%%%%% stalb contains deletions %%%%%%%%%%
                            //get the latest deletion wts


                            /* uint8_t is_delete_l2_flag = 0;
                             is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                             if (!is_delete_l2_flag) {

                                 for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                 {

                                     /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                     uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k +ind_l2 *8);
                                     if (!IsWTS)
                                     {
                                         EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1
                                                 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                         if (ei->getWTsEdge() < min_read_versions)
                                         {
                                             IsWTS = true;
                                             if (front.get_bit(dst))
                                             {
                                                 distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                 awake_count++;
                                                 next.set_bit(u);
                                                 break;
                                             }

                                         }
                                     }
                                     else
                                     {

                                         if (front.get_bit(dst)) {
                                             distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                             awake_count++;
                                             next.set_bit(u);
                                             break;
                                         }
                                     }
                                     /// %%%%%%%%%%% end %%%%%%%%%%
                                 }

                             }
                             else
                             {*/
                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                            {

                                /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                if (!IsWTS)
                                {
                                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                    if (ei->getWTsEdge() < min_read_versions)
                                    {
                                        IsWTS = true;
                                        uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                        if (!check_version(dst))
                                        {
                                            if (front.get_bit(dst))
                                            {
                                                distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                awake_count++;
                                                next.set_bit(u);
                                                break;
                                            }
                                        }
                                        else
                                        {

                                            if (!check_version_read(dst))
                                            {


                                                if (min_read_versions < ei->getInvldTime()->getWts())
                                                {
                                                    if (front.get_bit(remove_version(dst)))
                                                    {
                                                        distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                        awake_count++;
                                                        next.set_bit(u);
                                                        break;
                                                    }
                                                }
                                                else
                                                {

                                                    *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                    // }
                                                }
                                                // }
                                            }



                                            //
                                        }

                                    }
                                }
                                else
                                {
                                    uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                    if (!check_version(dst))
                                    {
                                        if (front.get_bit(dst))
                                        {
                                            distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                            awake_count++;
                                            next.set_bit(u);
                                            break;
                                        }
                                    }
                                    else
                                    {

                                        if (!check_version_read(dst))
                                        {


                                            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                            if (min_read_versions < ei->getInvldTime()->getWts())
                                            {
                                                if (front.get_bit(remove_version(dst)))
                                                {
                                                    distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                    awake_count++;
                                                    next.set_bit(u);
                                                    break;
                                                }
                                            }
                                            else
                                            {

                                                *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                // }
                                                //

                                            }
                                            // }
                                        }

                                    }

                                }
                                /// %%%%%%%%%%% end %%%%%%%%%%
                            }
                            /*  }*/

                            // %%%%%%%%%% stalb block end %%%%%%%%%%%



                        }


                    }
                    else
                    {
                        /// %%%%%%%%%%%%%%%%% if indirection array have deletions STAL %%%%%%%%%%%%

                        for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                        {
                            if (ind_ptr->getperVertexBlockArr()[ind_l1] != ((stalb_type *) FLAG_EMPTY_SLOT))
                            {
                                stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                //// %%%%%%%%%% stalb contains deletions %%%%%%%%%%
                                //get the latest deletion wts

                                //  std::cout<<"u 1.2 start"<<std::endl;
                                /*  uint8_t is_delete_l2_flag = 0;
                                  is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                  if (!is_delete_l2_flag)
                                  {
                                      for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                      {

                                          /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                          uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k +ind_l2 *8);
                                          if (!IsWTS)
                                          {
                                              EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1
                                                      << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                              if (ei->getWTsEdge() < min_read_versions)
                                              {
                                                  IsWTS = true;
                                                  if (front.get_bit(dst))
                                                  {
                                                      distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                      awake_count++;
                                                      next.set_bit(u);
                                                      break;
                                                  }

                                              }
                                          }
                                          else
                                          {

                                              if (front.get_bit(dst)) {
                                                  distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                  awake_count++;
                                                  next.set_bit(u);
                                                  break;
                                              }
                                          }
                                          /// %%%%%%%%%%% end %%%%%%%%%%
                                      }

                                  }
                                  else
                                  {*/
                                for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                {

                                    /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                    if (!IsWTS)
                                    {
                                        EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                        if (ei->getWTsEdge() < min_read_versions)
                                        {
                                            IsWTS = true;
                                            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                            if (!check_version(dst))
                                            {
                                                if (front.get_bit(dst))
                                                {
                                                    distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                    awake_count++;
                                                    next.set_bit(u);
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (!check_version_read(dst))
                                                {


                                                    if (min_read_versions < ei->getInvldTime()->getWts())
                                                    {
                                                        if (front.get_bit(remove_version(dst)))
                                                        {
                                                            distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                            awake_count++;
                                                            next.set_bit(u);
                                                            break;
                                                        }
                                                    }
                                                    else
                                                    {

                                                        *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                        // }
                                                        //

                                                    }
                                                    // }
                                                }
                                                //
                                            }

                                        }
                                    }
                                    else
                                    {
                                        uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                        if (!check_version(dst))
                                        {
                                            if (front.get_bit(dst))
                                            {
                                                distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                awake_count++;
                                                next.set_bit(u);
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            if (!check_version_read(dst))
                                            {


                                                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                if (min_read_versions < ei->getInvldTime()->getWts())
                                                {
                                                    if (front.get_bit(remove_version(dst)))
                                                    {
                                                        distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                        awake_count++;
                                                        next.set_bit(u);
                                                        break;
                                                    }
                                                }
                                                else
                                                {

                                                    *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                    // }
                                                    //

                                                }
                                                // }
                                            }

                                        }

                                    }
                                    /// %%%%%%%%%%% end %%%%%%%%%%
                                }
                                /* }*/

                                // %%%%%%%%%% stalb block end %%%%%%%%%%%



                            }

                        }


                        /// %%%%%%%%%%%%%%%%% end %%%%%%%%%%%%%%%%%%%%%%
                    }

                }
                else
                {

                    stalb_type *eb_k = static_cast<stalb_type *>(index[u].getEdgePtr());

                    int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;


                    //// %%%%%%%%%% stalb contains deletions %%%%%%%%%%
                    //get the latest deletion wts
                    /*  uint8_t is_delete_l2_flag = 0;
                      is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                      if (!is_delete_l2_flag)
                      {
                          for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                          {

                              /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                              uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k +ind_l2 *8);
                              if (!IsWTS)
                              {
                                  EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1
                                          << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                  if (ei->getWTsEdge() < min_read_versions)
                                  {
                                      IsWTS = true;
                                      if (front.get_bit(dst))
                                      {
                                          distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                          awake_count++;
                                          next.set_bit(u);
                                          break;
                                      }

                                  }
                              }
                              else
                              {

                                  if (front.get_bit(dst)) {
                                      distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                      awake_count++;
                                      next.set_bit(u);
                                      break;
                                  }
                              }
                              /// %%%%%%%%%%% end %%%%%%%%%%
                          }

                      }
                      else {*/
                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                    {

                        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                        if (!IsWTS)
                        {
                            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                            if (ei->getWTsEdge() < min_read_versions)
                            {
                                IsWTS = true;
                                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                if (!check_version(dst))
                                {
                                    if (front.get_bit(dst))
                                    {
                                        distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                        awake_count++;
                                        next.set_bit(u);
                                        break;
                                    }
                                }
                                else
                                {
                                    if (!check_version_read(dst))
                                    {


                                        if (min_read_versions < ei->getInvldTime()->getWts())
                                        {
                                            if (front.get_bit(remove_version(dst)))
                                            {
                                                distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                awake_count++;
                                                next.set_bit(u);
                                                break;
                                            }
                                        }
                                        else
                                        {

                                            *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                            // }
                                            //

                                        }
                                        // }
                                    }
                                    //
                                }

                            }
                        }
                        else
                        {
                            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                            if (!check_version(dst))
                            {
                                if (front.get_bit(dst))
                                {
                                    distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                    awake_count++;
                                    next.set_bit(u);
                                    break;
                                }
                            }
                            else
                            {

                                if (!check_version_read(dst))
                                {


                                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                    if (min_read_versions < ei->getInvldTime()->getWts())
                                    {
                                        if (front.get_bit(remove_version(dst)))
                                        {
                                            distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                            awake_count++;
                                            next.set_bit(u);
                                            break;
                                        }
                                    }
                                    else
                                    {

                                        *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                        // }
                                        //

                                    }
                                    // }
                                }


                            }

                        }
                        /// %%%%%%%%%%% end %%%%%%%%%%
                    }
                    /*   } */

                    // %%%%%%%%%% stalb block end %%%%%%%%%%%


                    // #### per source edge reading end


                }

            }
            index[u].indirectionLock.unlock();
            // index[u].perVertexLock.unlock();

        }
    }
    return awake_count;
}




////////////////

//////////////
int64_t HBALStore::do_bfs_TDStep(uint64_t max_vertex_id, int64_t *distances, int64_t distance, gapbs::SlidingQueue<int64_t> &queue, int read_version)
{
    timestamp_t min_read_versions = min_read_version;
    int64_t scout_count = 0;
#pragma omp parallel reduction(+ : scout_count)
    {
        gapbs::QueueBuffer<int64_t> lqueue(queue);

#pragma omp for schedule(dynamic, 64)
        for (auto q_iter = queue.begin(); q_iter < queue.end(); q_iter++)
        {
            int64_t u = *q_iter;

            // stal
            bool IsWTS = false;

            index[u].indirectionLock.lock();
            // index[u].perVertexLock.lock();
            if (index[u].edgePtr != 0)
            {
                if (!(index[u].edgePtr & 0x1))
                {
                    PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[u].getEdgePtr());
                    int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                    bool flag_l1_delete = false;
                    flag_l1_delete = ind_ptr->isDeletionBlock();
                    if (!flag_l1_delete)
                    {
                        // deletion bit per source-vertex not active direct read L2 vector
                        for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                        {

                            stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                            int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                            //// %%%%%%%%%% stalb contains deletions %%%%%%%%%%
                            //get the latest deletion wts
                            //  timestamp_t latest_deletion = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1<< *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8))[0];//

                            /*   uint8_t is_delete_l2_flag = 0;
                               is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                               if (!is_delete_l2_flag)
                               {
                                   for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                   {

                                       /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                       uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k +ind_l2 *8);
                                       if (!IsWTS)
                                       {
                                           EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1
                                                   << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                           if (ei->getWTsEdge() < min_read_versions)
                                           {
                                               IsWTS = true;
                                               int64_t curr_val = distances[dst];
                                               if (curr_val < 0 &&
                                                   gapbs::compare_and_swap(distances[dst], curr_val, distance)) {

                                                   lqueue.push_back(dst);
                                                   scout_count += -curr_val;
                                               }

                                           }
                                       }
                                       else
                                       {

                                           int64_t curr_val = distances[dst];
                                           if (curr_val < 0 &&
                                               gapbs::compare_and_swap(distances[dst], curr_val, distance)) {

                                               lqueue.push_back(dst);
                                               scout_count += -curr_val;
                                           }
                                       }
                                       /// %%%%%%%%%%% end %%%%%%%%%%
                                   }

                               }
                               else
                               {*/
                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                            {

                                /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                if (!IsWTS)
                                {
                                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                    if (ei->getWTsEdge() < min_read_versions)
                                    {
                                        IsWTS = true;
                                        uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                        if (!check_version(dst))
                                        {
                                            int64_t curr_val = distances[dst];
                                            if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                            {

                                                lqueue.push_back(dst);
                                                scout_count += -curr_val;
                                            }
                                        }
                                        else
                                        {


                                            if (!check_version_read(dst))
                                            {


                                                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                if (min_read_versions < ei->getInvldTime()->getWts())
                                                {
                                                    int64_t curr_val = distances[remove_version(dst)];
                                                    if (curr_val < 0 && gapbs::compare_and_swap(distances[remove_version(dst)], curr_val, distance))
                                                    {

                                                        lqueue.push_back(remove_version(dst));
                                                        scout_count += -curr_val;
                                                    }
                                                }
                                                else
                                                {

                                                    *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                    // }
                                                    //

                                                }
                                                // }
                                            }

                                            //
                                        }

                                    }
                                }
                                else
                                {
                                    uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                    if (!check_version(dst))
                                    {
                                        int64_t curr_val = distances[dst];
                                        if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                        {

                                            lqueue.push_back(dst);
                                            scout_count += -curr_val;
                                        }
                                    }
                                    else
                                    {
                                        if (!check_version_read(dst))
                                        {


                                            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                            if (min_read_versions < ei->getInvldTime()->getWts())
                                            {
                                                int64_t curr_val = distances[remove_version(dst)];
                                                if (curr_val < 0 && gapbs::compare_and_swap(distances[remove_version(dst)], curr_val, distance))
                                                {

                                                    lqueue.push_back(remove_version(dst));
                                                    scout_count += -curr_val;
                                                }
                                            }
                                            else
                                            {

                                                *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                // }
                                                //

                                            }
                                            // }
                                        }

                                    }

                                }
                                /// %%%%%%%%%%% end %%%%%%%%%%
                            }
                            /*  } */
                            // %%%%%%%%%% stalb block end %%%%%%%%%%%



                        }
                    }
                    else
                    {
                        /// %%%%%%%%%%%%%%%%% if indirection array have deletions STAL %%%%%%%%%%%%

                        for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                        {
                            if (ind_ptr->getperVertexBlockArr()[ind_l1] != ((stalb_type *) FLAG_EMPTY_SLOT))
                            {
                                stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer
                                int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size
                                //// %%%%%%%%%% stalb contains deletions %%%%%%%%%%
                                //get the latest deletion wts
                                // timestamp_t latest_deletion = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1<< *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8))[0];//

                                /*  uint8_t is_delete_l2_flag = 0;
                                  is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                  if (!is_delete_l2_flag)
                                  {
                                      for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                      {

                                          /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                          uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k +ind_l2 *8);
                                          if (!IsWTS)
                                          {
                                              EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1
                                                      << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                              if (ei->getWTsEdge() < min_read_versions)
                                              {
                                                  IsWTS = true;
                                                  int64_t curr_val = distances[dst];
                                                  if (curr_val < 0 &&
                                                      gapbs::compare_and_swap(distances[dst], curr_val, distance)) {

                                                      lqueue.push_back(dst);
                                                      scout_count += -curr_val;
                                                  }

                                              }
                                          }
                                          else
                                          {

                                              int64_t curr_val = distances[dst];
                                              if (curr_val < 0 &&
                                                  gapbs::compare_and_swap(distances[dst], curr_val, distance)) {

                                                  lqueue.push_back(dst);
                                                  scout_count += -curr_val;
                                              }
                                          }
                                          /// %%%%%%%%%%% end %%%%%%%%%%
                                      }

                                  }
                                  else
                                  {*/
                                for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                {

                                    /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                    if (!IsWTS)
                                    {
                                        EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                        if (ei->getWTsEdge() < min_read_versions)
                                        {
                                            IsWTS = true;
                                            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                            if (!check_version(dst))
                                            {
                                                int64_t curr_val = distances[dst];
                                                if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                                {

                                                    lqueue.push_back(dst);
                                                    scout_count += -curr_val;
                                                }
                                            }
                                            else
                                            {
                                                if (!check_version_read(dst))
                                                {


                                                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                    if (min_read_versions < ei->getInvldTime()->getWts())
                                                    {
                                                        int64_t curr_val = distances[remove_version(dst)];
                                                        if (curr_val < 0 && gapbs::compare_and_swap(distances[remove_version(dst)], curr_val, distance))
                                                        {

                                                            lqueue.push_back(remove_version(dst));
                                                            scout_count += -curr_val;
                                                        }
                                                    }
                                                    else
                                                    {

                                                        *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                        // }
                                                        //

                                                    }
                                                    // }
                                                }
                                                //
                                            }

                                        }
                                    }
                                    else
                                    {
                                        uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                        if (!check_version(dst))
                                        {
                                            int64_t curr_val = distances[dst];
                                            if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                            {

                                                lqueue.push_back(dst);
                                                scout_count += -curr_val;
                                            }
                                        }
                                        else
                                        {
                                            if (!check_version_read(dst))
                                            {


                                                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                if (min_read_versions < ei->getInvldTime()->getWts())
                                                {
                                                    int64_t curr_val = distances[remove_version(dst)];
                                                    if (curr_val < 0 && gapbs::compare_and_swap(distances[remove_version(dst)], curr_val, distance))
                                                    {

                                                        lqueue.push_back(remove_version(dst));
                                                        scout_count += -curr_val;
                                                    }
                                                }
                                                else
                                                {

                                                    *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                    // }
                                                    //

                                                }
                                                // }
                                            }

                                        }

                                    }
                                    /// %%%%%%%%%%% end %%%%%%%%%%
                                }
                                /*  }*/
                                // %%%%%%%%%% stalb block end %%%%%%%%%%%



                            }

                        }

                        /// %%%%%%%%%%%%%%%%% end %%%%%%%%%%%%%%%%%%%%%%
                    }
                }
                else
                {

                    stalb_type *eb_k = static_cast<stalb_type *>(index[u].getEdgePtr());

                    int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;

                    //// %%%%%%%%%% stalb contains deletions %%%%%%%%%%
                    //get the latest deletion wts




                    /*  uint8_t is_delete_l2_flag = 0;
                      is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                      if (!is_delete_l2_flag)
                      {
                          for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                          {

                              /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                              uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k +ind_l2 *8);
                              if (!IsWTS)
                              {
                                  EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1
                                          << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                  if (ei->getWTsEdge() < min_read_versions)
                                  {
                                      IsWTS = true;
                                      int64_t curr_val = distances[dst];
                                      if (curr_val < 0 &&
                                          gapbs::compare_and_swap(distances[dst], curr_val, distance)) {

                                          lqueue.push_back(dst);
                                          scout_count += -curr_val;
                                      }

                                  }
                              }
                              else
                              {

                                  int64_t curr_val = distances[dst];
                                  if (curr_val < 0 &&
                                      gapbs::compare_and_swap(distances[dst], curr_val, distance)) {

                                      lqueue.push_back(dst);
                                      scout_count += -curr_val;
                                  }
                              }
                              /// %%%%%%%%%%% end %%%%%%%%%%
                          }

                      }
                      else {
  */

                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                    {

                        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                        if (!IsWTS)
                        {
                            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                            if (ei->getWTsEdge() < min_read_versions)
                            {
                                IsWTS = true;
                                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                if (!check_version(dst))
                                {
                                    int64_t curr_val = distances[dst];
                                    if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                    {

                                        lqueue.push_back(dst);
                                        scout_count += -curr_val;
                                    }
                                }
                                else
                                {
                                    if (!check_version_read(dst))
                                    {


                                        EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                        if (min_read_versions < ei->getInvldTime()->getWts())
                                        {
                                            int64_t curr_val = distances[remove_version(dst)];
                                            if (curr_val < 0 && gapbs::compare_and_swap(distances[remove_version(dst)], curr_val, distance))
                                            {

                                                lqueue.push_back(remove_version(dst));
                                                scout_count += -curr_val;
                                            }
                                        }
                                        else
                                        {

                                            *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                            // }
                                            //

                                        }
                                        // }
                                    }
                                    //
                                }

                            }
                        }
                        else
                        {
                            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                            if (!check_version(dst))
                            {
                                int64_t curr_val = distances[dst];
                                if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                {

                                    lqueue.push_back(dst);
                                    scout_count += -curr_val;
                                }
                            }
                            else
                            {
                                if (!check_version_read(dst))
                                {


                                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                    if (min_read_versions < ei->getInvldTime()->getWts())
                                    {
                                        int64_t curr_val = distances[remove_version(dst)];
                                        if (curr_val < 0 && gapbs::compare_and_swap(distances[remove_version(dst)], curr_val, distance))
                                        {

                                            lqueue.push_back(remove_version(dst));
                                            scout_count += -curr_val;
                                        }
                                    }
                                    else
                                    {
                                        // if(readTable->readQuery[read_version] == getMinReadVersion())
                                        // {
                                        *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                        // }
                                        //

                                    }
                                    // }
                                }

                            }

                        }
                        /// %%%%%%%%%%% end %%%%%%%%%%
                    }
                    /*    }*/
                    // %%%%%%%%%% stalb block end %%%%%%%%%%%


                    // #### per source edge reading end


                }
            }
            index[u].indirectionLock.unlock();
            // index[u].perVertexLock.unlock();
            // ## end reading edge



        }

        lqueue.flush();
    }
    return scout_count;
}

void HBALStore::do_bfs_QueueToBitmap(uint64_t max_vertex_id, const gapbs::SlidingQueue<int64_t> &queue, gapbs::Bitmap &bm)
{
#pragma omp parallel for
    for (auto q_iter = queue.begin(); q_iter < queue.end(); q_iter++)
    {
        int64_t u = *q_iter;
        bm.set_bit_atomic(u);

    }
}

void HBALStore::do_bfs_BitmapToQueue(uint64_t max_vertex_id, const gapbs::Bitmap &bm, gapbs::SlidingQueue<int64_t> &queue)
{
#pragma omp parallel
    {
        gapbs::QueueBuffer<int64_t> lqueue(queue);
#pragma omp for
        for (uint64_t n = 0; n < max_vertex_id; n++)
            if (bm.get_bit(n))
            {

                lqueue.push_back(n);
            }
        lqueue.flush();
    }
    queue.slide_window();
}


// taken from gfe_driver livegraph part
std::unique_ptr<int64_t[]> HBALStore::do_bfs_init_distances(uint64_t max_vertex_id, int read_version)
{

    std::unique_ptr<int64_t[]> distances{new int64_t[max_vertex_id]};
    // int64_t sumedges = 0;
#pragma omp parallel for
    for (uint64_t n = 0; n < max_vertex_id; n++)
    {
        index[n].perVertexLock.lock();

        int64_t out_degree = perSourceGetDegree(n, read_version, NULL);//index[n].degree;

        index[n].perVertexLock.unlock();

        distances[n] = distances[n] = out_degree != 0 ? (-1 * out_degree) : -1;

    }
    return distances;
}


std::unique_ptr<int64_t[]> HBALStore::do_bfs(uint64_t num_vertices, uint64_t max_vertex_id, uint64_t root, int alpha, int beta, int read_version)
{

    //std::cout<<"read version :"<<read_version<<std::endl;
    //std::cout<<"read version timestamp :"<<readTable->readQuery[read_version]<<std::endl;

    max_vertex_id = get_high_water_mark();
    num_vertices = get_vertex_count(0);

    // std::cout<<"read version distance 0:"<<readTable->readQuery[read_version]<<std::endl;

    std::unique_ptr<int64_t[]> ptr_distances = do_bfs_init_distances(max_vertex_id, read_version);
    min_read_degree_version = MaxValue;

    //std::cout<<"read version distance 1:"<<readTable->readQuery[read_version]<<std::endl;
    uint64_t num_edges = 0; // = CalculateNumberOfEdges();

    // #pragma omp parallel for reduction(+:num_edges)
    for (uint64_t n = 0; n < max_vertex_id; n++)
    {
        num_edges += (ptr_distances[n] * -1);
    }


    int64_t *__restrict distances = ptr_distances.get();


    int64_t root_degree = 0;

    if (distances[root] != -1)
    {
        // std::cout<<"distance :"<<distances[root]<<std::endl;

        root_degree = (distances[root] * -1);
    }
    else
    {
        root_degree = 0;
    }

    distances[root] = 0;


    gapbs::SlidingQueue<int64_t> queue(max_vertex_id);
    queue.push_back(root);
    queue.slide_window();
    gapbs::Bitmap curr(max_vertex_id);
    curr.reset();
    gapbs::Bitmap front(max_vertex_id);
    front.reset();

    int64_t edges_to_check = num_edges / 2; //g.num_edges_directed();

    int64_t scout_count = root_degree;


    int64_t distance = 1; // current distance

    while (!queue.empty())
    {

        if (scout_count > edges_to_check / alpha)
        {
            int64_t awake_count, old_awake_count;

            //   std::cout<<"read version distance 3:"<<std::endl;

            do_bfs_QueueToBitmap(max_vertex_id, queue, front);

            awake_count = queue.size();
            queue.slide_window();

            do
            {
                old_awake_count = awake_count;

                awake_count = do_bfs_BUStep(max_vertex_id, distances, distance, front, curr, read_version);

                front.swap(curr);
                distance++;
            } while ((awake_count >= old_awake_count) || (awake_count > (int64_t) num_vertices / beta));

            do_bfs_BitmapToQueue(max_vertex_id, front, queue);
            scout_count = 1;
        }
        else
        {

            edges_to_check -= scout_count;
            scout_count = do_bfs_TDStep(max_vertex_id, distances, distance, queue, read_version);
            queue.slide_window();
            distance++;
        }
    }
    return ptr_distances;
}

// BFS end

// %%%%%%%%%%%%%%%%%%% pagerank algorithm start %%%%%%%%%%%%%%%%%%%%%%%%%%

// STALB scan no deletion and no out-of-order update
void HBALStore::pagerank_no_deletion_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total)
{
    //double stalb_incomming_total = 0.0;
    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
    {

        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!(*IsWTS))
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei->getWTsEdge() < min_read_versions)
            {
                *IsWTS = 1;
                *stalb_incomming_total += outgoing_contrib->start_[*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8)];
            }
        }
        else
        {
            *stalb_incomming_total += outgoing_contrib->start_[*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8)];
        }
        /// %%%%%%%%%%% end %%%%%%%%%%

    }
    // return stalb_incomming_total;
}

// STALB scan deletion and no out-of-order update scan
void HBALStore::pagerank_deletion_no_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total)
{
    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
    {
        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!*IsWTS)
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei->getWTsEdge() < min_read_versions)
            {
                *IsWTS = 1;
                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                if (!check_version(dst))
                {
                    *stalb_incomming_total += outgoing_contrib->start_[dst];
                }
                else
                {
                    if (!check_version_read(dst))
                    {
                        if (min_read_versions < ei->getInvldTime()->getWts())
                        {
                            *stalb_incomming_total += outgoing_contrib->start_[remove_version(dst)];
                        }
                        else
                        {
                            *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));

                        }
                    }

                }

            }
        }
        else
        {

            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
            if (!check_version(dst))
            {
                *stalb_incomming_total += outgoing_contrib->start_[dst];
            }
            else
            {
                if (!check_version_read(dst))
                {

                    // if ((readTable->readQuery[read_version] < latest_deletion))
                    //{

                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                    if (min_read_versions < ei->getInvldTime()->getWts())
                    {
                        *stalb_incomming_total += outgoing_contrib->start_[dst];
                    }
                    else
                    {

                        *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                    }

                }

            }

        }
        /// %%%%%%%%%%% end %%%%%%%%%%
    }
}

// STALB no deletion and out-of-order update
void HBALStore::pagerank_no_deletion_ooo_exist_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total)
{
    //double stalb_incomming_total = 0.0;
    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
    {

        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!(*IsWTS))
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei->getWTsEdge() < min_read_versions)
            {
                *IsWTS = 1;
                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                *stalb_incomming_total += outgoing_contrib->start_[dst];

                if (check_version_ooo(dst))
                {

                    // ooo updates exist
                    if (ei->getOutOfOrderUpdt()->getCount() > 1)
                    {

                        //   ArtPerIndex *art_read = ei->getOutOfOrderUpdt();
                        art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                        //%%%%%%%%%%%% adaptive radix tree scan start
                        // ei->outOfOrderUpdt.scanArtTree();
                        auto it = art_val->begin();
                        auto it_end = art_val->end();
                        for (int i = 0; it != it_end; ++i, ++it)
                        {
                            LeafNode *ln_cur = *it;
                            if (ln_cur->getWts() < min_read_versions)
                            {
                                if (ln_cur->getInvalEntry() == NULL)
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                                else
                                {
                                    if (ln_cur->getInvalEntry()->getWts() > min_read_versions)
                                    {
                                        *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                    }
                                }
                            }
                        }
                        //%%%%%%% end adaptive radix tree scan
                    }
                    else
                    {
                        if (ei->getOutOfOrderUpdt()->getCount() == 1)
                        {
                            art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                            //%%%%%%%%%%%% adaptive radix tree scan start
                            // ei->outOfOrderUpdt.scanArtTree();
                            auto it = art_val->begin();
                            LeafNode *ln_cur = *it;
                            if (ln_cur->getWts() < min_read_versions)
                            {
                                if (ln_cur->getInvalEntry() == NULL)
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                                else
                                {
                                    if (ln_cur->getInvalEntry()->getWts() > min_read_versions)
                                    {
                                        *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {

            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
            *stalb_incomming_total += outgoing_contrib->start_[*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8)];

            if (check_version_ooo(dst))
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

                // ooo updates exist
                // ooo updates exist
                if (ei->getOutOfOrderUpdt()->getCount() > 1)
                {

                    //   ArtPerIndex *art_read = ei->getOutOfOrderUpdt();
                    art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                    //%%%%%%%%%%%% adaptive radix tree scan start
                    // ei->outOfOrderUpdt.scanArtTree();
                    auto it = art_val->begin();
                    auto it_end = art_val->end();
                    for (int i = 0; it != it_end; ++i, ++it)
                    {
                        LeafNode *ln_cur = *it;
                        if (ln_cur->getWts() < min_read_versions)
                        {
                            if (ln_cur->getInvalEntry() == NULL)
                            {
                                *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                            }
                            else
                            {
                                if (ln_cur->getInvalEntry()->getWts() > min_read_versions)
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                            }
                        }
                    }
                    //%%%%%%% end adaptive radix tree scan
                }
                else
                {
                    if (ei->getOutOfOrderUpdt()->getCount() == 1)
                    {
                        art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                        //%%%%%%%%%%%% adaptive radix tree scan start
                        // ei->outOfOrderUpdt.scanArtTree();
                        auto it = art_val->begin();
                        LeafNode *ln_cur = *it;
                        if (ln_cur->getWts() < min_read_versions)
                        {
                            if (ln_cur->getInvalEntry() == NULL)
                            {
                                *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                            }
                            else
                            {
                                if (ln_cur->getInvalEntry()->getWts() > min_read_versions)
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                            }
                        }
                    }
                }
                // ooo updates part end

            }
        }
        /// %%%%%%%%%%% end %%%%%%%%%%

    }
    // return stalb_incomming_total;
}

// STALB deletion and out-of-order update scan
void HBALStore::pagerank_deletion_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total)
{
    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
    {
        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!*IsWTS)
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei->getWTsEdge() < min_read_versions)
            {
                *IsWTS = 1;
                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                if (!check_version(dst))
                {
                    *stalb_incomming_total += outgoing_contrib->start_[dst];
                    // ooo existence check
                    // ooo exist check

                }
                else
                {
                    if (!check_version_read(dst))
                    {
                        if (min_read_versions < ei->getInvldTime()->getWts())
                        {
                            *stalb_incomming_total += outgoing_contrib->start_[dst];
                        }
                        else
                        {
                            *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                        }
                    }
                }

                // check if dst contains ooo updates
                if (check_version_ooo(dst))
                {
                    // ooo updates exist
                    // ooo updates exist
                    if (ei->getOutOfOrderUpdt()->getCount() > 1)
                    {

                        //   ArtPerIndex *art_read = ei->getOutOfOrderUpdt();
                        art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                        //%%%%%%%%%%%% adaptive radix tree scan start
                        // ei->outOfOrderUpdt.scanArtTree();
                        auto it = art_val->begin();
                        auto it_end = art_val->end();
                        for (int i = 0; it != it_end; ++i, ++it)
                        {
                            LeafNode *ln_cur = *it;
                            if (ln_cur->getWts() < min_read_versions)
                            {
                                if (ln_cur->getInvalEntry() == NULL)
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                                else
                                {
                                    if (ln_cur->getInvalEntry()->getWts() > min_read_versions)
                                    {
                                        *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                    }
                                }
                            }
                        }
                        //%%%%%%% end adaptive radix tree scan
                    }
                    else
                    {
                        if (ei->getOutOfOrderUpdt()->getCount() == 1)
                        {
                            art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                            //%%%%%%%%%%%% adaptive radix tree scan start
                            // ei->outOfOrderUpdt.scanArtTree();
                            auto it = art_val->begin();
                            LeafNode *ln_cur = *it;
                            if (ln_cur->getWts() < min_read_versions)
                            {
                                if (ln_cur->getInvalEntry() == NULL)
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                                else
                                {
                                    if (ln_cur->getInvalEntry()->getWts() > min_read_versions)
                                    {
                                        *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                    }
                                }
                            }
                        }
                    }
                    // ooo updates part end
                }

            }
        }
        else
        {

            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
            if (!check_version(dst))
            {
                *stalb_incomming_total += outgoing_contrib->start_[dst];
            }
            else
            {
                if (!check_version_read(dst))
                {

                    // if ((readTable->readQuery[read_version] < latest_deletion))
                    //{

                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                    if (min_read_versions < ei->getInvldTime()->getWts())
                    {
                        *stalb_incomming_total += outgoing_contrib->start_[dst];
                    }
                    else
                    {

                        *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                    }

                }

            }

            // check if dst contains ooo updates
            if (check_version_ooo(dst))
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

                // ooo updates exist
                if (ei->getOutOfOrderUpdt()->getCount() > 1)
                {

                    //   ArtPerIndex *art_read = ei->getOutOfOrderUpdt();
                    art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                    //%%%%%%%%%%%% adaptive radix tree scan start
                    // ei->outOfOrderUpdt.scanArtTree();
                    auto it = art_val->begin();
                    auto it_end = art_val->end();
                    for (int i = 0; it != it_end; ++i, ++it)
                    {
                        LeafNode *ln_cur = *it;
                        if (ln_cur->getWts() < min_read_versions)
                        {
                            if (ln_cur->getInvalEntry() == NULL)
                            {
                                *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                            }
                            else
                            {
                                if (ln_cur->getInvalEntry()->getWts() > min_read_versions)
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                            }
                        }
                    }
                    //%%%%%%% end adaptive radix tree scan
                }
                else
                {
                    if (ei->getOutOfOrderUpdt()->getCount() == 1)
                    {
                        art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                        //%%%%%%%%%%%% adaptive radix tree scan start
                        // ei->outOfOrderUpdt.scanArtTree();
                        auto it = art_val->begin();
                        LeafNode *ln_cur = *it;
                        if (ln_cur->getWts() < min_read_versions)
                        {
                            if (ln_cur->getInvalEntry() == NULL)
                            {
                                *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                            }
                            else
                            {
                                if (ln_cur->getInvalEntry()->getWts() > min_read_versions)
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                            }
                        }
                    }
                }
                // ooo updates part end
            }

        }
        /// %%%%%%%%%%% end %%%%%%%%%%
    }
}



//implementation in gfe_driver

std::unique_ptr<double[]> HBALStore::do_pagerank(uint64_t max_vertex_id, uint64_t num_iterations, double damping_factor, int read_version, MemoryAllocator *me)
{

    u_int64_t num_vertices = get_vertex_count(0);
    // std::cout<<"Number of vertices "<<num_vertices<<std::endl;
    max_vertex_id = get_high_water_mark();

    const double init_score = 1.0 / num_vertices;
    const double base_score = (1.0 - damping_factor) / num_vertices;
    std::unique_ptr<double[]> ptr_scores{new double[max_vertex_id]()}; // avoid memory leaks
    std::unique_ptr<uint64_t[]> ptr_degrees{new uint64_t[max_vertex_id]()}; // avoid memory leaks
    double *scores = ptr_scores.get();
    uint64_t *__restrict degrees = ptr_degrees.get();
    // std::cout<<"hello 1"<<std::endl;
    //  PerSourceVertexIndr ps;
    //std::cout<<"start degree"<<std::endl;

#pragma omp parallel for
    for (uint64_t v = 0; v < max_vertex_id; v++)
    {
        scores[v] = init_score;
        index[v].perVertexLock.lock(); ///////
        degrees[v] = perSourceGetDegree(v, read_version, me);//index[v].degree; //perSourceGetDegree(v, read_version);
        index[v].perVertexLock.unlock();
    }
    //std::cout<<"start min version"<<std::endl;

    min_read_degree_version = MaxValue;
    // std::cout<<"end min version"<<std::endl;
    gapbs::pvector<double> outgoing_contrib(max_vertex_id, 0.0);

    // pagerank iterations
    // readTable->readQuery[read_version]
    timestamp_t min_read_versions = min_read_version;
    // pagerank iterations
    if (min_read_versions == getMinReadVersion())
    {
        for (uint64_t iteration = 0; iteration < num_iterations; iteration++)
        {
            // std::cout<<"iteration # "<<iteration<<std::endl;
            double dangling_sum = 0.0;

            // for each node, precompute its contribution to all of its outgoing neighbours and, if it's a sink,
            // add its rank to the `dangling sum' (to be added to all nodes).

#pragma omp parallel for reduction(+:dangling_sum)
            for (uint64_t v = 0; v < max_vertex_id; v++)
            {
                uint64_t out_degree = degrees[v];
                if (out_degree == 0)
                { // this is a sink
                    dangling_sum += scores[v];
                }
                else
                {
                    outgoing_contrib[v] = scores[v] / out_degree;
                    //  std::cout<<outgoing_contrib[v]<<" "<<outgoing_contrib.start_[v]<<std::endl;
                }
            }

            dangling_sum /= num_vertices;

            // compute the new score for each node in the graph
#pragma omp parallel for schedule(dynamic, 64)
            for (uint64_t v = 0; v < max_vertex_id; v++)
            {

                //if(degrees[v] == std::numeric_limits<uint64_t>::max()){ continue; } // the vertex does not exist
                if (degrees[v] == 0)
                {
                    continue;
                }
                index[v].indirectionLock.lock();

                double incoming_total = 0;

                // per source STAL access

                // PerSourceVertexIndr *ps_stal = getSourceVertexPointer(v);//transaction.get_edges(u,   0); // fixme: incoming edges for directed graphs


                //
                uint8_t IsWTS = 0;
                // *IsWTS = 0;
                if (index[v].edgePtr != 0)
                {

                    if (!(index[v].edgePtr & 0x1))
                    {
                        PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[v].getEdgePtr());

                        int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                        bool flag_l1_delete = false;
                        flag_l1_delete = ind_ptr->isDeletionBlock();

                        if (!flag_l1_delete)
                        {
                            // deletion bit per source-vertex not active direct read L2 vector
                            for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                            {

                                stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                uint8_t is_delete_l2_flag = 0;
                                uint8_t is_ooo_l2_flag = 0;

                                is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                is_ooo_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 4);

                                if (!is_delete_l2_flag && !is_ooo_l2_flag)
                                {
                                    // no deletion and ooo entries
                                    pagerank_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);

                                }
                                else if (is_delete_l2_flag && !is_ooo_l2_flag)
                                {
                                    pagerank_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                }
                                else if (!is_delete_l2_flag && is_ooo_l2_flag)
                                {
                                    pagerank_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                    //double stalb_incomming_total = 0.0;

                                    //// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% out of order update exist


                                }
                                else
                                {
                                    pagerank_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                }


                            }
                        }
                        else
                        {
                            /// %%%%%%%%%%%%%%%%% if indirection array have deletions STAL %%%%%%%%%%%%

                            for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                            {
                                if (reinterpret_cast<vertex_t>(ind_ptr->getperVertexBlockArr()[ind_l1]) != FLAG_EMPTY_SLOT)
                                {
                                    stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                    int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                    uint8_t is_delete_l2_flag = 0;
                                    uint8_t is_ooo_l2_flag = 0;

                                    is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                    is_ooo_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 4);


                                    if (!is_delete_l2_flag && !is_ooo_l2_flag)
                                    {
                                        // no deletion and ooo entries
                                        pagerank_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);

                                    }
                                    else if (is_delete_l2_flag && !is_ooo_l2_flag)
                                    {
                                        pagerank_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                    }
                                    else if (!is_delete_l2_flag && is_ooo_l2_flag)
                                    {
                                        pagerank_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                    }
                                    else
                                    {
                                        pagerank_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                    }
                                }

                            }

                            /// %%%%%%%%%%%%%%%%% end %%%%%%%%%%%%%%%%%%%%%%
                        }
                    }
                    else
                    {

                        stalb_type *eb_k = static_cast<stalb_type *>(index[v].getEdgePtr());

                        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                        uint8_t is_delete_l2_flag = 0;
                        uint8_t is_ooo_l2_flag = 0;

                        is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                        is_ooo_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 4);


                        if (!is_delete_l2_flag && !is_ooo_l2_flag)
                        {
                            // no deletion and ooo entries
                            pagerank_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);

                        }
                        else if (is_delete_l2_flag && !is_ooo_l2_flag)
                        {
                            pagerank_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                        }
                        else if (!is_delete_l2_flag && is_ooo_l2_flag)
                        {
                            pagerank_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                            //// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% out of order update exist

                        }
                        else
                        {
                            pagerank_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                        }
                        // #### per source edge reading end
                    }
                }

                // STAL end

                // update the score
                // std::cout<<"incoming_total "<<incoming_total<<std::endl;
                scores[v] = base_score + damping_factor * (incoming_total + dangling_sum);
                index[v].indirectionLock.unlock();

            }

        }
    }
    // std::cout<<"total deletion "<<total_deletion<<" "<<false_deletion<<std::endl;
    //std::cout<<"hello 2"<<std::endl;
    //   std::cout<<"iteration end"<<std::endl;

    return ptr_scores;
}

// %%%%%%%%%%%%%%%%%%% pagerank algorithm end %%%%%%%%%%%%%%%%%%%%%%%%%%


// wcc
std::unique_ptr<uint64_t[]> HBALStore::do_wcc(uint64_t max_vertex_id)
{
    // init
    std::unique_ptr<uint64_t[]> ptr_components{new uint64_t[max_vertex_id]};
    uint64_t *comp = ptr_components.get();

#pragma omp parallel for
    for (uint64_t n = 0; n < max_vertex_id; n++)
    {
        comp[n] = n;
    }
    bool change = true;
    while (change)
    {
        change = false;

#pragma omp parallel for schedule(dynamic, 64)
        for (uint64_t u = 0; u < max_vertex_id; u++)
        {
            if (!(index[u].edgePtr & 0x1))
            {

                PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[u].getEdgePtr());

                bool flag_l1_delete = false;
                flag_l1_delete = ind_ptr->isDeletionBlock();

                if (!flag_l1_delete)
                {
                    // deletion bit per source-vertex not active direct read L2 vector
                    for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < (u_int64_t) 1 << ind_ptr->getBlockLen(); ind_l1++)
                    {

                        stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];

                        // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                        uint8_t is_delete_l2_flag = 0;
                        is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                        if (!is_delete_l2_flag)
                        {

                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                            {
                                // uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);

                                uint64_t comp_u = comp[u];
                                uint64_t comp_v = comp[v];
                                if (comp_u != comp_v)
                                {
                                    // Hooking condition so lower component ID wins independent of direction
                                    uint64_t high_comp = std::max(comp_u, comp_v);
                                    uint64_t low_comp = std::min(comp_u, comp_v);
                                    if (high_comp == comp[high_comp])
                                    {
                                        change = true;
                                        //   COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
                                        comp[high_comp] = low_comp;
                                    }
                                }
                            }

                        }
                        else
                        {
                            /* for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++) {
                                 if (eb_k->getEdgeUpdate()[ind_l2] != NULL) {
                                     if (eb_k->getEdgeUpdate()[ind_l2]->getInvldTime() != NULL)
                                     {
                                         uint64_t v = eb_k->getDestIdArr()[ind_l2];

                                         uint64_t comp_u = comp[u];
                                         uint64_t comp_v = comp[v];
                                         if (comp_u != comp_v)
                                         {
                                             // Hooking condition so lower component ID wins independent of direction
                                             uint64_t high_comp = std::max(comp_u, comp_v);
                                             uint64_t low_comp = std::min(comp_u, comp_v);
                                             if (high_comp == comp[high_comp])
                                             {
                                                 change = true;
                                                 //     COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
                                                 comp[high_comp] = low_comp;
                                             }
                                         }
                                         //incoming_total += outgoing_contrib[dst];
                                     }
                                 }
                             }*/
                        }
                    }
                    //
                }
                else
                {

// deletion bit per source-vertex not active direct read L2 vector (when deletion in indr_array)
                    /* for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                     {
                         stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];
                         if (eb_k != NULL)
                         {
                             int l2_len = (u_int64_t) 1 << eb_k->getBlockSize();
                             bool is_delete_l2_flag = false;
                             is_delete_l2_flag = eb_k->getIsDeletion();
                             if (!is_delete_l2_flag)
                             {

                                 for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++)
                                 {
                                     uint64_t v = eb_k->getDestIdArr()[ind_l2];

                                     uint64_t comp_u = comp[u];
                                     uint64_t comp_v = comp[v];
                                     if (comp_u != comp_v)
                                     {
                                         // Hooking condition so lower component ID wins independent of direction
                                         uint64_t high_comp = std::max(comp_u, comp_v);
                                         uint64_t low_comp = std::min(comp_u, comp_v);
                                         if (high_comp == comp[high_comp])
                                         {
                                             change = true;
                                             //  COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
                                             comp[high_comp] = low_comp;
                                         }
                                     }
                                 }

                             }
                             else
                             {*/
                    /*  for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++) {
                          if (eb_k->getEdgeUpdate()[ind_l2] != NULL) {
                              if (eb_k->getEdgeUpdate()[ind_l2]->getInvldTime() != NULL)
                              {
                                  uint64_t v = eb_k->getDestIdArr()[ind_l2];

                                  uint64_t comp_u = comp[u];
                                  uint64_t comp_v = comp[v];
                                  if (comp_u != comp_v)
                                  {
                                      // Hooking condition so lower component ID wins independent of direction
                                      uint64_t high_comp = std::max(comp_u, comp_v);
                                      uint64_t low_comp = std::min(comp_u, comp_v);
                                      if (high_comp == comp[high_comp])
                                      {
                                          change = true;
                                          //    COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
                                          comp[high_comp] = low_comp;
                                      }
                                  }
                              }
                          }
                      }*/
                    /*    }
                    }
                }*/
                }
            }
            else
            {
                // directly read from edge block
                stalb_type *eb_k = static_cast<stalb_type *>(index[u].getEdgePtr());

                // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                uint8_t is_delete_l2_flag = 0;
                is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                if (!is_delete_l2_flag)
                {

                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                    {
                        // uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                        uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);

                        uint64_t comp_u = comp[u];
                        uint64_t comp_v = comp[v];
                        if (comp_u != comp_v)
                        {
                            // Hooking condition so lower component ID wins independent of direction
                            uint64_t high_comp = std::max(comp_u, comp_v);
                            uint64_t low_comp = std::min(comp_u, comp_v);
                            if (high_comp == comp[high_comp])
                            {
                                change = true;
                                // COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
                                comp[high_comp] = low_comp;
                            }
                        }
                    }

                }
                else
                {
                    /* for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++)
                     {
                         if (eb_k->getEdgeUpdate()[ind_l2] != NULL)
                         {
                             if (eb_k->getEdgeUpdate()[ind_l2]->getInvldTime() != NULL)
                             {
                                 uint64_t v = eb_k->getDestIdArr()[ind_l2];

                                 uint64_t comp_u = comp[u];
                                 uint64_t comp_v = comp[v];
                                 if (comp_u != comp_v)
                                 {
                                     // Hooking condition so lower component ID wins independent of direction
                                     uint64_t high_comp = std::max(comp_u, comp_v);
                                     uint64_t low_comp = std::min(comp_u, comp_v);
                                     if (high_comp == comp[high_comp])
                                     {
                                         change = true;
                                         //     COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
                                         comp[high_comp] = low_comp;
                                     }
                                 }
                             }
                         }
                     }*/
                }
            }
//
// STAL end
//
// STAL end

//

/* while(iterator.valid()){
    uint64_t v = iterator.dst_id();

    uint64_t comp_u = comp[u];
    uint64_t comp_v = comp[v];
    if (comp_u != comp_v) {
        // Hooking condition so lower component ID wins independent of direction
        uint64_t high_comp = std::max(comp_u, comp_v);
        uint64_t low_comp = std::min(comp_u, comp_v);
        if (high_comp == comp[high_comp]) {
            change = true;
            COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
            comp[high_comp] = low_comp;
        }
    }

    iterator.next();
}*/
        }

#pragma omp parallel for schedule(dynamic, 64)
        for (uint64_t n = 0; n < max_vertex_id; n++)
        {
            // if(comp[n] == std::numeric_limits<uint64_t>::max()) continue; // the vertex does not exist

            while (comp[n] != comp[comp[n]])
            {
                comp[n] = comp[comp[n]];
            }
        }

        //  COUT_DEBUG_WCC("change: " << change);
    }

    return ptr_components;
}

// cdlp
std::unique_ptr<uint64_t[]> HBALStore::do_cdlp(uint64_t max_vertex_id, bool is_graph_directed, uint64_t max_iterations)
{

    std::unique_ptr<uint64_t[]> ptr_labels0{new uint64_t[max_vertex_id]};
    std::unique_ptr<uint64_t[]> ptr_labels1{new uint64_t[max_vertex_id]};
    uint64_t *labels0 = ptr_labels0.get(); // current labels
    uint64_t *labels1 = ptr_labels1.get(); // labels for the next iteration

    // initialisation
#pragma omp parallel for
    for (uint64_t v = 0; v < max_vertex_id; v++)
    {
        labels0[v] = v;
    }

    // algorithm pass
    bool change = true;
    uint64_t current_iteration = 0;
    while (current_iteration < max_iterations && change)
    {
        change = false; // reset the flag

#pragma omp parallel for schedule(dynamic, 64) shared(change)
        for (uint64_t v = 0; v < max_vertex_id; v++)
        {

            std::unordered_map<uint64_t, uint64_t> histogram;

            /// get dest node entries start


            //
            if (!(index[v].edgePtr & 0x1))
            {

                PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[v].getEdgePtr());

                int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                bool flag_l1_delete = false;
                flag_l1_delete = ind_ptr->isDeletionBlock();

                if (!flag_l1_delete)
                {
                    // deletion bit per source-vertex not active direct read L2 vector
                    for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                    {

                        stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];

                        // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                        uint8_t is_delete_l2_flag = 0;
                        is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                        if (!is_delete_l2_flag)
                        {

                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                            {
                                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                histogram[labels0[dst]]++;
                            }

                        }
                        else
                        {
                            /* for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++)
                             {
                                 if (eb_k->getEdgeUpdate()[ind_l2] != NULL)
                                 {
                                     if (eb_k->getEdgeUpdate()[ind_l2]->getInvldTime() != NULL)
                                     {
                                         uint64_t dst = eb_k->getDestIdArr()[ind_l2];
                                         histogram[labels0[dst]]++;
                                     }
                                 }
                             }*/
                        }
                    }
                }
                else
                {
                    /*
                        // deletion bit per source-vertex not active direct read L2 vector (when deletion in indr_array)
                        for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                        {
                            EdgeBlock *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];
                            if (eb_k != NULL)
                            {
                                int l2_len = (u_int64_t) 1 << eb_k->getBlockSize();
                                bool is_delete_l2_flag = false;
                                is_delete_l2_flag = eb_k->getIsDeletion();
                                if (!is_delete_l2_flag)
                                {

                                    for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++)
                                    {
                                        uint64_t dst =  eb_k->getDestIdArr()[ind_l2];
                                        histogram[labels0[dst]]++;
                                    }

                                } else {
                                    for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++)
                                    {
                                        if (eb_k->getEdgeUpdate()[ind_l2] != NULL)
                                        {
                                            if (eb_k->getEdgeUpdate()[ind_l2]->getInvldTime() != NULL)
                                            {
                                                uint64_t dst =  eb_k->getDestIdArr()[ind_l2];
                                                histogram[labels0[dst]]++;
                                            }
                                        }
                                    }
                                }
                            }
                        }*/
                }
            }
            else
            {
                // directly read from edge block
                stalb_type *eb_k = static_cast<stalb_type *>(index[v].getEdgePtr());

                // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                uint8_t is_delete_l2_flag = 0;
                is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                if (!is_delete_l2_flag)
                {

                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                    {
                        uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                        histogram[labels0[dst]]++;
                    }

                }
                else
                {
                    /*for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++)
                    {
                        if (eb_k->getEdgeUpdate()[ind_l2] != NULL)
                        {
                            if (eb_k->getEdgeUpdate()[ind_l2]->getInvldTime() != NULL)
                            {
                                uint64_t dst =  eb_k->getDestIdArr()[ind_l2];
                                histogram[labels0[dst]]++;
                            }
                        }
                    }*/
                }
            }

            // dest node entry end

            uint64_t label_max = std::numeric_limits<int64_t>::max();
            uint64_t count_max = 0;
            for (const auto pair: histogram)
            {
                if (pair.second > count_max || (pair.second == count_max && pair.first < label_max))
                {
                    label_max = pair.first;
                    count_max = pair.second;
                }
            }

            labels1[v] = label_max;
            change |= (labels0[v] != labels1[v]);
        }

        std::swap(labels0, labels1); // next iteration
        current_iteration++;
    }

    if (labels0 == ptr_labels0.get())
    {
        return ptr_labels0;
    }
    else
    {
        return ptr_labels1;
    }
}

// LCC end

// sssp algorithm

std::vector<WeightT> HBALStore::do_sssp(uint64_t num_edges, uint64_t max_vertex_id, uint64_t source, double delta, int read_version, MemoryAllocator *me)
{
    // Init
    std::vector<WeightT> dist(max_vertex_id, std::numeric_limits<WeightT>::infinity());
    dist[source] = 0;
    std::unique_ptr<uint64_t[]> ptr_edges_count_per_vertex{new uint64_t[max_vertex_id]()}; // avoid memory leaks
    uint64_t *__restrict edges_count_per_vertex = ptr_edges_count_per_vertex.get();
    for (uint64_t v = 0; v < max_vertex_id; v++)
    {
        //scores[v] = init_score;
        index[v].perVertexLock.lock(); ///////
        edges_count_per_vertex[v] = perSourceGetDegree(v, 0, me);//index[v].degree; //perSourceGetDegree(v, read_version);
        index[v].perVertexLock.unlock();
    }
    num_edges = 0; // = CalculateNumberOfEdges();

    // #pragma omp parallel for reduction(+:num_edges)
    for (uint64_t n = 0; n < max_vertex_id; n++)
    {
        num_edges += (edges_count_per_vertex[n]);
    }

    min_read_degree_version = MaxValue;
    // std::cout<<"end min version"<<std::endl;

    // pagerank iterations
    // readTable->readQuery[read_version]
    timestamp_t min_read_versions = min_read_version;
    gapbs::pvector<NodeID> frontier(num_edges);
    // two element arrays for double buffering curr=iter&1, next=(iter+1)&1
    size_t shared_indexes[2] = {0, kMaxBin};
    size_t frontier_tails[2] = {1, 0};
    frontier[0] = source;

#pragma omp parallel
    {
        std::vector<std::vector<NodeID> > local_bins(0);
        size_t iter = 0;

        while (shared_indexes[iter & 1] != kMaxBin)
        {
            size_t &curr_bin_index = shared_indexes[iter & 1];
            size_t &next_bin_index = shared_indexes[(iter + 1) & 1];
            size_t &curr_frontier_tail = frontier_tails[iter & 1];
            size_t &next_frontier_tail = frontier_tails[(iter + 1) & 1];
#pragma omp for nowait schedule(dynamic, 64)
            for (size_t i = 0; i < curr_frontier_tail; i++)
            {
                NodeID u = frontier[i];
                if (dist[u] >= delta * static_cast<WeightT>(curr_bin_index))
                {

                    // stal start
                    //    PerSourceVertexIndr *ps = ds->getSourceVertexPointer(u);//transaction.get_edges(u,   0); // fixme: incoming edges for directed graphs

                    //
                    bool IsWTS = false;
                    if (!(index[u].edgePtr & 0x1))
                    {

                        PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[u].getEdgePtr());

                        int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                        bool flag_l1_delete = false;
                        flag_l1_delete = ind_ptr->isDeletionBlock();

                        if (!flag_l1_delete)
                        {
                            // deletion bit per source-vertex not active direct read L2 vector
                            for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                            {

                                stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];

                                // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                                int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                uint8_t is_delete_l2_flag = 0;
                                is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                if (!is_delete_l2_flag)
                                {

                                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                    {

                                        //////////
                                        if (!IsWTS)
                                        {
                                            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                            if (ei->getWTsEdge() < min_read_versions)
                                            {
                                                IsWTS = true;
                                                uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                                // get dest
                                                double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                double w = property[ind_l2];
                                                WeightT old_dist = dist[v];
                                                WeightT new_dist = dist[u] + w;
                                                if (new_dist < old_dist)
                                                {
                                                    bool changed_dist = true;
                                                    while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                    {
                                                        old_dist = dist[v];
                                                        if (old_dist <= new_dist)
                                                        {
                                                            changed_dist = false;
                                                            break;
                                                        }
                                                    }
                                                    if (changed_dist)
                                                    {
                                                        size_t dest_bin = new_dist / delta;
                                                        if (dest_bin >= local_bins.size())
                                                        {
                                                            local_bins.resize(dest_bin + 1);
                                                        }
                                                        local_bins[dest_bin].push_back(v);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                            // get dest
                                            double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                            double w = property[ind_l2];
                                            WeightT old_dist = dist[v];
                                            WeightT new_dist = dist[u] + w;
                                            if (new_dist < old_dist)
                                            {
                                                bool changed_dist = true;
                                                while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                {
                                                    old_dist = dist[v];
                                                    if (old_dist <= new_dist)
                                                    {
                                                        changed_dist = false;
                                                        break;
                                                    }
                                                }
                                                if (changed_dist)
                                                {
                                                    size_t dest_bin = new_dist / delta;
                                                    if (dest_bin >= local_bins.size())
                                                    {
                                                        local_bins.resize(dest_bin + 1);
                                                    }
                                                    local_bins[dest_bin].push_back(v);
                                                }
                                            }

                                        }


                                        /////////

                                        // dest end
                                        // incoming_total += outgoing_contrib[dst];
                                    }

                                }
                                else
                                {
                                    // %%%%%%%%%%%%%% if stalb contains deletion start %%%%%%%%

                                    //get the latest deletion wts
                                    // timestamp_t latest_deletion = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1<< *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8))[0]; //
                                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                    {

                                        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                        if (!IsWTS)
                                        {
                                            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                            if (ei->getWTsEdge() < min_read_versions)
                                            {
                                                IsWTS = true;
                                                uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                                if (!check_version(v))
                                                {
                                                    // incoming_total += outgoing_contrib[dst];
                                                    double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                    double w = property[ind_l2];
                                                    WeightT old_dist = dist[v];
                                                    WeightT new_dist = dist[u] + w;
                                                    if (new_dist < old_dist)
                                                    {
                                                        bool changed_dist = true;
                                                        while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                        {
                                                            old_dist = dist[v];
                                                            if (old_dist <= new_dist)
                                                            {
                                                                changed_dist = false;
                                                                break;
                                                            }
                                                        }
                                                        if (changed_dist)
                                                        {
                                                            size_t dest_bin = new_dist / delta;
                                                            if (dest_bin >= local_bins.size())
                                                            {
                                                                local_bins.resize(dest_bin + 1);
                                                            }
                                                            local_bins[dest_bin].push_back(v);
                                                        }
                                                    }

                                                }
                                                else
                                                {
                                                    if (!check_version_read(v))
                                                    {

                                                        // if ((readTable->readQuery[read_version] < latest_deletion))
                                                        //{
                                                        if (min_read_versions < ei->getInvldTime()->getWts())
                                                        {

                                                            double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                            double w = property[ind_l2];
                                                            WeightT old_dist = dist[v];
                                                            WeightT new_dist = dist[u] + w;
                                                            if (new_dist < old_dist)
                                                            {
                                                                bool changed_dist = true;
                                                                while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                                {
                                                                    old_dist = dist[v];
                                                                    if (old_dist <= new_dist)
                                                                    {
                                                                        changed_dist = false;
                                                                        break;
                                                                    }
                                                                }
                                                                if (changed_dist)
                                                                {
                                                                    size_t dest_bin = new_dist / delta;
                                                                    if (dest_bin >= local_bins.size())
                                                                    {
                                                                        local_bins.resize(dest_bin + 1);
                                                                    }
                                                                    local_bins[dest_bin].push_back(v);
                                                                }
                                                            }


                                                            //incoming_total += outgoing_contrib[dst];
                                                        }
                                                        else
                                                        {
                                                            //   if(readTable->readQuery[read_version] == getMinReadVersion())
                                                            //  {
                                                            *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                            // }
                                                        }
                                                        // }
                                                    }
                                                    //
                                                }

                                            }
                                        }
                                        else
                                        {

                                            uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                            if (!check_version(v))
                                            {
                                                double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                double w = property[ind_l2];
                                                WeightT old_dist = dist[v];
                                                WeightT new_dist = dist[u] + w;
                                                if (new_dist < old_dist)
                                                {
                                                    bool changed_dist = true;
                                                    while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                    {
                                                        old_dist = dist[v];
                                                        if (old_dist <= new_dist)
                                                        {
                                                            changed_dist = false;
                                                            break;
                                                        }
                                                    }
                                                    if (changed_dist)
                                                    {
                                                        size_t dest_bin = new_dist / delta;
                                                        if (dest_bin >= local_bins.size())
                                                        {
                                                            local_bins.resize(dest_bin + 1);
                                                        }
                                                        local_bins[dest_bin].push_back(v);
                                                    }
                                                }

                                            }
                                            else
                                            {
                                                if (!check_version_read(v))
                                                {

                                                    // if ((readTable->readQuery[read_version] < latest_deletion))
                                                    //{

                                                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                    if (min_read_versions < ei->getInvldTime()->getWts())
                                                    {
                                                        double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                        double w = property[ind_l2];
                                                        WeightT old_dist = dist[v];
                                                        WeightT new_dist = dist[u] + w;
                                                        if (new_dist < old_dist)
                                                        {
                                                            bool changed_dist = true;
                                                            while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                            {
                                                                old_dist = dist[v];
                                                                if (old_dist <= new_dist)
                                                                {
                                                                    changed_dist = false;
                                                                    break;
                                                                }
                                                            }
                                                            if (changed_dist)
                                                            {
                                                                size_t dest_bin = new_dist / delta;
                                                                if (dest_bin >= local_bins.size())
                                                                {
                                                                    local_bins.resize(dest_bin + 1);
                                                                }
                                                                local_bins[dest_bin].push_back(v);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        // if(readTable->readQuery[read_version] == getMinReadVersion())
                                                        // {
                                                        *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                        // }
                                                        //

                                                    }
                                                    // }
                                                }

                                            }

                                        }
                                        /// %%%%%%%%%%% end %%%%%%%%%%
                                    }



                                    // %%%%%%%%%%%%%% if stalb contains deletion end %%%%%%%%

                                }
                            }
                        }
                        else
                        {

                            ////////////////
                            for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                            {
                                if (ind_ptr->getperVertexBlockArr()[ind_l1] != ((stalb_type *) FLAG_EMPTY_SLOT))
                                {
                                    stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];

                                    // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                                    int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                    uint8_t is_delete_l2_flag = 0;
                                    is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                    if (!is_delete_l2_flag)
                                    {

                                        for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                        {

                                            //////////
                                            if (!IsWTS)
                                            {
                                                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                if (ei->getWTsEdge() < min_read_versions)
                                                {
                                                    IsWTS = true;
                                                    uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                                    // get dest
                                                    double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                    double w = property[ind_l2];
                                                    WeightT old_dist = dist[v];
                                                    WeightT new_dist = dist[u] + w;
                                                    if (new_dist < old_dist)
                                                    {
                                                        bool changed_dist = true;
                                                        while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                        {
                                                            old_dist = dist[v];
                                                            if (old_dist <= new_dist)
                                                            {
                                                                changed_dist = false;
                                                                break;
                                                            }
                                                        }
                                                        if (changed_dist)
                                                        {
                                                            size_t dest_bin = new_dist / delta;
                                                            if (dest_bin >= local_bins.size())
                                                            {
                                                                local_bins.resize(dest_bin + 1);
                                                            }
                                                            local_bins[dest_bin].push_back(v);
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                                // get dest
                                                double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                double w = property[ind_l2];
                                                WeightT old_dist = dist[v];
                                                WeightT new_dist = dist[u] + w;
                                                if (new_dist < old_dist)
                                                {
                                                    bool changed_dist = true;
                                                    while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                    {
                                                        old_dist = dist[v];
                                                        if (old_dist <= new_dist)
                                                        {
                                                            changed_dist = false;
                                                            break;
                                                        }
                                                    }
                                                    if (changed_dist)
                                                    {
                                                        size_t dest_bin = new_dist / delta;
                                                        if (dest_bin >= local_bins.size())
                                                        {
                                                            local_bins.resize(dest_bin + 1);
                                                        }
                                                        local_bins[dest_bin].push_back(v);
                                                    }
                                                }

                                            }


                                            /////////

                                            // dest end
                                            // incoming_total += outgoing_contrib[dst];
                                        }

                                    }
                                    else
                                    {
                                        // %%%%%%%%%%%%%% if stalb contains deletion start %%%%%%%%

                                        //get the latest deletion wts
                                        // timestamp_t latest_deletion = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1<< *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8))[0]; //
                                        for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                        {

                                            /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                            if (!IsWTS)
                                            {
                                                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                if (ei->getWTsEdge() < min_read_versions)
                                                {
                                                    IsWTS = true;
                                                    uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                                    if (!check_version(v))
                                                    {
                                                        // incoming_total += outgoing_contrib[dst];
                                                        double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                        double w = property[ind_l2];
                                                        WeightT old_dist = dist[v];
                                                        WeightT new_dist = dist[u] + w;
                                                        if (new_dist < old_dist)
                                                        {
                                                            bool changed_dist = true;
                                                            while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                            {
                                                                old_dist = dist[v];
                                                                if (old_dist <= new_dist)
                                                                {
                                                                    changed_dist = false;
                                                                    break;
                                                                }
                                                            }
                                                            if (changed_dist)
                                                            {
                                                                size_t dest_bin = new_dist / delta;
                                                                if (dest_bin >= local_bins.size())
                                                                {
                                                                    local_bins.resize(dest_bin + 1);
                                                                }
                                                                local_bins[dest_bin].push_back(v);
                                                            }
                                                        }

                                                    }
                                                    else
                                                    {
                                                        if (!check_version_read(v))
                                                        {

                                                            // if ((readTable->readQuery[read_version] < latest_deletion))
                                                            //{
                                                            if (min_read_versions < ei->getInvldTime()->getWts())
                                                            {

                                                                double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                                double w = property[ind_l2];
                                                                WeightT old_dist = dist[v];
                                                                WeightT new_dist = dist[u] + w;
                                                                if (new_dist < old_dist)
                                                                {
                                                                    bool changed_dist = true;
                                                                    while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                                    {
                                                                        old_dist = dist[v];
                                                                        if (old_dist <= new_dist)
                                                                        {
                                                                            changed_dist = false;
                                                                            break;
                                                                        }
                                                                    }
                                                                    if (changed_dist)
                                                                    {
                                                                        size_t dest_bin = new_dist / delta;
                                                                        if (dest_bin >= local_bins.size())
                                                                        {
                                                                            local_bins.resize(dest_bin + 1);
                                                                        }
                                                                        local_bins[dest_bin].push_back(v);
                                                                    }
                                                                }


                                                                //incoming_total += outgoing_contrib[dst];
                                                            }
                                                            else
                                                            {
                                                                //   if(readTable->readQuery[read_version] == getMinReadVersion())
                                                                //  {
                                                                *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                                // }
                                                            }
                                                            // }
                                                        }
                                                        //
                                                    }

                                                }
                                            }
                                            else
                                            {

                                                uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                                if (!check_version(v))
                                                {
                                                    double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                    double w = property[ind_l2];
                                                    WeightT old_dist = dist[v];
                                                    WeightT new_dist = dist[u] + w;
                                                    if (new_dist < old_dist)
                                                    {
                                                        bool changed_dist = true;
                                                        while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                        {
                                                            old_dist = dist[v];
                                                            if (old_dist <= new_dist)
                                                            {
                                                                changed_dist = false;
                                                                break;
                                                            }
                                                        }
                                                        if (changed_dist)
                                                        {
                                                            size_t dest_bin = new_dist / delta;
                                                            if (dest_bin >= local_bins.size())
                                                            {
                                                                local_bins.resize(dest_bin + 1);
                                                            }
                                                            local_bins[dest_bin].push_back(v);
                                                        }
                                                    }

                                                }
                                                else
                                                {
                                                    if (!check_version_read(v))
                                                    {

                                                        // if ((readTable->readQuery[read_version] < latest_deletion))
                                                        //{

                                                        EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                        if (min_read_versions < ei->getInvldTime()->getWts())
                                                        {
                                                            double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                            double w = property[ind_l2];
                                                            WeightT old_dist = dist[v];
                                                            WeightT new_dist = dist[u] + w;
                                                            if (new_dist < old_dist)
                                                            {
                                                                bool changed_dist = true;
                                                                while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                                {
                                                                    old_dist = dist[v];
                                                                    if (old_dist <= new_dist)
                                                                    {
                                                                        changed_dist = false;
                                                                        break;
                                                                    }
                                                                }
                                                                if (changed_dist)
                                                                {
                                                                    size_t dest_bin = new_dist / delta;
                                                                    if (dest_bin >= local_bins.size())
                                                                    {
                                                                        local_bins.resize(dest_bin + 1);
                                                                    }
                                                                    local_bins[dest_bin].push_back(v);
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            // if(readTable->readQuery[read_version] == getMinReadVersion())
                                                            // {
                                                            *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                            // }
                                                            //

                                                        }
                                                        // }
                                                    }

                                                }

                                            }
                                            /// %%%%%%%%%%% end %%%%%%%%%%
                                        }



                                        // %%%%%%%%%%%%%% if stalb contains deletion end %%%%%%%%

                                    }


                                }


                                //////////////


                            }
                        }
                    }
                    else
                    {
                        // directly read from edge block
                        stalb_type *eb_k = static_cast<stalb_type *>(index[u].getEdgePtr());

                        // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                        uint8_t is_delete_l2_flag = 0;
                        is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                        if (!is_delete_l2_flag)
                        {

                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                            {

                                uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                // get dest
                                double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                double w = property[ind_l2];
                                WeightT old_dist = dist[v];
                                WeightT new_dist = dist[u] + w;
                                if (new_dist < old_dist)
                                {
                                    bool changed_dist = true;
                                    while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                    {
                                        old_dist = dist[v];
                                        if (old_dist <= new_dist)
                                        {
                                            changed_dist = false;
                                            break;
                                        }
                                    }
                                    if (changed_dist)
                                    {
                                        size_t dest_bin = new_dist / delta;
                                        if (dest_bin >= local_bins.size())
                                        {
                                            local_bins.resize(dest_bin + 1);
                                        }
                                        local_bins[dest_bin].push_back(v);
                                    }
                                }
                                // get dest end
                                // incoming_total += outgoing_contrib[dst];
                            }

                        }
                        else
                        {
                            /*   for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++) {
                                   if (eb_k->getEdgeUpdate()[ind_l2] != NULL) {
                                       if (eb_k->getEdgeUpdate()[ind_l2]->getInvldTime() != NULL) {

                                           // get dest
                                           uint64_t v   =  eb_k->getDestIdArr()[ind_l2];
                                           double w     =  eb_k->getpropertyIdArr()[ind_l2];
                                           WeightT old_dist = dist[v];
                                           WeightT new_dist = dist[u] + w;
                                           if (new_dist < old_dist)
                                           {
                                               bool changed_dist = true;
                                               while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                               {
                                                   old_dist = dist[v];
                                                   if (old_dist <= new_dist)
                                                   {
                                                       changed_dist = false;
                                                       break;
                                                   }
                                               }
                                               if (changed_dist)
                                               {
                                                   size_t dest_bin = new_dist/delta;
                                                   if (dest_bin >= local_bins.size())
                                                   {
                                                       local_bins.resize(dest_bin+1);
                                                   }
                                                   local_bins[dest_bin].push_back(v);
                                               }
                                           }
                                           // get dest end
                                       }
                                   }
                               }*/
                        }
                    }
                    // stal end
                }
            }

            for (size_t i = curr_bin_index; i < local_bins.size(); i++)
            {
                if (!local_bins[i].empty())
                {
#pragma omp critical
                    next_bin_index = std::min(next_bin_index, i);
                    break;
                }
            }

#pragma omp barrier
#pragma omp single nowait
            {
                curr_bin_index = kMaxBin;
                curr_frontier_tail = 0;
            }

            if (next_bin_index < local_bins.size())
            {
                size_t copy_start = gapbs::fetch_and_add(next_frontier_tail, local_bins[next_bin_index].size());
                std::copy(local_bins[next_bin_index].begin(), local_bins[next_bin_index].end(), frontier.data() + copy_start);
                local_bins[next_bin_index].resize(0);
            }

            iter++;
#pragma omp barrier
        }

#if defined(DEBUG)
#pragma omp single
        COUT_DEBUG("took " << iter << " iterations");
#endif
    }

    return dist;
}

// sssp end
std::unique_ptr<double[]> HBALStore::do_scanedges(uint64_t max_vertex_id, uint64_t num_iterations, double damping_factor, int read_version, MemoryAllocator *me)
{
    max_vertex_id = get_high_water_mark();
    int64_t min_read_versions = 0;
    uint64_t countedges = 0;

    if (0 == 0)
    {
        for (uint64_t iteration = 0; iteration < 1; iteration++)
        {
            // std::cout<<"iteration # "<<iteration<<std::endl;

            // compute the new score for each node in the graph
            for (uint64_t v = 0; v < max_vertex_id; v++)
            {

                //  if(degrees[v] == std::numeric_limits<uint64_t>::max()){ continue; } // the vertex does not exist

                index[v].indirectionLock.lock();

                double incoming_total = 0;

                // per source STAL access

                // PerSourceVertexIndr *ps_stal = getSourceVertexPointer(v);//transaction.get_edges(u,   0); // fixme: incoming edges for directed graphs


                //
                bool IsWTS = false;
                if (index[v].edgePtr != 0)
                {

                    if (!(index[v].edgePtr & 0x1))
                    {
                        PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[v].getEdgePtr());

                        int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                        bool flag_l1_delete = false;
                        flag_l1_delete = ind_ptr->isDeletionBlock();

                        if (1)
                        {
                            // deletion bit per source-vertex not active direct read L2 vector
                            for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                            {

                                stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                uint8_t is_delete_l2_flag = 0;
                                is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                if (1)
                                {

                                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                    {

                                        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                        if (1)
                                        {
                                            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                            countedges++;
                                            // check if dst contains ooo updates
                                            if (check_version_ooo(dst))
                                            {
                                                // ooo updates exist
                                                // ooo updates exist
                                                if (ei->getOutOfOrderUpdt()->getCount() > 1)
                                                {

                                                    //   ArtPerIndex *art_read = ei->getOutOfOrderUpdt();
                                                    art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                                                    //%%%%%%%%%%%% adaptive radix tree scan start
                                                    // ei->outOfOrderUpdt.scanArtTree();
                                                    auto it = art_val->begin();
                                                    auto it_end = art_val->end();
                                                    for (int i = 0; it != it_end; ++i, ++it)
                                                    {
                                                        LeafNode *ln_cur = *it;
                                                        countedges++;
                                                        if (ln_cur->getWts() < min_read_versions)
                                                        {
                                                            if (ln_cur->getInvalEntry() == NULL)
                                                            {
                                                                //  countedges++;
                                                                // *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                                            }
                                                            else
                                                            {
                                                                if (ln_cur->getInvalEntry()->getWts() > min_read_versions)
                                                                {
                                                                    //  countedges++;
                                                                    // *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                                                }
                                                            }
                                                        }
                                                    }
                                                    //%%%%%%% end adaptive radix tree scan
                                                }
                                                else
                                                {
                                                    //  std::cout<<"ooo update == 1"<<std::endl;
                                                    if (ei->getOutOfOrderUpdt()->getCount() == 1)
                                                    {
                                                        art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                                                        //%%%%%%%%%%%% adaptive radix tree scan start
                                                        // ei->outOfOrderUpdt.scanArtTree();
                                                        auto it = art_val->begin();
                                                        LeafNode *ln_cur = *it;
                                                        countedges++;
                                                        if (ln_cur->getWts() < min_read_versions)
                                                        {
                                                            if (ln_cur->getInvalEntry() == NULL)
                                                            {
                                                                // countedges++;
                                                                // *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                                            }
                                                            else
                                                            {
                                                                if (ln_cur->getInvalEntry()->getWts() > min_read_versions)
                                                                {
                                                                    // countedges++;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                // ooo updates part end
                                            }
                                            // std::cout<<ei->getSTime() << "    "<<ei->getWTsEdge()<<std::endl;
                                        }
                                        /// %%%%%%%%%%% end %%%%%%%%%%
                                    }

                                }


                            }
                        }

                    }
                    else
                    {
                        stalb_type *eb_k = static_cast<stalb_type *>(index[v].getEdgePtr());

                        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                        uint8_t is_delete_l2_flag = 0;
                        is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                        if (1)
                        {

                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                            {

                                /// %%%%%%%%% transaction time comparison to read updates that are < RTS time

                                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

                                // std::cout<<ei->getSTime() << "    "<<ei->getWTsEdge()<<std::endl;
                                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                // EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) +((ind_l2 * 8) - 8));
                                countedges++;
                                // check if dst contains ooo updates
                                if (check_version_ooo(dst))
                                {

                                    // ooo updates exist
                                    // ooo updates exist
                                    if (ei->getOutOfOrderUpdt()->getCount() > 1)
                                    {

                                        //   ArtPerIndex *art_read = ei->getOutOfOrderUpdt();
                                        art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                                        //%%%%%%%%%%%% adaptive radix tree scan start
                                        // ei->outOfOrderUpdt.scanArtTree();
                                        auto it = art_val->begin();
                                        auto it_end = art_val->end();
                                        for (int i = 0; it != it_end; ++i, ++it)
                                        {
                                            LeafNode *ln_cur = *it;
                                            countedges++;
                                            if (ln_cur->getWts() < min_read_versions)
                                            {
                                                if (ln_cur->getInvalEntry() == NULL)
                                                {
                                                    // countedges++;
                                                    // *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                                }
                                                else
                                                {
                                                    if (ln_cur->getInvalEntry()->getWts() > min_read_versions)
                                                    {
                                                        //  countedges++;
                                                        // *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                                    }
                                                }
                                            }
                                        }
                                        //%%%%%%% end adaptive radix tree scan
                                    }
                                    else
                                    {
                                        if (ei->getOutOfOrderUpdt()->getCount() == 1)
                                        {
                                            art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                                            //%%%%%%%%%%%% adaptive radix tree scan start
                                            // ei->outOfOrderUpdt.scanArtTree();
                                            auto it = art_val->begin();
                                            LeafNode *ln_cur = *it;
                                            countedges++;
                                            if (ln_cur->getWts() < min_read_versions)
                                            {
                                                if (ln_cur->getInvalEntry() == NULL)
                                                {

                                                    // *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                                }
                                                else
                                                {
                                                    if (ln_cur->getInvalEntry()->getWts() > min_read_versions)
                                                    {
                                                        //countedges++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    // ooo updates part end
                                }
                                /// %%%%%%%%%%% end %%%%%%%%%%
                            }

                        }
                    }
                }

                index[v].indirectionLock.unlock();

            }
        }
    }
    // std::cout<<"total deletion "<<total_deletion<<" "<<false_deletion<<std::endl;
    //std::cout<<"hello 2"<<std::endl;
    //   std::cout<<"iteration end"<<std::endl;
    std::cout << "total number of edges: " << countedges << std::endl;
    return 0;
}


/*
// lcc algorithm
std::unique_ptr<double[]> HBALStore::do_lcc_undirected(uint64_t max_vertex_id)
{
    std::unique_ptr<double[]> ptr_lcc { new double[max_vertex_id] };
    double* lcc = ptr_lcc.get();
    std::unique_ptr<uint32_t[]> ptr_degrees_out { new uint32_t[max_vertex_id] };
    uint32_t* __restrict degrees_out = ptr_degrees_out.get();

    // precompute the degrees of the vertices
#pragma omp parallel for schedule(dynamic, 4096)
    for(uint64_t v = 0; v < max_vertex_id; v++){
        degrees_out[v] = index[v].degree;
    }


#pragma omp parallel for schedule(dynamic, 64)
    for(uint64_t v = 0; v < max_vertex_id; v++){

        lcc[v] = 0.0;
        uint64_t num_triangles = 0; // number of triangles found so far for the node v

        // Cfr. Spec v.0.9.0 pp. 15: "If the number of neighbors of a vertex is less than two, its coefficient is defined as zero"
        uint64_t v_degree_out = degrees_out[v];
        if(v_degree_out < 2) continue;

        // again, visit all neighbours of v
        // for directed graphs, edges1 contains the intersection of both the incoming and the outgoing edges
       ///////// Level 0 start ///////////
       // auto iterator1 = transaction.get_edges(v,  0);
        if (!(index[v].edgePtr & 0x1))
        {

            PerSourceIndrPointer *ind_ptr = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(index[v].edgePtr));

            //int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
            bool flag_l1_delete = false;
            flag_l1_delete = ind_ptr->isDeletionBlock();

            if (!flag_l1_delete)
            {
                // deletion bit per source-vertex not active direct read L2 vector
                for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < (u_int64_t) 1 << ind_ptr->getBlockLen(); ind_l1++)
                {

                    stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];

                    // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                    uint8_t is_delete_l2_flag = 0;
                    is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                    if (!is_delete_l2_flag)
                    {

                        for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1);
                             ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                        {
                            // uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                            uint64_t u = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8); // dest id

                            ///////////////////////////// Level 1 start /////////////////////////////////////////////////////////////

                            if (!(index[u].edgePtr & 0x1))
                            {

                                PerSourceIndrPointer *ind_ptr_u = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(index[u].edgePtr));

                                //int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                                bool flag_l1_delete_u = false;
                                flag_l1_delete_u = ind_ptr_u->isDeletionBlock();

                                if (!flag_l1_delete_u)
                                {
                                    // deletion bit per source-vertex not active direct read L2 vector
                                    for (int ind_l1_u = ind_ptr_u->getCurPos(); ind_l1_u < (u_int64_t) 1 << ind_ptr_u->getBlockLen(); ind_l1_u++)
                                    {

                                        stalb_type *eb_k_u = ind_ptr_u->getperVertexBlockArr()[ind_l1_u];

                                        // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                                        uint8_t is_delete_l2_flag_u = 0;
                                        is_delete_l2_flag_u = *reinterpret_cast<uint8_ptr>(eb_k_u + 3);
                                        if (!is_delete_l2_flag_u)
                                        {

                                            for (int ind_l2_u = *reinterpret_cast<uint16_ptr>(eb_k_u + 1);
                                                 ind_l2_u < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k_u)) / 2); ind_l2_u++)
                                            {
                                                // uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                                uint64_t w = *reinterpret_cast<uint64_ptr>(eb_k_u + ind_l2_u * 8); // dest id

                                                //// hash table lookup start ////

                                                AdjacentPos ap;// = static_cast<AdjacentPos *>(me->Allocate(sizeof(AdjacentPos), thread_id));
                                                ap.edgeBlockIndex = -1;
                                                ap.indrIndex = -1;
                                                /// now check based on edge block indr index
                                                AdjacentPos *ap_pos = index[v].getDestNodeHashTableVal(w, &ap);
                                                if(ap_pos->edgeBlockIndex != -1)
                                                {
                                                    // triangle count
                                                    num_triangles++;
                                                }
                                                ///// hash table lookup end /////


                                            }

                                        }
                                    }
                                }
                                ////////// Level 0 end /////////////////
                            }
                            else
                            {
                                //// Level 0 start ////
                                // directly read from edge block
                                stalb_type *eb_k_u = reinterpret_cast<stalb_type *>(get_pointer(index[u].edgePtr));

                                // int l2_len = (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2);
                                uint8_t is_delete_l2_flag_u = 0;
                                is_delete_l2_flag_u = *reinterpret_cast<uint8_ptr>(eb_k_u + 3);
                                if (!is_delete_l2_flag_u)
                                {

                                    for (int ind_l2_u = *reinterpret_cast<uint16_ptr>(eb_k_u + 1); ind_l2_u < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k_u)) / 2); ind_l2_u++)
                                    {

                                        uint64_t w = *reinterpret_cast<uint64_ptr>(eb_k_u + ind_l2_u * 8); // dest id

                                        //// hash table lookup start ////

                                        AdjacentPos ap;// = static_cast<AdjacentPos *>(me->Allocate(sizeof(AdjacentPos), thread_id));
                                        ap.edgeBlockIndex = -1;
                                        ap.indrIndex = -1;
                                        /// now check based on edge block indr index
                                        AdjacentPos *ap_pos = index[v].getDestNodeHashTableVal(w, &ap);
                                        if(ap_pos->edgeBlockIndex != -1)
                                        {
                                            // triangle count
                                            num_triangles++;
                                        }
                                        ///// hash table lookup end /////
                                    }
                                }

                                //// level 0 end ////


                            }

                            ///////////////////////////// Level 1 end /////////////////////////////////////////////////////////////


                        }

                    }
                }
            }
            ////////// Level 0 end /////////////////

        }
        else
        {
            //// Level 0 start ////
            // directly read from edge block
            stalb_type *eb_k = reinterpret_cast<stalb_type *>(get_pointer(index[v].edgePtr));

            // int l2_len = (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2);
            uint8_t is_delete_l2_flag = 0;
            is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
            if (!is_delete_l2_flag)
            {

                for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                {
                    uint64_t u = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);

                //    incoming_total += outgoing_contrib[*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8)];

                ///////////////////////////// Level 1 start /////////////////////////////////////////////////////////////

                    if (!(index[u].edgePtr & 0x1))
                    {

                        PerSourceIndrPointer *ind_ptr_u = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(index[u].edgePtr));

                        //int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                        bool flag_l1_delete_u = false;
                        flag_l1_delete_u = ind_ptr_u->isDeletionBlock();

                        if (!flag_l1_delete_u)
                        {
                            // deletion bit per source-vertex not active direct read L2 vector
                            for (int ind_l1_u = ind_ptr_u->getCurPos(); ind_l1_u < (u_int64_t) 1 << ind_ptr_u->getBlockLen(); ind_l1_u++)
                            {

                                stalb_type *eb_k_u = ind_ptr_u->getperVertexBlockArr()[ind_l1_u];

                                // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                                uint8_t is_delete_l2_flag_u = 0;
                                is_delete_l2_flag_u = *reinterpret_cast<uint8_ptr>(eb_k_u + 3);
                                if (!is_delete_l2_flag_u)
                                {

                                    for (int ind_l2_u = *reinterpret_cast<uint16_ptr>(eb_k_u + 1);
                                         ind_l2_u < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k_u)) / 2); ind_l2_u++)
                                    {
                                        // uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                        uint64_t w = *reinterpret_cast<uint64_ptr>(eb_k_u + ind_l2_u * 8); // dest id

                                        //// hash table lookup start ////

                                        AdjacentPos ap;// = static_cast<AdjacentPos *>(me->Allocate(sizeof(AdjacentPos), thread_id));
                                        ap.edgeBlockIndex = -1;
                                        ap.indrIndex = -1;
                                        /// now check based on edge block indr index
                                        AdjacentPos *ap_pos = index[v].getDestNodeHashTableVal(w, &ap);
                                        if(ap_pos->edgeBlockIndex != -1)
                                        {
                                            // triangle count
                                            num_triangles++;
                                        }
                                        ///// hash table lookup end /////


                                    }

                                }
                            }
                        }
                        ////////// Level 0 end /////////////////
                    }
                    else
                    {
                        //// Level 0 start ////
                        // directly read from edge block
                        stalb_type *eb_k_u = reinterpret_cast<stalb_type *>(get_pointer(index[u].edgePtr));

                        // int l2_len = (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2);
                        uint8_t is_delete_l2_flag_u = 0;
                        is_delete_l2_flag_u = *reinterpret_cast<uint8_ptr>(eb_k_u + 3);
                        if (!is_delete_l2_flag_u)
                        {

                            for (int ind_l2_u = *reinterpret_cast<uint16_ptr>(eb_k_u + 1); ind_l2_u < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k_u)) / 2); ind_l2_u++)
                            {

                                uint64_t w = *reinterpret_cast<uint64_ptr>(eb_k_u + ind_l2_u * 8); // dest id

                                //// hash table lookup start ////

                                AdjacentPos ap;// = static_cast<AdjacentPos *>(me->Allocate(sizeof(AdjacentPos), thread_id));
                                ap.edgeBlockIndex = -1;
                                ap.indrIndex = -1;
                                /// now check based on edge block indr index
                                AdjacentPos *ap_pos = index[v].getDestNodeHashTableVal(w, &ap);
                                if(ap_pos->edgeBlockIndex != -1)
                                {
                                    // triangle count
                                    num_triangles++;
                                }
                                ///// hash table lookup end /////
                            }
                        }

                        //// level 0 end ////


                    }

                    ///////////////////////////// Level 1 end /////////////////////////////////////////////////////////////

                }
            }

            //// level 0 end ////

        }

        // register the final score
        uint64_t max_num_edges = v_degree_out * (v_degree_out -1);
        lcc[v] = static_cast<double>(num_triangles) / max_num_edges;
        //COUT_DEBUG_LCC("Score computed: " << (num_triangles) << "/" << max_num_edges << " = " << lcc[v]);
    }

    return ptr_lcc;
}*/

// lcc end

/* Algorithm specfic analytics function for historical queries part */

// per source degree based on history
// STALB scan no deletion and no out-of-order update
void HBALStore::history_degree_no_deletion_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, int index_start, vertex_t *per_source_degree)
{
    //double stalb_incomming_total = 0.0;
    for (int ind_l2 = index_start; ind_l2 < l2_len; ind_l2++)
    {
        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!(*IsWTS))
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei->getWTsEdge() < min_read_versions)
            {
                *IsWTS = 1;
                *per_source_degree += 1;
            }
        }
        else
        {
            *per_source_degree += 1;
        }
        /// %%%%%%%%%%% end %%%%%%%%%%
    }
    // return stalb_incomming_total;
}

// STALB scan deletion and no out-of-order update scan
void HBALStore::history_degree_deletion_no_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, int index_start, vertex_t *per_source_degree, timestamp_t src_time)
{
    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
    {
        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!*IsWTS)
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei->getWTsEdge() < min_read_versions)
            {
                *IsWTS = 1;
                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                if (!check_version(dst))
                {
                    *per_source_degree += 1; //outgoing_contrib->start_[dst];
                }
                else
                {
                    if (min_read_versions < ei->getInvldTime()->getWts() && src_time < ei->getInvldTime()->getSrcTime())
                    {
                        *per_source_degree += 1;
                    }

                }

            }

        }
        else
        {

            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
            if (!check_version(dst))
            {
                *per_source_degree += 1;
            }
            else
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                if (min_read_versions < ei->getInvldTime()->getWts())
                {
                    *per_source_degree += 1;
                }
            }

        }
        /// %%%%%%%%%%% end %%%%%%%%%%
    }
}

// STALB no deletion and out-of-order update
void HBALStore::history_degree_no_deletion_ooo_exist_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, int index_start, vertex_t *per_source_degree, timestamp_t src_time)
{
    //double stalb_incomming_total = 0.0;
    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
    {

        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!(*IsWTS))
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei->getWTsEdge() < min_read_versions)
            {
                *IsWTS = 1;
                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                *per_source_degree += 1;

                if (check_version_ooo(dst))
                {

                    // ooo updates exist
                    if (ei->getOutOfOrderUpdt()->getCount() > 1)
                    {

                        //   ArtPerIndex *art_read = ei->getOutOfOrderUpdt();
                        art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                        //%%%%%%%%%%%% adaptive radix tree scan start
                        // ei->outOfOrderUpdt.scanArtTree();
                        auto it = art_val->begin();
                        auto it_end = art_val->end();
                        for (int i = 0; it != it_end; ++i, ++it)
                        {
                            LeafNode *ln_cur = *it;
                            if (ln_cur->getWts() < min_read_versions)
                            {
                                if (ln_cur->getInvalEntry() == NULL)
                                {
                                    *per_source_degree += 1;
                                }
                                else
                                {
                                    if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                    {
                                        *per_source_degree += 1;
                                    }
                                }
                            }
                        }
                        //%%%%%%% end adaptive radix tree scan
                    }
                    else
                    {
                        if (ei->getOutOfOrderUpdt()->getCount() == 1)
                        {
                            art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                            //%%%%%%%%%%%% adaptive radix tree scan start
                            // ei->outOfOrderUpdt.scanArtTree();
                            auto it = art_val->begin();
                            LeafNode *ln_cur = *it;
                            if (ln_cur->getWts() < min_read_versions)
                            {
                                if (ln_cur->getInvalEntry() == NULL)
                                {
                                    *per_source_degree += 1;
                                }
                                else
                                {
                                    if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                    {
                                        *per_source_degree += 1;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {

            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
            *per_source_degree += 1;

            if (check_version_ooo(dst))
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

                // ooo updates exist
                // ooo updates exist
                if (ei->getOutOfOrderUpdt()->getCount() > 1)
                {

                    //   ArtPerIndex *art_read = ei->getOutOfOrderUpdt();
                    art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                    //%%%%%%%%%%%% adaptive radix tree scan start
                    // ei->outOfOrderUpdt.scanArtTree();
                    auto it = art_val->begin();
                    auto it_end = art_val->end();
                    for (int i = 0; it != it_end; ++i, ++it)
                    {
                        LeafNode *ln_cur = *it;
                        if (ln_cur->getWts() < min_read_versions)
                        {
                            if (ln_cur->getInvalEntry() == NULL)
                            {
                                *per_source_degree += 1;
                            }
                            else
                            {
                                if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                {
                                    *per_source_degree += 1;
                                }
                            }
                        }
                    }
                    //%%%%%%% end adaptive radix tree scan
                }
                else
                {
                    if (ei->getOutOfOrderUpdt()->getCount() == 1)
                    {
                        art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                        //%%%%%%%%%%%% adaptive radix tree scan start
                        // ei->outOfOrderUpdt.scanArtTree();
                        auto it = art_val->begin();
                        LeafNode *ln_cur = *it;
                        if (ln_cur->getWts() < min_read_versions)
                        {
                            if (ln_cur->getInvalEntry() == NULL)
                            {
                                *per_source_degree += 1;
                            }
                            else
                            {
                                if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                {
                                    *per_source_degree += 1;
                                }
                            }
                        }
                    }
                }
                // ooo updates part end

            }
        }
        /// %%%%%%%%%%% end %%%%%%%%%%

    }
    // return stalb_incomming_total;
}

// STALB deletion and out-of-order update scan
void HBALStore::history_degree_deletion_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, int index_start, vertex_t *per_source_degree, timestamp_t src_time)
{
    for (int ind_l2 = index_start; ind_l2 < l2_len; ind_l2++)
    {
        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!*IsWTS)
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei->getWTsEdge() < min_read_versions)
            {
                *IsWTS = 1;
                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                if (!check_version(dst))
                {
                    *per_source_degree += 1;
                    // ooo existence check
                    // ooo exist check

                }
                else
                {

                    if (ei->getInvldTime()->getWts() > min_read_versions && src_time < ei->getInvldTime()->getSrcTime())
                    {
                        *per_source_degree += 1;
                    }

                }

                // check if dst contains ooo updates
                if (check_version_ooo(dst))
                {
                    // ooo updates exist
                    // ooo updates exist
                    if (ei->getOutOfOrderUpdt()->getCount() > 1)
                    {

                        //   ArtPerIndex *art_read = ei->getOutOfOrderUpdt();
                        art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                        //%%%%%%%%%%%% adaptive radix tree scan start
                        // ei->outOfOrderUpdt.scanArtTree();
                        auto it = art_val->begin();
                        auto it_end = art_val->end();
                        for (int i = 0; it != it_end; ++i, ++it)
                        {
                            LeafNode *ln_cur = *it;
                            if (ln_cur->getWts() < min_read_versions)
                            {
                                if (ln_cur->getInvalEntry() == NULL)
                                {
                                    *per_source_degree += 1;
                                }
                                else
                                {
                                    if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                    {
                                        *per_source_degree += 1;
                                    }
                                }
                            }
                        }
                        //%%%%%%% end adaptive radix tree scan
                    }
                    else
                    {
                        if (ei->getOutOfOrderUpdt()->getCount() == 1)
                        {
                            art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                            //%%%%%%%%%%%% adaptive radix tree scan start
                            // ei->outOfOrderUpdt.scanArtTree();
                            auto it = art_val->begin();
                            LeafNode *ln_cur = *it;
                            if (ln_cur->getWts() < min_read_versions)
                            {
                                if (ln_cur->getInvalEntry() == NULL)
                                {
                                    *per_source_degree += 1;
                                }
                                else
                                {
                                    if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                    {
                                        *per_source_degree += 1;
                                    }
                                }
                            }
                        }
                    }
                    // ooo updates part end
                }

            }
        }
        else
        {

            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
            if (!check_version(dst))
            {
                *per_source_degree += 1;
            }
            else
            {

                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                if (min_read_versions < ei->getInvldTime()->getWts() && src_time < ei->getInvldTime()->getSrcTime())
                {
                    *per_source_degree += 1;
                }
            }

            // check if dst contains ooo updates
            if (check_version_ooo(dst))
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

                // ooo updates exist
                if (ei->getOutOfOrderUpdt()->getCount() > 1)
                {

                    //   ArtPerIndex *art_read = ei->getOutOfOrderUpdt();
                    art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                    //%%%%%%%%%%%% adaptive radix tree scan start
                    // ei->outOfOrderUpdt.scanArtTree();
                    auto it = art_val->begin();
                    auto it_end = art_val->end();
                    for (int i = 0; it != it_end; ++i, ++it)
                    {
                        LeafNode *ln_cur = *it;
                        if (ln_cur->getWts() < min_read_versions)
                        {
                            if (ln_cur->getInvalEntry() == NULL)
                            {
                                *per_source_degree += 1;
                            }
                            else
                            {
                                if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                {
                                    *per_source_degree += 1;
                                }
                            }
                        }
                    }
                    //%%%%%%% end adaptive radix tree scan
                }
                else
                {
                    if (ei->getOutOfOrderUpdt()->getCount() == 1)
                    {
                        art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                        //%%%%%%%%%%%% adaptive radix tree scan start
                        // ei->outOfOrderUpdt.scanArtTree();
                        auto it = art_val->begin();
                        LeafNode *ln_cur = *it;
                        if (ln_cur->getWts() < min_read_versions)
                        {
                            if (ln_cur->getInvalEntry() == NULL)
                            {
                                *per_source_degree += 1;
                            }
                            else
                            {
                                if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                {
                                    *per_source_degree += 1;
                                }
                            }
                        }
                    }
                }
                // ooo updates part end
            }

        }
        /// %%%%%%%%%%% end %%%%%%%%%%
    }
}

// calculate degree for historical queries
uint64_t HBALStore::perSourceHistoryDegreeList(uint64_t vertex_id, uint64_t timestamp, double damping_factor, int read_version, MemoryAllocator *me)
{
    vertex_t v = vertex_id;
    min_read_degree_version = MaxValue;
    // readTable->readQuery[read_version]
    timestamp_t min_read_versions = min_read_version;
    // compute the new score for each node in the graph
    index[vertex_id].indirectionLock.lock();

    uint64_t per_source_degree = 0;

    //
    uint8_t IsWTS = 0;
    // *IsWTS = 0;
    if (index[v].edgePtr != 0)
    {

        if (!(index[vertex_id].edgePtr & 0x1))
        {
            //%%%%%%%%%%%%
            PerSourceIndrPointer *ptr_per_src = static_cast<PerSourceIndrPointer *>(index[v].getEdgePtr());

            int startPos = ptr_per_src->getCurPos();
            int endPos = ((u_int64_t) 1 << ptr_per_src->getBlockLen());
            int indexIndrArray = findIndexForIndr((ptr_per_src->getperVertexBlockArr()), startPos, endPos, timestamp);

            u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray]);
            EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + ((eb_block_size / 2) * 8) + ((((u_int16_t) *reinterpret_cast<uint16_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + 1)) * 8) - 8));

            if (eih_cur->getSTime() < timestamp)
            {
                if (indexIndrArray != 0)
                {
                    indexIndrArray--;
                }
            }
            //fixed size block binary search
            int indexFixedBlock = findIndexForBlockArr(ptr_per_src->getperVertexBlockArr()[indexIndrArray], ((u_int16_t) *reinterpret_cast<uint16_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + 1)), (eb_block_size / 2), timestamp);

            /// eb block
            EdgeInfoHal *eih_index = *reinterpret_cast<EdgeInfoHal **>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + ((eb_block_size / 2) * 8) + ((indexFixedBlock * 8) - 8));

            if (eih_index->getSTime() < timestamp)
            {
                indexFixedBlock--;
            }

            //%%%%%%%%%%%%%%%%


            int l1_len = endPos;//(u_int64_t) 1 << ind_ptr->getBlockLen();
            bool flag_l1_delete = false;
            flag_l1_delete = ptr_per_src->isDeletionBlock();

            if (!flag_l1_delete)
            {
                // deletion bit per source-vertex not active direct read L2 vector
                for (int ind_l1 = indexIndrArray; ind_l1 < l1_len; ind_l1++)
                {

                    stalb_type *eb_k = ptr_per_src->getperVertexBlockArr()[ind_l1]; // stalb pointer

                    int l2_len = (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); // get block size

                    uint8_t is_delete_l2_flag = 0;
                    uint8_t is_ooo_l2_flag = 0;

                    is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                    is_ooo_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 4);

                    if (!is_delete_l2_flag && !is_ooo_l2_flag)
                    {
                        // no deletion and ooo entries
                        if (ind_l1 == indexIndrArray)
                        {
                            history_degree_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, indexFixedBlock, &per_source_degree);
                        }
                        else
                        {
                            history_degree_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, 0, &per_source_degree);
                        }

                    }
                    else if (is_delete_l2_flag && !is_ooo_l2_flag)
                    {
                        if (ind_l1 == indexIndrArray)
                        {
                            history_degree_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, indexFixedBlock, &per_source_degree, timestamp);
                        }
                        else
                        {
                            history_degree_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, 0, &per_source_degree, timestamp);
                        }
                    }
                    else if (!is_delete_l2_flag && is_ooo_l2_flag)
                    {
                        if (ind_l1 == indexIndrArray)
                        {
                            history_degree_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, indexFixedBlock, &per_source_degree, timestamp);
                        }
                        else
                        {
                            history_degree_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, 0, &per_source_degree, timestamp);
                        }
                        //// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% out of order update exist
                    }
                    else
                    {
                        if (ind_l1 == indexIndrArray)
                        {
                            history_degree_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, indexFixedBlock, &per_source_degree, timestamp);
                        }
                        else
                        {
                            history_degree_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, 0, &per_source_degree, timestamp);
                        }
                    }


                }
            }
            else
            {
                /// %%%%%%%%%%%%%%%%% if indirection array have deletions STAL %%%%%%%%%%%%

                for (int ind_l1 = indexIndrArray; ind_l1 < l1_len; ind_l1++)
                {
                    if (reinterpret_cast<vertex_t>(ptr_per_src->getperVertexBlockArr()[ind_l1]) != FLAG_EMPTY_SLOT)
                    {
                        stalb_type *eb_k = ptr_per_src->getperVertexBlockArr()[ind_l1]; // stalb pointer

                        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                        uint8_t is_delete_l2_flag = 0;
                        uint8_t is_ooo_l2_flag = 0;

                        is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                        is_ooo_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 4);


                        if (!is_delete_l2_flag && !is_ooo_l2_flag)
                        {
                            // no deletion and ooo entries
                            if (ind_l1 == indexIndrArray)
                            {
                                history_degree_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, indexFixedBlock, &per_source_degree);
                            }
                            else
                            {

                                history_degree_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, 0, &per_source_degree);
                            }

                        }
                        else if (is_delete_l2_flag && !is_ooo_l2_flag)
                        {
                            if (ind_l1 == indexIndrArray)
                            {
                                history_degree_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, indexFixedBlock, &per_source_degree, timestamp);
                            }
                            else
                            {
                                history_degree_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, 0, &per_source_degree, timestamp);
                            }
                        }
                        else if (!is_delete_l2_flag && is_ooo_l2_flag)
                        {
                            if (ind_l1 == indexIndrArray)
                            {
                                history_degree_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, indexFixedBlock, &per_source_degree, timestamp);
                            }
                            else
                            {
                                history_degree_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, 0, &per_source_degree, timestamp);
                            }
                        }
                        else
                        {
                            if (ind_l1 == indexIndrArray)
                            {
                                history_degree_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, indexFixedBlock, &per_source_degree, timestamp);
                            }
                            else
                            {
                                history_degree_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, 0, &per_source_degree, timestamp);
                            }
                        }
                    }

                }

                /// %%%%%%%%%%%%%%%%% end %%%%%%%%%%%%%%%%%%%%%%
            }
        }
        else
        {

            stalb_type *eb_k = static_cast<stalb_type *>(index[v].getEdgePtr());

            int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
            uint8_t is_delete_l2_flag = 0;
            uint8_t is_ooo_l2_flag = 0;

            is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
            is_ooo_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 4);
            int indexFixedBlock = findIndexForBlockArr(eb_k, ((u_int16_t) *reinterpret_cast<uint16_ptr>(eb_k + 1)), l2_len, timestamp);

            /// eb block
            EdgeInfoHal *eih_index = *reinterpret_cast<EdgeInfoHal **>(eb_k + (l2_len * 8) + ((indexFixedBlock * 8) - 8));

            if (eih_index->getSTime() < timestamp)
            {
                indexFixedBlock--;
            }

            if (!is_delete_l2_flag && !is_ooo_l2_flag)
            {
                // no deletion and ooo entries
                history_degree_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, indexFixedBlock, &per_source_degree);
            }
            else if (is_delete_l2_flag && !is_ooo_l2_flag)
            {
                history_degree_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, indexFixedBlock, &per_source_degree, timestamp);
            }
            else if (!is_delete_l2_flag && is_ooo_l2_flag)
            {
                history_degree_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, indexFixedBlock, &per_source_degree, timestamp);
            }
            else
            {
                history_degree_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, indexFixedBlock, &per_source_degree, timestamp);
            }
            // #### per source edge reading end
        }
    }
    // STAL end
    index[v].indirectionLock.unlock();

}

/* Historical page rank */

// STALB scan no deletion and no out-of-order update
void HBALStore::history_pagerank_no_deletion_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total, int index_start, timestamp_t src_time)
{
    //double stalb_incomming_total = 0.0;
    for (int ind_l2 = index_start; ind_l2 < l2_len; ind_l2++)
    {

        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!(*IsWTS))
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei->getWTsEdge() < min_read_versions)
            {
                *IsWTS = 1;
                *stalb_incomming_total += outgoing_contrib->start_[*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8)];
            }
        }
        else
        {
            *stalb_incomming_total += outgoing_contrib->start_[*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8)];
        }
        /// %%%%%%%%%%% end %%%%%%%%%%

    }
    // return stalb_incomming_total;
}

// STALB scan deletion and no out-of-order update scan
void HBALStore::history_pagerank_deletion_no_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total, int index_start, timestamp_t src_time)
{
    for (int ind_l2 = index_start; ind_l2 < l2_len; ind_l2++)
    {
        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!*IsWTS)
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei->getWTsEdge() < min_read_versions)
            {
                *IsWTS = 1;
                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                if (!check_version(dst))
                {
                    *stalb_incomming_total += outgoing_contrib->start_[dst];
                }
                else
                {
                    if (min_read_versions < ei->getInvldTime()->getWts() && src_time < ei->getInvldTime()->getSrcTime())
                    {
                        *stalb_incomming_total += outgoing_contrib->start_[dst];
                    }
                }

            }
        }
        else
        {

            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
            if (!check_version(dst))
            {
                *stalb_incomming_total += outgoing_contrib->start_[dst];
            }
            else
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                if (min_read_versions < ei->getInvldTime()->getWts() && src_time < ei->getInvldTime()->getSrcTime())
                {
                    *stalb_incomming_total += outgoing_contrib->start_[dst];
                }
            }

        }
        /// %%%%%%%%%%% end %%%%%%%%%%
    }
}

// STALB no deletion and out-of-order update
void HBALStore::history_pagerank_no_deletion_ooo_exist_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total, int index_start, timestamp_t src_time)
{
    //double stalb_incomming_total = 0.0;
    for (int ind_l2 = index_start; ind_l2 < l2_len; ind_l2++)
    {

        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!(*IsWTS))
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei->getWTsEdge() < min_read_versions)
            {
                *IsWTS = 1;
                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                *stalb_incomming_total += outgoing_contrib->start_[dst];

                if (check_version_ooo(dst))
                {

                    // ooo updates exist
                    if (ei->getOutOfOrderUpdt()->getCount() > 1)
                    {

                        //   ArtPerIndex *art_read = ei->getOutOfOrderUpdt();
                        art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                        //%%%%%%%%%%%% adaptive radix tree scan start
                        // ei->outOfOrderUpdt.scanArtTree();
                        auto it = art_val->begin();
                        auto it_end = art_val->end();
                        for (int i = 0; it != it_end; ++i, ++it)
                        {
                            LeafNode *ln_cur = *it;
                            if (ln_cur->getWts() < min_read_versions)
                            {
                                if (ln_cur->getInvalEntry() == NULL)
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                                else
                                {
                                    if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                    {
                                        *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                    }
                                }
                            }
                        }
                        //%%%%%%% end adaptive radix tree scan
                    }
                    else
                    {
                        if (ei->getOutOfOrderUpdt()->getCount() == 1)
                        {
                            art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                            //%%%%%%%%%%%% adaptive radix tree scan start
                            // ei->outOfOrderUpdt.scanArtTree();
                            auto it = art_val->begin();
                            LeafNode *ln_cur = *it;
                            if (ln_cur->getWts() < min_read_versions)
                            {
                                if (ln_cur->getInvalEntry() == NULL)
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                                else
                                {
                                    if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                    {
                                        *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        else
        {

            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
            *stalb_incomming_total += outgoing_contrib->start_[*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8)];

            if (check_version_ooo(dst))
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

                // ooo updates exist
                // ooo updates exist
                if (ei->getOutOfOrderUpdt()->getCount() > 1)
                {

                    //   ArtPerIndex *art_read = ei->getOutOfOrderUpdt();
                    art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                    //%%%%%%%%%%%% adaptive radix tree scan start
                    // ei->outOfOrderUpdt.scanArtTree();
                    auto it = art_val->begin();
                    auto it_end = art_val->end();
                    for (int i = 0; it != it_end; ++i, ++it)
                    {
                        LeafNode *ln_cur = *it;
                        if (ln_cur->getWts() < min_read_versions)
                        {
                            if (ln_cur->getInvalEntry() == NULL)
                            {
                                *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                            }
                            else
                            {
                                if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                            }
                        }
                    }
                    //%%%%%%% end adaptive radix tree scan
                }
                else
                {
                    if (ei->getOutOfOrderUpdt()->getCount() == 1)
                    {
                        art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                        //%%%%%%%%%%%% adaptive radix tree scan start
                        // ei->outOfOrderUpdt.scanArtTree();
                        auto it = art_val->begin();
                        LeafNode *ln_cur = *it;
                        if (ln_cur->getWts() < min_read_versions)
                        {
                            if (ln_cur->getInvalEntry() == NULL)
                            {
                                *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                            }
                            else
                            {
                                if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                            }
                        }
                    }
                }
                // ooo updates part end

            }
        }
        /// %%%%%%%%%%% end %%%%%%%%%%

    }
    // return stalb_incomming_total;
}

// STALB deletion and out-of-order update scan
void HBALStore::history_pagerank_deletion_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total, int index_start, timestamp_t src_time)
{
    for (int ind_l2 = index_start; ind_l2 < l2_len; ind_l2++)
    {
        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!*IsWTS)
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei->getWTsEdge() < min_read_versions)
            {
                *IsWTS = 1;
                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                if (!check_version(dst))
                {
                    *stalb_incomming_total += outgoing_contrib->start_[dst];
                }
                else
                {
                    if (min_read_versions < ei->getInvldTime()->getWts() && src_time < ei->getInvldTime()->getSrcTime())
                    {
                        *stalb_incomming_total += outgoing_contrib->start_[dst];
                    }

                }

                // check if dst contains ooo updates
                if (check_version_ooo(dst))
                {
                    // ooo updates exist
                    // ooo updates exist
                    if (ei->getOutOfOrderUpdt()->getCount() > 1)
                    {

                        //   ArtPerIndex *art_read = ei->getOutOfOrderUpdt();
                        art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                        //%%%%%%%%%%%% adaptive radix tree scan start
                        // ei->outOfOrderUpdt.scanArtTree();
                        auto it = art_val->begin();
                        auto it_end = art_val->end();
                        for (int i = 0; it != it_end; ++i, ++it)
                        {
                            LeafNode *ln_cur = *it;
                            if (ln_cur->getWts() < min_read_versions)
                            {
                                if (ln_cur->getInvalEntry() == NULL)
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                                else
                                {
                                    if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                    {
                                        *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                    }
                                }
                            }
                        }
                        //%%%%%%% end adaptive radix tree scan
                    }
                    else
                    {
                        if (ei->getOutOfOrderUpdt()->getCount() == 1)
                        {
                            art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                            //%%%%%%%%%%%% adaptive radix tree scan start
                            // ei->outOfOrderUpdt.scanArtTree();
                            auto it = art_val->begin();
                            LeafNode *ln_cur = *it;
                            if (ln_cur->getWts() < min_read_versions)
                            {
                                if (ln_cur->getInvalEntry() == NULL)
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                                else
                                {
                                    if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                    {
                                        *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                    }
                                }
                            }
                        }
                    }
                    // ooo updates part end
                }

            }
        }
        else
        {

            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
            if (!check_version(dst))
            {
                *stalb_incomming_total += outgoing_contrib->start_[dst];
            }
            else
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                if (min_read_versions < ei->getInvldTime()->getWts() && src_time < ei->getInvldTime()->getSrcTime())
                {
                    *stalb_incomming_total += outgoing_contrib->start_[dst];
                }
            }

            // check if dst contains ooo updates
            if (check_version_ooo(dst))
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

                // ooo updates exist
                if (ei->getOutOfOrderUpdt()->getCount() > 1)
                {

                    //   ArtPerIndex *art_read = ei->getOutOfOrderUpdt();
                    art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                    //%%%%%%%%%%%% adaptive radix tree scan start
                    // ei->outOfOrderUpdt.scanArtTree();
                    auto it = art_val->begin();
                    auto it_end = art_val->end();
                    for (int i = 0; it != it_end; ++i, ++it)
                    {
                        LeafNode *ln_cur = *it;
                        if (ln_cur->getWts() < min_read_versions)
                        {
                            if (ln_cur->getInvalEntry() == NULL)
                            {
                                *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                            }
                            else
                            {
                                if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                            }
                        }
                    }
                    //%%%%%%% end adaptive radix tree scan
                }
                else
                {
                    if (ei->getOutOfOrderUpdt()->getCount() == 1)
                    {
                        art::art<LeafNode *> *art_val = ei->getOutOfOrderUpdt()->getArtTree();
                        //%%%%%%%%%%%% adaptive radix tree scan start
                        // ei->outOfOrderUpdt.scanArtTree();
                        auto it = art_val->begin();
                        LeafNode *ln_cur = *it;
                        if (ln_cur->getWts() < min_read_versions)
                        {
                            if (ln_cur->getInvalEntry() == NULL)
                            {
                                *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                            }
                            else
                            {
                                if (ln_cur->getInvalEntry()->getWts() > min_read_versions && src_time < ln_cur->getInvalEntry()->getSrcTime())
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];
                                }
                            }
                        }
                    }
                }
                // ooo updates part end
            }

        }
        /// %%%%%%%%%%% end %%%%%%%%%%
    }
}


std::unique_ptr<double[]> HBALStore::history_pagerank(uint64_t max_vertex_id, uint64_t timestamp, uint64_t num_iterations, double damping_factor, int read_version, MemoryAllocator *me)
{

    u_int64_t num_vertices = get_vertex_count(0);
    // std::cout<<"Number of vertices "<<num_vertices<<std::endl;
    max_vertex_id = get_high_water_mark();


    const double init_score = 1.0 / num_vertices;
    const double base_score = (1.0 - damping_factor) / num_vertices;
    std::unique_ptr<double[]> ptr_scores{new double[max_vertex_id]()}; // avoid memory leaks
    std::unique_ptr<uint64_t[]> ptr_degrees{new uint64_t[max_vertex_id]()}; // avoid memory leaks
    double *scores = ptr_scores.get();
    uint64_t *__restrict degrees = ptr_degrees.get();
    // std::cout<<"hello 1"<<std::endl;
    //  PerSourceVertexIndr ps;
    //std::cout<<"start degree"<<std::endl;

#pragma omp parallel for
    for (uint64_t v = 0; v < max_vertex_id; v++)
    {
        scores[v] = init_score;
        index[v].perVertexLock.lock(); ///////
        degrees[v] = perSourceGetDegree(v, read_version, me);//index[v].degree; //perSourceGetDegree(v, read_version);
        index[v].perVertexLock.unlock();
    }
    //std::cout<<"start min version"<<std::endl;

    min_read_degree_version = MaxValue;
    // std::cout<<"end min version"<<std::endl;
    gapbs::pvector<double> outgoing_contrib(max_vertex_id, 0.0);

    // pagerank iterations
    // readTable->readQuery[read_version]
    timestamp_t min_read_versions = min_read_version;
    // pagerank iterations
    if (min_read_versions == getMinReadVersion())
    {
        for (uint64_t iteration = 0; iteration < num_iterations; iteration++)
        {
            // std::cout<<"iteration # "<<iteration<<std::endl;
            double dangling_sum = 0.0;

            // for each node, precompute its contribution to all of its outgoing neighbours and, if it's a sink,
            // add its rank to the `dangling sum' (to be added to all nodes).

#pragma omp parallel for reduction(+:dangling_sum)
            for (uint64_t v = 0; v < max_vertex_id; v++)
            {
                uint64_t out_degree = degrees[v];
                if (out_degree == 0)
                { // this is a sink
                    dangling_sum += scores[v];
                }
                else
                {
                    outgoing_contrib[v] = scores[v] / out_degree;
                    //  std::cout<<outgoing_contrib[v]<<" "<<outgoing_contrib.start_[v]<<std::endl;
                }
            }

            dangling_sum /= num_vertices;

            // compute the new score for each node in the graph
#pragma omp parallel for schedule(dynamic, 64)
            for (uint64_t v = 0; v < max_vertex_id; v++)
            {

                //if(degrees[v] == std::numeric_limits<uint64_t>::max()){ continue; } // the vertex does not exist
                if (degrees[v] == 0)
                {
                    continue;
                }
                index[v].indirectionLock.lock();

                double incoming_total = 0;

                // per source STAL access

                uint8_t IsWTS = 0;
                // *IsWTS = 0;
                if (index[v].edgePtr != 0)
                {
                    if (!(index[v].edgePtr & 0x1))
                    {
                        //%%%%%%%%%%%%
                        PerSourceIndrPointer *ptr_per_src = static_cast<PerSourceIndrPointer *>(index[v].getEdgePtr());

                        int startPos = ptr_per_src->getCurPos();
                        int endPos = ((u_int64_t) 1 << ptr_per_src->getBlockLen());
                        int indexIndrArray = findIndexForIndr((ptr_per_src->getperVertexBlockArr()), startPos, endPos, timestamp);

                        u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray]);
                        EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + ((eb_block_size / 2) * 8) + ((((u_int16_t) *reinterpret_cast<uint16_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + 1)) * 8) - 8));

                        if (eih_cur->getSTime() < timestamp)
                        {
                            if (indexIndrArray != 0)
                            {
                                indexIndrArray--;
                            }
                        }
                        //fixed size block binary search
                        int indexFixedBlock = findIndexForBlockArr(ptr_per_src->getperVertexBlockArr()[indexIndrArray], ((u_int16_t) *reinterpret_cast<uint16_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + 1)), (eb_block_size / 2), timestamp);

                        /// eb block
                        EdgeInfoHal *eih_index = *reinterpret_cast<EdgeInfoHal **>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + ((eb_block_size / 2) * 8) + ((indexFixedBlock * 8) - 8));

                        if (eih_index->getSTime() < timestamp)
                        {
                            indexFixedBlock--;
                        }

                        //%%%%%%%%%%%%%%%%

                        int l1_len = (u_int64_t) 1 << ptr_per_src->getBlockLen();
                        bool flag_l1_delete = false;
                        flag_l1_delete = ptr_per_src->isDeletionBlock();

                        if (!flag_l1_delete)
                        {
                            // deletion bit per source-vertex not active direct read L2 vector
                            for (int ind_l1 = indexIndrArray; ind_l1 < l1_len; ind_l1++)
                            {
                                stalb_type *eb_k = ptr_per_src->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                uint8_t is_delete_l2_flag = 0;
                                uint8_t is_ooo_l2_flag = 0;

                                is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                is_ooo_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 4);

                                if (!is_delete_l2_flag && !is_ooo_l2_flag)
                                {
                                    // no deletion and ooo entries
                                    if (ind_l1 == indexIndrArray)
                                    {
                                        history_pagerank_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, indexFixedBlock, timestamp);
                                    }
                                    else
                                    {
                                        history_pagerank_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, 0, timestamp);

                                    }

                                }
                                else if (is_delete_l2_flag && !is_ooo_l2_flag)
                                {
                                    if (ind_l1 == indexIndrArray)
                                    {
                                        history_pagerank_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, indexFixedBlock, timestamp);
                                    }
                                    else
                                    {
                                        history_pagerank_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, 0, timestamp);

                                    }
                                }
                                else if (!is_delete_l2_flag && is_ooo_l2_flag)
                                {
                                    if (ind_l1 == indexIndrArray)
                                    {
                                        history_pagerank_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, indexFixedBlock, timestamp);
                                    }
                                    else
                                    {
                                        history_pagerank_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, 0, timestamp);

                                    }
                                }
                                else
                                {
                                    if (ind_l1 == indexIndrArray)
                                    {
                                        history_pagerank_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, indexFixedBlock, timestamp);
                                    }
                                    else
                                    {
                                        history_pagerank_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, 0, timestamp);
                                    }
                                }
                            }
                        }
                        else
                        {
                            /// %%%%%%%%%%%%%%%%% if indirection array have deletions STAL %%%%%%%%%%%%

                            for (int ind_l1 = indexIndrArray; ind_l1 < l1_len; ind_l1++)
                            {
                                if (reinterpret_cast<vertex_t>(ptr_per_src->getperVertexBlockArr()[ind_l1]) != FLAG_EMPTY_SLOT)
                                {
                                    stalb_type *eb_k = ptr_per_src->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                    int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                    uint8_t is_delete_l2_flag = 0;
                                    uint8_t is_ooo_l2_flag = 0;

                                    is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                    is_ooo_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 4);


                                    if (!is_delete_l2_flag && !is_ooo_l2_flag)
                                    {
                                        // no deletion and ooo entries
                                        if (ind_l1 == indexIndrArray)
                                        {
                                            history_pagerank_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, indexFixedBlock, timestamp);
                                        }
                                        else
                                        {
                                            history_pagerank_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, 0, timestamp);

                                        }

                                    }
                                    else if (is_delete_l2_flag && !is_ooo_l2_flag)
                                    {
                                        if (ind_l1 == indexIndrArray)
                                        {
                                            history_pagerank_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, indexFixedBlock, timestamp);
                                        }
                                        else
                                        {
                                            history_pagerank_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, 0, timestamp);

                                        }
                                    }
                                    else if (!is_delete_l2_flag && is_ooo_l2_flag)
                                    {
                                        if (ind_l1 == indexIndrArray)
                                        {
                                            history_pagerank_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, indexFixedBlock, timestamp);
                                        }
                                        else
                                        {
                                            history_pagerank_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, 0, timestamp);

                                        }
                                    }
                                    else
                                    {
                                        if (ind_l1 == indexIndrArray)
                                        {
                                            history_pagerank_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, indexFixedBlock, timestamp);
                                        }
                                        else
                                        {
                                            history_pagerank_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, 0, timestamp);
                                        }
                                    }
                                }

                            }

                            /// %%%%%%%%%%%%%%%%% end %%%%%%%%%%%%%%%%%%%%%%
                        }
                    }
                    else
                    {

                        stalb_type *eb_k = static_cast<stalb_type *>(index[v].getEdgePtr());

                        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                        uint8_t is_delete_l2_flag = 0;
                        uint8_t is_ooo_l2_flag = 0;

                        int indexFixedBlock = findIndexForBlockArr(eb_k, ((u_int16_t) *reinterpret_cast<uint16_ptr>(eb_k + 1)), (*reinterpret_cast<uint8_ptr>(eb_k) / 2), timestamp);

                        /// eb block
                        EdgeInfoHal *eih_index = *reinterpret_cast<EdgeInfoHal **>(eb_k + ((*reinterpret_cast<uint8_ptr>(eb_k) / 2) * 8) + ((indexFixedBlock * 8) - 8));

                        if (eih_index->getSTime() < timestamp)
                        {
                            indexFixedBlock--;
                        }

                        is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                        is_ooo_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 4);

                        if (!is_delete_l2_flag && !is_ooo_l2_flag)
                        {
                            // no deletion and ooo entries
                            history_pagerank_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, indexFixedBlock, timestamp);

                        }
                        else if (is_delete_l2_flag && !is_ooo_l2_flag)
                        {
                            history_pagerank_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, indexFixedBlock, timestamp);
                        }
                        else if (!is_delete_l2_flag && is_ooo_l2_flag)
                        {
                            history_pagerank_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, indexFixedBlock, timestamp);
                            //// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% out of order update exist

                        }
                        else
                        {
                            history_pagerank_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total, indexFixedBlock, timestamp);
                        }
                        // #### per source edge reading end
                    }
                }

                // STAL end

                // update the score
                // std::cout<<"incoming_total "<<incoming_total<<std::endl;
                scores[v] = base_score + damping_factor * (incoming_total + dangling_sum);
                index[v].indirectionLock.unlock();

            }

        }
    }
    // std::cout<<"total deletion "<<total_deletion<<" "<<false_deletion<<std::endl;
    //std::cout<<"hello 2"<<std::endl;
    //   std::cout<<"iteration end"<<std::endl;

    return ptr_scores;
}