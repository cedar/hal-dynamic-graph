//
// Created by Ghufran on 25/01/2023.
//
// convertion of key to 64 bit
#include "VertexBlock.h"
#include <iostream>

// each F contains 4 bit

/* PerSourceIndrPointer*/

void PerSourceIndrPointer::setBlockLen(uint8_t blen)
{
    blockLen = blen;
}

int PerSourceIndrPointer::addNewBlock(stalb_type *tb)
{
    blockArr[--curPos] = tb;
    return curPos;
    //    std::cout<<"hello";
}

void PerSourceIndrPointer::setCurPos(int curPos)
{
    this->curPos = curPos;
}

int PerSourceIndrPointer::getCurPos() const
{
    return curPos;
}

void PerSourceIndrPointer::initBlockSize()
{
    blockLen = log2(1);
}

void PerSourceIndrPointer::setPerVertexBlockArr(stalb_type **newArr)
{
    blockArr = newArr;
}

stalb_type **PerSourceIndrPointer::getperVertexBlockArr()
{
    return blockArr;
}

uint8_t PerSourceIndrPointer::getBlockLen()
{
    return blockLen;
}

void PerSourceIndrPointer::setEmptySpace()
{
    emptySpace++;
}

int PerSourceIndrPointer::getEmptySpace()
{
    return emptySpace;
}

void PerSourceIndrPointer::setDeletionBlock(bool d)
{
    is_deletion_block = d;
}

bool PerSourceIndrPointer::isDeletionBlock()
{
    return is_deletion_block;
}

void PerSourceIndrPointer::setIndexList(MemoryAllocator *me, int thread_id)
{
    if (blockArr == NULL)
    {
        // blockLen = 1;
        u_int64_t block_l = 2; //((u_int64_t) 1 << blockLen);
        curPos = fixedSize * block_l;
        blockArr = static_cast<stalb_type **>(me->Allocate(8 * (block_l * fixedSize), thread_id));///static_cast<EdgeBlock **>(aligned_alloc(8,8 * (blockLen * fixedSize)));//static_cast<EdgeBlock **>(malloc(8 * (blockLen * fixedSize)));
        memset(blockArr, 0, 8 * (block_l * fixedSize));
        blockLen = 1;
        /*
          for(int i=0;i<(blockLen * fixedSize);i++)
          {
              blockArr[i] = NULL;
          }

          */
    }
    else
    {
        // downsize_lock.lock();

        u_int64_t blenprev = ((u_int64_t) 1 << blockLen);
        u_int64_t b_len_new = 2 * blenprev;

        stalb_type **temp = static_cast<stalb_type **>(me->Allocate(8 * ((b_len_new) * fixedSize), thread_id));
        memset(temp, 0, 8 * ((b_len_new) * fixedSize));

        //  FixedTimeIndexBlock **temp2;
        // todo deletion mechanism ()create node in globle linklist and add the address of deleted index array and with time to replace
        // the garbage collector will deallocate the memory than
        int k = 0;
        int tempC = (blenprev) * fixedSize;

        memcpy(temp + tempC, blockArr, tempC * 8);
        curPos = tempC;
        void *ptr = blockArr;
        blockArr = temp;

        me->free(ptr, (blenprev * fixedSize * 8), thread_id);
        //  free(ptr);
        // blenprev++;
        blockLen = log2(b_len_new);

        //  downsize_lock.unlock();

    }
}

/* PerSourceVertexIndr class defination */
void PerSourceVertexIndr::setEdgePtr(vertex_t ptr, bool flag)
{
    if (flag)
    {
        edgePtr = reinterpret_cast<vertex_t>(ptr |= 0x1);
    }
    else
    {
        edgePtr = reinterpret_cast<vertex_t>(ptr &= ~0x1);
    }
}

void *PerSourceVertexIndr::getEdgePtr()
{
    return reinterpret_cast<void *>(get_pointer(edgePtr));
}

/* Degree class */
void PerSourceVertexIndr::setDegreePtr(vertex_t ptr, bool flag)
{
    if (flag)
    {
        degree = reinterpret_cast<vertex_t>(ptr |= 0x1);
    }
    else
    {
        degree = reinterpret_cast<vertex_t>(ptr &= ~0x1);
    }
}

void *PerSourceVertexIndr::getDegreePtr()
{
    return reinterpret_cast<void *>(get_pointer(degree));
}

/* */


void PerSourceVertexIndr::setPerVertexLock()
{
    //lock.lock();
}

void PerSourceVertexIndr::unsetPerVertexLock()
{
    // lock.unlock();
}


u_int64_t PerSourceVertexIndr::hash(u_int64_t key, u_int64_t size)
{
    return key % size;
}

inline int PerSourceVertexIndr::findIndexForIndr(stalb_type **arr, int start, int n, timestamp_t K)
{
    // Lower and upper bounds
    int end = n - 1;
    // Traverse the search space
    timestamp_t eih_cur_time = 0;
    timestamp_t eih_end_time = 0;
    while (start <= end)
    {
        int mid = (start + end) / 2;
        if (!check_version(reinterpret_cast<vertex_t>(arr[mid])))
        {
            u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(arr[mid]);
            EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(arr[mid] + ((eb_block_size / 2) * 8) + (((*reinterpret_cast<uint16_ptr>(arr[mid] + 1)) * 8) - 8));
            EdgeInfoHal *eih_end = *reinterpret_cast<EdgeInfoHal **>(arr[mid] + ((eb_block_size / 2) * 8) + ((((eb_block_size / 2) - 1) * 8) - 8));
            eih_cur_time = eih_cur->getSTime();
            eih_end_time = eih_end->getSTime();
        }
        else
        {
            //
            eih_cur_time = remove_version(reinterpret_cast<vertex_t>(arr[mid]));
            eih_end_time = eih_cur_time;
        }
        if (eih_cur_time > K && eih_end_time < K)
            return mid;
        else if (eih_cur_time > K)
            start = mid + 1;
        else
            end = mid - 1;
    }
    // Return insert position
    return end;
}

inline int PerSourceVertexIndr::findIndexForBlockArr(stalb_type *arr, int start, int eb_block_size, timestamp_t K)
{
    // Lower and upper bounds
    int end = eb_block_size - 1;
    // Traverse the search space
    while (start <= end)
    {
        int mid = (start + end) / 2;
        // If K is found
        EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(arr + ((eb_block_size) * 8) + ((mid * 8) - 8)); // %%%% start here

        if (eih_cur->getSTime() == K)
            return mid;
        else if (eih_cur->getSTime() > K)
            start = mid + 1;
        else
            end = mid - 1;
    }
    // Return insert position
    return end;
}

ArtPerIndex *PerSourceVertexIndr::getOutOfOrderDestNode(timestamp_t srctimestamp, bool isIndrArray, void *edgePtr)
{

    if (isIndrArray)
    {
        PerSourceIndrPointer *ptr_edge = static_cast<PerSourceIndrPointer *>(edgePtr);
        int startPos = ptr_edge->getCurPos();
        int endPos = ((u_int64_t) 1 << ptr_edge->getBlockLen());
        int indexIndrArray = findIndexForIndr(ptr_edge->getperVertexBlockArr(), startPos, endPos, srctimestamp);
        u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_edge->getperVertexBlockArr()[indexIndrArray]);

        int indexFixedBlock = findIndexForBlockArr(ptr_edge->getperVertexBlockArr()[indexIndrArray], ((u_int16_t) *reinterpret_cast<uint16_ptr>(ptr_edge->getperVertexBlockArr()[indexIndrArray] + 1)), (eb_block_size / 2), srctimestamp);

        /// eb block
        EdgeInfoHal *eih_index = *reinterpret_cast<EdgeInfoHal **>(ptr_edge->getperVertexBlockArr()[indexIndrArray] + ((eb_block_size / 2) * 8) + ((indexFixedBlock * 8) - 8));
        // ArtPerIndex *perArt = eih_index->getOutOfOrderUpdt();

        // LeafNode *leaf = perArt->getLeaf(srctimestamp);
        return eih_index->getOutOfOrderUpdt();
        // findIndexForBlockArr(stalb_type *arr, int start, int eb_block_size, timestamp_t K)
    }
    else
    {

        stalb_type *ptr_edge = static_cast<stalb_type *>(edgePtr);

        u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_edge);

        int indexFixedBlock = findIndexForBlockArr(ptr_edge, ((u_int16_t) *reinterpret_cast<uint16_ptr>(ptr_edge + 1)), (eb_block_size / 2), srctimestamp);

        /// eb block
        EdgeInfoHal *eih_index = *reinterpret_cast<EdgeInfoHal **>(ptr_edge + ((eb_block_size / 2) * 8) + ((indexFixedBlock * 8) - 8));

        // ArtPerIndex *perArt = eih_index->getOutOfOrderUpdt();

        // LeafNode *leaf = perArt->getLeaf(srctimestamp);

        return eih_index->getOutOfOrderUpdt();

    }

}

void PerSourceVertexIndr::resizeHashTable(int requested_block, MemoryAllocator *la, int thread_id)
{

    // HashTablePerSource* oldArr = hashPtr;

    u_int32_t oldCap = hashPtr->hashBlockSize;

    // capacity =requested_block;
    // u_int64_t* newHashTable = static_cast<u_int64_t *>(la->Allocate(sizeof(HashTablePerSource), thread_id));

    u_int64_t *newHashTable = static_cast<u_int64_t *>(la->Allocate(sizeof(u_int64_t) * requested_block, thread_id));

    memset(newHashTable, FLAG_EMPTY_SLOT, requested_block * sizeof(u_int64_t));    //reset new array

    // newHashTable->hashBlockSize = requested_block;

    for (u_int32_t i = 0; i < oldCap; i++)
    {
        u_int64_t adj_p = hashPtr->hashtable[i];
        if (adj_p < FLAG_TOMB_STONE)
        {
            //oldArr->hashtable[i]
            u_int64_t is_ooo = getBitMask(adj_p, IsOutOrderStart, IsOutOrderBits);
            if (!is_ooo)
            {
                if (!(edgePtr & 0x1))
                {

                    PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

                    u_int64_t indr_index = getBitMask(adj_p, L1IndexStart, L1IndexBits);
                    u_int64_t indr_size = (u_int64_t) 1 << getBitMask(adj_p, L1SizeStart, L1SizeBits);

                    // lock on per source vertex to get the index_block edge block
                    u_int64_t index_block = (((u_int64_t) 1 << ptr_ind->getBlockLen()) - (indr_size - indr_index));

                    u_int64_t edge_block_index = getBitMask(adj_p, L2IndexStart, L2IndexBits);

                    u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(adj_p, L2SizeStart, L2SizeBits);
                    // per block deletion lock

                    u_int64_t cur_edge_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_ind->getperVertexBlockArr()[index_block]); //->getBlockSize();

                    u_int64_t index_edge_block = ((cur_edge_block_size / 2) - ((edge_block_size / 2) - edge_block_index));

                    //
                    //  *reinterpret_cast<uint64_ptr>(ptr_ind->getperVertexBlockArr()[index_block] + ((index_edge_block - 1) * 8))
                    //  ptr_ind->getperVertexBlockArr()[index_block]->getDestIdArr()[index_edge_block]

                    //
                    size_t index = hash(*reinterpret_cast<uint64_ptr>(ptr_ind->getperVertexBlockArr()[index_block] + ((index_edge_block) * 8)), requested_block);

                    while (newHashTable[index] != FLAG_EMPTY_SLOT)
                    {
                        index++;
                        if (index >= requested_block)
                        {
                            index = 0;
                        }

                    }
                    newHashTable[index] = adj_p;
                }
                else
                {
                    // only edge block ... degree < 1024
                    stalb_type *ptr_edge;

                    ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(edgePtr));

                    u_int64_t edge_block_index = getBitMask(adj_p, L2IndexStart, L2IndexBits);

                    u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(adj_p, L2SizeStart, L2SizeBits);
                    // per block deletion lock

                    u_int64_t cur_edge_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_edge);

                    u_int64_t index_edge_block = ((cur_edge_block_size / 2) - ((edge_block_size / 2) - edge_block_index));

                    size_t index = hash(*reinterpret_cast<uint64_ptr>(ptr_edge + ((index_edge_block) * 8)), requested_block); //   /->getEdgeUpdate()[index_edge_block]->getDestId(),requested_block);

                    while (newHashTable[index] != FLAG_EMPTY_SLOT)
                    {
                        index++;
                        if (index >= requested_block)
                        {
                            index = 0;
                        }

                    }
                    newHashTable[index] = adj_p;
                }
            }
            else
            {
                // out-of-order update
                if (!(edgePtr & 0x1))
                {

                    PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

                    timestamp_t src_timestamp = getBitMask(adj_p, t_s_start, t_s_bits);

                    ArtPerIndex *perArt = getOutOfOrderDestNode(src_timestamp, 1, ptr_ind);

                    LeafNode *leaf = perArt->getLeaf(src_timestamp);

                    // per block deletion lock
                    // if (leaf->getDNode() == dest_node)
                    //{

                    size_t index = hash(leaf->getDNode(), requested_block);

                    while (newHashTable[index] != FLAG_EMPTY_SLOT)
                    {
                        index++;
                        if (index >= requested_block)
                        {
                            index = 0;
                        }
                    }
                    newHashTable[index] = adj_p;
                }
                else
                {
                    // only edge block ... degree < 1024
                    stalb_type *ptr_edge;

                    ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(edgePtr));

                    timestamp_t src_timestamp = getBitMask(adj_p, t_s_start, t_s_bits);

                    ArtPerIndex *perArt = getOutOfOrderDestNode(src_timestamp, 0, ptr_edge);

                    LeafNode *leaf = perArt->getLeaf(src_timestamp);

                    // per block deletion lock
                    // if (leaf->getDNode() == dest_node)
                    //{

                    size_t index = hash(leaf->getDNode(), requested_block);

                    while (newHashTable[index] != FLAG_EMPTY_SLOT)
                    {
                        index++;
                        if (index >= requested_block)
                        {
                            index = 0;
                        }

                    }
                    newHashTable[index] = adj_p;
                }


            }
        }
    }

    u_int64_t *oldArr = hashPtr->hashtable;

    hashPtr->hashBlockSize = requested_block;
    hashPtr->hashtable = newHashTable;

    la->free(oldArr, oldCap * 8, thread_id);
    // for resize everytime you need to move to the STAL to get dest node and store it in the array

}

void PerSourceVertexIndr::setDestNodeHashTableVal(vertex_t dest_node, u_int64_t adj_pos, MemoryAllocator *la, int thread_id)
{

    if (hashPtr->entryCounter >= ((u_int32_t) hashPtr->hashBlockSize >> 1))
    {
        resizeHashTable(hashPtr->hashBlockSize * 2, la, thread_id);
    }

    hashPtr->entryCounter++;

    size_t index = hash(dest_node, hashPtr->hashBlockSize);
//int count =0;
    while (hashPtr->hashtable[index] != FLAG_EMPTY_SLOT && hashPtr->hashtable[index] != FLAG_TOMB_STONE)
    {
        //count++;
        index++;
        if (index >= hashPtr->hashBlockSize)
        {
            index = 0;
        }

    }
    hashPtr->hashtable[index] = adj_pos;
}


void PerSourceVertexIndr::removeDestNodeHashTableVal(vertex_t dest_node)
{

    // get the dest node
    u_int64_t index = hash(dest_node, hashPtr->hashBlockSize);
    bool isEntryExist = false;
    // int count = 0;
    while (hashPtr->hashtable[index] != FLAG_EMPTY_SLOT)
    {
        // count++;
        if (hashPtr->hashtable[index] != FLAG_TOMB_STONE)
        {

            u_int64_t pos_adj = hashPtr->hashtable[index];
            // get pos_adj bit position to compare
            //
            u_int64_t is_ooo = getBitMask(pos_adj, IsOutOrderStart, IsOutOrderBits);
            if (!is_ooo)
            {
                if (((u_int64_t) (dest_node & thread_prefix)) == getBitMask(pos_adj, DNPStart, DNPBits))
                {

                    //u_int64_t is_stal = getBitMask(pos_adj, IsSTALStart, IsSTALBits);

                    if (!(edgePtr & 0x1))
                    {

                        PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

                        u_int64_t indr_index = getBitMask(pos_adj, L1IndexStart, L1IndexBits);
                        u_int64_t indr_size = (u_int64_t) 1 << getBitMask(pos_adj, L1SizeStart, L1SizeBits);

                        // lock on per source vertex to get the index_block edge block
                        u_int64_t index_block = (((u_int64_t) 1 << ptr_ind->getBlockLen()) - (indr_size - indr_index));

                        u_int64_t edge_block_index = getBitMask(pos_adj, L2IndexStart, L2IndexBits);

                        u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(pos_adj, L2SizeStart, L2SizeBits);
                        // per block deletion lock

                        u_int64_t cur_edge_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_ind->getperVertexBlockArr()[index_block]); //->getBlockSize();

                        u_int64_t index_edge_block = ((cur_edge_block_size / 2) - ((edge_block_size / 2) - edge_block_index));


                        // per block deletion lock
                        if (*reinterpret_cast<uint64_ptr>(ptr_ind->getperVertexBlockArr()[index_block] + ((index_edge_block) * 8)) != FLAG_TOMB_STONE)
                        {

                            if (*reinterpret_cast<uint64_ptr>(ptr_ind->getperVertexBlockArr()[index_block] + ((index_edge_block) * 8)) == dest_node)
                            {
                                hashPtr->hashtable[index] = FLAG_TOMB_STONE;
                                isEntryExist = true;
                                hashPtr->entryCounter--;
                                break;
                            }
                        }

                    }
                    else
                    {
                        // edge block index remove
                        stalb_type *ptr_edge;
                        /*  if (degree > (((uint32_t) 1 << StartFixedBlockThreshold) - 1))
                          {
                              PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

                              ptr_edge = static_cast<stalb_type *>(ptr_ind->getperVertexBlockArr()[(
                                      ((u_int64_t) 1 << ptr_ind->getBlockLen()) - 1)]);
                          }
                          else
                          {*/
                        ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(edgePtr));
                        //  }

                        u_int64_t edge_block_index = getBitMask(pos_adj, L2IndexStart, L2IndexBits);

                        u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(pos_adj, L2SizeStart, L2SizeBits);

                        // per block deletion lock
                        u_int64_t cur_edge_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_edge);

                        u_int64_t index_edge_block = ((cur_edge_block_size / 2) - ((edge_block_size / 2) - edge_block_index));

                        if (*reinterpret_cast<uint64_ptr>(ptr_edge + ((index_edge_block) * 8)) != FLAG_TOMB_STONE)
                        {
                            if (*reinterpret_cast<uint64_ptr>(ptr_edge + ((index_edge_block) * 8)) == dest_node)
                            {
                                hashPtr->hashtable[index] = FLAG_TOMB_STONE;
                                isEntryExist = true;
                                hashPtr->entryCounter--;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                // ooo part
                // out-of-order update part
                if (((u_int64_t) (dest_node & thread_prefix_OOO)) == getBitMask(pos_adj, DNPStart_OOO, DNP_ooo_bits))
                {

                    //u_int64_t is_stal = getBitMask(pos_adj, IsSTALStart, IsSTALBits);

                    // issue is here degree > ((uint32_t) 1 << StartFixedBlockThreshold)-1)
                    if (!(edgePtr & 0x1))
                    {
                        // should see the degree part issue is upward

                        //

                        PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

                        timestamp_t src_timestamp = getBitMask(pos_adj, t_s_start, t_s_bits);

                        ArtPerIndex *perArt = getOutOfOrderDestNode(src_timestamp, 1, ptr_ind);

                        LeafNode *leaf = perArt->getLeaf(src_timestamp);

                        // per block deletion lock
                        if (leaf->getDNode() == dest_node)
                        {
                            hashPtr->hashtable[index] = FLAG_TOMB_STONE;
                            isEntryExist = true;
                            hashPtr->entryCounter--;
                            break;
                        }

                    }
                    else
                    {
                        // edge block entry
                        // edge block index remove


                        // only edge block ... degree < 1024
                        stalb_type *ptr_edge;

                        ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(edgePtr));

                        timestamp_t src_timestamp = getBitMask(pos_adj, t_s_start, t_s_bits);

                        ArtPerIndex *perArt = getOutOfOrderDestNode(src_timestamp, 0, ptr_edge);

                        LeafNode *leaf = perArt->getLeaf(src_timestamp);

                        // per block deletion lock
                        if (leaf->getDNode() == dest_node)
                        {
                            hashPtr->hashtable[index] = FLAG_TOMB_STONE;
                            isEntryExist = true;
                            hashPtr->entryCounter--;
                            break;
                        }
                    }
                }
            }
        }
        index = index + 1;
        if (index >= hashPtr->hashBlockSize)
        {
            index = 0;
        }


    }
    if (!isEntryExist)
    {
        std::cout << "Entry does not exist: " << std::endl;
    }

}

AdjacentPos *PerSourceVertexIndr::getDestNodeHashTableVal(vertex_t dest_node, AdjacentPos *ap)
{

    // get the dest node
    size_t index = hash(dest_node, hashPtr->hashBlockSize);
    bool isEntryExist = false;
    //  int count = 0;
    while (hashPtr->hashtable[index] != FLAG_EMPTY_SLOT)
    {
        //   count++;
        if (hashPtr->hashtable[index] != FLAG_TOMB_STONE)
        {

            u_int64_t pos_adj = hashPtr->hashtable[index];
            // get pos_adj bit position to compare
            u_int64_t is_ooo = getBitMask(pos_adj, IsOutOrderStart, IsOutOrderBits);
            if (!is_ooo)
            {
                if (((u_int64_t) (dest_node & thread_prefix)) == getBitMask(pos_adj, DNPStart, DNPBits))
                {

                    u_int64_t is_stal = getBitMask(pos_adj, IsSTALStart, IsSTALBits);

                    // issue is here degree > ((uint32_t) 1 << StartFixedBlockThreshold)-1)
                    if (!(edgePtr & 0x1))
                    {
                        // should see the degree part issue is upward
                        PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

                        u_int64_t indr_index = getBitMask(pos_adj, L1IndexStart, L1IndexBits);
                        u_int64_t indr_size = (u_int64_t) 1 << getBitMask(pos_adj, L1SizeStart, L1SizeBits);

                        // lock on per source vertex to get the index_block edge block
                        u_int64_t index_block = (((u_int64_t) 1 << ptr_ind->getBlockLen()) - (indr_size - indr_index));

                        u_int64_t edge_block_index = getBitMask(pos_adj, L2IndexStart, L2IndexBits);

                        u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(pos_adj, L2SizeStart, L2SizeBits);
                        // per block deletion lock

                        u_int64_t cur_edge_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_ind->getperVertexBlockArr()[index_block]); //->getBlockSize();

                        u_int64_t index_edge_block = ((cur_edge_block_size / 2) - ((edge_block_size / 2) - edge_block_index));


                        // per block deletion lock
                        if (*reinterpret_cast<uint64_ptr>(ptr_ind->getperVertexBlockArr()[index_block] + ((index_edge_block) * 8)) != FLAG_TOMB_STONE)
                        {

                            if (*reinterpret_cast<uint64_ptr>(ptr_ind->getperVertexBlockArr()[index_block] + ((index_edge_block) * 8)) == dest_node)
                            {
                                ap->indrIndex = index_block;
                                ap->edgeBlockIndex = index_edge_block;
                                ap->hashTableIndex = index;
                                return ap;
                            }
                        }
                    }
                    else
                    {
                        // edge block entry
                        // edge block index remove
                        stalb_type *ptr_edge;
                        PerSourceIndrPointer *ptr_ind;
                        /*bool flagp = 0;
                        if(degree > (((uint32_t) 1 << StartFixedBlockThreshold)-1))
                        {
                            ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

                            ptr_edge = static_cast<stalb_type *>(ptr_ind->getperVertexBlockArr()[(((u_int64_t) 1 << ptr_ind->getBlockLen()) - 1)]);
                            flagp = 1;
                        }
                        else
                        {*/
                        ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(edgePtr));
                        //   flagp = 0;
                        // }

                        u_int64_t edge_block_index = getBitMask(pos_adj, L2IndexStart, L2IndexBits);

                        u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(pos_adj, L2SizeStart, L2SizeBits);

                        // per block deletion lock
                        u_int64_t cur_edge_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_edge);

                        u_int64_t index_edge_block = ((cur_edge_block_size / 2) - ((edge_block_size / 2) - edge_block_index));

                        if (*reinterpret_cast<uint64_ptr>(ptr_edge + ((index_edge_block) * 8)) != FLAG_TOMB_STONE)
                        {
                            if (*reinterpret_cast<uint64_ptr>(ptr_edge + ((index_edge_block) * 8)) == dest_node)
                            {

                                ap->edgeBlockIndex = index_edge_block;
                                ap->hashTableIndex = index;

                                return ap;
                            }
                        }
                    }
                }
            }
            else
            {
                // out-of-order update part
                if (((u_int64_t) (dest_node & thread_prefix_OOO)) == getBitMask(pos_adj, DNPStart_OOO, DNP_ooo_bits))
                {

                    //u_int64_t is_stal = getBitMask(pos_adj, IsSTALStart, IsSTALBits);

                    // issue is here degree > ((uint32_t) 1 << StartFixedBlockThreshold)-1)
                    if (!(edgePtr & 0x1))
                    {
                        // should see the degree part issue is upward

                        //

                        PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

                        timestamp_t src_timestamp = getBitMask(pos_adj, t_s_start, t_s_bits);

                        //ArtPerIndex *dest_id_ooo = getOutOfOrderDestNode(src_timestamp, 1,  ptr_ind);
                        ArtPerIndex *perArt = getOutOfOrderDestNode(src_timestamp, 1, ptr_ind);

                        LeafNode *leaf = perArt->getLeaf(src_timestamp);

                        // per block deletion lock
                        if (leaf->getDNode() == dest_node)
                        {
                            ap->indrIndex = add_version(reinterpret_cast<vertex_t>(perArt));
                            ap->edgeBlockIndex = add_version(reinterpret_cast<vertex_t>(leaf));;
                            ap->hashTableIndex = index;
                            return ap;
                        }

                    }
                    else
                    {
                        // edge block entry
                        // edge block index remove


                        // only edge block ... degree < 1024
                        stalb_type *ptr_edge;

                        ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(edgePtr));

                        timestamp_t src_timestamp = getBitMask(pos_adj, t_s_start, t_s_bits);

                        ArtPerIndex *perArt = getOutOfOrderDestNode(src_timestamp, 0, ptr_edge);

                        LeafNode *leaf = perArt->getLeaf(src_timestamp);

                        // per block deletion lock
                        if (leaf->getDNode() == dest_node)
                        {
                            ap->indrIndex = add_version(reinterpret_cast<vertex_t>(perArt));
                            ap->edgeBlockIndex = add_version(reinterpret_cast<vertex_t>(leaf));;
                            ap->hashTableIndex = index;
                            return ap;
                        }
                    }
                }
            }
        }

        index = index + 1;
        if (index >= hashPtr->hashBlockSize)
        {
            index = 0;
        }
    }

    return ap;

}

void PerSourceVertexIndr::initHashTable(MemoryAllocator *la, int thread_id)
{

    hashPtr = static_cast<HashTablePerSource *>(la->Allocate(sizeof(HashTablePerSource), thread_id));
    hashPtr->hashBlockSize = 2;

    hashPtr->hashtable = static_cast<u_int64_t *>(la->Allocate(16, thread_id));
    memset(hashPtr->hashtable, FLAG_EMPTY_SLOT, sizeof(uint64_t) * hashPtr->hashBlockSize);

    hashPtr->entryCounter = 0;
    //new (hashPtr) HashTableIndex(la,thread_id);

    //new HashTableIndex(la);
}

void PerSourceVertexIndr::setLatestSrcTime(timestamp_t lct)
{
    latestSrcTime = lct;
}

timestamp_t PerSourceVertexIndr::getLatestSrcTime()
{
    return latestSrcTime;
}

HashTablePerSource *PerSourceVertexIndr::getHashTable()
{
    return hashPtr;
}
