//
// Created by Ghufran on 01/08/2022.
//

#ifndef OUR_IDEA_BLOCKADJACENCYLIST_H
#define OUR_IDEA_BLOCKADJACENCYLIST_H

#include<vector>
#include <map>
#include <unordered_map>
#include "../ThirdParty/Key.h"
#include "../ThirdParty/ART/LeafNode.h"
#include "../ThirdParty/ART/Tree.h"
#include "../MemoryPool/BlockAllocator.hpp"
#include "../ThirdParty/RWSpinLock.h"
#include "../ThirdParty/ART-2/include/art.hpp"


/* HAL */
class EdgeBlock;

// Root Adaptive radix tree for out-of-order update
// Adaptive radix tree operation class
class ArtPerIndex
{
public:
    // convertion of key to 64 bit
    void static loadKey(int64_t tid, Key &key);

    // insert Adjacency Edge Metadata
    void insertArtLeaf(LeafNode *ln);

    void removeArtLeaf(timestamp_t src_time);

    // initialize art of specfic index for per source adjacency list
    void initArtTree();

    // get the Adjacent Edge Metadata in other word leaf
    LeafNode *getLeaf(timestamp_t src_time);

    art::art<LeafNode *> *getArtTree() const;

    void increCount() { count++; }

    void decCount() { count--; }

    uint32_t getCount() { return count; }

    void setLock() { artLock.lock(); }

    void unsetLock() { artLock.unlock(); }

private:
    art::art<LeafNode *> *artTree;
    uint32_t count;
    spinlock artLock;
};

// In order Adjacency Edge Metadata

class EdgeInfoHal
{
public:

    EdgeInfoHal();

    void initEdgeInfoHal()
    {
        sTime = 0;
        invld_time = NULL;
        wTs_edge = 0;
        outOfOrderUpdt = 0;
    }

    void setSTime(timestamp_t stime);

    void setDestId(vertex_t dest);

    void setOutOfOrderUpdt(ArtPerIndex *outOfOrderUpdt);

    ArtPerIndex *getOutOfOrderUpdt();

    void setInvldTime(ITM *de);

    void setWTsEdge(timestamp_t wtsE);

    ITM *getInvldTime();

    timestamp_t getWTsEdge();

    vertex_t getDestId() const;

    timestamp_t getSTime() const;

private:
    timestamp_t sTime;
    ITM *invld_time;
    timestamp_t wTs_edge;
    ArtPerIndex *outOfOrderUpdt;
};

// for hashtable adjacency edge metadata address storage
// if whichAem is 0 means its out-of-order (LeafNode) Aem block otherwise its in order Aem block (EdgeInfoHal)


// sequential adjacency block vector
class EdgeBlock
{

public:
    void initEdgeBlock(MemoryAllocator *me, int thread_id, int16_t prev_block_size);

    void UpEdgeBlock(MemoryAllocator *me, int thread_id, uint8_t cur_block_size);

    void setEdgeUpdt(EdgeInfoHal *e, int64_t dest_node, double w);

    void setCurIndex(int16_t b);

    void resizeEdgeBlock(MemoryAllocator *me, int thread_id, uint8_t block_size);

    void setIsDeletion(bool del);

    void setIsOutOfOrder(bool ooo);

    void setBlockSize(u_int8_t bsize);

    void setEmptySpace();

    EdgeInfoHal **getEdgeUpdate();

    vertex_t *getDestIdArr();

    double *getpropertyIdArr();

    uint8_t getBlockSize();

    int16_t getEmptySpace();

    int16_t getCurIndex() const;

    bool getIsDeletion();

    bool getIsOutOfOrder();

    void lock();

    void unlock();

private:
    EdgeInfoHal **edgeUpdt;
    vertex_t *destId;
    double *property;
    int16_t curIndx;
    bool isDeletion;
    bool isOutOfOrder;
    uint8_t blockSize;
    spinlock perBlockLock;
    // 1 byte atomic can use
    int16_t emptySpace;

};

struct AemBlockAddress
{
    bool whichAem;
    union
    {
        EdgeInfoHal *edgeHal;
        LeafNode *leafNode;
    } AemSet;
    int blockIndex;
    // blockIndex very important index to set deletion bit
};
#endif //OUR_IDEA_BLOCKADJACENCYLIST_H
