//
// Created by Ghufran on 13/10/2022.
//

#ifndef OUR_IDEA_THREAD1_H
#define OUR_IDEA_THREAD1_H
#include <thread>
std::mutex lock;
int size = 10;
int sum = 0;
int numberThread = 5;
int arr[10];
void arraySum(int id,int range_start, int range_end)
{
    int local_sum = 0;
    for(int i=range_start;i<range_end;i++)
    {
        local_sum += arr[i];
    }
    lock.lock();
    sum += local_sum;
    lock.unlock();
}
void printLabels(int id)
{
    lock.lock();
    std::cout << id<<std::endl;
    lock.unlock();
}

void startThread()
{
    for(int i=0;i<10;i++)
    {
        arr[i] = 1;
    }
    std::thread t1[5];
    int total_work_dest = size/numberThread;
    for(int i=0;i<5;i++)
    {
        t1[i] = std::thread(arraySum,i,i*total_work_dest,(i+1)*total_work_dest);
    }
    for(int i=0;i<5;i++)
    {
        t1[i].join();
    }
    std::cout<<" sum is: "<< sum;

}
#endif //OUR_IDEA_THREAD1_H
