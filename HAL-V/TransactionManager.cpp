//
// Created by Ghufran on 02/02/2023.
//

#include "TransactionManager.h"

HBALStore *TransactionManager::graph = NULL;
MemoryAllocator *TransactionManager::memAllocator = NULL;
size_t thread_local TransactionManager::thread_id = 0;

TransactionManager::TransactionManager(int initial_space, int no_of_thread)
{
    // EMPTY_SLOT = (void*) new EdgeBlock();
    // FLAG_DELETE = (void*) new EdgeBlock();
    memAllocator = new MemoryAllocator();
    memAllocator->init(initial_space, no_of_thread);

    graph = new HBALStore();
    graph->invalidBlock = NULL;
    HBALStore::isReadActive = false;

    graph->numberOfThread = no_of_thread;
    graph->perThreadNumberOfEdges = static_cast<u_int64_t *>(malloc(no_of_thread * 8));
    graph->perThreadNumberOfvertices = static_cast<u_int64_t *>(malloc(no_of_thread * 8));

    graph->invalidListDegree = static_cast<InvalidNodeAddress **>(malloc(THRESHOLDINVALID * 8));
    // graph->InvalidEdgeInfo =  static_cast<InvalidEdgeInfoAddress **>(malloc(THRESHOLDINVALID * 8));
#ifdef DBUGFLAGG
    graph->foutput.open (LOGFILEPATH,std::ios::out);
    if(!graph->foutput){std::cout<<"error open file";}
#endif
#ifdef DBUGFLAGPERSOURCE
    graph->foutput.open (LOGFILEPATH,std::ios::out);
    if(!graph->foutput){std::cout<<"error open file";}
#endif
    graph->min_read_version = MaxValue;
    graph->min_read_degree_version = MaxValue;
    graph->readTable = new ReadTable();

    graph->readTable->initReadTable();

    // graph->invalList = reinterpret_cast<InvalidList **>(std::aligned_alloc(8, no_of_thread*sizeof(InvalidList)));

    for (int i = 0; i < no_of_thread; i++)
    {
        //graph->invalList[i] = new InvalidListVal();
        // graph->invalList[i]->edgeInfo = NULL;
        //  graph->invalList[i]->leafInfo = NULL;
        graph->perThreadNumberOfEdges[i] = 0;
        graph->perThreadNumberOfvertices[i] = 0;
    }

    for (int j = 0; j < THRESHOLDINVALID; j++)
    {
        graph->invalidListDegree[j] = new InvalidNodeAddress();
        // graph->InvalidEdgeInfo[j]   = new InvalidEdgeInfoAddress();
    }

    // HBALStore::logical_to_physical
    HBALStore::initVertexIndrList(memAllocator);


}

HBALStore *TransactionManager::getGraphDataStore(bool isRead, int version_read)
{
    if (!isRead)
    {
        return graph;
    }
    else
    {
        HBALStore::isReadActive = true;
        timestamp_t time_stamp_read = getCurrTimeStamp();
        graph->min_read_version = time_stamp_read;
        graph->min_read_degree_version = time_stamp_read;

        return graph;
    }
}

void TransactionManager::addReadThread(int id)
{
    thread_id = id;
}

void TransactionManager::finishTransaction()
{
    //graph->readTable->lock.lock();
    // HBALStore::querylock.lock();

    //graph->readTable->readQuery[thread_id] = std::numeric_limits<timestamp_t>::max();
    graph->min_read_version = MaxValue;
    graph->min_read_degree_version = MaxValue;

    // graph->readTable->lock.unlock();
    // HBALStore::querylock.unlock();

}

MemoryAllocator *TransactionManager::getMemory()
{
    return memAllocator;
}
