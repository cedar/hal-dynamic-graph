//
// Created by Ghufran on 25/01/2023.
//

#include "InputEdge.h"

/* InputEdge defination */

void InputEdge::setSrcVertexId(vertex_t srcV)
{
    srcVertexId = srcV;
}

void InputEdge::setDestVertexId(vertex_t destV)
{
    destVertexId = destV;
}

void InputEdge::setUpdtSrcTime(vertex_t updtT)
{
    updtSrcTime = updtT;
}

vertex_t InputEdge::getSrcVertexId() const
{
    return srcVertexId;
}

vertex_t InputEdge::getDestVertexId() const
{
    return destVertexId;
}

const timestamp_t InputEdge::getUpdtSrcTime() const
{
    return updtSrcTime;
}
//#include "InputEdge.h"
// InputEdge defination


/*
void InputEdge::setSrcVertexId(vertex_t srcV)
{
    srcVertexId = srcV;
}
void InputEdge::setDestVertexId(vertex_t destV)
{
    destVertexId = destV;
}
void InputEdge::setUpdtSrcTime(vertex_t updtT)
{
    updtSrcTime = updtT;
}
vertex_t InputEdge::getSrcVertexId()
{
    return srcVertexId;
}
vertex_t InputEdge::getDestVertexId()
{
    return destVertexId;
}
timestamp_t InputEdge::getUpdtSrcTime()
{
    return updtSrcTime;
}
double InputEdge::getUpdtWeight()
{
    return updtWeight;
}
void InputEdge::setUpdtWeight(double w)
{
    updtWeight = w;
}
*/
