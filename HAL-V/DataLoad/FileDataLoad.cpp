//
// Created by Ghufran on 25/01/2023.
//
#include "FileDataLoad.h"
#include <omp.h>
#include "../TransactionManager.h"

/* FileDataLoad function defination */
std::unique_ptr<double[]> do_lcc_undirected(HBALStore *ds, vertex_t max_vertex_id);

void FileDataLoad::open_file(const char *file_name)
{
    filename = file_name;
    int f = open(filename.c_str(), O_RDONLY);
    if (f < 0)
    {
        perror("Cannot open the input file");
        abort();
    }
    file_ptr = fdopen(f, "rt");
    if (file_ptr == NULL)
    {
        perror("Cannot open the input stream");
        abort();
    }
    line_size = 64;
    _line = (char *) malloc(line_size);
    offset = 0;
}

void FileDataLoad::close_file()
{

    fclose(file_ptr);
}

bool FileDataLoad::next_line_reader(InputEdge *edgeInfo, bool readt)
{


    ssize_t read;
    ssize_t read_total = 0;
    char **_line_ = &_line;
    size_t *linesize = &line_size;
    while ((read = getline(_line_, linesize, file_ptr)) != -1)
    {
        char *line = _line;
        read_total += read;

        if (*line == '\0' || *line == '#' || *line == '\n' || *line == '\r')
            continue;

        char *str_s = line;
        char *end_num;

        while (isspace(*str_s)) str_s++;

        edgeInfo->setSrcVertexId(strtol(str_s, &end_num, 10));
        if (str_s == end_num || *end_num == '\0' || !isspace(*end_num))
        {
            // std::cout << line;
            continue;
        }
        str_s = end_num + 1;
        while (isspace(*str_s)) str_s++;

        edgeInfo->setDestVertexId(strtol(str_s, &end_num, 10));

        if (str_s == end_num || *end_num == '\0' || !isspace(*end_num))
        {
            // std::cout << line;
            continue;
        }
        //
        if (readt)
        {
            str_s = end_num + 1;
            while (isspace(*str_s)) str_s++;
            edgeInfo->setUpdtSrcTime(strtol(str_s, &end_num, 10));

            if (str_s == end_num || (*end_num != '\0' && *end_num != '\r' && *end_num != '\n'))
            {
                // std::cout << line;
                continue;
            }
        }
        //
        if (read_total > 0)
        {
            offset += read_total;
            return true;
        }
        else
        {
            offset = 0;
            return false;
        }
    }
    return false;
}

/* StreamDataLoad function defination */
HBALStore *StreamDataLoad::dataLoad(std::string filePath, MemoryAllocator *la, int num)
{
    TransactionManager *ts = new TransactionManager(50, 1);
    HBALStore *hs = ts->getGraphDataStore(0, 0);
    ////
    uint64_t count_edge_count = 0;
    std::vector<edge_t> edges;
    double datastore_loading_time =0;
    std::fstream file;
    std::string in_filename = FILEPATH_OOO_UPDATES_BINARY + filePath;
    file.open(in_filename, std::ios::in | std::ios::out);

    size_t edge_count;
    file.read((char *) &edge_count, sizeof(edge_count));

   // double datastore_loading_time;
    edges.clear();
    edges.resize(edge_count);
    file.read((char *) edges.data(), edge_count * sizeof(edge_t));
    file.close();
    auto begin = std::chrono::high_resolution_clock::now();
    for (int j = 0; j < edges.size(); j++)
    {
        InputEdge ie;
        ie.setSrcVertexId(edges[j].src);
        ie.setDestVertexId(edges[j].dst);
        ie.setUpdtSrcTime(edges[j].timestamp);
        if (edges[j].weight >= 0)
        {

            hs->InsertSrcVertex(ie.getSrcVertexId(), ts->getMemory(), 0);
            hs->InsertDestVertex(ie.getDestVertexId(), ts->getMemory(), 0);
            hs->InsertEdge(ie, ts->getMemory(), 0);
            hs->InsertEdge({ie.getSrcVertexId(), ie.getDestVertexId(),ie.getUpdtSrcTime()} , ts->getMemory(), 0);

            count_edge_count++;
        }
        else
        {
            std::cout<<edges[j].weight<<std::endl;
            std::cout<<"deletion"<<std::endl;
            break;
           // hs->removeEdge(0,0,0,ie, la, 0);

            //hs->removeEdge(0,0,0,{ie.getDestVertexId(), ie.getSrcVertexId(), ie.getUpdtSrcTime()}, la, 0);
        }
    }

    ////

   /* std::fstream file;

    std::string in_filename = FILEPATH_OOO_UPDATES_BINARY + filePath;
    file.open(in_filename, std::ios::in | std::ios::out);
    InputEdge inputEdge;

    std::vector<InputEdge> inputEdgeArr;
    while (file.read((char *) &inputEdge, sizeof(InputEdge)))
    {
        //hs->InsertUpdate(inputEdge, la);
        inputEdgeArr.push_back(inputEdge);

    }
    file.close();

    auto begin = std::chrono::high_resolution_clock::now();

    // multi-threaded application
//#pragma omp parallel for shared(hs, inputEdgeArr, la)
    uint64_t count_edge_count = 0;
    int count_e = 0;
    for (int j = 0; j < inputEdgeArr.size(); j++)
    {
        count_e++;

        if (inputEdgeArr[j].getDestVertexId() < ((vertex_t) 1 << 25))
        {

            hs->InsertSrcVertex(inputEdgeArr[j].getSrcVertexId(), la, 0);
            hs->InsertDestVertex(inputEdgeArr[j].getDestVertexId(), la, 0);
            hs->InsertEdge(inputEdgeArr[j], la, 0);
            hs->InsertEdge({inputEdgeArr[j].getSrcVertexId(),inputEdgeArr[j].getDestVertexId(),inputEdgeArr[j].getUpdtSrcTime()} , la, 0);

            count_edge_count++;
        }
        else
        {
            //  hs->removeEdge(inputEdgeArr[j], la,0);
        }
    }*/
    auto end = std::chrono::high_resolution_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
    datastore_loading_time = elapsed.count() * 1e-9;
    std::cout << "results" << datastore_loading_time << std::endl;


    auto begin2 = std::chrono::high_resolution_clock::now();

    hs->do_pagerank(0, 10, 0.85, 0, ts->getMemory());

    auto end2 = std::chrono::high_resolution_clock::now();
    auto elapsed2 = std::chrono::duration_cast<std::chrono::nanoseconds>(end2 - begin2);
    double datastore_loading_time2 = elapsed2.count() * 1e-9;

    std::cout << "Pagerank algorithm: " << datastore_loading_time2 << std::endl;

    std::ofstream outfile("/local/ghufran/result/out-of-order-g500-24-halv.txt", std::ios::app); // Append to the file

    if (outfile.is_open())
    {
        outfile <<num<<" "<< datastore_loading_time<< " " <<datastore_loading_time2<< std::endl;
    }
    else
    {
        std::cerr << "Unable to open file for writing." << std::endl;
    }
    // hs->do_lcc_undirected(hs, hs->getMaxSourceVertex());
    //todo remaining rows that are at the end and not achieved the threshold count_row

    // calculate space consumtion
    MemoryConsumption *memCal = new MemoryConsumption(hs);
    //    size_t spaceGb = memCal->getGB();
    hs->setLoadingTime(datastore_loading_time);
    hs->analytics_time = datastore_loading_time2;


    return hs;
    //    std::cout<<" Space in GB : "<<spaceGb<<std::endl;
    //  hs->getVertexIndrList()[0][0]->PerVertexBlockArr()[hs->getVertexIndrList()[0][0]->getCurPos()]->getEdgeUpdate()[]
}

void StreamDataLoad::delete_vertexArray()
{
    //  free(ws->getVertexArray());
}

StreamDataLoad::~StreamDataLoad()
{
    // std::cout<<""
}
