//
// Created by Ghufran on 09/08/2022.
//
#include <iostream>
#include<fstream>
//#include "ReadStore/ReadStore.h"
#include <ctime>
#include<vector>
#include <bits/stdc++.h>

#ifndef OUR_IDEA_SYNTHETIC_TIMESTAMP_H
#define OUR_IDEA_SYNTHETIC_TIMESTAMP_H

#include "FileDataLoad.h"
#include "../ThirdParty/plot/matplotlibcpp.h"

namespace plt = matplotlibcpp;

class PerSourceHashMap
{
public:
    void setCount(int c);

    void setPerSourceEdgeInfo(std::string s);

    int getCount();

    std::vector<std::string> &persourceEdgeInfo();

private:
    std::vector<std::string> persourceEdgeI;
    int count;
};

void generateTimestampDatasetRandom(std::string filename, std::string postfix, double divideby);

void calculate_Degreebyvector(std::string inputFile, std::string outputFile);

int outOforderUpdateGeneration(std::string filename, int out_percent, std::string outFile, int numberrandom);

int outOforderUpdateGenerationBypersource(std::string filename, int out_percent, std::string outFile);

void calculate_Degree(std::string inputFile);

void rangecheck(std::string filename, timestamp_t start_t1, timestamp_t end_t1, std::map<vertex_t, bool> vt);

void checkpowerlaw(std::string filename);

void generateTimestampDataset(std::string filename);

void ReadBinayFile(std::string inputFile);

void GenerateDeletionWorkload(std::string inputFile, std::string outputFile, int del_perc);

void BinayFileConversion(std::string inputFile, std::string OutPutFile);

void convertResultByMedian(std::string OutPutFile);

#endif //OUR_IDEA_SYNTHETIC_TIMESTAMP_H
