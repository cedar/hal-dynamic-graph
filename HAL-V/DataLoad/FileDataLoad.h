//
// Created by Ghufran on 19/07/2022.
//

#ifndef OUR_IDEA_FILEDATALOAD_H
#define OUR_IDEA_FILEDATALOAD_H

#include <stdio.h>
#include <fcntl.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include "InputEdge.h"
#include "../storage_engine/HBALStore.h"
#include "../MemoryPool/MemoryConsumption.h"

class FileDataLoad
{
public:
    void open_file(const char *file_name);

    void close_file();

    bool next_line_reader(InputEdge *edgeInfo, bool readt = true);

private:
    FILE *file_ptr;
    std::string filename;
    size_t line_size;
    char *_line;
    vertex_t offset;
};

class StreamDataLoad
{
public:
    HBALStore *dataLoad(std::string filePath, MemoryAllocator *la, int num);

    void delete_vertexArray();

    ~StreamDataLoad();

private:
    HBALStore *hs;
};

#endif //OUR_IDEA_FILEDATALOAD_H
