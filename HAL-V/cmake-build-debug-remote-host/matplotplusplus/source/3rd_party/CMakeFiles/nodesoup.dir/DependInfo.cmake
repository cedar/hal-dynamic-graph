
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/tmp/tmp.OGb4BQmGdU/matplotplusplus/source/3rd_party/nodesoup/src/algebra.cpp" "/tmp/tmp.OGb4BQmGdU/cmake-build-debug-remote-host/matplotplusplus/source/3rd_party/CMakeFiles/nodesoup.dir/nodesoup/src/algebra.cpp.o"
  "/tmp/tmp.OGb4BQmGdU/matplotplusplus/source/3rd_party/nodesoup/src/fruchterman_reingold.cpp" "/tmp/tmp.OGb4BQmGdU/cmake-build-debug-remote-host/matplotplusplus/source/3rd_party/CMakeFiles/nodesoup.dir/nodesoup/src/fruchterman_reingold.cpp.o"
  "/tmp/tmp.OGb4BQmGdU/matplotplusplus/source/3rd_party/nodesoup/src/kamada_kawai.cpp" "/tmp/tmp.OGb4BQmGdU/cmake-build-debug-remote-host/matplotplusplus/source/3rd_party/CMakeFiles/nodesoup.dir/nodesoup/src/kamada_kawai.cpp.o"
  "/tmp/tmp.OGb4BQmGdU/matplotplusplus/source/3rd_party/nodesoup/src/layout.cpp" "/tmp/tmp.OGb4BQmGdU/cmake-build-debug-remote-host/matplotplusplus/source/3rd_party/CMakeFiles/nodesoup.dir/nodesoup/src/layout.cpp.o"
  "/tmp/tmp.OGb4BQmGdU/matplotplusplus/source/3rd_party/nodesoup/src/nodesoup.cpp" "/tmp/tmp.OGb4BQmGdU/cmake-build-debug-remote-host/matplotplusplus/source/3rd_party/CMakeFiles/nodesoup.dir/nodesoup/src/nodesoup.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "_USE_MATH_DEFINES"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../matplotplusplus/source/3rd_party/nodesoup/include"
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
