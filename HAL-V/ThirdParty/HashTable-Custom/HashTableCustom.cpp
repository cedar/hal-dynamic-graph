//
// Created by Ghufran on 25/01/2023.
//

/* Bucket function defination */
#include <iostream>
#include "HashTableCustom.h"


void Bucket::setDestNode(vertex_d dest_node)
{
    DestNode = dest_node;
}
void Bucket::setAdjacencyPos(u_int64_t adj_pos)
{
    adjacencyPos = adj_pos;
}
void Bucket::setNextBucket(Bucket *b)
{
    nextBucket = b;
}

vertex_d  Bucket::getDestNode()
{

    return DestNode;
}
u_int64_t Bucket::getAdjacencyPos()
{
    return adjacencyPos;
}
Bucket* Bucket::getNextBucket()
{
    return nextBucket;
}

void Bucket::initBucket()
{
     DestNode = -1;
     adjacencyPos =-1;
    // blockIndex very important index to set deletion bit
     nextBucket = NULL;
}


/* HashTableIndex function defination */

HashTableIndex::HashTableIndex(MemoryAllocator *la,int thread_id)
{

curSize = 0;
entryCounter = 0;
//resizeLock.init();
resizeHashTable(la,thread_id);
}
void HashTableIndex::setEntryCounter(int c)
{
    entryCounter = c;
}
int HashTableIndex::getEntryCounter()
{
    return entryCounter;
}
void HashTableIndex::insert(vertex_t key, u_int64_t adj_pos, MemoryAllocator *la, int thread_id)
{

    entryCounter++;
    if(curSize<0)
    {
        std::cout<<curSize;
    }
    if(entryCounter > (curSize))
    {
      /*  resizeLock.lock();
        if(entryCounter > (curSize*bucketPerIndexLimit))
        {*/
            resizeHashTable(la,thread_id);
        /*}
        resizeLock.unlock();*/
    }
    Bucket* bucketResutn = getIndexVal(key);
    if(bucketResutn == NULL)
    {
        int index = key % curSize;
        Bucket *newB = (Bucket *) la->Allocate(sizeof(Bucket),thread_id,true);
        newB->setDestNode(key);
        newB->setAdjacencyPos(adj_pos);

       // locks[index].lock();
        if (hashIndexArr[index] == NULL)
        {
            newB->setNextBucket(NULL);
            hashIndexArr[index] = newB;
        }
        else
        {
            newB->setNextBucket(hashIndexArr[index]);
            hashIndexArr[index] = newB;
        }
     //   locks[index].unlock();
    }
    else
    {
        bucketResutn->setAdjacencyPos(adj_pos);
    }
}
Bucket* HashTableIndex::getIndexVal(vertex_d  key)
{
    int index = key % curSize;
    Bucket *nextPtr = hashIndexArr[index];
    while(nextPtr != NULL)
    {

        if(nextPtr->getDestNode() == key)
        {
            return nextPtr;
        }
        nextPtr = nextPtr->getNextBucket();
    }
    return NULL;
}

std::tuple<Bucket*,Bucket*> HashTableIndex::getIndexValDelete(vertex_d  key)
{
    int index = key % curSize;
    Bucket *nextPtr = hashIndexArr[index];
    Bucket *prevPtr = NULL;
    while(nextPtr != NULL)
    {

        if(nextPtr->getDestNode() == key)
        {
            return {nextPtr,prevPtr};
        }
        prevPtr = nextPtr;
        nextPtr = nextPtr->getNextBucket();
    }
    return {NULL,NULL};
}
void HashTableIndex::resizeHashTable(MemoryAllocator *la, int thread_id)
{

    int prevCurSize = curSize;
    if(prevCurSize<0)
    {
        std::cout<<prevCurSize<<std::endl;

    }
    if(curSize != 0)
    {
        int curSize1 = getCurSizeTable(curSize);
        Bucket **tempArray = static_cast<Bucket **>(la->Allocate(curSize1 * 8,thread_id));//reinterpret_cast<Bucket **>((Bucket *) malloc(curSize * 8));
      //  RWSpinLock *tempArrylock = static_cast<RWSpinLock *>(std::aligned_alloc(PAGE_SIZE,curSize1 * sizeof(RWSpinLock)));

         memset(tempArray, 0, curSize1 * 8);

        for(int i=0;i<prevCurSize;i++)
        {
          //  tempArrylock[i] = std::move(lock[i]);
            Bucket *nextPtr = hashIndexArr[i];
            while (nextPtr != NULL)
            {
                int newIndex = nextPtr->getDestNode() % curSize1;
                if (tempArray[newIndex] == NULL)
                {

                    Bucket *newBucket = (Bucket *) la->Allocate(sizeof(Bucket),thread_id,true);
                    //Bucket *update_ptr = nextPtr;
                    newBucket->setDestNode(nextPtr->getDestNode());
                    newBucket->setAdjacencyPos(nextPtr->getAdjacencyPos());

                    // blockindex should be inserted here
                    newBucket->setNextBucket(NULL);
                    tempArray[newIndex] = newBucket;
                }
                else
                {
                 //nextPtr->setNextBucket(tempArray[newIndex]);
                    Bucket *b =  (Bucket *) la->Allocate(sizeof(Bucket),thread_id,true);
                    b->setDestNode(nextPtr->getDestNode());
                    b->setNextBucket(tempArray[newIndex]);
                    b->setAdjacencyPos(nextPtr->getAdjacencyPos());


                    tempArray[newIndex] = b;
                }
                //  Bucket *prevPtr = nextPtr;
                nextPtr = nextPtr->getNextBucket();
                // delete prevPtr;
            }
        }

        Bucket **freeArr = hashIndexArr;
        // curIndVertexSize decremented becuase of neutralizing the increament effect few lines before
        // std::copy(hashIndexArr, hashIndexArr + prevCurSize, tempArray);
       //  delete[] hashIndexArr;
       //la->free(freeArr, prevCurSize*8,2);
       hashIndexArr = tempArray;

        // start of hash table section

        for(int i=0;i<prevCurSize;i++) {
            //  tempArrylock[i] = std::move(lock[i]);
            Bucket *nextPtr = freeArr[i];
            Bucket *prev = NULL;
            while (nextPtr != NULL)
            {
                prev = nextPtr;
                nextPtr = nextPtr->getNextBucket();
                prev->initBucket();
                la->free(prev, sizeof(Bucket),thread_id,true);
            }
        }
     //  memset(freeArr,0,prevCurSize*8);
       la->free((void*) freeArr, prevCurSize*8,thread_id,true);
        curSize = curSize1;
    }
    else
    {

        hashIndexArr = static_cast<Bucket **>(la->Allocate(defaultSize1 * 8,thread_id));

       // locks = static_cast<RWSpinLock *>(std::aligned_alloc(PAGE_SIZE,defaultSize1 * sizeof(RWSpinLock)));

         memset(hashIndexArr, 0, defaultSize1 * 8);


        curSize = defaultSize1;
    }
}
Bucket** HashTableIndex::getHashIndexArr()
{
    return hashIndexArr;
}
int HashTableIndex::getCurSize()
{
    return curSize;
}