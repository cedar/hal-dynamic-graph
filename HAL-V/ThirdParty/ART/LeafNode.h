//
// Created by Ghufran on 08/11/2022.
//

#ifndef OUR_IDEA_LEAFNODE_H
#define OUR_IDEA_LEAFNODE_H
// class to store deletion information related to destination node
//#include "../../SharedLib.h"
using vertex_d = int64_t;
using timestamp_l = int64_t;
class ITM;
class LeafNode
{
public:
    LeafNode()
    {
        dNode = 0;
        srcTime=0;
        wts = 0;
        invalEntry = NULL;
    }
    void intLeafNode()
    {
        dNode = 0;
        srcTime=0;
        wts = 0;
        invalEntry = NULL;
    }
    void setDNode(vertex_d d_node)
    {
        dNode = d_node;
    }
    void setSrcTime(timestamp_l src_time)
    {
        srcTime = src_time;
    }
    void setWts(timestamp_l wts)
    {
        this->wts = wts;
    }
    void setInvalEntry(ITM *itm)
    {
        invalEntry = itm;
    }
    vertex_d getDNode()
    {
        return dNode;
    }
    timestamp_l getSrcTime() const
    {
        return srcTime;
    }
    timestamp_l getWts()
    {
        return wts;
    }
    ITM* getInvalEntry()
    {
        return invalEntry;
    }
private:
    vertex_d  dNode; //destination node
    timestamp_l srcTime; // source time of the destination node
    timestamp_l wts; // transaction time of the destination node
    ITM *invalEntry; // default null; in case of deletion store the information of an update which consider the LeafNode as delete
};
class ITM
{
public:
    ITM()
    {

    }

    void setSrcTime(timestamp_l src_time)
    {
        srcTime = src_time;
    }
    void setWts(timestamp_l wts)
    {
        this->wts = wts;
    }
    timestamp_l getSrcTime()
    {
        return srcTime;
    }
    timestamp_l getWts()
    {
        return wts;
    }
private:
    timestamp_l srcTime;
    timestamp_l wts;
};
#endif //OUR_IDEA_LEAFNODE_H
