/*******************************************************************************
 * Copyright (INRIA) $2024$
 * This file is part of [HAL dynamic graph anlaytics].
 ******************************************************************************/

#include <cstddef>
#include <cstdint>
#include "SharedLib.h"

void *EMPTY_SLOT;
void *FLAG_DELETE;

inline std::size_t CalculatePadding(std::size_t baseAddress, const std::size_t alignment)
{
    std::size_t multiplier = (baseAddress / alignment) + 1;
    std::size_t alignedAddress = multiplier * alignment;
    std::size_t padding = alignedAddress - baseAddress;
    return padding;
}

int getCurSizeTable(int curSize2)
{
    if (curSize2 < PRIMETABLE[8])
    {
        for (int i = 0; i < 8; i++)
        {
            if (curSize2 == PRIMETABLE[i])
            {
                return PRIMETABLE[i + 1];
            }
        }
    }
    else
    {
        return curSize2 * 2;
    }


    /* if(curSize == 0)
     {
         return 10;
     }
     else if(curSize == 10)
     {
         return 100;
     }
     else if(curSize == 100)
     {
         return 500;
     }
     else if(curSize == 500)
     {
         return 1000;
     }
     else if(curSize == 1000)
     {
         return 5000;
     }
     else if(curSize == 5000)
     {
         return 10000;
     }
     else if(curSize == 10000)
     {
         return 50000;
     }
     else if(curSize == 50000)
     {
         return 100000;
     }
     else if(curSize == 100000)
     {
         return 500000;
     }
     else
     {
         return -1;
     }*/
}

uint64_t rdtsc()
{
    unsigned int lo, hi;
    __asm__ __volatile__ ("rdtsc" : "=a" (lo), "=d" (hi));
    return ((uint64_t) hi << 32) | lo;
}


int getIndrVertexArrayIndex(vertex_t srcVertex)
{
    return srcVertex / ((vertex_t) 1 << vertexArraySize);
}

int getVertexArrayIndex(vertex_t srcVertex, int divSrcVertexId)
{
    return (srcVertex - (((vertex_t) 1 << vertexArraySize) * divSrcVertexId));
}


timestamp_t getCurrTimeStamp()
{
    // return  version.fetch_add(1);
    return rdtsc();//(duration_cast<seconds>(system_clock::now().time_since_epoch()).count());
}

timestamp_t getNextSecond(timestamp_t cur_time)
{
    time_t now = cur_time;//time(0);
    return (now + 1);
}

time_t getArtIndex(time_t des_t)
{
    tm *gmtm = gmtime(&des_t);
    return (des_t - ((gmtm->tm_min * 60) + gmtm->tm_sec));
}

std::string addZero(int mon_v)
{
    std::string mon = "";
    if (mon_v < 10)
    {
        mon = "0" + std::to_string(mon_v);
    }
    else
    {
        mon = std::to_string(mon_v);
    }
    return mon;
}

artKeyType getHourT(std::string keyStr)
{

    return (stoul(keyStr, 0, 10));
}

int random1(int range_from, int range_to)
{
    std::random_device rand_dev;
    std::mt19937 generator(rand_dev());
    std::uniform_int_distribution<int> distr(range_from, range_to);
    return distr(generator);
}

artKeyType extractHour(timestamp_t time_u)
{
    tm *gmtm = localtime(reinterpret_cast<const time_t *>(&time_u));

    return getHourT((std::to_string(gmtm->tm_year + 1900) + addZero(gmtm->tm_mon + 1) + addZero(gmtm->tm_mday) + addZero(gmtm->tm_hour)));
}

const char *extractKeyStr(timestamp_t time_u)
{
    return std::to_string(extractHour(time_u)).c_str();
}

std::vector<artKeyType> getCheckpointKeys(timestamp_t start_c, timestamp_t end_c)
{
    artKeyType start_key = extractHour(start_c);
    artKeyType end_key = extractHour(end_c);
    std::vector<artKeyType> checkpoints_keys;
    timestamp_t middle_keys = 0;

    if (start_key == end_key)
    {
        checkpoints_keys.push_back(end_key);
    }
    else
    {
        start_c = start_c - 3600; //extractHour(start_c);
        start_key = extractHour(start_c);
        checkpoints_keys.push_back(start_key);
        if (start_key != end_key)
        {
            middle_keys = start_c - 3600;
            // todo only insert single time the keys if that condition exist(if there is no middlekeys than store only the start_key)
            while (extractHour(middle_keys) != end_key)
            {

                checkpoints_keys.push_back(extractHour(middle_keys));
                middle_keys = middle_keys - 3600;
            }
            if (extractHour(middle_keys) == end_key)
            {
                checkpoints_keys.push_back(end_key);
            }

        }
    }
    return checkpoints_keys;
}

// taken from https://stackoverflow.com/questions/29585971/storing-multiple-values-via-bitmask-in-c-sharp
// index is main variable u_int64_t where we store multiple values
// val is the value that we need to store in specfic bits range
// pos is the bit position from where we start storing val (starting position)
// bitsNeeded (number of bits that we need for val)
u_int64_t setBitMasktest(u_int64_t index, u_int64_t val, u_int64_t pos, u_int64_t bitsNeeded)
{
    return index | ((u_int64_t) val << pos);
}

u_int64_t getBitMasktest(u_int64_t index, u_int64_t pos, u_int64_t bitsNeeded)
{
    return 0;// return ((u_int64_t)(index & mask) >> pos);
}

u_int64_t setBitMask(u_int64_t index, u_int64_t val, u_int64_t pos, u_int64_t bitsNeeded)
{
    /*

     for indrArrayIndex: pos 0 and bitsNeeded 32
     for indrArraySize: pos 32 and bitsNeeded 5
     for blockIndex: pos 37 and bitsNeeded 12
     for prefix dest_node: pos 49 and bitsNeeded 14
     for updateFlage(in order,ooo) pos 63 and bitNeeded 1

     pos : 0 means start from 0th - 32 bit
     pos: 32 means start from 32th bit
     pos: 37 means start from 37th bit
     pos: 49 means start from 49th bit
     pos: 63 means start from 63th bit

    */

    u_int64_t mask = ((u_int64_t) (((u_int64_t) 1 << bitsNeeded) - 1) << pos);

    return ((index & ~mask) | ((u_int64_t) val << pos));
}

u_int64_t getBitMask(u_int64_t index, u_int64_t pos, u_int64_t bitsNeeded)
{
    /*

     int start = 2; // extract 3 bits starting at position 2
     int n = 3;
     int subset = (mask >> start) & ((1 << n) - 1);

     */
    // u_int64_t mask = ((((u_int64_t)1<<bitsNeeded) -1)<<pos);
    return (((u_int64_t) index >> pos) & (((u_int64_t) 1 << bitsNeeded) - 1));//((u_int64_t)(index & mask) >> pos);
}
