/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This C++ file manages edge metadata extracted from dataset edges (source ID, destination ID, weight, timestamp).
 ******************************************************************************/
#ifndef OUR_IDEA_INPUTEDGE_H
#define OUR_IDEA_INPUTEDGE_H

#include "../SharedLib.h"

struct edge_t
{
    uint64_t src;
    uint64_t dst;
    double weight;
    uint64_t timestamp;
};

class InputEdge_P
{
public:
    InputEdge_P() : srcVertexId(-1), destVertexId(-1), updtSrcTime(-1) {};

    InputEdge_P(vertex_t src_id, vertex_t dst_id, timestamp_t update_time) : srcVertexId(src_id), destVertexId(dst_id), updtSrcTime(update_time) {};

    void setSrcVertexId(vertex_t srcV);

    void setDestVertexId(vertex_t destV);

    void setUpdtSrcTime(vertex_t updtT);

    vertex_t getSrcVertexId() const;

    vertex_t getDestVertexId() const;

    const timestamp_t getUpdtSrcTime() const;

private:
    vertex_t srcVertexId;
    vertex_t destVertexId;
    timestamp_t updtSrcTime;
};


class InputEdge
{
public:
    InputEdge() : srcVertexId(0), destVertexId(0), updtSrcTime(0), updtWeight(0.0) {};

    InputEdge(vertex_t src_id, vertex_t dst_id, timestamp_t update_time, double w) : srcVertexId(src_id), destVertexId(dst_id), updtSrcTime(update_time), updtWeight(w) {};

    InputEdge(vertex_t src_id, vertex_t dst_id, timestamp_t update_time) : srcVertexId(src_id), destVertexId(dst_id), updtSrcTime(update_time) {};

    void setSrcVertexId(vertex_t srcV);

    void setDestVertexId(vertex_t destV);

    void setUpdtSrcTime(vertex_t updtT);

    void setUpdtWeight(double updtT);

    vertex_t getSrcVertexId();

    vertex_t getDestVertexId();

    double getUpdtWeight();

    timestamp_t getUpdtSrcTime();

private:
    vertex_t srcVertexId;
    vertex_t destVertexId;
    timestamp_t updtSrcTime;
    double updtWeight;
};

#endif //OUR_IDEA_INPUTEDGE_H
