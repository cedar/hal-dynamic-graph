/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 ******************************************************************************/
#include "InputEdge.h"

/* InputEdge defination */
void InputEdge_P::setSrcVertexId(vertex_t srcV)
{
    srcVertexId = srcV;
}

void InputEdge_P::setDestVertexId(vertex_t destV)
{
    destVertexId = destV;
}

void InputEdge_P::setUpdtSrcTime(vertex_t updtT)
{
    updtSrcTime = updtT;
}

vertex_t InputEdge_P::getSrcVertexId() const
{
    return srcVertexId;
}

vertex_t InputEdge_P::getDestVertexId() const
{
    return destVertexId;
}

const timestamp_t InputEdge_P::getUpdtSrcTime() const
{
    return updtSrcTime;
}
//#include "InputEdge.h"
// InputEdge defination



void InputEdge::setSrcVertexId(vertex_t srcV)
{
    srcVertexId = srcV;
}

void InputEdge::setDestVertexId(vertex_t destV)
{
    destVertexId = destV;
}

void InputEdge::setUpdtSrcTime(vertex_t updtT)
{
    updtSrcTime = updtT;
}

vertex_t InputEdge::getSrcVertexId()
{
    return srcVertexId;
}

vertex_t InputEdge::getDestVertexId()
{
    return destVertexId;
}

timestamp_t InputEdge::getUpdtSrcTime()
{
    return updtSrcTime;
}

double InputEdge::getUpdtWeight()
{
    return updtWeight;
}

void InputEdge::setUpdtWeight(double w)
{
    updtWeight = w;
}

