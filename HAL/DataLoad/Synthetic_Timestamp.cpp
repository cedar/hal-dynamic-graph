/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This C++ header file is used to manipulate binary dataset files.
 ******************************************************************************/

#include "Synthetic_Timestamp.h"

/* PerSourceHashMap */

void PerSourceHashMap::setCount(int c)
{
    count = c;
}

void PerSourceHashMap::setPerSourceEdgeInfo(std::string s)
{
    persourceEdgeI.push_back(s);
}

int PerSourceHashMap::getCount()
{
    return count;
}

std::vector<std::string> &PerSourceHashMap::persourceEdgeInfo()
{
    return persourceEdgeI;
}

/* Out of order update by source vertex generation file */
int outOforderUpdateGenerationBypersource(std::string filename, int out_percent, std::string outFile)
{
    std::ofstream foutput;
    std::string path = FILEPATH_OOO_UPDATES + outFile;
    foutput.open(path.c_str(), std::ios::trunc);
    if (!foutput) { std::cout << "error open file"; }
    InputEdge edgeInfo;
    FileDataLoad fileLoad;
    std::string *strArr = new std::string[out_percent];
    std::string path1 = FILEPATH_SORTED_BY_GRAPH500_24 + filename;
    fileLoad.open_file(path1.c_str());
    int c = 0;
    vertex_t counter = 1;
    std::map<vertex_t, PerSourceHashMap *> shm;
    while (fileLoad.next_line_reader(&edgeInfo, true))
    {
        std::string str = std::to_string(edgeInfo.getSrcVertexId()) + " " + std::to_string(edgeInfo.getDestVertexId()) + " " + std::to_string(edgeInfo.getUpdtSrcTime()) + "\n";
        if (shm.find(edgeInfo.getSrcVertexId()) != shm.end())
        {
            shm[edgeInfo.getSrcVertexId()]->setCount(shm[edgeInfo.getSrcVertexId()]->getCount() + 1);
            shm[edgeInfo.getSrcVertexId()]->setPerSourceEdgeInfo(str);
        }
        else
        {
            PerSourceHashMap *p = new PerSourceHashMap();
            p->setCount(1);
            p->setPerSourceEdgeInfo(str);
            shm[edgeInfo.getSrcVertexId()] = p;
        }
        counter++;
    }

    std::map<vertex_t, PerSourceHashMap *>::iterator it;
    int vertex_count = 0;
    for (it = shm.begin(); it != shm.end(); it++)
    {
        if (it->second->getCount() >= 10)
        {
            vertex_count++;
            int number_of_edges_omit = it->second->getCount() % 10;
            int total_edges_source_vertex = it->second->getCount() - number_of_edges_omit;

            for (int j = 10; j <= total_edges_source_vertex; j += 10)
            {
                int index1 = j - 1; // end index shuffle after 10 edges so every line last index is 9
                int index2 = index1 - out_percent; // subtract end index - line to suffle for example if 2 then 9-2 suffle 7 line with 9
                std::string temp = it->second->persourceEdgeInfo()[index1];
                it->second->persourceEdgeInfo()[index1] = it->second->persourceEdgeInfo()[index2];
                it->second->persourceEdgeInfo()[index2] = temp;
                int start_index = j - 10;
                int last_index = j;
                for (int i = start_index; i < last_index; i++)
                {
                    foutput << it->second->persourceEdgeInfo()[i];
                }
            }
        }
    }
    std::cout << "vertex count :" << vertex_count << std::endl;
    fileLoad.close_file();
    foutput.close();
    return out_percent;
}

/* out of order update generation */

int outOforderUpdateGeneration(std::string filename, int out_percent, std::string outFile, int numberrandom)
{
    std::ofstream foutput;
    std::string path = FILEPATH + outFile;
    foutput.open(path.c_str(), std::ios::trunc);
    if (!foutput) { std::cout << "error open file"; }
    InputEdge edgeInfo;
    FileDataLoad fileLoad;
    std::string *strArr = new std::string[out_percent];
    std::string path1 = FILEPATH + filename;
    fileLoad.open_file(path1.c_str());
    int c = 0;
    vertex_t counter = 1;
    while (fileLoad.next_line_reader(&edgeInfo, true))
    {
        strArr[c++] = std::to_string(edgeInfo.getSrcVertexId()) + " " + std::to_string(edgeInfo.getDestVertexId()) + " " + std::to_string(edgeInfo.getUpdtSrcTime()) + "\n";
        if ((counter % out_percent) == 0 && counter != 1)
        {

            for (int i = 0; i < numberrandom; i++)
            {
                int numbersw = random1(0, (out_percent - numberrandom) - 1);
                std::string temp = strArr[(out_percent - 1) - i];
                strArr[(out_percent - 1) - i] = strArr[numbersw];
                strArr[numbersw] = temp;
            }
            for (int j = 0; j < out_percent; j++)
            {
                foutput << strArr[j];
            }
            c = 0;
        }
        counter++;
    }
    fileLoad.close_file();
    foutput.close();
    return out_percent;
}

/* Timesries data set random */
void generateTimestampDatasetRandom(std::string filename, std::string postfix, double divideby)
{
    size_t result = 1048576 * divideby;
    size_t arr[result];
    std::set<int> setA;
    vertex_t counter = 0;
    while (setA.size() != result)
    {
        size_t df = random1(0, 1048576);
        if (setA.find(df) == setA.end())
        {
            setA.insert(df);
            arr[counter] = df;
            counter++;
        }
    }
    std::ofstream foutput;
    FileDataLoad fileLoad;
    filename = FILEPATH + filename;
    std::string out_filename = FILEPATH + postfix;
    foutput.open(out_filename.c_str(), std::ios::trunc);
    if (!foutput) { std::cout << "error open file"; }
    InputEdge edgeInfo;
    fileLoad.open_file(filename.c_str());
    timestamp_t cur_time = start_time; // europian time stamp
    vertex_t count = 0;
    while (fileLoad.next_line_reader(&edgeInfo, true))
    {
        if (setA.find(count) == setA.end())
        {
            foutput << edgeInfo.getSrcVertexId() << " " << edgeInfo.getDestVertexId() << " " << edgeInfo.getUpdtSrcTime() << "\n";
        }
        count++;
    }
    fileLoad.close_file();
    fileLoad.open_file(filename.c_str());
    count = 0;
    while (fileLoad.next_line_reader(&edgeInfo, true))
    {
        if (setA.find(count) != setA.end())
        {
            foutput << edgeInfo.getSrcVertexId() << " " << edgeInfo.getDestVertexId() << " " << edgeInfo.getUpdtSrcTime() << "\n";
        }
        count++;
    }
    fileLoad.close_file();
    foutput.close();
    setA.clear();
}

/* calculate degree by source vertex */

void calculate_Degreebyvector(std::string inputFile, std::string outputFile)
{
    int i, j, temp, pass = 0;

    std::fstream file;
    std::string in_filename = "/local/user/gfe_datasets/graph500-26.e";  // FILEPATH + inputFile;
    file.open(in_filename, std::ios::in | std::ios::binary);
    InputEdge inputEdge;

    std::vector<vertex_t> src_vertex;
    std::vector<vertex_t> srcdegree;
    src_vertex.reserve(67108862);
    srcdegree.reserve(67108862);
    for (int i = 0; i <= 67108862; i++)
    {
        src_vertex[i] = i;
        srcdegree[i] = 0;
    }
    vertex_t counter = 0;
    while (file.read((char *) &inputEdge, sizeof(InputEdge)))
    {
        srcdegree[inputEdge.getSrcVertexId()] = srcdegree[inputEdge.getSrcVertexId()] + 1;
        //  map[inputEdge.getSrcVertexId()] = 1;
        //  std::cout<<inputEdge.getSrcVertexId()<<" "<<inputEdge.getDestVertexId()<<" "<<inputEdge.getUpdtSrcTime()<<std::endl;
    }

    //
    std::string out_filename = "/local/user/graph500_26_stat.txt"; //FILEPATH + outputFile;
    // foutput.open (out_filename.c_str(),std::ios::trunc);

    std::ofstream outFile;
    outFile.open(out_filename, std::ios::app);
    vertex_t maxDegree = 0;
    vertex_t indexVertex = 0;
    vertex_t minDegree = 99999999;
    vertex_t counter10 = 0;
    vertex_t counter100 = 0;
    vertex_t counter500 = 0;
    vertex_t counter1k = 0;
    vertex_t counter5k = 0;
    vertex_t counter10k = 0;
    vertex_t counter100k = 0;
    vertex_t counter200k = 0;
    vertex_t counter300k = 0;
    vertex_t counter400k = 0;
    vertex_t counter500k = 0;
    vertex_t counter1000k = 0;
    vertex_t counter10000k = 0;

    for (int i = 0; i <= 67108862; i++)
    {
        if (srcdegree[i] != 0)
        {
            if (srcdegree[i] > 0 && srcdegree[i] <= 10)
            {
                counter10++;

            }
            if (srcdegree[i] > 10 && srcdegree[i] <= 100)
            {
                counter100++;
            }
            if (srcdegree[i] > 100 && srcdegree[i] <= 500)
            {
                counter500++;
            }
            if (srcdegree[i] > 500 && srcdegree[i] <= 1000)
            {
                counter1k++;
            }
            if (srcdegree[i] > 1000 && srcdegree[i] <= 5000)
            {
                counter5k++;
            }
            if (srcdegree[i] > 5000 && srcdegree[i] <= 10000)
            {
                counter10k++;
            }
            if (srcdegree[i] > 10000 && srcdegree[i] <= 100000)
            {
                counter100k++;
            }
            if (srcdegree[i] > 100000 && srcdegree[i] <= 200000)
            {
                counter200k++;
            }
            if (srcdegree[i] > 200000 && srcdegree[i] <= 300000)
            {
                counter300k++;
            }
            if (srcdegree[i] > 300000 && srcdegree[i] <= 400000)
            {
                counter400k++;
            }
            if (srcdegree[i] > 400000 && srcdegree[i] <= 500000)
            {
                counter500k++;
            }
            if (srcdegree[i] > 500000 && srcdegree[i] <= 1000000)
            {
                counter1000k++;
            }
        }
        // outFile<<srcdegree[i]<<std::endl;
    }
    outFile << "between 0 and 10 :" << counter10 << std::endl;
    outFile << "between 10 and 100 :" << counter100 << std::endl;
    outFile << "between 100 and 500 :" << counter500 << std::endl;
    outFile << "between 500 and 1000 :" << counter1k << std::endl;
    outFile << "between 1000 and 5000 :" << counter5k << std::endl;
    outFile << "between 5000 and 10000 :" << counter10k << std::endl;
    outFile << "between 10k and 100k :" << counter100k << std::endl;
    outFile << "between 100k and 200k :" << counter200k << std::endl;
    outFile << "between 200k and 300k :" << counter300k << std::endl;
    outFile << "between 300k and 400k :" << counter400k << std::endl;
    outFile << "between 400k and 500k :" << counter500k << std::endl;
    outFile << "between 500k and 1000k :" << counter1000k << std::endl;


    std::cout << counter10 << std::endl;
    std::cout << counter100 << std::endl;
    std::cout << counter500 << std::endl;
    std::cout << counter1k << std::endl;
    std::cout << counter5k << std::endl;
    std::cout << counter10k << std::endl;
    std::cout << counter100k << std::endl;
    std::cout << counter200k << std::endl;
    std::cout << counter300k << std::endl;
    std::cout << counter400k << std::endl;
    std::cout << counter500k << std::endl;

    outFile.close();
    // Set the size of output image = 1200x780 pixels
/*    plt::figure_size(1200, 780);

    // Plot line from given x and y data. Color is selected automatically.
    plt::plot(srcdegree);


    // Set x-axis to interval [0,1000000]
     //plt::xlim(0, 1000*1000);

    // Add graph title
    plt::title("Sample figure");

    // Enable legend.
    plt::legend();

    // save figure
    const char* filename = "/tmp/tmp.OGb4BQmGdU/degreeplot4.png";
    std::cout << "Saving result to " << filename << std::endl;;
    plt::save(filename);
*/
    // std::cout<<map.size();
    file.close();
}

/* */
void calculate_Degree(std::string inputFile)
{
    std::fstream file;
    std::string in_filename = FILEPATH + inputFile;
    file.open(in_filename, std::ios::in | std::ios::binary);
    InputEdge inputEdge;
    std::map<vertex_t, vertex_d> map;
    while (file.read((char *) &inputEdge, sizeof(InputEdge)))
    {
        map[inputEdge.getSrcVertexId()] = 1;
        //  std::cout<<inputEdge.getSrcVertexId()<<" "<<inputEdge.getDestVertexId()<<" "<<inputEdge.getUpdtSrcTime()<<std::endl;
    }
    std::cout << map.size();
    file.close();
}

/* */
void rangecheck(std::string filename, timestamp_t start_t1, timestamp_t end_t1, std::map<vertex_t, bool> vt)
{

    std::ofstream foutput;
    FileDataLoad fileLoad;
    filename = FILEPATH + filename;
    // end_t1 = end_t1 - 3600;
    InputEdge edgeInfo;
    fileLoad.open_file(filename.c_str());
    timestamp_t cur_time = start_time;
    vertex_t counter = 0;
    std::map<vertex_t, vertex_t, std::greater<vertex_t>> vt1;
    vertex_t count_a = 0;
    // europian time stamp
    while (fileLoad.next_line_reader(&edgeInfo, true))
    {
        vt1[edgeInfo.getSrcVertexId()] += 1;

    }
    std::cout << " " << counter << "   , " << vt1.size() << std::endl;
    std::cout << " " << count_a << "   - " << vt.size() << std::endl;

    fileLoad.close_file();
}

/* */
void checkpowerlaw(std::string filename)
{

    std::ofstream foutput;
    FileDataLoad fileLoad;
    filename = FILEPATH + filename;
    // end_t1 = end_t1 - 3600;
    InputEdge edgeInfo;
    fileLoad.open_file(filename.c_str());
    timestamp_t cur_time = start_time;
    vertex_t counter = 0;
    std::map<vertex_t, vertex_t, std::greater<vertex_t>> vt1;
    vertex_t count_a = 0;
    // europian time stamp
    while (fileLoad.next_line_reader(&edgeInfo, true))
    {
        vt1[edgeInfo.getSrcVertexId()] += 1;
        if (vt1.find(edgeInfo.getSrcVertexId()) == vt1.end())
        {
            vt1[edgeInfo.getSrcVertexId()] = 1;
        }
        else
        {
            vt1[edgeInfo.getSrcVertexId()] = vt1[edgeInfo.getSrcVertexId()] + 1;
        }
    }
    std::cout << " " << counter << "   , " << vt1.size() << std::endl;

    fileLoad.close_file();
}

/* */
void generateTimestampDataset(std::string filename)
{
    std::ofstream foutput;
    FileDataLoad fileLoad;
    filename = "/local/user/" + filename;
    std::string postfix = "graph500-24ts.txt";
    std::string out_filename = "/local/user/" + postfix;
    foutput.open(out_filename.c_str(), std::ios::trunc);
    if (!foutput) { std::cout << "error open file"; }
    InputEdge edgeInfo;
    fileLoad.open_file(filename.c_str());
    timestamp_t cur_time = start_time; // europian time stamp
    while (fileLoad.next_line_reader(&edgeInfo, false))
    {
        foutput << edgeInfo.getSrcVertexId() << " " << edgeInfo.getDestVertexId() << " " << cur_time << "\n";
        cur_time = getNextSecond(cur_time);
    }
    fileLoad.close_file();
}

/* */
void ReadBinayFile(std::string inputFile)
{
    std::fstream file;
    std::string in_filename = "/local/user/" + inputFile;
    file.open(in_filename, std::ios::in | std::ios::binary);
    InputEdge inputEdge;

    while (file.read((char *) &inputEdge, sizeof(InputEdge)))
    {
        std::cout << inputEdge.getSrcVertexId() << " " << inputEdge.getDestVertexId() << " " << inputEdge.getUpdtSrcTime() << std::endl;

    }

    file.close();
}

/* */
void GenerateDeletionWorkload(std::string inputFile, std::string outputFile, int del_perc)
{
    std::fstream file;
    std::string in_filename = FILEPATH + inputFile;
    file.open(in_filename, std::ios::in | std::ios::binary);
    InputEdge inputEdge;

    std::string out_filename = FILEPATH + outputFile;
    // foutput.open (out_filename.c_str(),std::ios::trunc);
    std::ofstream outFile;
    outFile.open(out_filename, std::ios::out | std::ios::binary);
    if (!outFile) { std::cout << "error open file"; }
    std::vector<InputEdge> Delupdate;
    vertex_t counter = 0;
    while (file.read((char *) &inputEdge, sizeof(InputEdge)))
    {
        counter++;
        if (counter % del_perc == 0)
        {
            Delupdate.push_back(inputEdge);
        }
        outFile.write((char *) &inputEdge, sizeof(InputEdge));
    }
    for (int i = 0; i < Delupdate.size(); i++)
    {
        InputEdge uie;
        uie.setUpdtSrcTime(Delupdate[i].getSrcVertexId());
        uie.setDestVertexId(-Delupdate[i].getDestVertexId());
        uie.setSrcVertexId(Delupdate[i].getUpdtSrcTime());
        outFile.write((char *) &uie, sizeof(uie));
    }
    file.close();
    outFile.close();
}

/* */
void BinayFileConversion(std::string inputFile, std::string OutPutFile)
{
    //std::ofstream foutput;
    FileDataLoad fileLoad;
    inputFile = FILEPATH_OOO_UPDATES + inputFile;
    std::string postfix = OutPutFile;
    std::string out_filename = FILEPATH_OOO_UPDATES_BINARY + postfix;
    // foutput.open (out_filename.c_str(),std::ios::trunc);
    std::ofstream outFile;
    outFile.open(out_filename, std::ios::out | std::ios::binary);
    if (!outFile) { std::cout << "error open file"; }
    InputEdge edgeInfo;
    fileLoad.open_file(inputFile.c_str());
    // timestamp_t cur_time = start_time; // europian time stamp
    while (fileLoad.next_line_reader(&edgeInfo, true))
    {
        outFile.write((char *) &edgeInfo, sizeof(edgeInfo));
        // cur_time = getNextSecond(cur_time);
    }
    fileLoad.close_file();
}

int number_of_times = 2;
int number_of_thread = 41;

class MeanVal
{
public:
    int cur_index;
    double arr[5] = {};
};

double median(double arr[], int size)
{
    std::sort(arr, arr + size);
    if (size % 2 != 0)
        return (double) arr[size / 2];
    return (double) (arr[(size - 1) / 2] + arr[size / 2]) / 2.0;
}

void convertResultByMedian(std::string OutPutFile)
{
    FILE *file_ptr;
    std::string filename;
    size_t line_size;
    char *_line;
    vertex_t offset;

    /// open file
    filename = "/local/user/graph500_22_result_updated-HAL.txt";
    int f = open(filename.c_str(), O_RDONLY);
    if (f < 0)
    {
        perror("Cannot open the input file");
        abort();
    }
    file_ptr = fdopen(f, "rt");
    if (file_ptr == NULL)
    {
        perror("Cannot open the input stream");
        abort();
    }
    line_size = 64;
    _line = (char *) malloc(line_size);
    offset = 0;

    /// output file
    std::ofstream foutput;
    std::string postfix = "graph500-22-halResultfinal1.txt";
    std::string out_filename = "/local/user/" + postfix;
    foutput.open(out_filename.c_str(), std::ios::out);
    if (!foutput) { std::cout << "error open file"; }

    /// reading file code

    ssize_t read;
    ssize_t read_total = 0;
    char **_line_ = &_line;
    size_t *linesize = &line_size;
    MeanVal obj[41] = {};
    for (int i = 0; i < number_of_thread; i++)
    {
        obj[i].cur_index = 0;
        for (int j = 0; j < number_of_times; j++)
        {
            obj[i].arr[j] = 0.0;
        }
    }
    int counter_no_time = 0;
    while ((read = getline(_line_, linesize, file_ptr)) != -1)
    {
        char *line = _line;
        read_total += read;

        if (*line == '\0' || *line == '#' || *line == '\n' || *line == '\r')
            continue;

        char *str_s = line;
        char *end_num;

        while (isspace(*str_s)) str_s++;

        // std::cout<<strtol(str_s, &end_num, 10)<<" ";
        int index = strtol(str_s, &end_num, 10);
        // edgeInfo->setSrcVertexId(strtol(str_s, &end_num, 10));
        if (str_s == end_num || *end_num == '\0' || !isspace(*end_num))
        {
            // std::cout << line;
            continue;
        }
        str_s = end_num + 1;
        while (isspace(*str_s)) str_s++;
        //   obj[index].count++;
        //  edgeInfo->setDestVertexId(strtol(str_s, &end_num, 10));
        obj[index].arr[obj[index].cur_index] = strtof(str_s, &end_num);
        obj[index].cur_index++;
        if (str_s == end_num || *end_num == '\0' || !isspace(*end_num))
        {
            // std::cout << line;
            continue;
        }

        if (read_total > 0)
        {
            offset += read_total;
            // break;
        }
        else
        {
            offset = 0;
            // break;
        }
    }
    for (int i = 0; i < 41; i++)
    {
        if (obj[i].cur_index > 0)
        {
            foutput << i << std::endl;
        }
    }
    for (int i = 0; i < 41; i++)
    {
        if (obj[i].cur_index > 0)
        {
            foutput << ((datasetSize / median(obj[i].arr, obj[i].cur_index)) / 1000000) << std::endl;
        }
    }
}