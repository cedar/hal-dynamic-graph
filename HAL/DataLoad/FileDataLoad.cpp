/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This C++ header file is used to manipulate binary dataset files.
 ******************************************************************************/
#include "FileDataLoad.h"
#include <omp.h>
#include "../TransactionManager.h"

/* FileDataLoad function defination */
std::unique_ptr<double[]> do_lcc_undirected(HBALStore *ds, vertex_t max_vertex_id);

void FileDataLoad::open_file(const char *file_name)
{
    filename = file_name;
    int f = open(filename.c_str(), O_RDONLY);
    if (f < 0)
    {
        perror("Cannot open the input file");
        abort();
    }
    file_ptr = fdopen(f, "rt");
    if (file_ptr == NULL)
    {
        perror("Cannot open the input stream");
        abort();
    }
    line_size = 64;
    _line = (char *) malloc(line_size);
    offset = 0;
}

void FileDataLoad::close_file()
{

    fclose(file_ptr);
}

bool FileDataLoad::next_line_reader(InputEdge *edgeInfo, bool readt)
{

    ssize_t read;
    ssize_t read_total = 0;
    char **_line_ = &_line;
    size_t *linesize = &line_size;
    while ((read = getline(_line_, linesize, file_ptr)) != -1)
    {
        char *line = _line;
        read_total += read;
        if (*line == '\0' || *line == '#' || *line == '\n' || *line == '\r')
            continue;

        char *str_s = line;
        char *end_num;

        while (isspace(*str_s)) str_s++;

        edgeInfo->setSrcVertexId(strtol(str_s, &end_num, 10));
        if (str_s == end_num || *end_num == '\0' || !isspace(*end_num))
        {
            // std::cout << line;
            continue;
        }
        str_s = end_num + 1;
        while (isspace(*str_s)) str_s++;

        edgeInfo->setDestVertexId(strtol(str_s, &end_num, 10));

        if (str_s == end_num || *end_num == '\0' || !isspace(*end_num))
        {
            // std::cout << line;
            continue;
        }

        //
        if (readt)
        {
            str_s = end_num + 1;
            while (isspace(*str_s)) str_s++;
            edgeInfo->setUpdtSrcTime(strtol(str_s, &end_num, 10));

            /* if (str_s == end_num || (*end_num != '\0' && *end_num != '\r' && *end_num != '\n')) {
                 // std::cout << line;
                 continue;
             }*/
            str_s = end_num + 1;
            while (isspace(*str_s)) str_s++;

            edgeInfo->setUpdtWeight(strtol(str_s, &end_num, 10));

            if (str_s == end_num || *end_num == '\0' || !isspace(*end_num))
            {
                // std::cout << line;
                continue;
            }

        }
        //
        if (read_total > 0)
        {
            offset += read_total;
            return true;
        }
        else
        {
            offset = 0;
            return false;
        }
    }
    return false;
}

/* StreamDataLoad function defination */
HBALStore *StreamDataLoad::dataLoad(std::string filePath, MemoryAllocator *la, bool isDuplicate)
{
   /* TransactionManager *ts = new TransactionManager(50, 1, isDuplicate, isDuplicate);
    HBALStore *hs = ts->getGraphDataStore(0, 0);
    uint64_t count_edge_count = 0;
    std::vector<edge_t> edges;
    double datastore_loading_time = 0;

    if (!isDuplicate)
    {
        std::fstream file;
        std::string in_filename = FILEPATH + filePath;
        file.open(in_filename, std::ios::in | std::ios::out);
        InputEdge inputEdge;
        std::vector<InputEdge> inputEdgeArr;
        while (file.read((char *) &inputEdge, sizeof(InputEdge)))
        {
            //hs->InsertUpdate(inputEdge, la);
            inputEdgeArr.push_back(inputEdge);
            std::cout << inputEdge.getSrcVertexId() << "," << inputEdge.getDestVertexId() << "," << inputEdge.getUpdtSrcTime() << "," << inputEdge.getUpdtWeight() << std::endl;

        }
        file.close();

        auto begin = std::chrono::high_resolution_clock::now();

        for (int j = 0; j < inputEdgeArr.size(); j++)
        {

            if (inputEdgeArr[j].getUpdtWeight() > 0)
            {
                hs->InsertSrcVertex(inputEdgeArr[j].getSrcVertexId(), la, 0);
                hs->InsertDestVertex(inputEdgeArr[j].getDestVertexId(), la, 0);
                hs->InsertEdge(inputEdgeArr[j], 0, la, 0);
                //  hs->InsertEdge({inputEdgeArr[j].getDestVertexId(),inputEdgeArr[j].getSrcVertexId(),inputEdgeArr[j].getUpdtSrcTime()}, la, 0);

                count_edge_count++;
            }
            else
            {
                hs->removeEdge(0, 0, 0, inputEdgeArr[j], la, 0);
            }
        }

        auto end = std::chrono::high_resolution_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);

        datastore_loading_time = elapsed.count() * 1e-9;
        std::cout << "results" << datastore_loading_time << std::endl;

    }
    else
    {
        // duplicate workload
        std::fstream file;
        std::string in_filename = FILEPATH_DUP + filePath;
        file.open(in_filename, std::ios::in | std::ios::out);

        size_t edge_count;
        file.read((char *) &edge_count, sizeof(edge_count));

        double datastore_loading_time;
        edges.clear();
        edges.resize(edge_count);
        file.read((char *) edges.data(), edge_count * sizeof(edge_t));
        file.close();
        auto begin = std::chrono::high_resolution_clock::now();

        for (int j = 0; j < edges.size(); j++)
        {
            if (j == 1000)
            {
                // break;

            }
            InputEdge ie;
            ie.setSrcVertexId(edges[j].src);
            ie.setDestVertexId(edges[j].dst);
            ie.setUpdtSrcTime(edges[j].timestamp);
            ie.setUpdtWeight(edges[j].weight);
            if (ie.getUpdtWeight() > 0)
            {

                hs->InsertSrcVertex(ie.getSrcVertexId(), la, 0);
                hs->InsertDestVertex(ie.getDestVertexId(), la, 0);
                hs->InsertEdge(ie, 0, la, 0);
                hs->InsertEdge({ie.getDestVertexId(), ie.getSrcVertexId(), ie.getUpdtSrcTime()}, 0, la, 0);
                count_edge_count++;
            }
            else
            {
                //removeEdge(vertex_t phy_src_id, vertex_t phy_dest_id, timestamp_t commit_time, InputEdge inputedge_info, MemoryAllocator *me, int thread_id)
                timestamp_t commit_time = getCurrTimeStamp();
                vertex_t phy_src_id = hs->physical_id(ie.getSrcVertexId());
                vertex_t phy_dest_id = hs->physical_id(ie.getDestVertexId());
                hs->InsertSrcVertex(ie.getSrcVertexId(), la, 0);
                hs->InsertDestVertex(ie.getDestVertexId(), la, 0);
                hs->removeEdge(phy_src_id, phy_dest_id, commit_time, ie, la, 0);
                hs->removeEdge(phy_dest_id, phy_src_id, commit_time, {ie.getDestVertexId(), ie.getSrcVertexId(), ie.getUpdtSrcTime()}, la, 0);
            }
        }
        auto end = std::chrono::high_resolution_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin);
        datastore_loading_time = elapsed.count() * 1e-9;
    }

    auto begin2 = std::chrono::high_resolution_clock::now();

    uint64_t src_vertex_m = hs->get_high_water_mark();
    int numberofedges = 0;
    for (int i = 0; i < src_vertex_m; i++)
    {
        numberofedges += hs->perSourceGetDegree(i, 0, la);

    }
    auto end2 = std::chrono::high_resolution_clock::now();
    auto elapsed2 = std::chrono::duration_cast<std::chrono::nanoseconds>(end2 - begin2);
    double datastore_loading_time2 = elapsed2.count() * 1e-9;

    std::cout << "Pagerank algorithm: " << datastore_loading_time2 << std::endl;

    //todo remaining rows that are at the end and not achieved the threshold count_row

    // calculate space consumtion
    //MemoryConsumption *memCal = new MemoryConsumption(hs);
    //    size_t spaceGb = memCal->getGB();
    hs->setLoadingTime(datastore_loading_time);
    hs->analytics_time = datastore_loading_time2;*/
    return 0;
}

void StreamDataLoad::delete_vertexArray()
{
    //  free(ws->getVertexArray());
}

StreamDataLoad::~StreamDataLoad()
{
    // std::cout<<""
}
