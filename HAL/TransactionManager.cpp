/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This C++ header file is used to manipulate binary dataset files.
 ******************************************************************************/


#include "TransactionManager.h"
HBALStore* TransactionManager::graph = NULL;
MemoryAllocator* TransactionManager::memAllocator = NULL;
size_t thread_local TransactionManager::thread_id = 0;

TransactionManager::TransactionManager(int initial_space, int no_of_thread, int ooo, bool duplicate_support)
{
    // EMPTY_SLOT = (void*) new EdgeBlock();
    // FLAG_DELETE = (void*) new EdgeBlock();
     memAllocator = new MemoryAllocator();
     memAllocator->init(initial_space, no_of_thread);

    graph = new HBALStore();
    graph->IsOutOfOrder = ooo;
    graph->IsDuplicateSupport = duplicate_support;
    std::cout<<graph->IsDuplicateSupport<<std::endl;

    graph->invalidBlock  = NULL;
    HBALStore::isReadActive = false;

    graph->numberOfThread = no_of_thread;
    graph->perThreadNumberOfEdges    = static_cast<u_int64_t *>(malloc(no_of_thread * 8));
    graph->perThreadNumberOfvertices = static_cast<u_int64_t *>(malloc(no_of_thread * 8));

    graph->invalidListDegree         = static_cast<InvalidNodeAddress **>(malloc(THRESHOLDINVALID * 8));
    memset(graph->invalidListDegree,0, THRESHOLDINVALID * 8);
#ifdef DBUGFLAGG
    graph->foutput.open (LOGFILEPATH,std::ios::out);
    if(!graph->foutput){std::cout<<"error open file";}
#endif
#ifdef DBUGFLAGPERSOURCE
    graph->foutput.open (LOGFILEPATH,std::ios::out);
    if(!graph->foutput){std::cout<<"error open file";}
#endif
    graph->min_read_version.store( MaxValue);
    graph->min_read_degree_version.store(MaxValue);
    graph->readTable = new ReadTable();

     graph->readTable->initReadTable();

    // graph->invalList = reinterpret_cast<InvalidList **>(std::aligned_alloc(8, no_of_thread*sizeof(InvalidList)));

     for(int i=0;i<no_of_thread;i++)
     {
           //graph->invalList[i] = new InvalidListVal();
          // graph->invalList[i]->edgeInfo = NULL;
         //  graph->invalList[i]->leafInfo = NULL;
         graph->perThreadNumberOfEdges[i] = 0;
         //graph->perThreadNumberOfvertices[i] = 0;
     }

    for(int j=0;j<THRESHOLDINVALID;j++)
     {
         graph->invalidListDegree[j] = new InvalidNodeAddress();
         graph->invalidListDegree[j]->ptr = NULL;
         graph->invalidListDegree[j]->lock.initLock();// = {0};
        // graph->InvalidEdgeInfo[j]   = new InvalidEdgeInfoAddress();
     }

    // HBALStore::logical_to_physical
  // HBALStore::initVertexIndrList(memAllocator);



}

HBALStore* TransactionManager::getGraphDataStore(bool isRead, int version_read)
{
    if(!isRead)
    {
        return graph;
    }
    else
    {
        HBALStore::isReadActive = true;

     //  graph->readTable->readQuery[version_read] = getCurrTimeStamp();
        timestamp_t time_stamp_read = getCurrTimeStamp();
        graph->min_read_version.store(time_stamp_read);
        graph->min_read_degree_version.store(time_stamp_read);
     //   std::cout<<"hello ghufran 0.2 end"<<version_read<<std::endl;

        // std::cout<<graph->readTable->readQuery[version_read]<<std::endl;
       // graph->readTable->lock.unlock();
       // HBALStore::querylock.unlock();

        return graph;
    }
}

void TransactionManager::addReadThread(int id)
{
    thread_id = id;
}
void TransactionManager::finishTransaction()
{
    //graph->readTable->lock.lock();
   // HBALStore::querylock.lock();

    //graph->readTable->readQuery[thread_id] = std::numeric_limits<timestamp_t>::max();
    graph->min_read_version = MaxValue;
    graph->min_read_degree_version = MaxValue;

    // graph->readTable->lock.unlock();
   // HBALStore::querylock.unlock();

}

MemoryAllocator* TransactionManager::getMemory()
{
    return memAllocator;
}
