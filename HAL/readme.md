## HAL dynamic graph
The source code to the paper "Dynamic Graph Databases with Out-of-order Updates". To be used with the [GFE experiment driver](https://gitlab.inria.fr/cedar/gfe-driver).

---
### Prerequisite

- Intel Threading Building Blocks 2 (version 2020.1-2)
- A compiler compliant with C++17 standards and equipped with OpenMP support. We employed GCC 10.
---
### Structure of Repository

- <kbd>DataLoad/</kbd> Data Handling Folder
- <kbd>MemoryPool/</kbd> Memory Allocator Suite
- <kbd>StorageEngine/</kbd> HAL data structure
- <kbd>ThirdParty/</kbd> External Libraries Folder

### Building
To build the static HAL dynamic graph library

<kbd>mkdir build && cd build && cmake .. &&  make</kbd>

---
### Integrate in your project
```c++
#include TransactionManager.h
#include DataLoad/InputEdge.h
TransactionManager *tm = new TransactionManager(initial_space_of_memory, number_of_thread, isOutOfOrderInsertionEnable, isOutOfOrderUpdatesEnable);
HBALStore *database = tm->getGraphDataStore(false, 0);
// example input edge
InputEdge edge; 
edge.setUpdtSrcTime(1);
edge.setDestVertexId(2);
edge.setUpdtSrcTime(1);
edgeInsertion.setUpdtWeight(0.4);
// Insertion of an edge @executeEdgeInsertion(InputEdge, memory allocator, threadId)
database->executeEdgeInsertion(edge, tm->getMemory(), 0);
// deletion of an edge @executeEdgeDeletion(InputEdge, memory allocator, threadId)
database->executeEdgeDeletion(edge, tm->getMemory(), 0); 
