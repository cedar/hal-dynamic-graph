
/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This C++ header file is used to manipulate binary dataset files.
 ******************************************************************************/

#ifndef OUR_IDEA_LEAFNODE_H
#define OUR_IDEA_LEAFNODE_H
// class to store deletion information related to destination node
//#include "../../SharedLib.h"
using vertex_d = int64_t;
using timestamp_l = int64_t;
class ITM;
class LeafNode
{
public:
    LeafNode()
    {
        srcTime=0;
        wts = 0;
        invalEntry = NULL;
    }
    void intLeafNode()
    {
        srcTime=0;
        wts = 0;
        invalEntry = NULL;
    }

    void setSTime(timestamp_l src_time)
    {
        srcTime = src_time;
    }
    void setWTsEdge(timestamp_l wts)
    {
        this->wts = wts;
    }
    void setInvldTime(ITM *itm)
    {
        invalEntry = itm;
    }

    timestamp_l getSTime() const
    {
        return srcTime;
    }
    timestamp_l getWTsEdge()
    {
        return wts;
    }
    ITM* getInvalEntry()
    {
        return invalEntry;
    }
private:
   // vertex_d  dNode; //destination node
    timestamp_l srcTime; // source time of the destination node
    timestamp_l wts; // transaction time of the destination node
    ITM *invalEntry; // default null; in case of deletion store the information of an update which consider the LeafNode as delete
};
class ITM
{
public:
    ITM()
    {

    }
    void setSrcTime(timestamp_l src_time)
    {
        srcTime = src_time;
    }
    void setWts(timestamp_l wts)
    {
        this->wts = wts;
    }
    void setNext(ITM *next)
    {
        this->next = next;
    }
    ITM* getNext()
    {
        return next;
    }
    timestamp_l getSrcTime()
    {
        return srcTime;
    }
    timestamp_l getWts()
    {
        return wts;
    }
private:
    timestamp_l srcTime;
    timestamp_l wts;
    ITM *next;
};
#endif //OUR_IDEA_LEAFNODE_H
