/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This C++ header file is used to manipulate binary dataset files.
 ******************************************************************************/

#ifndef OUR_IDEA_HASHTABLECUSTOM_H
#define OUR_IDEA_HASHTABLECUSTOM_H
#include "../RWSpinLock.h"
#include "../../MemoryPool/BlockAllocator.hpp"
#include "../../StorageEngine/BlockAdjacencyList.h"

const int defaultSize1 = 4u;

class Bucket
{
public:
    void initBucket();
    void setDestNode(vertex_d dest_node);
    void setAdjacencyPos(u_int64_t aem);
    void setNextBucket(Bucket *b);
    vertex_d  getDestNode();
    u_int64_t getAdjacencyPos();
    Bucket* getNextBucket();
private:
    vertex_d DestNode;
    u_int64_t adjacencyPos;

    // we maintain block size of index at the time of insertion so that we can compare at the time of deletion
    // block indexes for edges are dynamic consider the indirection resizes change the index of blocks
    // for that we need to maintain the the block size at the time of insertion and get the block size at the time of deletion
    // formula to get the correct index: (current Indr size - (previous indr size - blockIndex))

    // blockIndex very important index to set deletion bit

    Bucket *nextBucket;
};

class HashTableIndex
{

public:
    HashTableIndex(MemoryAllocator *la,int thread_id);
    void insert(vertex_t key, u_int64_t adj_pos, MemoryAllocator *la, int thread_id);
    Bucket* getIndexVal(vertex_d  key);
    std::tuple<Bucket*,Bucket*> getIndexValDelete(vertex_d  key);
    void resizeHashTable(MemoryAllocator *la, int thread_id);
    Bucket** getHashIndexArr();
    int getCurSize();
    void setEntryCounter(int c);
    int getEntryCounter();
  //  RWSpinLock *locks{};
  //  RWSpinLock resizeLock{};
private:
Bucket **hashIndexArr;
int curSize;
int entryCounter;
};




#endif //OUR_IDEA_HASHTABLECUSTOM_H
