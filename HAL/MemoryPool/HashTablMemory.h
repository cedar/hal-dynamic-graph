/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This file is related to the custom memory allocator that we implemented, in which we used a two-level queue, a hash table, and a two-level vector.
 ******************************************************************************/
#ifndef OUR_IDEA_HASHTABLMEMORY_H
#define OUR_IDEA_HASHTABLMEMORY_H

#include <string.h>

#include <vector>

#include "../SharedLib.h"
#include "../ThirdParty/RWSpinLock.h"

#include <mutex>

class TwoLevelQueue;

class TwoLevelVector;

/* forward declaration block allocator.h*/
class FixedSizeVector;

class PerThreadMemoryAllocator;

class GlobalAllocator;

class MemoryAllocator;

const int defaultSize = 907;
struct AllocatorBlock
{
    TwoLevelVector *memBlock;
    TwoLevelQueue *freeList;
    spinlock lock{0};
};

class MemoryBucket
{

public:
    void setBlockSize(std::size_t block_size);

    void setFreeBlockList(AllocatorBlock *free_list);

    void setNextBucket(MemoryBucket *b);

    int64_t getBlockSize();

    AllocatorBlock *getFreeBlockList();

    MemoryBucket *getNextBucket();

    bool isAvailable = false;


    std::size_t blockSize;
    AllocatorBlock *freeBlock;
    MemoryBucket *nextBucket;
};

class HashTableMemoryIndex
{
public:
    // RWSpinLock perHashIndexLock[defaultSize];
    // std::mutex hashTableMutex;
    HashTableMemoryIndex(PerThreadMemoryAllocator *ptr);

    void insert(std::size_t key, AllocatorBlock *val, PerThreadMemoryAllocator *ptr);

    MemoryBucket *getIndexVal(std::size_t key);

    void resizeHashTable(PerThreadMemoryAllocator *ptr);

    int getDefaultSize();

    MemoryBucket **getHashIndexArr();

    int getCurSize();

    MemoryBucket **hashIndexArr;
    int curSize1;
    int entryCounter1;
    int64_t multiplier1;
};

// two level queue
class FixedSizeBlock
{
public:
    void initVal();

    void push(void *val);

    void *pop();

    void initBlockArr(PerThreadMemoryAllocator *me);

    void **getBlockArr();

    int getCurIndex() const;

    bool IscurIndexFull() const;


    void **blockArr;
    int cur;
};

// Two level vector implementation
class TwoLevelQueue
{
public:
    void initVal();

    void insert(void *ga, PerThreadMemoryAllocator *ptr);

    void *get_block();

    bool empty();

    bool emptyMultipleBlock(int required_block);

    void addBlock(PerThreadMemoryAllocator *ptr);

    void setIndrArray(PerThreadMemoryAllocator *ptr);


    FixedSizeBlock **IndrArray;
    int curIndex;
    int curSize;
    int blockSize;
};

#endif //OUR_IDEA_HASHTABLMEMORY_H
