/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 ******************************************************************************/
#include "BlockAllocator.hpp"
#include "HashTablMemory.h"
#include <iostream>
/* GlobalAllocator function defination */
void GlobalAllocator::init(int size, bool isInit, bool greater_than_16_mb)
{

    if (isInit)
    {
        totalSpace = size * (1024ul * 1024ul);
        mem = mmap(nullptr, totalSpace, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_NORESERVE, -1, 0);
        // touch each page to insert int to allocate memory
        if (mem == MAP_FAILED && totalSpace == 0)
        {
            mem = mmap(nullptr, 16 * (1024ul * 1024ul), PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_NORESERVE, -1, 0);
        }
        std::size_t currentAddress1 = 0;


        size_t y;
        for (size_t i = 0; i < totalSpace; i += 0x1000)
        {
            currentAddress1 = (std::size_t) mem + i;
            size_t *y = reinterpret_cast<size_t *>((void *) currentAddress1);
            *y = 0;
        }

        offset = 0;
    }
    else
    {

        // totalSpace = size;
        totalSpace = size * (1024ul * 1024ul);

        mem = mmap(nullptr, totalSpace, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_NORESERVE, -1, 0);
        if (mem == MAP_FAILED)
        {
            std::cout << size << std::endl;
            std::cout << totalSpace << std::endl;

            std::cout << "error memory" << std::endl;
        }
        // touch each page to insert int to allocate memory
        offset = 0;
    }
}

void *GlobalAllocator::allocate(int size)
{
    const std::size_t cur_addr = (std::size_t) mem + offset;
    offset += size;
    return (void *) cur_addr;
}

vertex_t GlobalAllocator::getOffset()
{
    return offset;
}

vertex_t GlobalAllocator::getTotalSpace()
{
    return totalSpace;
}

bool GlobalAllocator::isFreeSpace(vertex_t block_size)
{
    if ((offset + block_size) > totalSpace)
    {
        return false;
    }
    else
    {
        return true;
    }
}

/* FixedSizeVector class defination */
void FixedSizeVector::initVal()
{
    curIndex = -1;
}

void FixedSizeVector::setBlockArr(GlobalAllocator *val)
{
    blockArr[++curIndex] = val;
}

GlobalAllocator **FixedSizeVector::getBlockArr()
{
    return blockArr;
}

int FixedSizeVector::getCurIndex() const
{
    return curIndex;
}

bool FixedSizeVector::IscurIndexFull() const
{
    if (curIndex == (MEMORY_VECTOR_SIZE - 1))
    {
        return true;
    }
    else
    {
        return false;
    }
}

void FixedSizeVector::initBlockArray(PerThreadMemoryAllocator *me)
{
    blockArr = reinterpret_cast<GlobalAllocator **>(std::aligned_alloc(8, 8 * MEMORY_VECTOR_SIZE));

}

/* Two level vector */

void TwoLevelVector::initVal()
{
    IndrArray = NULL;
    curIndex = -1;
    curSize = 1;
    blockSize = 0;
}

void TwoLevelVector::insert(GlobalAllocator *ga)
{
    if (curIndex == (blockSize - 1))
    {
        if (IndrArray[curIndex]->IscurIndexFull())
        {
            setIndrArray();
            addBlock();
            IndrArray[curIndex]->setBlockArr(ga);
        }
        else
        {
            IndrArray[curIndex]->setBlockArr(ga);
        }
    }
    else
    {
        // start from here
        if (IndrArray[curIndex]->IscurIndexFull())
        {
            addBlock();
            IndrArray[curIndex]->setBlockArr(ga);
        }
        else
        {
            IndrArray[curIndex]->setBlockArr(ga);
        }
    }
}

void *TwoLevelVector::getBlock(int requested_block)
{

    if (IndrArray[curIndex]->getBlockArr()[IndrArray[curIndex]->getCurIndex()]->isFreeSpace(requested_block))
    {
        return IndrArray[curIndex]->getBlockArr()[IndrArray[curIndex]->getCurIndex()]->allocate(requested_block);
    }
    else
    {
        GlobalAllocator *ptr = static_cast<GlobalAllocator *>(std::aligned_alloc(8, sizeof(GlobalAllocator)));
        if (requested_block == 40 || requested_block == 32)
        {
            ptr->init(512, false);
        }
        else if (requested_block <= FOURMB)
        {
            ptr->init(4, false);
        }
        else if (requested_block > FOURMB && requested_block <= EIGHTMB)
        {
            ptr->init(8, false);
        }
        else if (requested_block > EIGHTMB && requested_block <= SIXTEENMB)
        {
            ptr->init(16, false);
        }
        else
        {
            ptr->init(requested_block, false, true);
        }
        insert(ptr);
        return IndrArray[curIndex]->getBlockArr()[IndrArray[curIndex]->getCurIndex()]->allocate(requested_block);
    }
}

void TwoLevelVector::addBlock()
{
    FixedSizeVector *new_block = static_cast<FixedSizeVector *>(aligned_alloc(8, sizeof(FixedSizeVector)));
    new_block->initVal();
    new_block->initBlockArray(NULL);
    IndrArray[++curIndex] = new_block;
}

void TwoLevelVector::setIndrArray()
{
    if (IndrArray == NULL)
    {
        // blockLen = 1;

        IndrArray = reinterpret_cast<FixedSizeVector **>(static_cast<FixedSizeVector **>(aligned_alloc(8, 8 * (curSize * fixedSize))));//static_cast<EdgeBlock **>(malloc(8 * (blockLen * fixedSize)));
        blockSize = fixedSize;

        FixedSizeVector *new_block = static_cast<FixedSizeVector *>(aligned_alloc(8, sizeof(FixedSizeVector)));
        new_block->initVal();
        new_block->initBlockArray(NULL);

        IndrArray[++curIndex] = new_block;
        /*
for(int i=0;i<(blockLen * fixedSize);i++)
{
blockArr[i] = NULL;
}

*/


    }
    else
    {
        int blenprev = curSize;
        int b_len_new = 2 * curSize;

        FixedSizeVector **temp = static_cast<FixedSizeVector **>(aligned_alloc(8, 8 * ((b_len_new) * fixedSize)));

        //  FixedTimeIndexBlock **temp2;
        // todo deletion mechanism ()create node in globle linklist and add the address of deleted index array and with time to replace
        // the garbage collector will deallocate the memory than
        // int k=0;
        // int tempC = (blenprev) * fixedSize;

        int bprev = (blenprev) * fixedSize;

        memcpy(temp, IndrArray, bprev * 8);

        curIndex = blenprev - 1;

        void *ptr = IndrArray;

        IndrArray = temp;
        free(ptr);
        blockSize = b_len_new * fixedSize;

        curSize = b_len_new;
    }
}

/* PerThreadMemoryAllocator class defination */
void PerThreadMemoryAllocator::init(int space, int total_thread)
{

    /*

     8
     16
     32 more frequent block request
     40 more frequent block request
     64
     128
     256
     512
     1024
     2048
     4096
     8192


     // one thing I understand for bigger datasets graph500-26 I should miss the hashtable for element <= 32
     */

    int BlockSize[15] = {8, 16, 24, 32, 40, 48, 56, 64, 128, 256, 512, 1024, 2048, 4096, 8192};
    int spaceNeeded[15] = {4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4};
    int extraSpaceAlloc[12] = {1, 1, 8, 8, 1, 1, 1, 1, 1, 1, 1, 1};

    block = static_cast<HashTableMemoryIndex *>(std::aligned_alloc(8, sizeof(HashTableMemoryIndex)));
    new(block) HashTableMemoryIndex(NULL);

    for (int i = 0; i < 15; i++)
    {
        AllocatorBlock *ptr = new AllocatorBlock();

        TwoLevelVector *vec_two = static_cast<TwoLevelVector *>(std::aligned_alloc(8, sizeof(TwoLevelVector)));
        vec_two->initVal();
        vec_two->setIndrArray();

        GlobalAllocator *gl = static_cast<GlobalAllocator *>(std::aligned_alloc(8, sizeof(GlobalAllocator)));

        gl->init((spaceNeeded[i]), true); // space should be block dependent

        vec_two->insert(gl);

        ptr->memBlock = vec_two;

        block->insert(BlockSize[i], ptr, NULL);
        block->getIndexVal(BlockSize[i])->isAvailable = false;
    }

    for (int i = 0; i < 11; i++)
    {
        AllocatorBlock *ptr = new AllocatorBlock();

        TwoLevelVector *vec_two = static_cast<TwoLevelVector *>(std::aligned_alloc(8, sizeof(TwoLevelVector)));
        vec_two->initVal();
        vec_two->setIndrArray();

        GlobalAllocator *gl = static_cast<GlobalAllocator *>(std::aligned_alloc(8, sizeof(GlobalAllocator)));

        if (((std::size_t) 1 << (i + 14)) <= SIXTEENMB && ((std::size_t) 1 << (i + 14)) > EIGHTMB)
        {
            gl->init(16, true); // space should be block dependent
        }
        else if (((std::size_t) 1 << (i + 14)) > FOURMB && ((std::size_t) 1 << (i + 14)) <= EIGHTMB)
        {
            gl->init(8, true); // space should be block dependent
        }
        else
        {
            gl->init(4, true); // space should be block dependent
        }
        vec_two->insert(gl);

        ptr->memBlock = vec_two;

        block->insert(((std::size_t) 1 << (i + 14)), ptr, NULL);
        block->getIndexVal((std::size_t) 1 << (i + 14))->isAvailable = false;
    }

    //Hash table resize block that are not power of two
    /* for(int i=0;i<9;i++)
     {
         MemoryBucket *m1 = block->getIndexVal(PRIMETABLE[i]*8);
         if(m1 == NULL)
         {
             AllocatorBlock *ptr = new AllocatorBlock();

             TwoLevelVector *vec_two = static_cast<TwoLevelVector *>(std::aligned_alloc(8,sizeof(TwoLevelVector)));
             vec_two->initVal();
             vec_two->setIndrArray();

             GlobalAllocator *gl = static_cast<GlobalAllocator *>(std::aligned_alloc(8,sizeof(GlobalAllocator)));
             if((PRIMETABLE[i]*8) <= SIXTEENMB && (PRIMETABLE[i]*8) > EIGHTMB) {
                 gl->init(16, true); // space should be block dependent
             }
             else if((PRIMETABLE[i]*8) > FOURMB && ((std::size_t) 1 << (i+14)) <= EIGHTMB){
                 gl->init(8, true); // space should be block dependent


             }else{
                 gl->init(4, true); // space should be block dependent

             }

             vec_two->insert(gl);

             ptr->memBlock = vec_two;

             block->insert((PRIMETABLE[i]*8),ptr,NULL);

             block->getIndexVal((PRIMETABLE[i]*8))->isAvailable = false;
         }
     }*/
}

std::size_t PerThreadMemoryAllocator::getPerThreadFreeMemory()
{
    std::size_t memory_per_thread_free = 0;
    for (int i = 0; i < block->getCurSize(); i++)
    {
        MemoryBucket *nextPtr = block->hashIndexArr[i];
        if (nextPtr != NULL)
        {
            while (nextPtr != NULL)
            {
                // block size and bucket that contains AllocateBlock class (freequeue)
                AllocatorBlock *aloc_block = nextPtr->getFreeBlockList();
                TwoLevelQueue *two_level_queue = aloc_block->freeList;
                if (two_level_queue != NULL)
                {
                    // two level vector first access vector[0] then vector[1]
                    FixedSizeBlock **IndrArray = two_level_queue->IndrArray;
                    for (int j = 0; j <= two_level_queue->curIndex; j++)
                    {
                        // get block
                        FixedSizeBlock *fixed_block = IndrArray[j];
                        if (fixed_block != NULL)
                        {
                            for (int k = 0; k <= fixed_block->cur; k++)
                            {
                                void *block = fixed_block->blockArr[k];
                                if (block != NULL)
                                {
                                    memory_per_thread_free += nextPtr->blockSize;
                                }
                            }
                        }
                    }
                }
                //
                nextPtr = nextPtr->getNextBucket();
            }
        }

    }
    /*std::size_t memory_per_thread_alloc = 0;
        for(int i=0;i<block->getCurSize();i++)
    {
        MemoryBucket *nextPtr = block->hashIndexArr[i];
        if(nextPtr != NULL)
        {
            while(nextPtr != NULL)
            {
                // block size and bucket that contains AllocateBlock class (freequeue)
                AllocatorBlock *aloc_block      =  nextPtr->getFreeBlockList();
                TwoLevelVector *two_level_queue =  aloc_block->memBlock;
                if(two_level_queue != NULL)
                {
                    // two level vector first access vector[0] then vector[1]
                    FixedSizeVector **IndrArray =  two_level_queue->IndrArray;
                    for(int j=0;j<=two_level_queue->curIndex;j++)
                    {
                        // get block
                        FixedSizeVector *fixed_block = IndrArray[j];
                        if(fixed_block !=NULL)
                        {
                            for (int k = 0; k <= fixed_block->curIndex; k++)
                            {
                                GlobalAllocator* block = fixed_block->blockArr[k];
                                if(block !=NULL)
                                {
                                    memory_per_thread_alloc += block->totalSpace;
                                }
                            }
                        }
                    }
                }

                //
                // std::cout<<"Block size: "<<nextPtr->blockSize<<" Memory allocation: "<<memory_per_thread_alloc<<std::endl;

                nextPtr = nextPtr->getNextBucket();
            }
        }

    }
    */
    return memory_per_thread_free;
}

std::size_t PerThreadMemoryAllocator::getPerThreadAllocMemory()
{

    std::size_t memory_per_thread_alloc = 0;
    for (int i = 0; i < block->getCurSize(); i++)
    {
        MemoryBucket *nextPtr = block->hashIndexArr[i];
        if (nextPtr != NULL)
        {
            while (nextPtr != NULL)
            {
                // block size and bucket that contains AllocateBlock class (freequeue)
                AllocatorBlock *aloc_block = nextPtr->getFreeBlockList();
                TwoLevelVector *two_level_queue = aloc_block->memBlock;
                if (two_level_queue != NULL)
                {
                    // two level vector first access vector[0] then vector[1]
                    FixedSizeVector **IndrArray = two_level_queue->IndrArray;
                    for (int j = 0; j <= two_level_queue->curIndex; j++)
                    {
                        // get block
                        FixedSizeVector *fixed_block = IndrArray[j];
                        if (fixed_block != NULL)
                        {
                            for (int k = 0; k <= fixed_block->curIndex; k++)
                            {
                                GlobalAllocator *block = fixed_block->blockArr[k];
                                if (block != NULL)
                                {
                                    memory_per_thread_alloc += block->totalSpace;
                                }
                            }
                        }
                    }
                }
                nextPtr = nextPtr->getNextBucket();
            }
        }

    }
    return memory_per_thread_alloc;
}

std::size_t PerThreadMemoryAllocator::getPerThreadAllocMemoryBlock()
{

    std::size_t memory_per_thread_alloc = 0;
    for (int i = 0; i < block->getCurSize(); i++)
    {
        MemoryBucket *nextPtr = block->hashIndexArr[i];
        if (nextPtr != NULL)
        {
            while (nextPtr != NULL)
            {
                // block size and bucket that contains AllocateBlock class (freequeue)
                AllocatorBlock *aloc_block = nextPtr->getFreeBlockList();
                TwoLevelVector *two_level_queue = aloc_block->memBlock;
                if (two_level_queue != NULL)
                {
                    // two level vector first access vector[0] then vector[1]
                    FixedSizeVector **IndrArray = two_level_queue->IndrArray;
                    for (int j = 0; j <= two_level_queue->curIndex; j++)
                    {
                        // get block
                        FixedSizeVector *fixed_block = IndrArray[j];
                        if (fixed_block != NULL)
                        {
                            for (int k = 0; k <= fixed_block->curIndex; k++)
                            {
                                GlobalAllocator *block = fixed_block->blockArr[k];
                                if (block != NULL)
                                {
                                    memory_per_thread_alloc += block->offset;
                                }
                            }
                        }
                    }
                }
                nextPtr = nextPtr->getNextBucket();
            }
        }

    }
    return memory_per_thread_alloc;
}

void *PerThreadMemoryAllocator::Allocate(std::size_t requestedBlock, bool IsHash)
{
    if (requestedBlock <= SIXTEENMB)
    {
        // return std::malloc(requestedBlock);
        MemoryBucket *m = block->getIndexVal(requestedBlock);
        if (!m->isAvailable)
        {
            /////

            void *ptr = m->getFreeBlockList()->memBlock->getBlock(requestedBlock);

            return ptr;
        }
        else
        {
            ///// if size is 24 byte then lock

            TwoLevelQueue *q = m->getFreeBlockList()->freeList;

            if (q->empty())
            {
                void *ptr = m->getFreeBlockList()->memBlock->getBlock(requestedBlock);

                return ptr;
            }
            else
            {
                void *ptr = q->get_block();

                return ptr;
            }

        }
    }
    else
    {
        return mmap(nullptr, requestedBlock, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_NORESERVE, -1, 0);//aligned_alloc(8,requestedBlock);//mmap(nullptr, requestedBlock, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_NORESERVE, -1, 0);//aligned_alloc(8,requestedBlock);
    }
}


void PerThreadMemoryAllocator::free(void *ptr, std::size_t deletedBlock, bool isHash)
{
    if (deletedBlock <= SIXTEENMB)
    {
        TwoLevelQueue *two = block->getIndexVal(deletedBlock)->getFreeBlockList()->freeList;
        if (two == NULL)
        {
            ///
            TwoLevelQueue *newB = static_cast<TwoLevelQueue *>(this->Allocate((sizeof(TwoLevelQueue))));
            newB->initVal();
            newB->setIndrArray(this);
            newB->insert(ptr, this);
            block->getIndexVal(deletedBlock)->getFreeBlockList()->freeList = newB;
            block->getIndexVal(deletedBlock)->isAvailable = true;
            ////
        }
        else
        {
            two->insert(ptr, this);
        }
    }
    else
    {
        munmap(ptr, deletedBlock);
    }


}

/* MemoryAllocator class defination */
void MemoryAllocator::init(int totalSpace, int totalThread)
{
    int per_thread_space = totalSpace / totalThread;
    // std::cout<<
    // todo perthreadalloc
    perThreadArr = static_cast<PerThreadMemoryAllocator **>(malloc(totalThread * 8));

    for (int i = 0; i < totalThread; i++)
    {
        PerThreadMemoryAllocator *ptr = static_cast<PerThreadMemoryAllocator *>(aligned_alloc(8, sizeof(PerThreadMemoryAllocator)));
        ptr->init(per_thread_space, totalThread);
        ptr->thread_id = i;
        // ptr->lock.initLock();// = 0;
        perThreadArr[i] = ptr;
    }
}

PerThreadMemoryAllocator *MemoryAllocator::getPerThreadMemoryAllocator(int thread_id)
{
    return perThreadArr[thread_id];
}

void *MemoryAllocator::Allocate(std::size_t requestedBlock, int thread_id, bool IsHash)
{
    void *ptr;
    ptr = perThreadArr[thread_id]->Allocate(requestedBlock, IsHash);
    return ptr;

}

void MemoryAllocator::free(void *ptr, std::size_t deletedBlock, int thread_id, bool isHash)
{
    perThreadArr[thread_id]->free(ptr, deletedBlock, isHash);
}

