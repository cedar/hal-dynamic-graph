/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This file is related to our custom memory allocator interface to the external driver. We used it in the HAL data structure for dynamic memory allocation.
 ******************************************************************************/
#ifndef LINEARALLOCATOR_H
#define LINEARALLOCATOR_H

#include <sys/mman.h>
#include <cassert>
#include <queue>
#include <utility>  //declarations of unique_ptr
#include <atomic>
#include <random>
#include <chrono>
#include <ctime>
#include<vector>
#include <memory>
#include <unordered_map>
#include <mutex>
#include "../SharedLib.h"
#include "../ThirdParty/RWSpinLock.h"

class TwoLevelVector;

class FixedSizeVector;

class PerThreadMemoryAllocator;

class HashTableMemoryIndex;

class GlobalAllocator
{
public:
    std::mutex globalAllocator;

    void init(int size, bool isInit, bool greater_than_16_mb = false);

    void *allocate(int size);

    vertex_t getOffset();

    vertex_t getTotalSpace();

    bool isFreeSpace(vertex_t block_size);

    void *mem;
    vertex_t offset;
    vertex_t totalSpace;
    //todo mutex lock on allocation
};


// fixed size vector

class FixedSizeVector
{

public:
    void initVal();

    void setBlockArr(GlobalAllocator *val);

    void initBlockArray(PerThreadMemoryAllocator *me);

    GlobalAllocator **getBlockArr();

    int getCurIndex() const;

    bool IscurIndexFull() const;


    GlobalAllocator **blockArr;
    int curIndex;
};

// Two level vector implementation
class TwoLevelVector
{

public:
    void initVal();

    void insert(GlobalAllocator *ga);

    void addBlock();

    void *getBlock(int requested_block);

    void setIndrArray();

    FixedSizeVector **IndrArray;
    int curIndex;
    int curSize;
    int blockSize;
};


struct PerThreadMemoryAllocator
{
public:
    PerThreadMemoryAllocator();

    void init(int space, int total_thread);

    void *Allocate(std::size_t requestedBlock, bool IsHash = false);

    std::size_t getPerThreadFreeMemory();

    std::size_t getPerThreadAllocMemory();

    std::size_t getPerThreadAllocMemoryBlock();

    // void* getBlock(int index, int requestedBlock);
    void free(void *ptr, std::size_t deletedBlock, bool isHash = false);

    int curIndex;
    int totalThread;
    int spacePerThread;
    bool freeBlockHash = false;
    bool freeBlockBlock = false;
    // own two level vector implementation
    int thread_id;
    // own hash table and queue implementation
    HashTableMemoryIndex *block;
    spinlock lock{0};
    // GlobalAllocator* memAllocation[10];
    //  AllocatorSize **allocatorBlockArr;

    // todo  mutex lock array 14 size
};

class MemoryAllocator
{
public:
    void init(int totalSpace, int totalThread);

    void *Allocate(std::size_t requestedBlock, int thread_id, bool IsHash = false);

    void free(void *ptr, std::size_t deletedBlock, int thread_id, bool isHash = false);

    PerThreadMemoryAllocator *getPerThreadMemoryAllocator(int thread_id);


    PerThreadMemoryAllocator **perThreadArr;
    spinlock **perThreadArrLock;

};


#endif /* LINEARALLOCATOR_H */