/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This C++ file is an entry point to our system if we want to run independently and also gives us access to calculate the five-time median of all the experiments (insertion-only, analytics-only, etc.).
 ******************************************************************************/
#include <iostream>
#include<fstream>
#include "SharedLib.h"
#include "DataLoad/FileDataLoad.h"
#include "Test/integerToByte.h"
#include "Test/testPointer.h"

//#include "ReadStore/ReadStore.h"
#include <sys/mman.h>
#include <cassert>
#include <linux/mman.h>
#include <rocksdb/db.h>

#include <rocksdb/utilities/transaction_db.h>

#include <oneapi/tbb/concurrent_vector.h>
#include <ctime>
#include "StorageEngine/BlockAdjacencyList.h"
#include<vector>
#include "DataLoad/Synthetic_Timestamp.h"
#include "MemoryPool/BlockAllocator.hpp"
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
//#include "ReadQuery/ReadQuery.h"

#include "ThirdParty/HashTable-Custom/HashTableCustom.h"
//#include "Test/Thread1.h"
#include "ThirdParty/plot/matplotlibcpp.h"
#include "ThirdParty/RWSpinLock.h"
#include "MemoryPool/HashTablMemory.h"

namespace plt = matplotlibcpp;
//#include "ThirdParty/ART/Tree.h"
//#include "Test/concurrent_Array.h"
#include "TransactionManager.h"
#include "DataLoad/Synthetic_Timestamp.h"
#include "ThirdParty/libcommon/include/lib/common/timer.hpp"
#include "ThirdParty/ART-2/include/art.hpp"

//using namespace art;
#define MB_TO_RESERVE 1
#define LENGTH (MB_TO_RESERVE*1024*1024*1024)

#define MAP_HUGE_1GB (30 << MAP_HUGE_SHIFT)
#define LENGTH (MB_TO_RESERVE*1024*1024*1024)

int main(int argc, char *argv[])
{

    if (/*!(argc < 2)*/0)
    {
        /// In this block, we compute the five-times median of all the experiments.
        // Get the argument as a string from argv
        std::string argString = argv[1];

        // Convert the string to an integer using atoi (or use std::stoi for C++11 and later)
        int integerValue = std::atoi(argString.c_str());

        if (integerValue == 1)
        {
            // insertion-only wotkload five times median
            std::cout << "five times medians of insertion-only workload" << std::endl;
            //get_fivetime_median_insertion_only_workload();
        }
        else if (integerValue == 2)
        {
            // analytics only workload five times median
            std::cout << "five times medians of analytics-only workload" << std::endl;
            //get_fivetime_median_analytics_only_workload();
        }
        else if (integerValue == 3)
        {
            // updates workload five times median
            std::cout << "five times medians of updates workload" << std::endl;
            //get_fivetime_median_updates_only_workload();
        }
        else if (integerValue == 4)
        {
            std::cout << "five times medians of concurrent read_write workload" << std::endl;
            // concurrent read-write
            //get_fivetime_median_concurrent_read_write_workload();
        }
        else if (integerValue == 5)
        {
            std::cout << "five times medians of out-of-order insertion workload" << std::endl;
            // out-of-order updates workload
            //get_fivetime_median_out_of_order_workload_sensitivity_analysis();
        }
        else if (integerValue == 6)
        {
            std::cout << "five times medians of updates only workload space estimation" << std::endl;
            //get_fivetime_median_updates_only_workload_cal_space();
        }
        else if (integerValue == 7)
        {
            std::cout << "five times medians of insertion-only workload scalability analysis" << std::endl;
            // insertion only scalability five times median
           // get_fivetime_median_insertion_only_scalability_workload();
        }
        else
        {
            // five times  insertion and deletion workload
            std::cout << "five times medians of out-of-order updates workload" << std::endl;
           // get_fivetime_median_duplicate_insertion_deletion_workload_sensitivity_analysis();
        }
    }
    else
    {

        // Open RocksDB with Transactions
        rocksdb::TransactionDB* txn_db;
        rocksdb::TransactionDBOptions txn_db_options;
        rocksdb::Options options;
        options.create_if_missing = true;

        rocksdb::Status status = rocksdb::TransactionDB::Open(options, txn_db_options, "/tmp/hal_rocksdb", &txn_db);
        if (!status.ok()) {
            std::cerr << "Error opening RocksDB with transactions: " << status.ToString() << std::endl;
            return 1;
        }
        std::cout<<""
        // Create a Transaction
        rocksdb::WriteOptions write_options;
        rocksdb::TransactionOptions txn_options;
        rocksdb::Transaction* txn = txn_db->BeginTransaction(write_options, txn_options);

        // Perform Write Operations
        txn->Put("node1", "Updated Node 1 Data");
        txn->Put("node2", "New Node 2 Data");

        // Read Data within Transaction
        std::string value;
        rocksdb::Status s = txn->Get(rocksdb::ReadOptions(), "node1", &value);

        if (s.ok()) {
            std::cout << "Read inside transaction: " << value << std::endl;
        } else {
            std::cout << "Key not found in transaction!" << std::endl;
        }

        // Commit Transaction
        rocksdb::Status commit_status = txn->Commit();
        if (!commit_status.ok()) {
            std::cerr << "Transaction commit failed: " << commit_status.ToString() << std::endl;
        } else {
            std::cout << "Transaction committed successfully!" << std::endl;
        }

        // Cleanup
        delete txn;
        delete txn_db;


        int initial_space_of_memory = 100; // 50GB
        int number_of_thread = 1;
        uint8_t isOutOfOrderInsertionEnable = 0;
        bool isOutOfOrderUpdatesEnable = false;
        TransactionManager *tm = new TransactionManager(initial_space_of_memory, number_of_thread, isOutOfOrderInsertionEnable, isOutOfOrderUpdatesEnable);
        HBALStore *database = tm->getGraphDataStore(false, 0);
        // example input edge
        InputEdge edgeInsertion; //= new InputEdge();
        edgeInsertion.setUpdtSrcTime(1);
        edgeInsertion.setDestVertexId(2);
        edgeInsertion.setUpdtSrcTime(1);
        edgeInsertion.setUpdtWeight(0.4);
        //Insertion of an edge @executeEdgeInsertion(InputEdge, memory allocator, threadId)
       // database->executeEdgeInsertion(edgeInsertion, tm->getMemory(), 0);
        // deletion of an edge @executeEdgeDeletion(InputEdge, memory allocator, threadId)
      //  database->executeEdgeDeletion(edgeInsertion, tm->getMemory(), 0);
    }
    return 0;
}
