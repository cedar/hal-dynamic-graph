//
// Created by Ghufran on 27/07/2022.
//

#ifndef OUR_IDEA_INTEGERTOBYTE_H
#define OUR_IDEA_INTEGERTOBYTE_H

void print(unsigned char byte)
{
    for (int i = 7; i >= 0; i--)
    {
        int b = byte >> i;
        if (b & 1)
            std::cout << "1";
        else
            std::cout << "0";
    }
}

void integerToByte()
{
    unsigned char bytes[4];
    unsigned long n = 1659000572;

    bytes[0] = (n >> 24) & 0xFF;
    bytes[1] = (n >> 16) & 0xFF;
    bytes[2] = (n >> 8) & 0xFF;
    bytes[3] = (n >> 0) & 0xFF;

    std::cout << "Integer: " << n << std::endl;
    std::cout << "Byte Array: ";

    /*   for(int i = 0; i < 4; i++)
       {
           print(bytes[i]);
           std::cout << "\t";
       }
   */

    for (int i = 0; i < 4; i++)
    {
        //print(bytes[i]);
        std::cout << bytes[1] << "\t";
    }
}

#endif //OUR_IDEA_INTEGERTOBYTE_H
