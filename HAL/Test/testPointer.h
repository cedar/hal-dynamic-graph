//
// Created by Ghufran on 24/07/2022.
//

#ifndef OUR_IDEA_TESTPOINTER_H
#define OUR_IDEA_TESTPOINTER_H

int find_index(int *arr, int n, int K)
{
    // Lower and upper bounds
    int start = 0;
    int end = n - 1;
    // Traverse the search space
    while (start <= end)
    {
        int mid = (start + end) / 2;
        // If K is found
        if (arr[mid] == K)
            return mid;
        else if (arr[mid] > K)
            start = mid + 1;
        else
            end = mid - 1;
    }
    // Return insert position
    return end + 1;
}

void initAddressArray()
{
    //binary search code
    int arr[] = {6, 4, 3, 1, 0};
    int n = sizeof(arr) / sizeof(arr[0]);
    int K = 5;
    std::cout << find_index(arr, n, K) << std::endl;

}


#endif //OUR_IDEA_TESTPOINTER_H
