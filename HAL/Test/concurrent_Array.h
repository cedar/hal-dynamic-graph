/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This C++ header file is used to manipulate binary dataset files.
 ******************************************************************************/



#ifndef OUR_IDEA_CONCURRENT_ARRAY_H
#define OUR_IDEA_CONCURRENT_ARRAY_H

#include <iostream>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

struct bucket1
{
    int val;
    bucket1 *node;
};
int a = 10;
int b = 12;
bucket1 arr[1000] = {};
std::mutex m[5];
std::mutex m1;

void testConcurrentArray(int i)
{
    int x = i % 5;
    m[x].lock();
    arr[i].val = i;
    m[x].unlock();

}

void resizeRequest(int resize)
{
    std::cout << "Hello :" << resize << " value change now:" << a << std::endl;

    a = resize;
}

void test1(int par)
{
    if (a < b)
    {
        m1.lock();
        if (a < b)
        {
            resizeRequest(par);
        }
        std::cout << "Hello :" << par << std::endl;

        m1.unlock();
    }
}

void test_thread()
{
    std::thread t1(test1, 13);
    std::thread t2(test1, 14);

    t1.join();
    t2.join();
    std::cout << a;
}

void main_thread()
{
    std::thread *threadpointer = new std::thread[10];

    for (int i = 0; i < 1000; i++)
    {
        threadpointer[i] = std::thread(testConcurrentArray, i);
    }
    for (int i = 0; i < 10; i++)
    {
        threadpointer[i].join();
    }
    delete[] threadpointer;
    std::cout << "done";

}

#endif //OUR_IDEA_CONCURRENT_ARRAY_H
