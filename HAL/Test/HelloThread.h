/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This C++ header file is used to manipulate binary dataset files.
 ******************************************************************************/


#ifndef OUR_IDEA_HELLOTHREAD_H
#define OUR_IDEA_HELLOTHREAD_H

#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void sayHelloWorld(int tth)
{
    int myid = tth;
    for (int g = 0; g < 100000000; g++);
    std::cout << "hello" << myid << endl;
}

void printLabels(std::string str)
{
    std::cout << str << std::endl;
}

void helloWorldThread()
{
    std::vector<std::string> s = {"Educated blog", "Educative", "courses", "are great"};
    std::thread tId(sayHelloWorld, 10);
    std::vector<std::thread> threads;
    for (int i = 0; i < s.size(); i++)
    {
        threads.push_back(std::thread(printLabels, s[i]));
    }
    for (auto &th: threads)
    {
        th.join();
    }
    tId.join();

}

void helloghufran()
{
    for (int i = 0; i < 100000; i++)
    {
        cout << "hello" << " " << i << endl;
    }
}

void withoutJoin()
{
    std::thread t(helloghufran);
    t.detach();
}

#endif //OUR_IDEA_HELLOTHREAD_H
