/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This C++ header file is used to manipulate binary dataset files.
 ******************************************************************************/

#include "HBALStore.h"

/* HBALStore defination */
// ghufran


/* HBALStore defination */
// ghufran
PerSourceVertexIndr ***HBALStore::vertexIndrList;
vertex_t HBALStore::curIndVertexSize;
int64_t HBALStore::numEdges = 0;
std::atomic<vertex_t> HBALStore::traverse_count_t = 0;
std::atomic<vertex_t>  HBALStore::maxSourceVertex;
//spinlock HBALStore::querylock{0};

bool HBALStore::isReadActive = false;
bool HBALStore::isDegreeActive = false;
bool HBALStore::IsReadWrite = false;

std::mutex HBALStore::vertexTableResizeLock;
double HBALStore::loadingTime = 0.0;
vertex_t HBALStore::outOfOrderCout = 0;
bool HBALStore::NODELETEOOO = false;
//%%%%%%%%%%%%%%
int HBALStore::findPosInSTALBBToStarLinearSearchInOOO(stalb_type *arr, int start, int eb_block_size, timestamp_t K)
{
    // Lower and upper bounds
    int end = eb_block_size - 1;
    // Traverse the search space
    int pos = -1;
    while (start <= end)
    {

        int mid = start + (end - start) / 2;

        // If K is found
        LeafNode *eih_cur = *reinterpret_cast<LeafNode **>(arr + ((eb_block_size) * 8) + ((mid * 8) - 8)); // %%%% start here

        if (eih_cur->getSTime() == K)
        {
            return mid;
        }
        else if (eih_cur->getSTime() < K)
        {
            pos = mid;
            end = mid - 1;
        }
        else
        {
            start = mid + 1;
        }
    }
    return pos;
}

int HBALStore::findPosInSTALToStartLinearSearch(stalb_type **arr, int start, int n, timestamp_t K)
{
    // Lower and upper bounds
    int end = n - 1;
    // Traverse the search space
    timestamp_t eih_cur_time = 0;
    timestamp_t eih_end_time = 0;
    int pos = -1;
    while (start <= end)
    {
        int mid = start + (end - start) / 2;
        if(!check_version(reinterpret_cast<vertex_t>(arr[mid])))
        {
            u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(arr[mid]);
            EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(arr[mid] + ((eb_block_size / 2) * 8) +((*reinterpret_cast<uint16_ptr>(arr[mid] + 1) * 8) -8));
            EdgeInfoHal *eih_end = *reinterpret_cast<EdgeInfoHal **>(arr[mid] + ((eb_block_size / 2) * 8) +((((eb_block_size / 2) - 1) * 8) - 8));
            eih_cur_time = eih_cur->getSTime();

            // check if out-of-order updates exist
            if(eih_end->getOutOfOrderUpdt() != 0)
            {
                if(!check_version(eih_end->getOutOfOrderUpdt()))
                {
                    // stalb_exist only
                    stalb_type  *stalb_ooo = reinterpret_cast<stalb_type *>(eih_end->getOutOfOrderUpdt());
                    u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(stalb_ooo);
                    LeafNode *eih_end_ooo = *reinterpret_cast<LeafNode **>(stalb_ooo + ((eb_block_size_ooo / 2) * 8) +((((eb_block_size_ooo / 2) - 1) * 8) - 8));
                    eih_end_time =  eih_end_ooo->getSTime();
                }
            }
            else
            {
                eih_end_time = eih_end->getSTime();
            }
        }
        else
        {
            //
            eih_cur_time =  remove_version(reinterpret_cast<vertex_t>(arr[mid]));
            eih_end_time = eih_cur_time;
            //std::cout<<"version deleted"<<std::endl;
        }
        //
        if (eih_cur_time > K && eih_end_time < K)
        {
            return mid;
        }
        else if (eih_cur_time < K)
        {
            pos = mid;

            end = mid - 1;
        }
        else
        {
            start = mid + 1;
        }
    }
    // Return insert position
    return pos;
}

int HBALStore::findPosInSTALBToStartLinearSearch(stalb_type *arr, int start, int eb_block_size, timestamp_t K, vertex_t *ooo_pos_start , vertex_t *stalb_addr_ooo)
{
    // Lower and upper bounds
    int end = eb_block_size - 1;
    // Traverse the search space
    int pos = -1;
    timestamp_t eih_cur_time = 0;
    while (start <= end)
    {
        int mid = start + (end - start) / 2;
        // If K is found
        EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(arr + ((eb_block_size) * 8) + ((mid * 8) - 8)); // %%%% start here

        // check if out-of-order updates exist
        if(eih_cur->getOutOfOrderUpdt() != 0)
        {
            if(!check_version(eih_cur->getOutOfOrderUpdt()))
            {
                // stalb_exist only
                stalb_type  *stalb_ooo = reinterpret_cast<stalb_type *>(eih_cur->getOutOfOrderUpdt());
                u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(stalb_ooo);
                LeafNode *eih_end_ooo = *reinterpret_cast<LeafNode **>(stalb_ooo + ((eb_block_size_ooo / 2) * 8) +((((eb_block_size_ooo / 2) - 1) * 8) - 8));
                if(eih_end_ooo->getSTime() > K)
                {
                    eih_cur_time =  eih_end_ooo->getSTime();
                }
                else
                {
                    // OOO least source time < key
                    // do binary search on stalb_ooo to find location in stalb_ooo to start with
                    vertex_t ooo_field_index = findPosInSTALBBToStarLinearSearchInOOO(stalb_ooo,((u_int16_t)*reinterpret_cast<uint16_ptr>(stalb_ooo + 1)),(eb_block_size_ooo / 2), K);
                    LeafNode *eih_cur_ooo = *reinterpret_cast<LeafNode **>(stalb_ooo + ((eb_block_size_ooo / 2) * 8) + ((ooo_field_index * 8) - 8));
                    *stalb_addr_ooo = reinterpret_cast<vertex_t>(stalb_ooo); /// need to verify
                    *ooo_pos_start  = add_version(ooo_field_index);
                    eih_cur_time = eih_cur_ooo->getSTime();
                }
            }
        }
        else
        {
            eih_cur_time = eih_cur->getSTime();
        }

        ///

        if (eih_cur_time == K)
        {
            return mid;
        }
        else if (eih_cur_time < K)
        {
            pos = mid;
            end = mid - 1;
        }
        else
        {
            start = mid + 1;
        }
    }

    return pos;
}

void HBALStore::findNewInsertionPrevAndNextPos(int *dup_indr_pos, int *prev_indr_pos, bool isIndr,int indexIndrArray,int indexFixedBlock,vertex_t *ooo_pos_start,bool flag_ooo, vertex_t *stalb_dup, vertex_t *stalb_prev, vertex_t *pos_dup, vertex_t *pos_prev, vertex_t phy_src_id, timestamp_t latest_Deletion, vertex_t dst_id, InputEdge inputedge_info)
{
    timestamp_t source_time = getTimestampSrc(inputedge_info, IsOutOfOrder);

    if(isIndr)
    {
        PerSourceIndrPointer *ptr_per_src = static_cast<PerSourceIndrPointer *>(index[phy_src_id].getEdgePtr());

        int ooo_index = -1;
        stalb_type *art_val;
        int indexStalb = indexFixedBlock;
        bool flag_check = false;
        int l1_len = (u_int64_t) 1 << ptr_per_src->getBlockLen();
        int ind_l1 = indexIndrArray;
        int ind_l2 = -1;
        int ind_l2_ooo = -1;
        uint8_t flag_b = 0;
        // no out-of-order update to start with
        for (int ind_l1 = indexIndrArray; ind_l1 < l1_len; ind_l1++)
        {
            stalb_type *eb_k = ptr_per_src->getperVertexBlockArr()[ind_l1]; // stalb pointer

            int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

            if (!flag_check) {
                flag_check = true;
            } else {
                indexStalb = *reinterpret_cast<uint16_ptr>(eb_k + 1);
            }

            ///// %%%%%%%%
            for (ind_l2 = indexStalb; ind_l2 < l2_len; ind_l2++)
            {
                /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                // EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();

                if (remove_version_read(remove_version(remove_version_ooo(dst))) == dst_id)
                {
                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                    // dst-id == STALB dst
                    if (ei->getSTime() < source_time)
                    {
                        //
                        flag_b = 1;
                        break;
                    }
                    else
                    {
                        //
                        *pos_prev = ind_l2;
                        *stalb_prev = remove_version(reinterpret_cast<vertex_t>(eb_k));
                        *prev_indr_pos = ind_l1;
                    }
                }

                // check if dst contains ooo updates
                if (check_version_ooo(dst))
                {
                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1
                            << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                    // ooo updates exist
                    if (!check_version(ei->getOutOfOrderUpdt()))
                    {
                        // only stalb exist
                        art_val = reinterpret_cast<stalb_type *>(ei->getOutOfOrderUpdt());

                        int l2_len_ooo = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2; // get block size
                        if (!flag_ooo)
                        {
                            ooo_index = remove_version(*ooo_pos_start);
                            flag_ooo = true;
                        }
                        else
                        {
                            ooo_index = *reinterpret_cast<uint16_ptr>(art_val + 1);
                        }
                        for (ind_l2_ooo = ooo_index; ind_l2_ooo < l2_len_ooo; ind_l2_ooo++) {
                            uint64_t dst_ooo = *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8);
                            if (remove_version_read(remove_version(remove_version_ooo(dst))) == dst_id) {
                                LeafNode *ln = *reinterpret_cast<LeafNode **>(eb_k + (((((u_int64_t) 1
                                        << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2_ooo * 8) - 8));
                                if (ln->getSTime() < source_time) {
                                    //
                                    flag_b = 2;
                                    break;
                                } else {
                                    //
                                    *pos_prev = ind_l2_ooo;
                                    *stalb_prev = add_version(reinterpret_cast<vertex_t>(art_val));
                                }
                            }
                        }
                    }
                }
                // ooo updates part end
            }

            if (flag_b)
            {
                if (flag_b == 1)
                {
                    // dup is in-order
                    *pos_dup = ind_l2;
                    *stalb_dup = remove_version(reinterpret_cast<vertex_t>(ptr_per_src->getperVertexBlockArr()[ind_l1]));
                    *dup_indr_pos = ind_l1;
                }
                else
                {
                    // dup is out-of-order
                    *pos_dup = ind_l2_ooo;
                    *stalb_dup = add_version(reinterpret_cast<vertex_t>(art_val));
                    *dup_indr_pos = ind_l1;

                }
                break;
            }
            ///// %%%%%%%%
        }
    }
    else
    {
        int ooo_index = -1;
        stalb_type *art_val;
        int indexStalb = indexFixedBlock;
        bool flag_check = false;

        int ind_l2 = -1;
        int ind_l2_ooo = -1;
        uint8_t flag_b = 0;

        // only stalb exist
        stalb_type *eb_k = static_cast<stalb_type *>(index[phy_src_id].getEdgePtr());

        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

        if (!flag_check)
        {
            flag_check = true;
        }
        else
        {
            indexStalb = *reinterpret_cast<uint16_ptr>(eb_k + 1);
        }

        ///// %%%%%%%%
        for (ind_l2 = indexStalb; ind_l2 < l2_len; ind_l2++)
        {
            /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
            // EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();

            if (remove_version_read(remove_version(remove_version_ooo(dst))) == dst_id)
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                // dst-id == STALB dst
                if (ei->getSTime() < source_time)
                {
                    //
                    flag_b = 1;
                    break;
                }
                else
                {
                    //
                    *pos_prev = ind_l2;
                    *stalb_prev = remove_version(reinterpret_cast<vertex_t>(eb_k));
                }
            }

            // check if dst contains ooo updates
            if (check_version_ooo(dst))
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1
                        << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                // ooo updates exist
                if (!check_version(ei->getOutOfOrderUpdt()))
                {
                    // only stalb exist
                    art_val = reinterpret_cast<stalb_type *>(ei->getOutOfOrderUpdt());

                    int l2_len_ooo = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2; // get block size
                    if (!flag_ooo)
                    {
                        ooo_index = remove_version(*ooo_pos_start);
                        flag_ooo = true;
                    }
                    else
                    {
                        ooo_index = *reinterpret_cast<uint16_ptr>(art_val + 1);
                    }
                    for (ind_l2_ooo = ooo_index; ind_l2_ooo < l2_len_ooo; ind_l2_ooo++) {
                        uint64_t dst_ooo = *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8);
                        if (remove_version_read(remove_version(remove_version_ooo(dst))) == dst_id) {
                            LeafNode *ln = *reinterpret_cast<LeafNode **>(eb_k + (((((u_int64_t) 1
                                    << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2_ooo * 8) - 8));
                            if (ln->getSTime() < source_time) {
                                //
                                flag_b = 2;
                                break;
                            } else {
                                //
                                *pos_prev = ind_l2_ooo;
                                *stalb_prev = add_version(reinterpret_cast<vertex_t>(art_val));
                            }
                        }
                    }
                }
            }
            // ooo updates part end
        }

        if (flag_b)
        {
            if (flag_b == 1) {
                // dup is in-order
                *pos_dup = ind_l2;
                *stalb_dup = remove_version(reinterpret_cast<vertex_t>(eb_k));
            }
            else
            {
                // dup is out-of-order
                *pos_dup = ind_l2_ooo;
                *stalb_dup = add_version(reinterpret_cast<vertex_t>(art_val));
            }
        }

        ///// %%%%%%%%
    }
}
void HBALStore::findNewInsertionNextPos(int *dup_indr_pos, bool isIndr,int indexIndrArray,int indexFixedBlock,vertex_t *ooo_pos_start,bool flag_ooo, vertex_t *stalb_dup, vertex_t *pos_dup, vertex_t phy_src_id, timestamp_t latest_Deletion, vertex_t dst_id, InputEdge inputedge_info)
{
    timestamp_t source_time = getTimestampSrc(inputedge_info, IsOutOfOrder);
    if(isIndr)
    {
        PerSourceIndrPointer *ptr_per_src = static_cast<PerSourceIndrPointer *>(index[phy_src_id].getEdgePtr());

        int ooo_index = -1;
        stalb_type *art_val;
        int indexStalb = indexFixedBlock;
        bool flag_check = false;
        int l1_len = (u_int64_t) 1 << ptr_per_src->getBlockLen();
        int ind_l1 = indexIndrArray;
        int ind_l2 = -1;
        int ind_l2_ooo = -1;
        uint8_t flag_b = 0;
        // no out-of-order update to start with
        for (int ind_l1 = indexIndrArray; ind_l1 < l1_len; ind_l1++)
        {
            stalb_type *eb_k = ptr_per_src->getperVertexBlockArr()[ind_l1]; // stalb pointer

            int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

            if (!flag_check) {
                flag_check = true;
            } else {
                indexStalb = *reinterpret_cast<uint16_ptr>(eb_k + 1);
            }

            ///// %%%%%%%%
            for (ind_l2 = indexStalb; ind_l2 < l2_len; ind_l2++)
            {
                /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                // EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();

                if (remove_version_read(remove_version(remove_version_ooo(dst))) == dst_id)
                {
                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                    // dst-id == STALB dst
                    if (ei->getSTime() < source_time)
                    {
                        //
                        flag_b = 1;
                        break;
                    }
                }

                // check if dst contains ooo updates
                if (check_version_ooo(dst))
                {
                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1
                            << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                    // ooo updates exist
                    if (!check_version(ei->getOutOfOrderUpdt()))
                    {
                        // only stalb exist
                        art_val = reinterpret_cast<stalb_type *>(ei->getOutOfOrderUpdt());

                        int l2_len_ooo = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2; // get block size
                        if (!flag_ooo)
                        {
                            ooo_index = remove_version(*ooo_pos_start);
                            flag_ooo = true;
                        }
                        else
                        {
                            ooo_index = *reinterpret_cast<uint16_ptr>(art_val + 1);
                        }
                        for (ind_l2_ooo = ooo_index; ind_l2_ooo < l2_len_ooo; ind_l2_ooo++) {
                            uint64_t dst_ooo = *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8);
                            if (remove_version_read(remove_version(remove_version_ooo(dst))) == dst_id) {
                                LeafNode *ln = *reinterpret_cast<LeafNode **>(eb_k + (((((u_int64_t) 1
                                        << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2_ooo * 8) - 8));
                                if (ln->getSTime() < source_time) {
                                    //
                                    flag_b = 2;
                                    break;
                                }
                            }
                        }
                    }
                }
                // ooo updates part end
            }

            if (flag_b)
            {
                if (flag_b == 1)
                {
                    // dup is in-order
                    *pos_dup = ind_l2;
                    *stalb_dup = remove_version(reinterpret_cast<vertex_t>(ptr_per_src->getperVertexBlockArr()[ind_l1]));
                    *dup_indr_pos = ind_l1;
                }
                else
                {
                    // dup is out-of-order
                    *pos_dup = ind_l2_ooo;
                    *stalb_dup = add_version(reinterpret_cast<vertex_t>(art_val));
                    *dup_indr_pos = ind_l1;

                }
                break;
            }
            ///// %%%%%%%%
        }
    }
    else
    {
        int ooo_index = -1;
        stalb_type *art_val;
        int indexStalb = indexFixedBlock;
        bool flag_check = false;

        int ind_l2 = -1;
        int ind_l2_ooo = -1;
        uint8_t flag_b = 0;

        // only stalb exist
        stalb_type *eb_k = static_cast<stalb_type *>(index[phy_src_id].getEdgePtr());

        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

        if (!flag_check)
        {
            flag_check = true;
        }
        else
        {
            indexStalb = *reinterpret_cast<uint16_ptr>(eb_k + 1);
        }

        ///// %%%%%%%%
        for (ind_l2 = indexStalb; ind_l2 < l2_len; ind_l2++)
        {
            /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
            // EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();

            if (remove_version_read(remove_version(remove_version_ooo(dst))) == dst_id)
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                // dst-id == STALB dst
                if (ei->getSTime() < source_time)
                {
                    //
                    flag_b = 1;
                    break;
                }

            }

            // check if dst contains ooo updates
            if (check_version_ooo(dst))
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1
                        << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                // ooo updates exist
                if (!check_version(ei->getOutOfOrderUpdt()))
                {
                    // only stalb exist
                    art_val = reinterpret_cast<stalb_type *>(ei->getOutOfOrderUpdt());

                    int l2_len_ooo = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2; // get block size
                    if (!flag_ooo)
                    {
                        ooo_index = remove_version(*ooo_pos_start);
                        flag_ooo = true;
                    }
                    else
                    {
                        ooo_index = *reinterpret_cast<uint16_ptr>(art_val + 1);
                    }
                    for (ind_l2_ooo = ooo_index; ind_l2_ooo < l2_len_ooo; ind_l2_ooo++) {
                        uint64_t dst_ooo = *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8);
                        if (remove_version_read(remove_version(remove_version_ooo(dst))) == dst_id) {
                            LeafNode *ln = *reinterpret_cast<LeafNode **>(eb_k + (((((u_int64_t) 1
                                    << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2_ooo * 8) - 8));
                            if (ln->getSTime() < source_time) {
                                //
                                flag_b = 2;
                                break;
                            }
                        }
                    }
                }
            }
            // ooo updates part end
        }

        if (flag_b)
        {
            if (flag_b == 1) {
                // dup is in-order
                *pos_dup = ind_l2;
                *stalb_dup = remove_version(reinterpret_cast<vertex_t>(eb_k));
            }
            else
            {
                // dup is out-of-order
                *pos_dup = ind_l2_ooo;
                *stalb_dup = add_version(reinterpret_cast<vertex_t>(art_val));
            }
        }

        ///// %%%%%%%%
    }
}
void HBALStore::findDuplicateInsertionPosInSTALBBinarySearchByLatestDeletion(int *dup_indr_pos, int *prev_indr_pos,vertex_t *stalb_dup, vertex_t *stalb_prev, vertex_t *pos_dup, vertex_t *pos_prev, vertex_t phy_src_id, timestamp_t latest_Deletion, vertex_t dst_id, InputEdge inputedge_info)
{
    vertex_t *ooo_pos_start = 0;
    vertex_t *stalb_ooo_start  = 0; /// vertex_t *ooo_pos_start , stalb_type *stalb_ooo;

    if (!(index[phy_src_id].edgePtr & 0x1))
    {
        // contains STAL
        PerSourceIndrPointer *ptr_per_src = static_cast<PerSourceIndrPointer *>(index[phy_src_id].getEdgePtr());

        int startPos = ptr_per_src->getCurPos();
        int endPos = ((u_int64_t) 1 <<ptr_per_src->getBlockLen());

        int indexIndrArray = findPosInSTALToStartLinearSearch((ptr_per_src->getperVertexBlockArr()), startPos, endPos,latest_Deletion);

        u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray]);

        //  EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + ((eb_block_size / 2) * 8) + ((((u_int16_t)*reinterpret_cast<uint16_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + 1)) * 8) - 8));

        //fixed size block binary search
        int indexFixedBlock = findPosInSTALBToStartLinearSearch(ptr_per_src->getperVertexBlockArr()[indexIndrArray],((u_int16_t)*reinterpret_cast<uint16_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + 1)),(eb_block_size / 2), latest_Deletion, ooo_pos_start, stalb_ooo_start);


        if(check_version(*ooo_pos_start))
        {
            findNewInsertionPrevAndNextPos(dup_indr_pos, prev_indr_pos,true, indexIndrArray, indexFixedBlock, ooo_pos_start, false, stalb_dup, stalb_prev, pos_dup, pos_prev, phy_src_id, latest_Deletion, dst_id, inputedge_info);
            /// %%%%%%
        }
        else
        {
            // out-of-order update to start with
            findNewInsertionPrevAndNextPos(dup_indr_pos, prev_indr_pos,true, indexIndrArray, indexFixedBlock, 0, true, stalb_dup, stalb_prev, pos_dup, pos_prev, phy_src_id, latest_Deletion, dst_id, inputedge_info);
        }
    }
    else
    {
        // only stalb
        stalb_type *ptr_per_src = static_cast<stalb_type *>(index[phy_src_id].getEdgePtr());

        u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_per_src);

        //fixed size block binary search
        int indexFixedBlock = findPosInSTALBToStartLinearSearch(ptr_per_src,((u_int16_t)*reinterpret_cast<uint16_ptr>(ptr_per_src + 1)),(eb_block_size / 2), latest_Deletion, ooo_pos_start, stalb_ooo_start);

        if(check_version(*ooo_pos_start))
        {
            findNewInsertionPrevAndNextPos(dup_indr_pos, prev_indr_pos,false, -1, indexFixedBlock, ooo_pos_start, false, stalb_dup, stalb_prev,pos_dup, pos_prev, phy_src_id, latest_Deletion, dst_id, inputedge_info);
        }
        else
        {
            findNewInsertionPrevAndNextPos(dup_indr_pos, prev_indr_pos,false, -1, indexFixedBlock, ooo_pos_start, true, stalb_dup, stalb_prev,pos_dup, pos_prev, phy_src_id, latest_Deletion, dst_id, inputedge_info);
        }

    }
}
// %%%%%%%%%%%

void HBALStore::findDeletionInsertionPosInSTALBBinarySearchByNewDeletion(int *dup_indr_pos,vertex_t *stalb_dup, vertex_t *pos_dup, vertex_t phy_src_id, timestamp_t latest_Deletion, vertex_t dst_id, InputEdge inputedge_info)
{
    vertex_t *ooo_pos_start = 0;
    vertex_t *stalb_ooo_start  = 0; /// vertex_t *ooo_pos_start , stalb_type *stalb_ooo;

    if (!(index[phy_src_id].edgePtr & 0x1))
    {
        // contains STAL
        PerSourceIndrPointer *ptr_per_src = static_cast<PerSourceIndrPointer *>(index[phy_src_id].getEdgePtr());

        int startPos = ptr_per_src->getCurPos();
        int endPos = ((u_int64_t) 1 <<ptr_per_src->getBlockLen());

        int indexIndrArray = findPosInSTALToStartLinearSearch((ptr_per_src->getperVertexBlockArr()), startPos, endPos,latest_Deletion);

        u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray]);

        //  EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + ((eb_block_size / 2) * 8) + ((((u_int16_t)*reinterpret_cast<uint16_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + 1)) * 8) - 8));

        //fixed size block binary search
        int indexFixedBlock = findPosInSTALBToStartLinearSearch(ptr_per_src->getperVertexBlockArr()[indexIndrArray],((u_int16_t)*reinterpret_cast<uint16_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + 1)),(eb_block_size / 2), latest_Deletion, ooo_pos_start, stalb_ooo_start);


        if(check_version(*ooo_pos_start))
        {
            findNewInsertionNextPos(dup_indr_pos, true,indexIndrArray,indexFixedBlock,ooo_pos_start,false, stalb_dup, pos_dup, phy_src_id, latest_Deletion, dst_id, inputedge_info);
            /// %%%%%%
        }
        else
        {
            // out-of-order update to start with
            findNewInsertionNextPos(dup_indr_pos, true,indexIndrArray,indexFixedBlock,ooo_pos_start,true, stalb_dup, pos_dup, phy_src_id, latest_Deletion, dst_id, inputedge_info);
        }
    }
    else
    {
        // only stalb
        stalb_type *ptr_per_src = static_cast<stalb_type *>(index[phy_src_id].getEdgePtr());

        u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_per_src);

        //fixed size block binary search
        int indexFixedBlock = findPosInSTALBToStartLinearSearch(ptr_per_src,((u_int16_t)*reinterpret_cast<uint16_ptr>(ptr_per_src + 1)),(eb_block_size / 2), latest_Deletion, ooo_pos_start, stalb_ooo_start);

        if(check_version(*ooo_pos_start))
        {
            findNewInsertionNextPos(dup_indr_pos, false,-1,indexFixedBlock,ooo_pos_start,false, stalb_dup, pos_dup, phy_src_id, latest_Deletion, dst_id, inputedge_info);
        }
        else
        {
            findNewInsertionNextPos(dup_indr_pos, false,-1,indexFixedBlock,ooo_pos_start,true, stalb_dup, pos_dup, phy_src_id, latest_Deletion, dst_id, inputedge_info);
        }

    }
}
// %%%%%%%%%%
// ReadTable classes
inline int HBALStore::findIndexForIndr(stalb_type **arr, int start, int n, timestamp_t K)
{
    // Lower and upper bounds
    int end = n - 1;
    // Traverse the search space
    timestamp_t eih_cur_time = 0;
    timestamp_t eih_end_time = 0;
    while (start <= end)
    {
        int mid = (start + end) / 2;
        if(!check_version(reinterpret_cast<vertex_t>(arr[mid])))
        {
            u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(arr[mid]);
            EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(arr[mid] + ((eb_block_size / 2) * 8) +((*reinterpret_cast<uint16_ptr>(arr[mid] + 1) * 8) -8));
            EdgeInfoHal *eih_end = *reinterpret_cast<EdgeInfoHal **>(arr[mid] + ((eb_block_size / 2) * 8) +((((eb_block_size / 2) - 1) * 8) - 8));
            eih_cur_time = eih_cur->getSTime();
            eih_end_time = eih_end->getSTime();
        }
        else
        {
            //
            eih_cur_time =  remove_version(reinterpret_cast<vertex_t>(arr[mid]));
            eih_end_time = eih_cur_time;
            //s/td::cout<<"verion deleted"<<std::endl;
        }
        //
        if (eih_cur_time > K && eih_end_time < K)
            return mid;
        else if (eih_cur_time > K)
            start = mid + 1;
        else
            end = mid - 1;
    }
    // Return insert position
    return end;
}

inline int HBALStore::findIndexForBlockArr(stalb_type *arr, int start, int eb_block_size, timestamp_t K) {
    // Lower and upper bounds
    int end = eb_block_size - 1;
    // Traverse the search space
    while (start <= end) {
        int mid = (start + end) / 2;
        // If K is found
        EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(arr + ((eb_block_size) * 8) + ((mid * 8) - 8)); // %%%% start here

        if (eih_cur->getSTime() == K)
            return mid;
        else if (eih_cur->getSTime() > K)
            start = mid + 1;
        else
            end = mid - 1;
    }
    // Return insert position
    //std::cout<<"Hello ghufran 1"<<std::endl;

    return end;
}

inline int HBALStore::findIndexForBlockArrOOO(stalb_type *arr, int start, int eb_block_size, timestamp_t K) {
    // Lower and upper bounds
    int end = eb_block_size - 1;
    // Traverse the search space
    while (start <= end) {
        int mid = (start + end) / 2;
        // If K is found
        LeafNode *eih_cur = *reinterpret_cast<LeafNode **>(arr + ((eb_block_size) * 8) + ((mid * 8) - 8)); // %%%% start here

        if (eih_cur->getSTime() == K)
            return mid;
        else if (eih_cur->getSTime() > K)
            start = mid + 1;
        else
            end = mid - 1;
    }
    // Return insert position
    //std::cout<<"Hello ghufran 1"<<std::endl;
    return end;
}

bool HBALStore::InsertSrcVertex(vertex_t vertex_id, MemoryAllocator *me, int thread_id) {

    vertex_t p_id;
    l_t_p_table::accessor w;
    if (logical_to_physical.insert(w, vertex_id)) {
        p_id = high_water_mark.fetch_add(1);

        grow_vector_if_smaller(index, p_id, PerSourceVertexIndr());  // TODO Should I do this earlier and assynchronous
        grow_vector_if_smaller(physical_to_logical, p_id, FLAG_EMPTY_SLOT);


        w->second = p_id;
        // aquire_vertex_lock_p(p_id);


        // Update index
        //  assert(index[p_id].adjacency_set & VERTEX_NOT_USED_MASK);
        index[p_id].edgePtr = NULL;
        index[p_id].degree = 0;
        index[p_id].latestSrcTime = 0;
        index[p_id].hashPtr = NULL;
        // index[p_id].lock_ = 0;
        // index[p_id].perVertexLock.initLock();

        //  index[p_id].lock.init(); // = perVertexLock.initLock();
        // Update logical mapping
        // assert(physical_to_logical[p_id] & VERTEX_NOT_USED_MASK);

        physical_to_logical[p_id] = vertex_id;

        // Update vertex count
        vertex_count.fetch_add(1);
        w.release();
        return true;

    }
    else
    {
        p_id = w->second;
        w.release();
        // aquire_vertex_lock_p(p_id);
        return false;
    }

}
// took from sortledton
vertex_t HBALStore::logical_id(vertex_t v) {
    assert(v < high_water_mark);
    return physical_to_logical[v];
}

vertex_t HBALStore::physical_id(vertex_t v) {
    l_t_p_table::const_accessor a;
    logical_to_physical.find(a, v);
    return a->second;
}

PerSourceVertexIndr const &HBALStore::operator[](size_t v) const{
    return index[v];
}
PerSourceVertexIndr &HBALStore::operator[](size_t v) {
    return index[v];
}

size_t HBALStore::get_high_water_mark() {
    return high_water_mark.load();
}

size_t HBALStore::get_vertex_count(vertex_t version) {
    return vertex_count.load();
}

// split block of STAL
void HBALStore::SplitBlock(stalb_type* per_stalb, stalb_type* new_stalb_block, uint64_t new_block_size, uint64_t split_main_block_index, uint64_t per_stalb_cur_index, bool flag)
{
    property_type* new_property = *reinterpret_cast<property_type**>( new_stalb_block + ((((uint64_t)  1 << *reinterpret_cast<uint8_ptr>(new_stalb_block))  * 8) - 8)); // property
    property_type* old_property = *reinterpret_cast<property_type**>( per_stalb + ((((uint64_t)  1 << *reinterpret_cast<uint8_ptr>(per_stalb))  * 8) - 8)); // property

    u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(per_stalb);
    uint64_t newEb_index = 0;
    if(flag)
    {
        newEb_index = 1;
    }
    else
    {
        newEb_index = 2;
    }
    for(uint64_t index_split_start = per_stalb_cur_index; index_split_start < split_main_block_index;index_split_start++)
    {
        *reinterpret_cast<uint64_ptr>(new_stalb_block + ((newEb_index) * 8)) = *reinterpret_cast<uint64_ptr>(per_stalb + (index_split_start * 8)); // dest id

        *reinterpret_cast<LeafNode **>(new_stalb_block + ((new_block_size/2) * 8) + (((newEb_index) * 8) - 8)) = *reinterpret_cast<LeafNode **>(per_stalb + ((eb_block_size_ooo / 2) * 8) + ((index_split_start * 8) - 8)); //  IEM block

        new_property[newEb_index] = old_property[index_split_start];

        if(check_version(*reinterpret_cast<uint64_ptr>(new_stalb_block + ((newEb_index) * 8))))
        {
            // decrement deletion counter from main stalb
            *reinterpret_cast<uint16_ptr>(per_stalb + 5) = *reinterpret_cast<uint16_ptr>(per_stalb + 5) - 1;

            // increment deletion counter in new stalb block
            *reinterpret_cast<uint16_ptr>(new_stalb_block + 5) = *reinterpret_cast<uint16_ptr>(new_stalb_block + 5) + 1;
        }
        newEb_index++;
        // init 0 with main_block indexes that < pos
        *reinterpret_cast<uint64_ptr>(per_stalb + (index_split_start * 8)) = 0;
        *reinterpret_cast<EdgeInfoHal **>(per_stalb + ((eb_block_size_ooo / 2) * 8) + ((index_split_start * 8) - 8)) = 0;
        old_property[index_split_start] = 0;
    }


    *reinterpret_cast<uint16_ptr>(per_stalb + 1) = split_main_block_index;

    if(flag)
    {
        *reinterpret_cast<uint16_ptr>(new_stalb_block + 1) = 1;
    }
    else
    {
        *reinterpret_cast<uint16_ptr>(new_stalb_block + 1) = 2;
    }

    *reinterpret_cast<uint8_ptr>(new_stalb_block + 3) = 1;

    if(*reinterpret_cast<uint16_ptr>(per_stalb + 5) == 0)
    {
        *reinterpret_cast<uint8_ptr>(per_stalb + 3) = 0;
    }
}

// sortedblock memmove inserting value on their corresponding order pos
void HBALStore::StoreKeyInBlock(stalb_type* stalb_block,uint64_t pos, vertex_t phy_dest_id, LeafNode* ln, double weight_w, bool IsDelete)
{

    int src_index = *reinterpret_cast<uint16_ptr>(stalb_block + 1);
    int dest_index = *reinterpret_cast<uint16_ptr>(stalb_block + 1) - 1;
    // number of element to move
    int move_element = (pos - *reinterpret_cast<uint16_ptr>(stalb_block + 1)) + 1;

    double *property = *reinterpret_cast<double **>( stalb_block + ((((uint64_t) 1<< *reinterpret_cast<uint8_ptr>(stalb_block)) * 8) - 8)); // blocksize

    // insert edge block update
    if (move_element > 0)
    {
        memmove((stalb_block + (dest_index * 8)), (stalb_block + (src_index * 8)),move_element * 8);

        memmove(property + dest_index, property + src_index, move_element * 8);

        memmove((stalb_block) +((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(stalb_block)) / 2) * 8) +((dest_index * 8) - 8), (stalb_block) + ((((uint64_t) 1<< *reinterpret_cast<uint8_ptr>(stalb_block)) / 2) * 8) + ((src_index * 8) - 8),
                move_element * 8);
    }

    if(!HBALStore::isReadActive)
    {
        *reinterpret_cast<uint64_ptr>(stalb_block + (pos * 8)) = add_version_ooo(phy_dest_id); // No read-query running hence no wts check
    }

/// %%%%%%%% update if consider new ln as duplicate
    if(IsDelete)
    {
        *reinterpret_cast<uint64_ptr>(stalb_block + (pos * 8)) = add_version(phy_dest_id);
        if (!HBALStore::isReadActive)
        {
            *reinterpret_cast<uint64_ptr>(stalb_block + (pos * 8)) = add_version_read(phy_dest_id); /// no read-only queries are active
        }
    }
    //// %%%%%%%
    *reinterpret_cast<uint64_ptr>((stalb_block) +((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(stalb_block)) /2) * 8) + ((pos * 8) - 8)) = reinterpret_cast<vertex_t>(ln); // dest node

    property[pos] = weight_w; // weight

    *reinterpret_cast<uint16_ptr>(stalb_block + 1) = *reinterpret_cast<uint16_ptr>(stalb_block + 1) - 1;
}

// Store block in ART
void HBALStore::StoreBlockInArt(stalb_type* current_block, stalb_type* new_block, uint64_t current_block_size, uint64_t new_block_size, EdgeInfoHal *setOutOfOrder, ArtPerIndex *perArt)
{
    LeafNode *eih_per_stalb = *reinterpret_cast<LeafNode **>(current_block + ((current_block_size / 2) * 8) + (( *reinterpret_cast<uint16_ptr>(current_block + 1) * 8) - 8));
    LeafNode *eih_new_stalb = *reinterpret_cast<LeafNode **>(new_block + ((new_block_size / 2) * 8) + ((1 * 8) - 8));
    perArt->insertArtLeaf(eih_per_stalb->getSTime(), current_block);
    perArt->insertArtLeaf(eih_new_stalb->getSTime(), new_block);
}
// resize block upward double STAL
UPBlockSizePointer* HBALStore::UpEdgeBlock(stalb_type* stalb, uint8_t cur_block_size, MemoryAllocator *me, int thread_id)
{
    uint8_t prev_size = cur_block_size;
    uint8_t new_size  = ++cur_block_size;
    uint32_t new_size_power = ((uint32_t) 1 << new_size);

    stalb_type* temp        =  static_cast<stalb_type*>(me->Allocate((8 * new_size_power), thread_id));

    double *property_temp   =  static_cast<double *>(me->Allocate((8 * (new_size_power/2)) ,thread_id));

    memset(temp, 0, (8 * new_size_power));
    memset(property_temp, 0, (8 * (new_size_power/2)));

    ////////////////////////////

    if(*reinterpret_cast<uint8_ptr>(stalb + 3))
    {
        *reinterpret_cast<uint8_ptr>(temp + 3) = 1;
        *reinterpret_cast<uint16_ptr>(temp + 5) = *reinterpret_cast<uint16_ptr>(stalb + 5);
    }

    uint32_t prev_block_element_size = ((uint32_t) 1 << prev_size)/2;

    memcpy(temp + ((prev_block_element_size * 8) + 8) ,stalb + 8, (prev_block_element_size - 1) * 8); // dest id

    memcpy(temp + ((prev_block_element_size * 8) + ((new_size_power / 2) * 8)), (stalb + (prev_block_element_size * 8)), ((prev_block_element_size - 1) * 8)); // edgeinfo

    double* property = *reinterpret_cast<double **>(stalb + ((((uint32_t) 1 << prev_size) * 8) - 8));

    memcpy(property_temp + prev_block_element_size + 1, property + 1, (prev_block_element_size - 1) * 8); // property block /// might have mistake%%%%%%%%%

    property_temp[0] = 0;

    *reinterpret_cast<double **>(temp + ((new_size_power * 8) - 8)) = property_temp; // update the address of property update

    *reinterpret_cast<uint8_ptr>(temp)   = log2(new_size_power); // block size

    // double* property_t = *reinterpret_cast<double **>(temp + ((new_size_power * 8) - 8));


    *reinterpret_cast<uint16_ptr>(temp + 1) = prev_block_element_size + 1; // cur_index


    UPBlockSizePointer *up_block = static_cast<UPBlockSizePointer*>(me->Allocate(sizeof(UPBlockSizePointer), thread_id));
    up_block->new_stalb         = temp;
    up_block->prev_stalb        = stalb;
    up_block->prev_property_arr = property;
    up_block->prev_block_size   = (uint64_t) 1 << prev_size;

    return up_block;
}
// downsize the block

// resize block upward double STAL
stalb_type* HBALStore::DownEdgeBlock(uint8_t cur_block_size, MemoryAllocator *me, int thread_id)
{
    uint8_t new_size  = --cur_block_size;
    uint32_t new_size_power = ((int32_t) 1 << new_size);

    stalb_type* temp = static_cast<stalb_type*>(me->Allocate((8 * new_size_power), thread_id));

    double *property_temp   =  static_cast<double *>(me->Allocate((8 * (new_size_power/2)) ,thread_id));

    memset(temp, 0, (8 * new_size_power));
    memset(property_temp, 0, (8 * (new_size_power/2)));

    *reinterpret_cast<double **>(temp + ((new_size_power * 8) - 8)) = property_temp; // update the address of property update

    *reinterpret_cast<uint8_ptr>(temp)   = log2(new_size_power); // block size

    *reinterpret_cast<uint16_ptr>(temp + 1) = (new_size_power / 2); // cur_index

    // property pointer to free
    // stalb prev pointer to delete
    //  prev_block_size
    //  new stalb pointer


    return temp;
}
/// end

bool HBALStore::InsertDestVertex(vertex_t vertex_id, MemoryAllocator *me, int thread_id)
{
    vertex_t p_id;
    l_t_p_table::accessor w;
    if (logical_to_physical.insert(w, vertex_id))
    {
        p_id = high_water_mark.fetch_add(1);

        grow_vector_if_smaller(index, p_id, PerSourceVertexIndr());  // TODO Should I do this earlier and assynchronous
        grow_vector_if_smaller(physical_to_logical, p_id,FLAG_EMPTY_SLOT);
        w->second = p_id;
        // aquire_vertex_lock_p(p_id);

        // Update index
        index[p_id].edgePtr = NULL;
        index[p_id].degree = 0;
        index[p_id].latestSrcTime = 0;
        index[p_id].hashPtr = NULL;
        // index[p_id].lock_ = 0;
        //  index[p_id].perVertexLock.initLock();

        // Update logical mapping
        physical_to_logical[p_id] = vertex_id;

        // Update vertex count
        vertex_count.fetch_add(1);
        w.release();

        return true;
    }
    else
    {
        p_id = w->second;
        w.release();
        return false;
    }
}

bool HBALStore::CheckOutOfOrder(timestamp_t src_timestamp, vertex_t phy_src) {

    if (src_timestamp >= index[phy_src].getLatestSrcTime())
    {
        index[phy_src].setLatestSrcTime(src_timestamp);
    }
    else
    {
        return true;
    }

    return false;
}

void HBALStore::updateNewSTALB(vertex_d cur_edge_b, vertex_t new_edge_b, vertex_t phy_src_id, stalb_type* old_eb, stalb_type* new_eb, property_type* old_property, MemoryAllocator *me, int thread_id, bool ooo_flag)
{

    if(!ooo_flag)
    {
        property_type* new_property = *reinterpret_cast<property_type**>( new_eb + ((((uint64_t)  1 << *reinterpret_cast<uint8_ptr>(new_eb))  * 8) - 8)); // property
        int counter_block =0;
        for (int l = ((cur_edge_b/2) - 1); l >= *reinterpret_cast<uint16_ptr>(old_eb + 1); l--)
        {
            if (!check_version(*reinterpret_cast<uint64_ptr>(old_eb + (l * 8))))
            {
                uint16_t newEb_index = *reinterpret_cast<uint16_ptr>(new_eb + 1);

                *reinterpret_cast<uint64_ptr>(new_eb + ((newEb_index - 1) * 8)) = *reinterpret_cast<uint64_ptr>(old_eb + (l * 8)); // dest id

                *reinterpret_cast<EdgeInfoHal **>(new_eb + ((new_edge_b/2) * 8) + (((newEb_index-1) * 8) - 8)) = *reinterpret_cast<EdgeInfoHal **>(old_eb + ((cur_edge_b / 2) * 8) + ((l * 8) - 8)); //  IEM block

                new_property[newEb_index-1] = old_property[l]; // property array

                *reinterpret_cast<uint16_ptr>(new_eb + 1) = *reinterpret_cast<uint16_ptr>(new_eb + 1) - 1; // update the current index in new stalb

                AdjacentPos apn; // = static_cast<AdjacentPos *>(me->Allocate(sizeof(AdjacentPos), thread_id));
                apn.indrIndex = -1;

                AdjacentPos *ap_pos = index[phy_src_id].getDestNodeHashTableVal(*reinterpret_cast<uint64_ptr>(old_eb + (l * 8)), &apn);

                if(check_version(ap_pos->edgeBlockIndex))
                {
                    DVE *dve = reinterpret_cast<DVE *>(remove_version(ap_pos->edgeBlockIndex));
                    // todo find greatest insertion in hash table and update ap_pos
                    // think about it
                    if(dve->next != NULL)
                    {
                        //
                        DuplicateUpdate *du = dve->next;
                        DuplicateUpdate *prev = NULL;

                        while(du != NULL)
                        {
                            if(!check_version(du->SrcTime))
                            {
                                prev = du;
                            }
                            du = du->next;
                        }
                        if(prev != NULL)
                        {
                            u_int64_t adjac_posi = prev->UpdatePos;
                            adjac_posi = setBitMask(adjac_posi, *reinterpret_cast<uint16_ptr>(new_eb + 1), L2IndexStart,
                                                    L2IndexBits);
                            adjac_posi = setBitMask(adjac_posi, *reinterpret_cast<uint8_ptr>(new_eb), L2SizeStart,
                                                    L2SizeBits);
                            prev->UpdatePos = adjac_posi;
                        }
                    }
                }
                else
                {

                    u_int64_t adjac_posi = index[phy_src_id].getHashTable()->hashtable[ap_pos->hashTableIndex];
                    adjac_posi = setBitMask(adjac_posi, *reinterpret_cast<uint16_ptr>(new_eb + 1), L2IndexStart,L2IndexBits);
                    adjac_posi = setBitMask(adjac_posi, *reinterpret_cast<uint8_ptr>(new_eb), L2SizeStart, L2SizeBits);

                    index[phy_src_id].getHashTable()->hashtable[ap_pos->hashTableIndex] = adjac_posi;
                }
            }
            else
            {
                // ooo check imposed here
                // ooo check imposed here
                EdgeInfoHal *edge_info = *reinterpret_cast<EdgeInfoHal **>(old_eb + ((cur_edge_b / 2) * 8) + ((l * 8) - 8));
                if (edge_info != NULL)
                {
                    if (edge_info->getInvldTime()->getWts() < getMinReadVersion())
                    {
                        AdjacentPos apn; // = static_cast<AdjacentPos *>(me->Allocate(sizeof(AdjacentPos), thread_id));
                        apn.indrIndex = -1;

                        AdjacentPos *ap_pos = index[phy_src_id].getDestNodeHashTableVal(remove_version_read(remove_version(*reinterpret_cast<uint64_ptr>(old_eb + (l * 8)))), &apn);

                        if (check_version_read(ap_pos->edgeBlockIndex))
                        {
                            DVE *dve = reinterpret_cast<DVE *>(remove_version(ap_pos->edgeBlockIndex));
                            //
                            if (dve->l_del == remove_version(edge_info->getInvldTime()->getSrcTime()))
                            {
                                if (dve->next == NULL)
                                {
                                    DestGc *d_gc = static_cast<DestGc *>(me->Allocate(sizeof(DestGc), thread_id));
                                    d_gc->Dnode = remove_version(dve->Dnode);
                                    d_gc->oldest_version = remove_version(edge_info->getInvldTime()->getSrcTime());
                                    index[phy_src_id].getHashTable()->hashtable[ap_pos->hashTableIndex] = add_version_read(add_version(reinterpret_cast<vertex_t>(d_gc)));
                                    me->free(dve, sizeof(DVE), thread_id);
                                }
                            }
                            else
                            {
                                dve->oldest_version = remove_version(edge_info->getInvldTime()->getSrcTime());
                            }
                        }
                        else
                        {
                        }


                        //
                        me->free(edge_info->getInvldTime(), sizeof(ITM), thread_id);
                        me->free(edge_info, sizeof(EdgeInfoHal), thread_id);
                    }
                    else
                    {
                        // std::cout << "dc dest" << std::endl;

                        uint16_t newEb_index = *reinterpret_cast<uint16_ptr>(new_eb + 1);

                        *reinterpret_cast<uint64_ptr>(new_eb + ((newEb_index - 1) * 8)) = *reinterpret_cast<uint64_ptr>(
                                old_eb + (l * 8)); // dest id

                        *reinterpret_cast<EdgeInfoHal **>(new_eb + ((new_edge_b / 2) * 8) + (((newEb_index - 1) * 8) - 8)) = edge_info;//  IEM block

                        new_property[newEb_index - 1] = old_property[l]; // property array

                        *reinterpret_cast<uint16_ptr>(new_eb + 1) = *reinterpret_cast<uint16_ptr>(new_eb + 1) - 1; // update the current index in new stalb
                        /// empty space
                        if (edge_info->getOutOfOrderUpdt() == NULL)
                        {
                            *reinterpret_cast<uint16_ptr>(new_eb + 5) = *reinterpret_cast<uint16_ptr>(new_eb + 5) + 1;
                        }
                        else
                        {
                            //std::cout << "hello not null" << std::endl;
                            //if(edge_info->getOutOfOrderUpdt()->getCount() == 0)
                            // {
                            *reinterpret_cast<uint16_ptr>(new_eb + 5) = *reinterpret_cast<uint16_ptr>(new_eb + 5) + 1;
                            //  }
                        }

                        *reinterpret_cast<uint8_ptr>(new_eb + 3) = 1;
                    }
                }
            }
        }
    }
    else
    {
        property_type* new_property = *reinterpret_cast<property_type**>( new_eb + ((((uint64_t)  1 << *reinterpret_cast<uint8_ptr>(new_eb))  * 8) - 8));
        int counter_block =0;
        for (int l = ((cur_edge_b/2) - 1); l >= *reinterpret_cast<uint16_ptr>(old_eb + 1); l--)
        {
            if (!check_version(*reinterpret_cast<uint64_ptr>(old_eb + (l * 8))))
            {
                uint16_t newEb_index = *reinterpret_cast<uint16_ptr>(new_eb + 1);

                *reinterpret_cast<uint64_ptr>(new_eb + ((newEb_index - 1) * 8)) = *reinterpret_cast<uint64_ptr>(old_eb + (l * 8)); // dest id

                *reinterpret_cast<LeafNode **>(new_eb + ((new_edge_b/2) * 8) + (((newEb_index-1) * 8) - 8)) = *reinterpret_cast<LeafNode **>(old_eb + ((cur_edge_b / 2) * 8) + ((l * 8) - 8)); //  IEM block

                new_property[newEb_index-1] = old_property[l]; // property array

                *reinterpret_cast<uint16_ptr>(new_eb + 1) = *reinterpret_cast<uint16_ptr>(new_eb + 1) - 1; // update the current index in new stalb

            }

        }
    }
}

// get min version for degree
timestamp_t HBALStore::getMinReadDegreeVersion()
{
    return min_read_degree_version.load();
}
// get min version

timestamp_t HBALStore::getMinReadVersion()
{
    return min_read_version.load();//readTable->readQuery[0];
    timestamp_t min_version = MaxValue;
    //  std::cout<<"start :"<<this->readTable->lock.data<<std::endl;

    //  this->readTable->lock.lock();
    // HBALStore::querylock.lock();

    // std::cout<<"end 0"<<std::endl;

    if(readTable->readQuery[1] != std::numeric_limits<timestamp_t>::max())
    {
        //  std::cout<<"end 0"<<std::endl;

        for(int i=0;i<readTable->len;i++)
        {
            if(readTable->readQuery[i] == std::numeric_limits<timestamp_t>::max())
            {
                break;
            }
            else
            {
                if((readTable->readQuery[i] < min_version))
                {
                    min_version = readTable->readQuery[i];
                }
            }
        }
        //  readTable->lock.unlock();
        // HBALStore::querylock.unlock();

        //  std::cout<<"end"<<std::endl;

        return min_version;
    }
    else
    {
        min_version  = readTable->readQuery[0];
        // std::cout<<"end"<<std::endl;

        //  readTable->lock.unlock();

        //std::cout<<"end"<<std::endl;

        return min_version;
    }

}


// %%%%%% gc step for invalid block if exist
void HBALStore::edgeBlockGC(vertex_t phy_src_id, bool is_single_block, MemoryAllocator *me, int thread_id)
{
    /// %%%%%%
    InvalidListVal *inval_chain = static_cast<InvalidListVal *>(index[phy_src_id].invalidBlock);
    if (inval_chain == NULL)
    {
        return;
    }
    InvalidLinkListNode *inval_node = inval_chain->head;
    InvalidLinkListNode *prev_node;
    timestamp_t rts = MaxValue;
    bool readFlag = false;

    if (HBALStore::isReadActive)
    {
        rts = getMinReadVersion();
        readFlag = true;
    }

    while (inval_node != NULL)
    {
        if (inval_node->latest_Del_Time > rts)
        {
            break;
        }
        if (!check_version(inval_node->edge_block_size)) // in-order update
        {
            if (!(index[phy_src_id].edgePtr & 0x1))
            {
                // 2.1
                PerSourceIndrPointer *ptr_ind = static_cast<PerSourceIndrPointer *>(index[phy_src_id].getEdgePtr());

                u_int64_t index_block = (((u_int64_t) 1 << ptr_ind->getBlockLen()) - ((u_int64_t) (inval_node->indr_block_size - inval_node->block_index)));


                stalb_type *eb = ptr_ind->getperVertexBlockArr()[index_block];

                int cur_edge_b = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

                if (cur_edge_b == inval_node->edge_block_size)
                {
                    int num_element_cap = ((cur_edge_b - 2) / 2);

                    // todo if block resize then need to check again 50% block is empty or not; if not 50%

                    bool flag_already_init_srcVertex = false;

                    u_int64_t new_edge_b = cur_edge_b / 2;
                    double *property = *reinterpret_cast<double **>( eb + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb)) * 8) - 8)); // property
                    if (property[0] == inval_node->latest_Del_Time) // check the eb latest time iss equal to when 50% was empty
                    {
                        index[phy_src_id].indirectionLock.lock();

                        if (new_edge_b != 2)
                        {

                            stalb_type *newEb = DownEdgeBlock(log2(cur_edge_b), me, thread_id);
                            u_int64_t counter_cur_index = new_edge_b;

                            // edge block resize value store

                            updateNewSTALB(cur_edge_b, new_edge_b, phy_src_id, eb, newEb, property, me, thread_id); // update stalb when resize

                            ptr_ind->getperVertexBlockArr()[index_block] = newEb;

                            me->free(eb, cur_edge_b * 8, thread_id); // free stalb

                            me->free(property, sizeof(property_type) * (cur_edge_b / 2), thread_id); // free property array
                        }
                        else
                        {
                            // the stalb need to null means after deletion no neighbour exist
                            EdgeInfoHal *eddgeInfo = *reinterpret_cast<EdgeInfoHal **>( eb + 16);
                            //

                            // new edge block siz 32 should be garbage collect
                            ptr_ind->getperVertexBlockArr()[index_block] = (stalb_type *) FLAG_EMPTY_SLOT; //add_version(eddgeInfo->getSTime());

                            ptr_ind->setEmptySpace();
                            //  testLock.lock();
                            //  std::cout<<"hello "<<ptr_ind->getCurPos()<<" "<<index_block<<" "<<(float) ((u_int64_t) 1 << ptr_ind->getBlockLen())<<" "<<(float) ptr_ind->getEmptySpace()<<std::endl;
                            //  testLock.unlock();

                            // *************** indr array resize start ***********

                            while (reinterpret_cast<u_int64_t>(ptr_ind->getperVertexBlockArr()[ptr_ind->getCurPos()]) == FLAG_EMPTY_SLOT)
                            {
                                ptr_ind->getperVertexBlockArr()[ptr_ind->getCurPos()] = NULL;
                                ptr_ind->setCurPos(ptr_ind->getCurPos() + 1);
                                ptr_ind->emptySpace--;
                            }

                            // testLock.lock();
                            //  std::cout<<"hello "<<ptr_ind->getperVertexBlockArr()[ptr_ind->getCurPos()]<<" "<<ptr_ind->getCurPos()<<" "<<index_block<<" "<<(float) ((u_int64_t) 1 << ptr_ind->getBlockLen())<<" "<<(float) ptr_ind->getEmptySpace()<<std::endl;
                            // testLock.unlock();

                            if (((float) ((u_int64_t) 1 << ptr_ind->getBlockLen()) / (float) ptr_ind->getEmptySpace()) <= 2.00 && ptr_ind->getEmptySpace() != 0)
                            {

                                if (ptr_ind->getBlockLen() == 1)
                                {

                                    //%%     index[phy_src_id].indirectionLock.lock();

                                    if (reinterpret_cast<u_int64_t>(ptr_ind->getperVertexBlockArr()[0]) == FLAG_EMPTY_SLOT)
                                    {
                                        index[phy_src_id].setEdgePtr(reinterpret_cast<vertex_t>(ptr_ind->getperVertexBlockArr()[1]), 1);
                                    }
                                    else
                                    {
                                        index[phy_src_id].setEdgePtr(reinterpret_cast<vertex_t>(ptr_ind->getperVertexBlockArr()[0]), 1);
                                    }
                                    //  isDegreeActive = false;
                                    index[phy_src_id].degreeBit = false;

                                    me->free(ptr_ind->getperVertexBlockArr(), ((u_int64_t) 1 << ptr_ind->getBlockLen()) * 8, thread_id);
                                    //   index[phy_src_id].indirectionLock.unlock();

                                    flag_already_init_srcVertex = true;

                                }
                                else
                                {

                                    //%% index[phy_src_id].indirectionLock.lock();

                                    //downsize the blockArr
                                    int indr_cur_s = (int) 1 << ptr_ind->getBlockLen();

                                    int new_indr_b = indr_cur_s / 2;

                                    stalb_type **tempBlock = static_cast<stalb_type **>(me->Allocate(8 * new_indr_b, thread_id));///static_cast<EdgeBlock **>(aligned_alloc(8,8 * (blockLen * fixedSize)));//static_cast<EdgeBlock **>(malloc(8 * (blockLen * fixedSize)));
                                    memset(tempBlock, 0, 8 * new_indr_b);

                                    // use temp array to store new downsize array indexes in previous size array for faster updates hash table
                                    int *tempArr = reinterpret_cast<int *>(me->Allocate(8 * indr_cur_s, thread_id));//reinterpret_cast<EdgeInfoHal **>(aligned_alloc(8,8 * START_BLOCK_UPDATE_SIZE));
                                    memset(tempArr, 0, 8 * indr_cur_s);

                                    int new_array_index_start = new_indr_b;

                                    bool flag = false; // when first non null index that will be the new indr array current size
                                    int new_array_cur_pos = -1;

                                    bool IsValFlag = false;
                                    for (int k = (indr_cur_s - 1); k >= ptr_ind->getCurPos(); k--)
                                    {

                                        if (reinterpret_cast<u_int64_t>(ptr_ind->getperVertexBlockArr()[k]) != FLAG_EMPTY_SLOT)
                                        {
                                            //
                                            tempBlock[--new_array_index_start] = ptr_ind->getperVertexBlockArr()[k];
                                            tempArr[k] = new_array_index_start;
                                            new_array_cur_pos = new_array_index_start;
                                            IsValFlag = true;
                                        }
                                    }

                                    // replace the new indr array
                                    if (IsValFlag)
                                    {

                                        void *ptr = ptr_ind->getperVertexBlockArr();
                                        ptr_ind->setPerVertexBlockArr(tempBlock);
                                        ptr_ind->setBlockLen(log2(new_indr_b));
                                        ptr_ind->setCurPos(new_array_cur_pos);
                                        // ptr_ind->initDownsizeVal();
                                        me->free(ptr, indr_cur_s * 8, thread_id);
                                        // hash table update

                                        for (u_int64_t u = 0; u < index[phy_src_id].getHashTable()->hashBlockSize; u++)
                                        {
                                            if (index[phy_src_id].getHashTable()->hashtable[u] != FLAG_EMPTY_SLOT && index[phy_src_id].getHashTable()->hashtable[u] != FLAG_TOMB_STONE)
                                            {
                                                u_int64_t adj_po = index[phy_src_id].getHashTable()->hashtable[u];
                                                u_int64_t indr_index1 = getBitMask(adj_po, L1IndexStart, L1IndexBits);
                                                u_int64_t indr_size1 = (u_int64_t) 1 << getBitMask(adj_po, L1SizeStart, L1SizeBits);

                                                // lock on per source vertex to get the index_block edge block
                                                u_int64_t index_block_temp = ((indr_cur_s) - (indr_size1 - indr_index1));
                                                u_int64_t new_pos_l2 = tempArr[index_block_temp];

                                                adj_po = setBitMask(adj_po, new_pos_l2, L1IndexStart, L1IndexBits);
                                                adj_po = setBitMask(adj_po, log2(new_indr_b), L1SizeStart, L1SizeBits);

                                                index[phy_src_id].getHashTable()->hashtable[u] = adj_po;
                                            }
                                        }

                                        //%%%%%%%%%%% update invalid list
                                        InvalidLinkListNode *update_invl_node = inval_node;
                                        while (update_invl_node->next != NULL)
                                        {
                                            update_invl_node = update_invl_node->next;
                                            u_int64_t index_block_pr = ((indr_cur_s) - (update_invl_node->indr_block_size - update_invl_node->block_index));
                                            update_invl_node->block_index = tempArr[index_block_pr];
                                            update_invl_node->indr_block_size = new_indr_b;
                                        }

                                        // %%%%%%%%%% end

                                        me->free(tempArr, indr_cur_s * 8, thread_id);

                                    }
                                    else
                                    {

                                        index[phy_src_id].initVal();

                                        //  index[phy_src_id].degreeBit = false;

                                    }
                                    //  index[phy_src_id].indirectionLock.unlock();

                                }
                            }


                            // *************** indr array resize end   ***********

                        }
                        index[phy_src_id].indirectionLock.unlock();

                    }
                }
            }
            else
            {
                stalb_type *eb = static_cast<stalb_type *>(index[phy_src_id].getEdgePtr());

                int cur_edge_b = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);
                //  int cur_edge_b1 = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(inval_node->ptr);
                int cur_edge_b2 = inval_node->indr_block_size;

                if (cur_edge_b == inval_node->edge_block_size)
                {
                    int num_element_cap = ((cur_edge_b - 2) / 2);

                    double *property = *reinterpret_cast<double **>(eb + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb)) * 8) - 8)); // property

                    if (property[0] == inval_node->latest_Del_Time) // check the eb latest time iss equal to when 50% was empty
                    {


                        /// %%%%%% single block
                        u_int64_t new_edge_b = cur_edge_b / 2;
                        index[phy_src_id].indirectionLock.lock();

                        if (new_edge_b != 2)
                        {
                            stalb_type *newEb = DownEdgeBlock(log2(cur_edge_b), me, thread_id);

                            u_int64_t counter_cur_index = new_edge_b;

                            // edge block resize value store

                            updateNewSTALB(cur_edge_b, new_edge_b, phy_src_id, eb, newEb, property, me, thread_id); // update stalb when resize

                            // ptr_ind and newEb

                            index[phy_src_id].setEdgePtr(reinterpret_cast<vertex_t>(newEb), 1);

                            me->free(eb, cur_edge_b * 8, thread_id); // free stalb

                            me->free(property, sizeof(property_type) * (cur_edge_b / 2), thread_id); // free property array
                        }
                        else
                        {
                            // the stalb need to null means after deletion no neighbour exist
                            index[phy_src_id].initVal(); // init all the information of src vertex
                            index[phy_src_id].degreeBit = false;
                        }
                        index[phy_src_id].indirectionLock.unlock();
                    }
                }
            }
        }
        else
        {
            if (inval_node->edge_block_size == FLAG_EMPTY_SLOT)
            {
                stalb_type *eb = reinterpret_cast<stalb_type *>(remove_version(inval_node->block_index));
                int cur_edge_b = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);
                int num_element_cap = ((cur_edge_b - 2) / 2);

                double *property = *reinterpret_cast<double **>(eb + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb)) * 8) - 8)); // property

                if (property[0] == inval_node->latest_Del_Time)
                {
                    /// %%%%%% single block
                    u_int64_t new_edge_b = cur_edge_b / 2;
                    index[phy_src_id].indirectionLock.lock();

                    if (new_edge_b != 2)
                    {

                        stalb_type *newEb = DownEdgeBlock(log2(cur_edge_b), me, thread_id);

                        u_int64_t counter_cur_index = new_edge_b;

                        // edge block resize value store

                        updateNewSTALB(cur_edge_b, new_edge_b, phy_src_id, eb, newEb, property, me, thread_id, true); // update stalb when resize


                        uint16_t newEb_index = *reinterpret_cast<uint16_ptr>(newEb + 1);


                        LeafNode *ln_new = *reinterpret_cast<LeafNode **>(newEb + ((new_edge_b / 2) * 8) + (((newEb_index) * 8) - 8));

                        ////%%%%%%% get the edgeblock index by time

                        EdgeInfoHal *out_of_order_field = getOutOfOrderFieldFromSTAL(phy_src_id, ln_new->getSTime(), me, thread_id);
                        out_of_order_field->setOutOfOrderUpdt(remove_version(reinterpret_cast<vertex_t>(newEb)));

                        ////%%%%%%%

                        // need index of stalb
                        me->free(eb, cur_edge_b * 8, thread_id); // free stalb

                        me->free(property, sizeof(property_type) * (cur_edge_b / 2), thread_id); // free property array
                    }
                    else
                    {
                        uint16_t cur_eb_index = *reinterpret_cast<uint16_ptr>(eb + 1);

                        LeafNode *ln_new = *reinterpret_cast<LeafNode **>(eb + ((new_edge_b / 2) * 8) + (((cur_eb_index) * 8) - 8));

                        EdgeInfoHal *out_of_order_field = getOutOfOrderFieldFromSTAL(phy_src_id, ln_new->getSTime(), me, thread_id);
                        out_of_order_field->setOutOfOrderUpdt(NULL);
                    }
                    index[phy_src_id].indirectionLock.unlock();
                }

            }
            else
            {
                // ART
                ArtPerIndex *art_per_index = reinterpret_cast<ArtPerIndex *>(remove_version_read(remove_version(inval_node->edge_block_size)));

                art::art < stalb_type * > *art_val = art_per_index->getArtTree();

                stalb_type *eb = reinterpret_cast<stalb_type *>(remove_version(inval_node->block_index));
                int cur_edge_b = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);
                //


                u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

                LeafNode *ln_art = *reinterpret_cast<LeafNode **>(eb + ((eb_block_size / 2) * 8) + ((((u_int64_t) 1 << *reinterpret_cast<uint16_ptr>(eb + 1)) * 8) - 8));


                //####
                int num_element_cap = ((cur_edge_b - 2) / 2);

                double *property = *reinterpret_cast<double **>(eb + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb)) * 8) - 8)); // property

                if (property[0] == inval_node->latest_Del_Time)
                {
                    /// %%%%%% single block
                    u_int64_t new_edge_b = cur_edge_b / 2;
                    index[phy_src_id].indirectionLock.lock();

                    if (new_edge_b != 2)
                    {

                        stalb_type *newEb = DownEdgeBlock(log2(cur_edge_b), me, thread_id);

                        u_int64_t counter_cur_index = new_edge_b;

                        // edge block resize value store
                        updateNewSTALB(cur_edge_b, new_edge_b, phy_src_id, eb, newEb, property, me, thread_id, true); // update stalb when resize


                        uint16_t newEb_index = *reinterpret_cast<uint16_ptr>(newEb + 1);


                        LeafNode *ln_new = *reinterpret_cast<LeafNode **>(newEb + ((new_edge_b / 2) * 8) + (((newEb_index) * 8) - 8));

                        art_per_index->insertArtLeaf(ln_art->getSTime(), newEb);

                        ////%%%%%%%

                        // need index of stalb
                        me->free(eb, cur_edge_b * 8, thread_id); // free stalb

                        me->free(property, sizeof(property_type) * (cur_edge_b / 2), thread_id); // free property array
                    }
                    else
                    {
                        art_per_index->removeArtLeaf(ln_art->getSTime());
                        me->free(eb, cur_edge_b * 8, thread_id); // free stalb

                        me->free(property, sizeof(property_type) * (cur_edge_b / 2), thread_id); // free property array
                    }
                    index[phy_src_id].indirectionLock.unlock();
                }
            }
        }

        prev_node = inval_node;

        /// %%%%%%%%% delete inval_node

        inval_node = inval_node->next;

        me->free(prev_node, sizeof(InvalidLinkListNode), thread_id);
    }

    //%%% change the srcvertex.invalblock

    if (inval_node != NULL)
    {
        inval_chain->head = inval_node;
    }
    else
    {
        me->free(index[phy_src_id].invalidBlock, sizeof(InvalidListVal), thread_id);
        index[phy_src_id].invalidBlock = NULL;
    }

    /// %%%%%%
}


/// %%%%%%% gc step end

/// %%%%%%%% gc step for per source vertex %%%%%% ///

void HBALStore::gcDegreeList(vertex_t phy_src_id, MemoryAllocator *me, int thread_id)
{
    if (this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.try_lock())
    {

        InvalidNodeAddress *invl_node = this->invalidListDegree[phy_src_id % THRESHOLDINVALID];


        if (invl_node->ptr != NULL)
        {
            //   std::cout<<"hello"<<std::endl;
            InvalidDegree *id = static_cast<InvalidDegree *>(invl_node->ptr);
            invl_node->ptr = NULL;
            this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.unlock();

            InvalidDegree *prev_i = NULL;

            while (id != NULL)
            {
                // %%%%%%%%%% Degree node garbage collection %%%%%%%

                DegreeNode *dn = static_cast<DegreeNode *>(id->ptr);
                DegreeNode *prev = NULL;
                while (dn != NULL)
                {
                    prev = dn;
                    dn = dn->next;
                    //std::cout<<"deleted"<<std::endl;
                    //  std::cout<<"deleted"<<std::endl;

                    me->free(prev, sizeof(DegreeNode), thread_id);
                }

                // %%%%%%%% end degreenode %%%%%%%%%
                prev_i = id;
                id = id->next;
                //me->free(prev_i,sizeof(InvalidDegree),thread_id);
                free(prev_i);
            }

            //invl_node->ptr = NULL;

        }
        else
        {
            this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.unlock();
        }
    }


}

void HBALStore::degreeDecrement(timestamp_t commit_time, vertex_t phy_src_id, MemoryAllocator *me, int thread_id)
{
    bool flagc = false;
    if (check_version(index[phy_src_id].degree))
    {
        // DegreeNode
        DegreeNode *cur_degree = reinterpret_cast<DegreeNode *>(remove_version(index[phy_src_id].degree));
        if (getMinReadDegreeVersion() > cur_degree->wts)
        {
            /// %%%%%%%%%%
            InvalidDegree *new_invl_node = static_cast<InvalidDegree *>(malloc(sizeof(InvalidDegree)));//static_cast<InvalidDegree *>(me->Allocate(sizeof(InvalidDegree), thread_id)); // InvalidDegree();

            index[phy_src_id].degree = remove_version(cur_degree->degree) - 1;

            new_invl_node->ptr = cur_degree;

            this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.lock();
            if (this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr == NULL)
            {
                this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr = new_invl_node;
                new_invl_node->next = NULL;
            }
            else
            {
                new_invl_node->next = static_cast<InvalidDegree *>(this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr);
                this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr = new_invl_node;
            }
            this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.unlock();
            flagc = true; // in case writer put node to InvalidListDegree
        }
        else
        {

            DegreeNode *new_degree;

            new_degree = (DegreeNode *) (me->Allocate(sizeof(DegreeNode), thread_id));

            new_degree->degree = remove_version(cur_degree->degree) - 1;

            new_degree->next = cur_degree;

            index[phy_src_id].degree = add_version(reinterpret_cast<vertex_t>(new_degree));
            new_degree->wts = commit_time;
        }

    }
    else
    {
        //
        if (getMinReadDegreeVersion() == MaxValue)
        {
            index[phy_src_id].degree = remove_version(index[phy_src_id].degree) - 1;
        }
        else
        {
            DegreeNode *cur_degree;
            DegreeNode *new_degree;

            cur_degree = (DegreeNode *) (me->Allocate(sizeof(DegreeNode), thread_id));
            new_degree = (DegreeNode *) (me->Allocate(sizeof(DegreeNode), thread_id));
            cur_degree->degree = remove_version(index[phy_src_id].degree);
            cur_degree->wts = 0;
            cur_degree->next = NULL;

            new_degree->degree = remove_version(index[phy_src_id].degree) - 1;
            new_degree->next = cur_degree;

            index[phy_src_id].degree = add_version(reinterpret_cast<vertex_t>(new_degree));
            new_degree->wts = commit_time;

        }
    }
    //%%%%%%%%%%% end decrement degree
    if (check_version(index[phy_src_id].degree))
    {
        flagc = true;
    }

    if (flagc)
    {
        gcDegreeList(phy_src_id, me, thread_id);
    }
}

/// %%%%%%%  gc step end %%%%%%%% ////
void HBALStore::inOrderDeletion(timestamp_t commit_time, vertex_t phy_src_id, vertex_t phy_dest_id, AdjacentPos *ap_pos, InputEdge inputedge_info, MemoryAllocator *me, int thread_id, AdjacentPos *old_adjc_pos)
{
    // std::cout<<ap_pos->indrIndex<<std::endl;
    EdgeInfoHal *eih;
    bool isInOrder = false;
    timestamp_t rts = MaxValue;
    bool readFlag = false;

    timestamp_t source_timestamp_delete = getTimestampSrc(inputedge_info, IsOutOfOrder);
    //getTimestampSrc(inputedge_info);
    // due to resize of indr array the block_index size that store in hash table bucket could change there position for that we use formula
    // current block len (indr array) - (indrsize size at the time of insertion - the block index of insertion)

    // check if the deletion dest node entry in order or ooo
    if (!(index[phy_src_id].edgePtr & 0x1))
    {

        u_int64_t index_block = ap_pos->indrIndex;

        PerSourceIndrPointer *ptr_ind = static_cast<PerSourceIndrPointer *>(index[phy_src_id].getEdgePtr());

        stalb_type *eb = ptr_ind->getperVertexBlockArr()[index_block];

        u_int64_t index_edge_block = ap_pos->edgeBlockIndex;

        ITM *itm = (ITM * )(me->Allocate(sizeof(ITM), thread_id));
        itm->setSrcTime(source_timestamp_delete);


        u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

        eih = *reinterpret_cast<EdgeInfoHal **>(eb + ((eb_block_size / 2) * 8) + ((index_edge_block * 8) - 8));

        //   timestamp_t read_time = getMinReadVersion();

        eih->setInvldTime(itm);

        *reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8)) = add_version(*reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8))); // dest-id FLAGEMTPY

        *reinterpret_cast<uint8_ptr>(eb + 3) = 1; // isdeletion flag
        // if no ooo exist in eih
        if (eih->getOutOfOrderUpdt() == 0)
        {
            *reinterpret_cast<uint16_ptr>(eb + 5) = *reinterpret_cast<uint16_ptr>(eb + 5) + 1; // set empty space
        }

        itm->setWts(commit_time);

        double *property = *reinterpret_cast<double **>( eb + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb)) * 8) - 8)); // property
        // deletion wts store in the unused index of the property array

        int cur_edge_b = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

        int num_element_cap = ((cur_edge_b - 2) / 2);

        bool flag_already_init_srcVertex = false;
        if (!HBALStore::isReadActive)
        {
            *reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8)) = add_version_read(*reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8))); /// no read-only queries are active
            me->free(itm, sizeof(ITM), thread_id);
            me->free(eih, sizeof(EdgeInfoHal), thread_id);
            *reinterpret_cast<uint64_ptr>(eb + ((eb_block_size / 2) * 8) + ((index_edge_block * 8) - 8)) = NULL;
            // index[phy_src_id].getHashTable()->hashtable[ap_pos->hashTableIndex];
            if (!check_version(old_adjc_pos->edgeBlockIndex))
            {
                DestGc *d_gc = static_cast<DestGc *>(me->Allocate(sizeof(DestGc), thread_id));
                d_gc->Dnode = remove_version_read(remove_version(*reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8))));
                d_gc->oldest_version = (source_timestamp_delete);
                index[phy_src_id].getHashTable()->hashtable[old_adjc_pos->hashTableIndex] = add_version_read(add_version(reinterpret_cast<vertex_t>(d_gc)));
            }
            else
            {
                DVE *dve_exist = reinterpret_cast<DVE *>(remove_version(old_adjc_pos->edgeBlockIndex));
                dve_exist->l_del = (source_timestamp_delete);
                dve_exist->oldest_version = (source_timestamp_delete);
            }
            // *reinterpret_cast<uint64_ptr>(eb + ((eb_block_size / 2) * 8) + ((index_edge_block * 8) - 8)) = NULL;
            //me->free(eih, sizeof(EdgeInfoHal), thread_id);
            // me->free(itm, sizeof(ITM), thread_id);*/
        }
        if ((((float) (num_element_cap + 1) / 2) == *reinterpret_cast<uint16_ptr>(eb + 5)) && *reinterpret_cast<uint16_ptr>(eb + 5) != 0) // half of the elements in the stalb is empty
        {
// %%%%%%%%%%%%%%%%%%%%%%%%%%%% store the block in invalid list
            //  testLock.lock();
            //  std::cout  << " debug 0" << std::endl;
            //  testLock.unlock();
            timestamp_t latest_deletion_time = commit_time;//eih->getInvldTime()->getWts();
            property[0] = latest_deletion_time;
            //  this->invalLock.lock();
            if (index[phy_src_id].invalidBlock == NULL)
            {
                InvalidListVal *inv = (InvalidListVal *) (me->Allocate(sizeof(InvalidListVal), thread_id));

                InvalidLinkListNode *new_inval_node = (InvalidLinkListNode *) (me->Allocate(sizeof(InvalidLinkListNode), thread_id));
                new_inval_node->edge_block_size = cur_edge_b;
                new_inval_node->latest_Del_Time = latest_deletion_time;
                new_inval_node->block_index = index_block;
                new_inval_node->indr_block_size = (u_int64_t) 1 << ptr_ind->getBlockLen(); //
                new_inval_node->next = NULL;
                inv->head = new_inval_node;
                inv->tail = new_inval_node;

                index[phy_src_id].invalidBlock = (inv);
                // index[phy_src_id].invalidBlock = (index[phy_src_id].invalidBlock);
            }
            else
            {
                InvalidLinkListNode *newInval = (InvalidLinkListNode *) (me->Allocate(sizeof(InvalidLinkListNode), thread_id));
                newInval->edge_block_size = cur_edge_b;
                newInval->latest_Del_Time = latest_deletion_time;
                newInval->block_index = index_block;
                newInval->indr_block_size = (u_int64_t) 1 << ptr_ind->getBlockLen(); //

                newInval->next = NULL;

                InvalidListVal *inv_per_src = reinterpret_cast<InvalidListVal *>(index[phy_src_id].invalidBlock);

                inv_per_src->tail->next = newInval;
                inv_per_src->tail = newInval;
            }

// %%%%%%%%%%%%%%% end %%%%%%%%%%%%%%

        }
        // ########

// 2592222
    }
    else
    {
        // edge block direct read

        // %%%%%%%%%%%%%% gc


        //   edgeBlockGC(phy_src_id, 1, me, thread_id);


        //
        stalb_type *ptr_edge;
        // bool flagIsIndr = 0;

        ptr_edge = static_cast<stalb_type *>(index[phy_src_id].getEdgePtr());

        //   PerSourceIndrPointer *ptr_ind = static_cast<PerSourceIndrPointer *>(vertexIndrList[divSrcVertexId][SrcVertexId]->getEdgePtr());

        stalb_type *eb = ptr_edge;    //->->()[index_block];

        //  assert( eb==NULL && "edge block does not exist");

        u_int64_t index_edge_block = ap_pos->edgeBlockIndex;

        ITM *itm = (ITM * )(me->Allocate(sizeof(ITM), thread_id));
        itm->setSrcTime(source_timestamp_delete);

        u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

        eih = *reinterpret_cast<EdgeInfoHal **>(eb + ((eb_block_size / 2) * 8) + ((index_edge_block * 8) - 8)); //->getEdgeUpdate()[index_edge_block];

        //  assert( eih==NULL && "edge info does not exist");


        eih->setInvldTime(itm);



        // itm->setWts(getCurrTimeStamp());
        *reinterpret_cast<uint8_ptr>(eb + 3) = 1; // deletion bit

        *reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8)) = add_version(*reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8))); // dest-id FLAGEMTPY


        //property[index_edge_block] = 0.0; // weight

        ///    me->free(eih, sizeof(EdgeInfoHal), thread_id);
        // if ooo is null then empty increment
        if (eih->getOutOfOrderUpdt() == NULL)
        {
            *reinterpret_cast<uint16_ptr>(eb + 5) = *reinterpret_cast<uint16_ptr>(eb + 5) + 1; // set empty space
        }

        itm->setWts(commit_time);


        double *property = *reinterpret_cast<double **>( eb + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb)) * 8) - 8)); // property
        // property[0] = itm->getWts(); // deletion wts store in the unused index of the property array
        if (!HBALStore::isReadActive)
        {
            *reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8)) = add_version_read(*reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8)));
            me->free(itm, sizeof(ITM), thread_id);
            me->free(eih, sizeof(EdgeInfoHal), thread_id);
            *reinterpret_cast<uint64_ptr>(eb + ((eb_block_size / 2) * 8) + ((index_edge_block * 8) - 8)) = NULL;

            // index[phy_src_id].getHashTable()->hashtable[ap_pos->hashTableIndex];
            if (!check_version(old_adjc_pos->edgeBlockIndex))
            {
                DestGc *d_gc = static_cast<DestGc *>(me->Allocate(sizeof(DestGc), thread_id));
                d_gc->Dnode = remove_version_read(remove_version(*reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8))));
                d_gc->oldest_version = (source_timestamp_delete);
                index[phy_src_id].getHashTable()->hashtable[old_adjc_pos->hashTableIndex] = add_version_read(add_version(reinterpret_cast<vertex_t>(d_gc)));
            }
            else
            {
                DVE *dve_exist = reinterpret_cast<DVE *>(remove_version(old_adjc_pos->edgeBlockIndex));
                dve_exist->l_del = (source_timestamp_delete);
                dve_exist->oldest_version = (source_timestamp_delete);
            }
            // *reinterpret_cast<uint64_ptr>(eb + ((eb_block_size / 2) * 8) + ((index_edge_block * 8) - 8)) = NULL;
            // me->free(eih, sizeof(EdgeInfoHal), thread_id);
            //me->free(itm, sizeof(ITM), thread_id);*/
        }

        // cur_b_b;

        // read table timestamps
        // Read Table wts comparison
        //  readTable->lock.lock_shared();
        int cur_edge_b = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

        int num_element_cap = ((cur_edge_b - 2) / 2);


        bool flag_already_init_srcVertex = false;


        if ((((float) (num_element_cap + 1) / 2) == *reinterpret_cast<uint16_ptr>(eb + 5)) && *reinterpret_cast<uint16_ptr>(eb + 5) != 0) // half of the elements in the stalb is empty
        {
            timestamp_t latest_deletion_time = commit_time;//eih->getInvldTime()->getWts();
            property[0] = latest_deletion_time;
            if ((index[phy_src_id].invalidBlock) == NULL)
            {

                InvalidListVal *inv = (InvalidListVal *) (me->Allocate(sizeof(InvalidListVal), thread_id));

                InvalidLinkListNode *new_inval_node = (InvalidLinkListNode *) (me->Allocate(sizeof(InvalidLinkListNode), thread_id));
                new_inval_node->edge_block_size = cur_edge_b;
                new_inval_node->latest_Del_Time = latest_deletion_time;
                new_inval_node->block_index = 0;
                new_inval_node->indr_block_size = 1;

                new_inval_node->next = NULL;
                inv->head = new_inval_node;
                inv->tail = new_inval_node;

                index[phy_src_id].invalidBlock = (inv);
                // add_version(index[phy_src_id].invalidBlock);
            }
            else
            {
                InvalidLinkListNode *newInval = (InvalidLinkListNode *) (me->Allocate(sizeof(InvalidLinkListNode), thread_id));
                newInval->edge_block_size = cur_edge_b;
                newInval->latest_Del_Time = latest_deletion_time;
                newInval->next = NULL;
                newInval->block_index = 0;
                newInval->indr_block_size = 1;

                InvalidListVal *inv_per_src = reinterpret_cast<InvalidListVal *>(index[phy_src_id].invalidBlock);

                inv_per_src->tail->next = newInval;
                inv_per_src->tail = newInval;
            }
        }


    }


}


timestamp_t HBALStore::getTimestampSrc(InputEdge inputedge_info, bool is_in_order, MemoryAllocator *la)
{
    if(is_in_order)
    {

        return inputedge_info.getUpdtSrcTime();
    }
    else
    {
        return 0;
    }
}
void HBALStore::outOfOrderDeletion(vertex_t phy_src_id, vertex_t phy_dest_id, AdjacentPos *ap_pos, uint64_t *pos_ooo, InputEdge inputedge_info, MemoryAllocator *me, int thread_id, vertex_t adjc_pos)
{
    // out of order deletion part
    timestamp_t source_timestamp_del = getTimestampSrc(inputedge_info, IsOutOfOrder);
    // LeafNode *ln = reinterpret_cast<LeafNode *> (remove_version(ap_pos->edgeBlockIndex));
    ITM *itm = (ITM *) (me->Allocate(sizeof(ITM), thread_id));
    itm->setSrcTime(source_timestamp_del);
    LeafNode *ln;
    //ln->setInvldTime(itm);
    //itm->setWts(getCurrTimeStamp());
    // %%%%%%%%%%%%%%%%%%%%%% start %%%%%%%%%%%%%%%

    // stalb exist only
    if(!check_version_read(ap_pos->indrIndex))
    {
        vertex_t index_edge_block = *pos_ooo;

        stalb_type  *eb = reinterpret_cast<stalb_type *>(remove_version(ap_pos->indrIndex));

        u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

        ln = *reinterpret_cast<LeafNode **>(eb + ((eb_block_size / 2) * 8) + ((index_edge_block * 8) - 8));

        ln->setInvldTime(itm);

        *reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8)) = add_version(*reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8))); // dest-id FLAGEMTPY

        *reinterpret_cast<uint8_ptr>(eb + 3) = 1; // isdeletion flag
        // if no ooo exist in eih

        *reinterpret_cast<uint16_ptr>(eb + 5) = *reinterpret_cast<uint16_ptr>(eb + 5) + 1; // set empty space


        itm->setWts(getCurrTimeStamp());

        double *property = *reinterpret_cast<double **>( eb + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb)) * 8) - 8)); // property
        // deletion wts store in the unused index of the property array

        int cur_edge_b = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

        int num_element_cap = ((cur_edge_b - 2) / 2);

        bool flag_already_init_srcVertex = false;
        if (!HBALStore::isReadActive)
        {
            *reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8)) = add_version_read(*reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8))); /// no read-only queries are active
        }

        if ((((float) (num_element_cap + 1) / 2) == *reinterpret_cast<uint16_ptr>(eb + 5)) &&*reinterpret_cast<uint16_ptr>(eb + 5) != 0) // half of the elements in the out-of-order stalb is empty
        {

// %%%%%%%%%%%%%%%%%%%%%%%%%%%% store the block in invalid list

            timestamp_t latest_deletion_time = ln->getInvalEntry()->getWts();

            property[0] = latest_deletion_time;

            if (index[phy_src_id].invalidBlock == NULL)
            {

                InvalidListVal *inv = (InvalidListVal *) (me->Allocate(sizeof(InvalidListVal), thread_id));
                InvalidLinkListNode *new_inval_node = (InvalidLinkListNode *) (me->Allocate(sizeof(InvalidLinkListNode), thread_id));

                new_inval_node->edge_block_size = FLAG_EMPTY_SLOT;
                new_inval_node->latest_Del_Time = latest_deletion_time;
                new_inval_node->block_index = reinterpret_cast<uint64_t>(eb);
                new_inval_node->indr_block_size = 0; //
                new_inval_node->next = NULL;

                inv->head = new_inval_node;
                inv->tail = new_inval_node;
                index[phy_src_id].invalidBlock = (inv);

            }
            else
            {
                InvalidLinkListNode *newInval = (InvalidLinkListNode *) (me->Allocate(sizeof(InvalidLinkListNode), thread_id));
                newInval->edge_block_size = FLAG_EMPTY_SLOT;
                newInval->latest_Del_Time = latest_deletion_time;
                newInval->block_index = reinterpret_cast<uint64_t>(eb);
                newInval->indr_block_size = 0; //

                newInval->next = NULL;

                InvalidListVal *inv_per_src = reinterpret_cast<InvalidListVal *>(index[phy_src_id].invalidBlock);

                inv_per_src->tail->next = newInval;
                inv_per_src->tail = newInval;
            }

        }
    }
    else
    {
        // ART
        ArtPerIndex *art_per_index = reinterpret_cast<ArtPerIndex *>(remove_version_read(remove_version(ap_pos->indrIndex)));

        art::art<stalb_type *> *art_val = art_per_index->getArtTree();
        //%%%%%%%%%%%% adaptive radix tree scan start
        // timestamp_t src_timestamp = index[phy_src_id].hashPtr->hashtable[ap_pos->hashTableIndex] getBitMask(, t_s_start, t_s_bits);

        // ############ stalb code for deletion #########

        vertex_t index_edge_block = *pos_ooo;

        stalb_type  *eb = reinterpret_cast<stalb_type*> (remove_version_read(remove_version(ap_pos->edgeBlockIndex)));

        u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

        ln = *reinterpret_cast<LeafNode **>(eb + ((eb_block_size / 2) * 8) + ((index_edge_block * 8) - 8));

        ln->setInvldTime(itm);

        *reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8)) = add_version(*reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8))); // dest-id FLAGEMTPY

        *reinterpret_cast<uint8_ptr>(eb + 3) = 1; // isdeletion flag
        // if no ooo exist in eih

        *reinterpret_cast<uint16_ptr>(eb + 5) = *reinterpret_cast<uint16_ptr>(eb + 5) + 1; // set empty space


        itm->setWts(getCurrTimeStamp());

        double *property = *reinterpret_cast<double **>( eb + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb)) * 8) - 8)); // property
        // deletion wts store in the unused index of the property array

        int cur_edge_b = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

        int num_element_cap = ((cur_edge_b - 2) / 2);

        bool flag_already_init_srcVertex = false;
        if (!HBALStore::isReadActive)
        {
            *reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8)) = add_version_read(*reinterpret_cast<uint64_ptr>(eb + (index_edge_block * 8))); /// no read-only queries are active
        }

        if ((((float) (num_element_cap + 1) / 2) == *reinterpret_cast<uint16_ptr>(eb + 5)) &&*reinterpret_cast<uint16_ptr>(eb + 5) != 0) // half of the elements in the out-of-order stalb is empty
        {

// %%%%%%%%%%%%%%%%%%%%%%%%%%%% store the block in invalid list

            timestamp_t latest_deletion_time = ln->getInvalEntry()->getWts();

            property[0] = latest_deletion_time;

            if (index[phy_src_id].invalidBlock == NULL)
            {

                InvalidListVal *inv = (InvalidListVal *) (me->Allocate(sizeof(InvalidListVal), thread_id));
                InvalidLinkListNode *new_inval_node = (InvalidLinkListNode *) (me->Allocate(sizeof(InvalidLinkListNode), thread_id));

                new_inval_node->edge_block_size = reinterpret_cast<vertex_t>(art_per_index);
                new_inval_node->latest_Del_Time = latest_deletion_time;
                new_inval_node->block_index = reinterpret_cast<uint64_t>(eb);
                new_inval_node->indr_block_size = 0; //
                new_inval_node->next = NULL;

                inv->head = new_inval_node;
                inv->tail = new_inval_node;
                index[phy_src_id].invalidBlock = (inv);

            }
            else
            {
                InvalidLinkListNode *newInval = (InvalidLinkListNode *) (me->Allocate(sizeof(InvalidLinkListNode), thread_id));
                newInval->edge_block_size = reinterpret_cast<vertex_t>(art_per_index);
                newInval->latest_Del_Time = latest_deletion_time;
                newInval->block_index = reinterpret_cast<uint64_t>(eb);
                newInval->indr_block_size = 0; //

                newInval->next = NULL;

                InvalidListVal *inv_per_src = reinterpret_cast<InvalidListVal *>(index[phy_src_id].invalidBlock);

                inv_per_src->tail->next = newInval;
                inv_per_src->tail = newInval;
            }

        }

        // ########## stalb code end ###################
    }

}


void HBALStore::addDuplicateNodeBaseOnSourceTimeInDVE(DuplicateUpdate *val, DVE *dve)
{
    DuplicateUpdate *cur = dve->next;
    DuplicateUpdate *prev = NULL;

    while(cur !=NULL)
    {
        if(remove_version(cur->SrcTime) > remove_version(val->SrcTime))
        {
            break;
        }
        prev = cur;
        cur = cur->next;
    }
    if(prev != NULL)
    {
        prev->next = val;
        val->next = cur;
    }
    else
    {
        val->next = cur;
        dve->next = val;
    }
}
void HBALStore::removeBaseOnSourceTimeInDVE(timestamp_t src_time, DVE *dve, MemoryAllocator *me, int thread_id)
{
    DuplicateUpdate *cur = dve->next;
    DuplicateUpdate *prev = NULL;
    DuplicateUpdate *node_to_delete = NULL;
    while(cur !=NULL)
    {
        if(remove_version(cur->SrcTime) == remove_version(src_time))
        {
            node_to_delete = cur;
            cur = cur->next;
            break;
        }
        prev = cur;
        cur = cur->next;
    }
    if(prev != NULL)
    {
        prev->next = cur;
        me->free(node_to_delete, sizeof(DuplicateUpdate), thread_id);
    }
    else
    {
        dve->next = cur;
        me->free(node_to_delete, sizeof(DuplicateUpdate), thread_id);
    }
}
void HBALStore::removeDuplicateNodeBaseOnSourceTimeInDVE(DuplicateUpdate *val, DVE *dve, MemoryAllocator *me, int thread_id)
{
    DuplicateUpdate *cur = dve->next;
    DuplicateUpdate *prev = NULL;

    while(cur !=NULL)
    {
        if(remove_version(cur->SrcTime) == remove_version(val->SrcTime))
        {
            break;
        }
        prev = cur;
        cur = cur->next;
    }
    if(prev != NULL)
    {
        prev->next = cur->next;
        //me->free(val, sizeof(DuplicateUpdate), thread_id);
    }
    else
    {
        dve->next = NULL;//cur->next;
        //me->free(val, sizeof(DuplicateUpdate), thread_id);
    }
    // std::cout<<"remove"<<std::endl;
    me->free(val, sizeof(DuplicateUpdate), thread_id);
}
//remove edge
void HBALStore::executeEdgeDeletion(InputEdge inputedge_info, MemoryAllocator *me, int thread_id)
{
    vertex_t phy_src_id = physical_id(inputedge_info.getSrcVertexId());
    vertex_t phy_dest_id = physical_id(inputedge_info.getDestVertexId());

    index[phy_src_id].perVertexLock.lock();
    index[phy_dest_id].perVertexLock.lock();

    timestamp_t commit_time = getCurrTimeStamp();

    removeEdge(phy_src_id, phy_dest_id, commit_time, inputedge_info,me, thread_id);
    removeEdge(phy_dest_id, phy_src_id, commit_time,{inputedge_info.getDestVertexId(),inputedge_info.getSrcVertexId(),inputedge_info.getUpdtSrcTime(),0},me, thread_id);

    index[phy_src_id].perVertexLock.unlock();
    index[phy_dest_id].perVertexLock.unlock();
}

void HBALStore::removeEdge(vertex_t phy_src_id, vertex_t phy_dest_id, timestamp_t commit_time, InputEdge inputedge_info, MemoryAllocator *me, int thread_id)
{

    bool is_out_of_order = IsOutOfOrder;
    timestamp_t source_timestamp_deletion = getTimestampSrc(inputedge_info, IsOutOfOrder);

    /*

      For InorderUpdate

      At the time of insertion we need indr_array_current_size + indr_cur_index (indirection array level)
      and in block size level we need current_index of the insrted_entry + the size of the fixed block

      For outoforder update
      1) we need indriection size + indr_cur_index
      2) address of leafNode


      Bucket

      has two possible value
      either a int_64 (position of insertion entry in two level vector) or leafNode address of Art
      We need lock to delete entry from EdgeHal

      Need to think on deletion part

      */

    bool flag = false;
    bool isDegreeAdd = true;
    // vertex_t divSrcVertexId = getIndrVertexArrayIndex(inputedge_info.getSrcVertexId());
    // vertex_t SrcVertexId = getVertexArrayIndex(inputedge_info.getSrcVertexId(), divSrcVertexId);

    //aquire_vertex_lock(inputedge_info.getSrcVertexId()); //.lock();


    // hash table bucket deletion part


    //vertex_t src  = 1082;
    //vertex_t dest = 3244;
    //std::cout<<"start deletion:"<<phy_src_id<<" "<<phy_dest_id<<std::endl;
    AdjacentPos ap;
    ap.edgeBlockIndex = NOTFOUNDKEY;
    ap.indrIndex = NOTFOUNDKEY;
    ap.hashTableIndex = NOTFOUNDKEY;
    AdjacentPos *ap_pos;


    // garbage collection call
    edgeBlockGC(phy_src_id, 0, me, thread_id);

    // 4394 dest vertex 600912


    uint64_t *pos_ooo = 0;
    /// now check based on edge block indr index
    if(index[phy_src_id].hashPtr == NULL)
    {
        index[phy_src_id].initHashTable(me, thread_id);
        ap_pos = index[phy_src_id].getDestNodeHashTableVal(phy_dest_id, &ap, pos_ooo);
    }
    else
    {
        ap_pos = index[phy_src_id].getDestNodeHashTableVal(phy_dest_id, &ap, pos_ooo);
    }

    if(!check_version(ap_pos->edgeBlockIndex))
    {
        // upi^0
        if (ap_pos->edgeBlockIndex != NOTFOUNDKEY)
        {
            if (!check_version_read(ap_pos->edgeBlockIndex)) // most significant bit shows either its in-order or out-of-order update
            {
                inOrderDeletion(commit_time, phy_src_id, phy_dest_id, ap_pos, inputedge_info, me, thread_id, ap_pos);
            }
            else
            {
                // out-of-order deletion part
                outOfOrderDeletion(phy_src_id, phy_dest_id, ap_pos, pos_ooo, inputedge_info, me, thread_id, ap_pos->edgeBlockIndex);
            }
            index[phy_src_id].removeDestNodeHashTableVal(phy_dest_id, ap_pos->hashTableIndex,source_timestamp_deletion, me, thread_id);
        }
        else
        {
            // std::cout<<"ap_pos->edgeBlockIndex != NOTFOUNDKEY"<<std::endl;
            // todo add if in case no duplicate part support
            if(IsDuplicateSupport)
            {
                index[phy_src_id].setDestNodeHashTableVal(add_version(phy_dest_id), source_timestamp_deletion, me,thread_id);
            }
            isDegreeAdd = false;
        }
    }
    else
    {
        // upi^1
        if(check_version_read(ap_pos->edgeBlockIndex))
        {
            // DVE *cur = reinterpret_cast<DVE *>(remove_version(remove_version_read(ap_pos->edgeBlockIndex)));
            //std::cout<<"check_version_read(ap_pos->edgeBlockIndex)"<<std::endl;
            if(IsDuplicateSupport)
            {
                DestGc *dc = reinterpret_cast<DestGc *>(remove_version(remove_version_read(ap_pos->edgeBlockIndex)));
                DVE *dve_new = static_cast<DVE *>(me->Allocate(sizeof(DVE), thread_id));
                dve_new->Dnode = add_version(phy_dest_id);
                dve_new->l_del = 0;
                dve_new->oldest_version = dc->oldest_version;
                DuplicateUpdate *dup = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate), thread_id));
                dup->SrcTime = add_version(source_timestamp_deletion);
                dup->next = NULL;
                dve_new->next = dup;
                index[phy_src_id].hashPtr->hashtable[ap_pos->hashTableIndex] = add_version(reinterpret_cast<vertex_t>(dve_new));
                me->free(dc, sizeof(DestGc), thread_id);
            }
            isDegreeAdd = false;
        }
        else
        {
            DVE *cur = reinterpret_cast<DVE *>(remove_version(ap_pos->edgeBlockIndex));

            if (source_timestamp_deletion < cur->oldest_version)
            {
                //std::cout<<"hello rollback()"<<std::endl;
                rollback();
            }
            else
            {
                if (source_timestamp_deletion >= cur->l_del)
                {
                    // new deletion > latest deletion occured in the STAL
                    DuplicateUpdate *cur_node = cur->next;
                    if (cur_node == NULL)
                    {
                        // current node is NULL
                        DuplicateUpdate *deleted_update = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate), thread_id));
                        deleted_update->SrcTime = add_version(source_timestamp_deletion);
                        cur->next = deleted_update;
                        isDegreeAdd = false;
                        //std::cout<<"cur_node == NULL"<<std::endl;
                    }
                    else
                    {
                        // dve->next contains duplicate node
                        DuplicateUpdate *prev;
                        while (remove_version(cur_node->SrcTime) <= source_timestamp_deletion)
                        {
                            if (!check_version(cur_node->SrcTime))
                            {
                                //
                                prev = cur_node;
                                cur_node = cur_node->next;
                            }
                            else
                            {
                                cur_node = cur_node->next;
                            }

                            if (cur_node == NULL) {
                                break;
                            }
                        }
                        //
                        if (prev != NULL)
                        {

                            AdjacentPos ap_del;
                            ap_del.edgeBlockIndex = NOTFOUNDKEY;
                            ap_del.indrIndex = NOTFOUNDKEY;
                            ap_del.hashTableIndex = NOTFOUNDKEY;
                            vertex_t *pos_dest_id_del = 0;

                            AdjacentPos *adj_pos_del = index[phy_src_id].getDestBySourceTimeNodeHashTableVal(prev->UpdatePos, &ap_del, pos_dest_id_del);

                            if (!check_version_read(adj_pos_del->edgeBlockIndex)) // most significant bit shows either its in-order or out-of-order update
                            {
                                inOrderDeletion(commit_time, phy_src_id, phy_dest_id, adj_pos_del, inputedge_info, me,thread_id, ap_pos);
                            }
                            else
                            {
                                // out-of-order deletion part
                                outOfOrderDeletion(phy_src_id, phy_dest_id, adj_pos_del, pos_dest_id_del,inputedge_info, me, thread_id, ap_pos->edgeBlockIndex);
                            }

                            // remove from dve the insertion
                            // cur->l_del = source_timestamp_deletion;

                            // remove prev insertion from DVE
                            //if(isReadActive)
                            //  {
                            removeDuplicateNodeBaseOnSourceTimeInDVE(prev, cur, me, thread_id);
                            //  }
                            if(cur->next == NULL)
                            {
                                /////
                                if (!HBALStore::isReadActive)
                                {

                                    DestGc *d_gc = static_cast<DestGc *>(me->Allocate(sizeof(DestGc), thread_id));
                                    d_gc->Dnode = remove_version(cur->Dnode);
                                    d_gc->oldest_version = remove_version(cur->oldest_version);

                                    index[phy_src_id].getHashTable()->hashtable[ap_pos->hashTableIndex] = add_version_read(add_version(reinterpret_cast<vertex_t>(d_gc)));
                                    me->free(cur, sizeof(DVE), thread_id);
                                }
                            }
                        }
                        else
                        {
                            //   std::cout<<"prev == NULL"<<std::endl;
                            //
                            DuplicateUpdate *deleted_update = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate), thread_id));
                            deleted_update->SrcTime = add_version(source_timestamp_deletion);
                            addDuplicateNodeBaseOnSourceTimeInDVE(deleted_update, cur);
                            isDegreeAdd = false;
                            //cur->next = deleted_update;
                        }
                    }
                }
                else
                {
                    // std::cout<<"source_timestamp_deletion < cur->l_del"<<std::endl;
                    // new deletion < latest deletion occured in the STAL
                    vertex_t *dup_stalb = 0;
                    vertex_t *pos_dup = 0;
                    stalb_type *stalb_ln = NULL;
                    int *dup_indr_pos = 0;
                    vertex_t latest_Deletion = source_timestamp_deletion;
                    vertex_t dst_id = phy_dest_id;

                    // binary search by  dve->l_del to find dup and prev reference to new_insertion

                    findDeletionInsertionPosInSTALBBinarySearchByNewDeletion(dup_indr_pos, dup_stalb, pos_dup, phy_src_id, latest_Deletion, dst_id, inputedge_info);

                    // prev_stalb exist
                    if (dup_stalb != 0) {
                        // new insertion either duplicate or deleted
                        EdgeInfoHal *ei_cur;
                        LeafNode *ln_ei;
                        bool flag_ooo = false;
                        bool isDelete = true;
                        vertex_t cur_source_time_itm = 0;
                        stalb_type *dup_s = NULL;
                        /////
                        if (!check_version(*dup_stalb)) {
                            // in-order dup_stalb
                            dup_s = reinterpret_cast<stalb_type *>(remove_version(*dup_stalb));
                            ei_cur = *reinterpret_cast<EdgeInfoHal **>(dup_s + (((((u_int64_t) 1
                                    << *reinterpret_cast<uint8_ptr>(dup_s)) / 2)) * 8) + ((*pos_dup * 8) - 8));
                            cur_source_time_itm = ei_cur->getInvldTime()->getSrcTime();
                        } else {
                            // out-of-order stalb
                            // in-order dup_stalb
                            dup_s = reinterpret_cast<stalb_type *>(remove_version(*dup_stalb));
                            ln_ei = *reinterpret_cast<LeafNode **>(dup_s + (((((u_int64_t) 1
                                    << *reinterpret_cast<uint8_ptr>(dup_s)) / 2)) * 8) + ((*pos_dup * 8) - 8));

                            cur_source_time_itm = ln_ei->getInvalEntry()->getSrcTime();
                            flag_ooo = true;
                        }
                        if (!flag_ooo) {
                            if (ei_cur->getInvldTime() == NULL) {
                                //
                                ITM *itm = ei_cur->getInvldTime();
                                createITMBlockAndUpdated(itm, source_timestamp_deletion, thread_id, me);
                                ei_cur->setInvldTime(itm);
                                removeBaseOnSourceTimeInDVE(ei_cur->getSTime(), cur, me, thread_id);
                            } else {
                                //
                                ITM *itm_cur = ei_cur->getInvldTime();
                                if (!check_version(itm_cur->getSrcTime())) {
                                    // duplicate
                                    createITMBlockAndUpdated(itm_cur, source_timestamp_deletion, thread_id, me);
                                    ei_cur->setInvldTime(itm_cur);
                                    removeBaseOnSourceTimeInDVE(ei_cur->getSTime(), cur, me, thread_id);
                                } else {
                                    // already deleted
                                    if (remove_version(itm_cur->getSrcTime()) > source_timestamp_deletion) {
                                        //
                                        DuplicateUpdate *deleted_update = static_cast<DuplicateUpdate *>(me->Allocate(
                                                sizeof(DuplicateUpdate), thread_id));
                                        deleted_update->SrcTime = add_version(itm_cur->getSrcTime());
                                        addDuplicateNodeBaseOnSourceTimeInDVE(deleted_update, cur);
                                        isDegreeAdd = false;

                                        createITMBlockAndUpdated(itm_cur, source_timestamp_deletion, thread_id, me);
                                        ei_cur->setInvldTime(itm_cur);
                                    } else {
                                        //
                                        DuplicateUpdate *deleted_update = static_cast<DuplicateUpdate *>(me->Allocate(
                                                sizeof(DuplicateUpdate), thread_id));
                                        deleted_update->SrcTime = add_version(source_timestamp_deletion);
                                        addDuplicateNodeBaseOnSourceTimeInDVE(deleted_update, cur);
                                        isDegreeAdd = false;

                                    }
                                }
                            }
                        } else {
                            //
                            if (ln_ei->getInvalEntry() == NULL) {
                                //
                                ITM *itm = ln_ei->getInvalEntry();
                                createITMBlockAndUpdated(itm, source_timestamp_deletion, thread_id, me);
                                ln_ei->setInvldTime(itm);
                                removeBaseOnSourceTimeInDVE(ln_ei->getSTime(), cur, me, thread_id);
                            } else {
                                //
                                ITM *itm_cur = ln_ei->getInvalEntry();
                                if (!check_version(itm_cur->getSrcTime())) {
                                    // duplicate
                                    createITMBlockAndUpdated(itm_cur, source_timestamp_deletion, thread_id, me);
                                    ln_ei->setInvldTime(itm_cur);
                                    removeBaseOnSourceTimeInDVE(ln_ei->getSTime(), cur, me, thread_id);
                                } else {
                                    // already deleted
                                    if (remove_version(itm_cur->getSrcTime()) > source_timestamp_deletion) {
                                        //
                                        DuplicateUpdate *deleted_update = static_cast<DuplicateUpdate *>(me->Allocate(
                                                sizeof(DuplicateUpdate), thread_id));
                                        deleted_update->SrcTime = add_version(itm_cur->getSrcTime());
                                        addDuplicateNodeBaseOnSourceTimeInDVE(deleted_update, cur);
                                        isDegreeAdd = false;

                                        createITMBlockAndUpdated(itm_cur, source_timestamp_deletion, thread_id, me);
                                        ln_ei->setInvldTime(itm_cur);

                                        // removeBaseOnSourceTimeInDVE(ei_cur->getSTime(), cur, me,  thread_id);
                                    } else {
                                        //
                                        DuplicateUpdate *deleted_update = static_cast<DuplicateUpdate *>(me->Allocate(
                                                sizeof(DuplicateUpdate), thread_id));
                                        deleted_update->SrcTime = add_version(source_timestamp_deletion);
                                        addDuplicateNodeBaseOnSourceTimeInDVE(deleted_update, cur);
                                        isDegreeAdd = false;

                                    }
                                }
                            }
                        }
                    } else {
                        // new insertion is duplicate %%%%
                        DuplicateUpdate *deleted_update = static_cast<DuplicateUpdate *>(me->Allocate(
                                sizeof(DuplicateUpdate), thread_id));
                        deleted_update->SrcTime = add_version(source_timestamp_deletion);
                        addDuplicateNodeBaseOnSourceTimeInDVE(deleted_update, cur);
                        isDegreeAdd = false;

                    }
                }
            }
        }

    }

    if(isDegreeAdd)
    {
        degreeDecrement(commit_time, phy_src_id, me, thread_id);
    }
}

bool HBALStore::checkIsDuplicateExist(vertex_t phy_src_id, vertex_t phy_dest_id)
{
    AdjacentPos ap;
    ap.edgeBlockIndex = NOTFOUNDKEY;
    ap.indrIndex = NOTFOUNDKEY;
    ap.hashTableIndex = NOTFOUNDKEY;
    index[phy_src_id].getDestNodeHashTableVal(phy_dest_id, &ap);
    if (ap.edgeBlockIndex == NOTFOUNDKEY)
    {
        return true;
    }
    else
    {
        return false;
    }

}

// degree version maintenance

void HBALStore::addDegreeVersion(timestamp_t  commit_time, vertex_t phy_src_id, vertex_t phy_dest_id, MemoryAllocator *me, int thread_id)
{
    bool flagc = false;
    if (check_version(index[phy_src_id].degree))
    {
        // DegreeNode
        DegreeNode *cur_degree = reinterpret_cast<DegreeNode *>(remove_version(index[phy_src_id].degree));
        if (getMinReadDegreeVersion() > cur_degree->wts )
        {

            /// %%%%%%%%%%
            InvalidDegree *new_invl_node = static_cast<InvalidDegree *>(malloc(sizeof(InvalidDegree)));//static_cast<InvalidDegree *>(me->Allocate(sizeof(InvalidDegree), thread_id)); // InvalidDegree();

            index[phy_src_id].degree = remove_version(cur_degree->degree) + 1;

            new_invl_node->ptr = cur_degree;

            this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.lock();
            if (this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr == NULL)
            {
                this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr = new_invl_node;
                new_invl_node->next = NULL;
            }
            else
            {
                new_invl_node->next = static_cast<InvalidDegree *>(this->invalidListDegree[phy_src_id %
                                                                                           THRESHOLDINVALID]->ptr);
                this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->ptr = new_invl_node;
            }
            this->invalidListDegree[phy_src_id % THRESHOLDINVALID]->lock.unlock();

            flagc = true; // in case writer put node to InvalidListDegree
        }
        else
        {

            DegreeNode *new_degree;

            new_degree = (DegreeNode *) (me->Allocate(sizeof(DegreeNode), thread_id));

            new_degree->degree = remove_version(cur_degree->degree) + 1;

            new_degree->next = cur_degree;

            index[phy_src_id].degree = add_version(reinterpret_cast<vertex_t>(new_degree));
            new_degree->wts = commit_time;
        }

    }
    else
    {
        //
        if (getMinReadDegreeVersion() == MaxValue)
        {
            index[phy_src_id].degree = remove_version(index[phy_src_id].degree) + 1;
        }
        else
        {

            DegreeNode *cur_degree;
            DegreeNode *new_degree;
            cur_degree = (DegreeNode *) (me->Allocate(sizeof(DegreeNode), thread_id));
            new_degree = (DegreeNode *) (me->Allocate(sizeof(DegreeNode), thread_id));
            cur_degree->degree = remove_version(index[phy_src_id].degree);
            cur_degree->wts = 0;
            cur_degree->next = NULL;

            new_degree->degree = remove_version(index[phy_src_id].degree) + 1;
            new_degree->next = cur_degree;

            index[phy_src_id].degree = add_version(reinterpret_cast<vertex_t>(new_degree));
            new_degree->wts = commit_time;

            // timestamp_t  wts_t = getMinReadVersion();
        }
    }


    ///%%%%%%%%% end degree %%%%%%%%%
    if (check_version(index[phy_src_id].degree))
    {
        flagc = true;
    }
    if (flagc)
    {
        gcDegreeList(phy_src_id, me, thread_id);
    }
}

EdgeInfoHal* HBALStore::getOutOfOrderFieldFromSTAL(vertex_t phy_src_id,timestamp_t key, MemoryAllocator *me, int thread_id)
{
    //timestamp_t key = inputedge_info.getUpdtSrcTime();

    if (!(index[phy_src_id].edgePtr & 0x1))
    {
        PerSourceIndrPointer *ptr_per_src = static_cast<PerSourceIndrPointer *>(index[phy_src_id].getEdgePtr());

        int startPos = ptr_per_src->getCurPos();
        int endPos = ((u_int64_t) 1 << ptr_per_src->getBlockLen());
        int indexIndrArray = findIndexForIndr((ptr_per_src->getperVertexBlockArr()), startPos, endPos, key);
        ///
        u_int64_t eb_block_size = (u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray]);
        EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] +((eb_block_size / 2) * 8) +((((u_int16_t) *reinterpret_cast<uint16_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] +1)) * 8) - 8));

        if (eih_cur->getSTime() < key)
        {
            if (indexIndrArray != 0)
            {
                indexIndrArray--;
            }
        }

        //fixed size block binary search
        int indexFixedBlock = findIndexForBlockArr(ptr_per_src->getperVertexBlockArr()[indexIndrArray],((u_int16_t) *reinterpret_cast<uint16_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + 1)),(eb_block_size / 2), key);

        /// eb block
        EdgeInfoHal *eih_index = *reinterpret_cast<EdgeInfoHal **>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] +
                                                                   ((eb_block_size / 2) * 8) +
                                                                   ((indexFixedBlock * 8) - 8));

        if (eih_index->getSTime() < key)
        {
            indexFixedBlock--;
        }

        EdgeInfoHal *setOutOfOrder = *reinterpret_cast<EdgeInfoHal **>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + ((eb_block_size / 2) * 8) +((indexFixedBlock * 8) - 8));
    }
    else
    {
        stalb_type *eb = static_cast<stalb_type *>(index[phy_src_id].getEdgePtr());
        u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

        int indexFixedBlock = findIndexForBlockArr(eb, ((u_int16_t) *reinterpret_cast<uint16_ptr>(eb + 1)),(eb_block_size / 2), key);

        /// eb block
        EdgeInfoHal *eih_index = *reinterpret_cast<EdgeInfoHal **>(eb + ((eb_block_size / 2) * 8) +
                                                                   ((indexFixedBlock * 8) - 8));

        if (eih_index->getSTime() < key)
        {
            indexFixedBlock--;
        }

        EdgeInfoHal *setOutOfOrder = *reinterpret_cast<EdgeInfoHal **>(eb + ((eb_block_size / 2) * 8) + ((indexFixedBlock * 8) - 8));
    }
}
// in-order insertion

vertex_t HBALStore::inOrderInsertion(timestamp_t commit_time, InputEdge inputedge_info, EdgeInfoHal *ei, vertex_t phy_src_id, vertex_t phy_dest_id, MemoryAllocator *me, int thread_id, bool IsDelete, stalb_type *stalb_cur)
{


    double weight_w         = inputedge_info.getUpdtWeight();
    timestamp_t source_time = getTimestampSrc(inputedge_info, IsOutOfOrder);
    // phy_src_id phy_dest_id

    //(EdgeInfoHal *) (std::aligned_alloc(PAGE_SIZE,sizeof(EdgeInfoHal))); //new EdgeInfoHal();
    ei->initEdgeInfoHal();
    ei->setSTime(source_time);
    ei->setOutOfOrderUpdt(NULL);


    u_int64_t dest_prefix = (u_int64_t) phy_dest_id & thread_prefix;

    u_int64_t adjcent_pos_store = 0;


    if (index[phy_src_id].getHashTable() == NULL || index[phy_src_id].edgePtr == 0)
    {
        // testing hash table



        // return edge block pointer after creation
        stalb_type *stalb = static_cast<stalb_type *>(me->Allocate(32, thread_id));

        double *property = static_cast<double *>(me->Allocate(16, thread_id));


        memset(stalb, 0, 32);
        memset(property, 0, 16);

        *reinterpret_cast<uint8_ptr>(stalb) = 2; // block size

        *reinterpret_cast<uint16_ptr>(stalb + 1) = 2; // cur_index

        // insertion steps

        *reinterpret_cast<uint64_ptr>(stalb + 8) = remove_version(phy_dest_id); // dest node

        *reinterpret_cast<EdgeInfoHal **>(stalb + 16) = ei; // edge info hal insertion

        property[1] = weight_w;

        *reinterpret_cast<double **>(stalb + 24) = property; // property insertion

        uint16_t *cur_ind = reinterpret_cast<uint16_ptr>(stalb + 1);

        *cur_ind = 1; // update cur index

        if(index[phy_src_id].getHashTable() == NULL)
        {
            index[phy_src_id].initHashTable(me, thread_id);
        }

        u_int64_t indr_index_bits_val = 0;
        u_int64_t indr_size_bits_val = 0;
        u_int64_t edge_block_index_bits_val = *cur_ind;
        u_int64_t edge_block_size_bits_val = 2;
        //u_int64_t adjcent_pos_store = 0;


        // for indr array 0,32 for indr size 32 to 5, for edgeblock index 37 to 11 bit, for edge block size 48 to 4, for prefix 52 to 62
        // for either pointer for edge per source is edgeblock or STAL 62 to 1 and for ooo 63 to 1

        adjcent_pos_store = setBitMask(adjcent_pos_store, indr_index_bits_val, L1IndexStart,L1IndexBits);
        adjcent_pos_store = setBitMask(adjcent_pos_store, indr_size_bits_val, L1SizeStart,
                                       L1SizeBits);
        adjcent_pos_store = setBitMask(adjcent_pos_store, edge_block_index_bits_val,
                                       L2IndexStart, L2IndexBits);
        adjcent_pos_store = setBitMask(adjcent_pos_store, edge_block_size_bits_val,
                                       L2SizeStart, L2SizeBits);
        adjcent_pos_store = setBitMask(adjcent_pos_store, dest_prefix, DNPStart, DNPBits);
        // 0 means edge block 1 means STAL
        // adjcent_pos_store = setBitMask(adjcent_pos_store, 0, IsSTALStart, IsSTALBits);
        // for ooo update 1 means no ooo 0 means ooo
        adjcent_pos_store = setBitMask(adjcent_pos_store, 0, IsInOrderStart, IsInOrderBits);


        perThreadNumberOfvertices[thread_id]++;
        index[phy_src_id].indirectionLock.lock();
        index[phy_src_id].setEdgePtr(reinterpret_cast<vertex_t>(stalb), true);
        index[phy_src_id].indirectionLock.unlock();

        // index[phy_src_id].incrDegree();

        ei->setWTsEdge(commit_time);

        /*   for indrArrayIndex: pos 0 and bitsNeeded 32
           for indrArraySize: pos 32 and bitsNeeded 5
           for blockIndex: pos 37 and bitsNeeded 12
           for blockSize: pos 49 and bitsNeeded 4
           for prefix dest_node: pos 53 and bitsNeeded 10
           for updateFlage(in order,ooo) pos 63 and bitNeeded 1
          */

        stalb_cur = stalb;


    }
    else
    {
        u_int64_t indr_index_bits_val = 0;
        u_int64_t indr_size_bits_val = 0;
        u_int64_t edge_block_index_bits_val = 0;
        u_int64_t edge_block_size_bits_val = 0;
        u_int64_t isSTAL = 0;

        // %%%%%% start



        // %%%%% end

        //if ((remove_version(index[phy_src_id].getDegree()) == (((uint32_t) 1 << StartFixedBlockThreshold) - 1)) )

        if (index[phy_src_id].edgePtr & 0x1) {

            stalb_type *ebCur = static_cast<stalb_type *>(index[phy_src_id].getEdgePtr());
            if (((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(ebCur)) == BLOCKSIZEFIXED &&(*reinterpret_cast<uint16_ptr>(ebCur + 1)) == 1)
            {
                // newly inserted STAL block as the StartFixedBlockThreshold condition meet
                PerSourceIndrPointer *per_source_stal = (PerSourceIndrPointer *) (me->Allocate(sizeof(PerSourceIndrPointer), thread_id));
                per_source_stal->initVal();
                per_source_stal->setIndexList(me, thread_id);

                per_source_stal->addNewBlock(static_cast<stalb_type *>(index[phy_src_id].getEdgePtr()));

                // create new edge block and insert value // init stalb

                stalb_type *new_stalb = static_cast<stalb_type *>(me->Allocate(32, thread_id));

                double *property = static_cast<double *>(me->Allocate(16, thread_id));


                memset(new_stalb, 0, 32);
                memset(property, 0, 16);

                //  std::memcpy(stalb,&blockSize, sizeof())
                *reinterpret_cast<uint8_ptr>(new_stalb) = 2; // block size

                *reinterpret_cast<uint16_ptr>(new_stalb + 1) = 2; // cur_index

                // insertion steps

                *reinterpret_cast<uint64_ptr>(new_stalb + 8) = remove_version(phy_dest_id); // dest node

                *reinterpret_cast<EdgeInfoHal **>(new_stalb + 16) = ei; // edge info hal insertion

                property[1] = weight_w;

                *reinterpret_cast<double **>(new_stalb + 24) = property; // property insertion

                uint16_t *cur_ind = reinterpret_cast<uint16_ptr>(new_stalb + 1);

                *cur_ind = 1; // update cur index


                per_source_stal->addNewBlock(new_stalb);

                // hash table related entries

                indr_index_bits_val = per_source_stal->getCurPos();
                indr_size_bits_val = per_source_stal->getBlockLen();

                edge_block_index_bits_val = *cur_ind;
                edge_block_size_bits_val = 2;

                isSTAL = 1;

                // replace edge block pointer with STAL in persource vertex array
                index[phy_src_id].indirectionLock.lock();

                index[phy_src_id].setEdgePtr(reinterpret_cast<vertex_t>(per_source_stal), false);

                index[phy_src_id].indirectionLock.unlock();

                ei->setWTsEdge(commit_time);

                stalb_cur = new_stalb;

            } else {
                // insert in the edge block

                uint16_t *curPos = reinterpret_cast<uint16_ptr>(ebCur +
                                                                1);  //ebCur->getCurIndex(); // *(ebCur + 1);
                if (*curPos > 1) {
                    *reinterpret_cast<uint64_ptr>((ebCur) + ((*curPos - 1) * 8)) = remove_version(
                            phy_dest_id); // dst id

                    *reinterpret_cast<EdgeInfoHal **>((ebCur) + ((((uint64_t) 1
                            << *reinterpret_cast<uint8_ptr>(ebCur)) / 2) * 8) +
                                                      (((*curPos - 1) * 8) -
                                                       8)) = ei; // edgeinfohal

                    double *property = *reinterpret_cast<double **>( ebCur + ((((uint64_t) 1<< *reinterpret_cast<uint8_ptr>(ebCur)) * 8) - 8)); // blocksize

                    property[(*curPos - 1)] = weight_w; // weight

                    *curPos = *curPos - 1;


                    ei->setWTsEdge(commit_time);
                    indr_index_bits_val = 0; //srcVertex->getCurPos();
                    indr_size_bits_val = 0; //srcVertex->getBlockLen();
                    edge_block_index_bits_val = *curPos;
                    edge_block_size_bits_val = *reinterpret_cast<uint8_ptr>(ebCur); // get block size
                    stalb_cur = ebCur;

                    //  index[phy_src_id].incrDegree();
                    //
                } else {
                    // allocate new stal block
                    index[phy_src_id].indirectionLock.lock();

                    UPBlockSizePointer *newblock_pointer = UpEdgeBlock(ebCur,
                                                                       *reinterpret_cast<uint8_ptr>(ebCur), me,
                                                                       thread_id);

                    stalb_type *new_ebCur = newblock_pointer->new_stalb;

                    index[phy_src_id].setEdgePtr(reinterpret_cast<vertex_t>(new_ebCur), true);

                    me->free(newblock_pointer->prev_stalb,
                             newblock_pointer->prev_block_size * 8,
                             thread_id); // free previous block after new block update

                    me->free(newblock_pointer->prev_property_arr,
                             (newblock_pointer->prev_block_size / 2) * 8,
                             thread_id); // free property array

                    // ebCur->UpEdgeBlock(me, thread_id, ebCur->getBlockSize());
                    uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(new_ebCur + 1);


                    // insert edge block update

                    *reinterpret_cast<uint64_ptr>((new_ebCur) + ((*new_curPos - 1) *
                                                                 8)) = remove_version(
                            phy_dest_id); // dst id

                    *reinterpret_cast<EdgeInfoHal **>((new_ebCur) + ((((uint64_t) 1
                            << *reinterpret_cast<uint8_ptr>(new_ebCur)) / 2) * 8) +
                                                      (((*new_curPos - 1) * 8) -
                                                       8)) = ei; // edgeinfohal

                    double *property = *reinterpret_cast<double **>( new_ebCur +
                                                                     ((((uint64_t) 1
                                                                             << *reinterpret_cast<uint8_ptr>(new_ebCur)) *
                                                                       8) -
                                                                      8)); // blocksize

                    property[(*new_curPos - 1)] = weight_w; // weight

                    *new_curPos = *new_curPos - 1;


                    //  ebCur->setEdgeUpdt(ei, inputedge_info.getDestVertexId(), weight_w);

                    indr_index_bits_val = 0; //srcVertex->getCurPos();
                    indr_size_bits_val = 0; //srcVertex->getBlockLen();
                    edge_block_index_bits_val = *new_curPos;
                    edge_block_size_bits_val = (*reinterpret_cast<uint8_ptr>(new_ebCur));

                    stalb_cur = new_ebCur;

                    index[phy_src_id].indirectionLock.unlock();

                    ei->setWTsEdge(commit_time);

                    //  index[phy_src_id].incrDegree();
                }


            } /// %%%


        } else {


            // STAL exist and we need to insert edge entry there
            PerSourceIndrPointer *ptr_per_src = static_cast<PerSourceIndrPointer *>(index[phy_src_id].getEdgePtr());

            int indr_arr_cur_pos = ptr_per_src->getCurPos();


            stalb_type *ebCur = ptr_per_src->getperVertexBlockArr()[indr_arr_cur_pos];

            uint16_t *curPos = reinterpret_cast<uint16_ptr>(ebCur + 1);//ebCur->getCurIndex();


            if (*curPos > 1) {

                *reinterpret_cast<uint64_ptr>((ebCur) + ((*curPos - 1) * 8)) = remove_version(
                        phy_dest_id); // dst id

                *reinterpret_cast<EdgeInfoHal **>((ebCur) + ((((uint64_t) 1
                        << *reinterpret_cast<uint8_ptr>(ebCur)) / 2) * 8) +
                                                  (((*curPos - 1) * 8) -
                                                   8)) = ei; // edgeinfohal

                double *property = *reinterpret_cast<double **>( ebCur + ((((uint64_t) 1
                        << *reinterpret_cast<uint8_ptr>(ebCur)) * 8) - 8)); // blocksize

                property[(*curPos - 1)] = weight_w; // weight

                *curPos = *curPos - 1;


                indr_index_bits_val = ptr_per_src->getCurPos();
                indr_size_bits_val = ptr_per_src->getBlockLen();
                edge_block_index_bits_val = *curPos;
                edge_block_size_bits_val = *reinterpret_cast<uint8_ptr>(ebCur);
                isSTAL = 1;
                stalb_cur = ebCur;

                // index[phy_src_id].incrDegree();
                ei->setWTsEdge(commit_time);

            } else {
                if (indr_arr_cur_pos == 0) {

                    // if block is full than create new block and commit previous block
                    stalb_type *ebNew;
                    //      2^*StartFixedBlockThreshold 32, 64, 128, 256, 512, 1024, 2048
                    // srcVertex->blockLen - srcVertex->blockLen - 0
                    // if blockLen *  StartFixedBlockThreshold < endFixedBlockThreshold then checkThreshold is blockLen *  StartFixedBlockThreshold
                    // else checkThreshold = endFixedBlockThreshold
                    // block size 8
                    // 8 * 32 = 256
                    // 32 64 128 256 512 1024 2048
                    // compute max threashold block size


                    uint64_t checkThreshold = EndFixedBlockThreshold;

                    if ((*reinterpret_cast<uint8_ptr>(ebCur) - 1) < checkThreshold)
                    {
                        // allocate new stal block

                        index[phy_src_id].indirectionLock.lock();

                        UPBlockSizePointer *newblock_pointer = UpEdgeBlock(ebCur,
                                                                           *reinterpret_cast<uint8_ptr>(ebCur),
                                                                           me,
                                                                           thread_id);

                        stalb_type *new_ebCur = newblock_pointer->new_stalb;

                        ptr_per_src->getperVertexBlockArr()[indr_arr_cur_pos] = new_ebCur;

                        me->free(newblock_pointer->prev_stalb, newblock_pointer->prev_block_size * 8,
                                 thread_id); // free previous block after new block update

                        me->free(newblock_pointer->prev_property_arr,
                                 (newblock_pointer->prev_block_size / 2) * 8,
                                 thread_id); // free property array

                        // ebCur->UpEdgeBlock(me, thread_id, ebCur->getBlockSize());
                        uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(new_ebCur + 1);

                        // insert edge block update

                        *reinterpret_cast<uint64_ptr>((new_ebCur) + ((*new_curPos - 1) * 8)) = remove_version(
                                phy_dest_id); // dst id

                        *reinterpret_cast<EdgeInfoHal **>((new_ebCur) + ((((uint64_t) 1
                                << *reinterpret_cast<uint8_ptr>(new_ebCur)) / 2) * 8) +
                                                          (((*new_curPos - 1) * 8) -
                                                           8)) = ei; // edgeinfohal

                        double *property = *reinterpret_cast<double **>( new_ebCur +
                                                                         ((((uint64_t) 1
                                                                                 << *reinterpret_cast<uint8_ptr>(new_ebCur)) *
                                                                           8) -
                                                                          8)); // blocksize

                        property[(*new_curPos - 1)] = weight_w; // weight

                        *new_curPos = *new_curPos - 1;

                        stalb_cur = new_ebCur;

                        indr_index_bits_val = ptr_per_src->getCurPos();
                        indr_size_bits_val = ptr_per_src->getBlockLen();
                        edge_block_index_bits_val = *new_curPos;
                        edge_block_size_bits_val = *reinterpret_cast<uint8_ptr>(new_ebCur);


                        isSTAL = 1;
                        index[phy_src_id].indirectionLock.unlock();

                        //  index[phy_src_id].incrDegree();
                        ei->setWTsEdge(commit_time);

                    } else {

                        // init stalb

                        index[phy_src_id].indirectionLock.lock();

                        stalb_type *new_stalb = static_cast<stalb_type *>(me->Allocate(
                                32, thread_id));

                        double *property = static_cast<double *>(me->Allocate(16,
                                                                              thread_id));


                        memset(new_stalb, 0, 32);
                        memset(property, 0, 16);

                        //  std::memcpy(stalb,&blockSize, sizeof())
                        *reinterpret_cast<uint8_ptr>(new_stalb) = 2; // block size log2

                        *reinterpret_cast<uint16_ptr>(new_stalb + 1) = 2; // cur_index

                        stalb_cur = new_stalb;

                        // insertion steps

                        *reinterpret_cast<uint64_ptr>(new_stalb +
                                                      8) = remove_version(phy_dest_id); // dest node

                        *reinterpret_cast<EdgeInfoHal **>(new_stalb +
                                                          16) = ei; // edge info hal insertion

                        property[1] = weight_w;

                        *reinterpret_cast<double **>(new_stalb +
                                                     24) = property; // property insertion

                        uint16_t *cur_ind = reinterpret_cast<uint16_ptr>(new_stalb + 1);

                        *cur_ind = 1; // update cur index


                        //  ebNew = (EdgeBlock *) ((me->Allocate(sizeof(EdgeBlock), thread_id)));
                        //   ebNew->initEdgeBlock(me, thread_id, 1);
                        //   ebNew->setEdgeUpdt(ei, inputedge_info.getDestVertexId(), weight_w);



                        ptr_per_src->setIndexList(me, thread_id);

                        ptr_per_src->addNewBlock(new_stalb); // add in STAL

                        indr_index_bits_val = ptr_per_src->getCurPos();
                        indr_size_bits_val = ptr_per_src->getBlockLen();
                        edge_block_index_bits_val = *cur_ind;
                        edge_block_size_bits_val = 2;
                        stalb_cur = new_stalb;
                        // index[phy_src_id].incrDegree();
                        isSTAL = 1;
                        index[phy_src_id].indirectionLock.unlock();

                        ei->setWTsEdge(commit_time);

                    }

                } else {


                    // blocklen - curpos = 4 and cur 2
                    // 4-2 = 2 = 3 - 2 = 1<<(1)
                    // (blocklen-1) - cur_index = 7 - 3 = (1<<4 *  StartFixedBlockThreshold
                    // 8-3 = 5
                    // 7 6 5 4 3

                    // 2^2 * 2^5
                    //

                    // compute max threashold block size

                    uint64_t checkThreshold = EndFixedBlockThreshold;

                    //

                    if ((*reinterpret_cast<uint8_ptr>(ebCur) - 1) < checkThreshold) {
                        // allocate new stal block
                        index[phy_src_id].indirectionLock.lock();

                        UPBlockSizePointer *newblock_pointer = UpEdgeBlock(ebCur,
                                                                           *reinterpret_cast<uint8_ptr>(ebCur),
                                                                           me,
                                                                           thread_id);

                        stalb_type *new_ebCur = newblock_pointer->new_stalb;

                        ptr_per_src->getperVertexBlockArr()[indr_arr_cur_pos] = new_ebCur;

                        me->free(newblock_pointer->prev_stalb,
                                 newblock_pointer->prev_block_size * 8,
                                 thread_id); // free previous block after new block update

                        me->free(newblock_pointer->prev_property_arr,
                                 (newblock_pointer->prev_block_size / 2) * 8,
                                 thread_id); // free property array

                        // ebCur->UpEdgeBlock(me, thread_id, ebCur->getBlockSize());
                        uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(new_ebCur +
                                                                            1);

                        // insert edge block update

                        *reinterpret_cast<uint64_ptr>((new_ebCur) + ((*new_curPos - 1) *
                                                                     8)) = phy_dest_id; // dst id

                        *reinterpret_cast<EdgeInfoHal **>((new_ebCur) + ((((uint64_t) 1
                                << *reinterpret_cast<uint8_ptr>(new_ebCur)) / 2) * 8) +
                                                          (((*new_curPos - 1) * 8) -
                                                           8)) = ei; // edgeinfohal

                        //  double *property = *reinterpret_cast<double **>( new_ebCur +
                        double *property = *reinterpret_cast<double **>( new_ebCur +
                                                                         ((((uint64_t) 1
                                                                                 << *reinterpret_cast<uint8_ptr>(new_ebCur)) *
                                                                           8) -
                                                                          8)); // blocksize

                        property[(*new_curPos - 1)] = weight_w; // weight

                        *new_curPos = *new_curPos - 1;

                        stalb_cur = new_ebCur;

                        // ebCur->UpEdgeBlock(me, thread_id, ebCur->getBlockSize());
                        //  ebCur->setEdgeUpdt(ei, inputedge_info.getDestVertexId(), weight_w);


                        indr_index_bits_val = ptr_per_src->getCurPos();
                        indr_size_bits_val = ptr_per_src->getBlockLen();
                        edge_block_index_bits_val = *new_curPos;
                        edge_block_size_bits_val = *reinterpret_cast<uint8_ptr>(new_ebCur);

                        //  index[phy_src_id].incrDegree();
                        isSTAL = 1;
                        index[phy_src_id].indirectionLock.unlock();

                        ei->setWTsEdge(commit_time);
                    } else {
                        // if block is full than create new block and commit previous block
                        index[phy_src_id].indirectionLock.lock();

                        // init stalb
                        stalb_type *new_stalb = static_cast<stalb_type *>(me->Allocate(32, thread_id));

                        double *property = static_cast<double *>(me->Allocate(16, thread_id));


                        memset(new_stalb, 0, 32);
                        memset(property, 0, 16);

                        //  std::memcpy(stalb,&blockSize, sizeof())
                        *reinterpret_cast<uint8_ptr>(new_stalb) = 2; // block size log2

                        *reinterpret_cast<uint16_ptr>(new_stalb + 1) = 2; // cur_index


                        // insertion steps

                        *reinterpret_cast<uint64_ptr>(new_stalb + 8) = remove_version(phy_dest_id); // dest node

                        *reinterpret_cast<EdgeInfoHal **>(new_stalb + 16) = ei; // edge info hal insertion

                        property[1] = weight_w;

                        *reinterpret_cast<double **>(new_stalb + 24) = property; // property insertion

                        uint16_t *cur_ind = reinterpret_cast<uint16_ptr>(new_stalb + 1);

                        *cur_ind = 1; // update cur index
                        ptr_per_src->addNewBlock(new_stalb);
                        index[phy_src_id].indirectionLock.unlock();

                        indr_index_bits_val = ptr_per_src->getCurPos();
                        indr_size_bits_val = ptr_per_src->getBlockLen();
                        edge_block_index_bits_val = *cur_ind;
                        edge_block_size_bits_val = 2;
                        isSTAL = 1;
                        stalb_cur = new_stalb;

                        //  index[phy_src_id].incrDegree();

                        ei->setWTsEdge(commit_time);
                    }

                }
            }

        }

        adjcent_pos_store = setBitMask(adjcent_pos_store, indr_index_bits_val, L1IndexStart, L1IndexBits);
        adjcent_pos_store = setBitMask(adjcent_pos_store, indr_size_bits_val, L1SizeStart, L1SizeBits);
        adjcent_pos_store = setBitMask(adjcent_pos_store, edge_block_index_bits_val, L2IndexStart, L2IndexBits);
        adjcent_pos_store = setBitMask(adjcent_pos_store, edge_block_size_bits_val, L2SizeStart, L2SizeBits);
        adjcent_pos_store = setBitMask(adjcent_pos_store, dest_prefix, DNPStart, DNPBits);
        adjcent_pos_store = setBitMask(adjcent_pos_store, 0, IsInOrderStart, IsInOrderBits);
        //adjcent_pos_store = setBitMask(adjcent_pos_store, 0, IsSTALStart, IsSTALBits);

    }

    //std::cout<<"adjacenct pos: "<<adjcent_pos_store<<std::endl;
    return adjcent_pos_store;
}
vertex_t HBALStore::outOfOrderInsertion(timestamp_t commit_time, InputEdge inputedge_info, LeafNode *ln , vertex_t phy_src_id, vertex_t phy_dest_id, MemoryAllocator *me, int thread_id, bool IsDelete, stalb_type *stalb_ln)
{
    timestamp_t source_timestamp = getTimestampSrc(inputedge_info, IsOutOfOrder);
    ln->intLeafNode();
    ln->setSTime(source_timestamp);
    double weight_w = inputedge_info.getUpdtWeight();

    // %%%%%%%%%%%% hash table end out-of-order update
    timestamp_t key = source_timestamp;


    if (!(index[phy_src_id].edgePtr & 0x1))
    {
        PerSourceIndrPointer *ptr_per_src = static_cast<PerSourceIndrPointer *>(index[phy_src_id].getEdgePtr());

        int startPos = ptr_per_src->getCurPos();
        int endPos = ((u_int64_t) 1 << ptr_per_src->getBlockLen());
        int indexIndrArray = findIndexForIndr((ptr_per_src->getperVertexBlockArr()), startPos, endPos, key);
        ///
        u_int64_t eb_block_size = (u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray]);
        EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + ((eb_block_size / 2) * 8) +((((u_int16_t) *reinterpret_cast<uint16_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + 1)) * 8) - 8));

        if (eih_cur->getSTime() < key)
        {
            if (indexIndrArray != 0)
            {
                indexIndrArray--;
            }
        }

        //fixed size block binary search
        int indexFixedBlock = findIndexForBlockArr(ptr_per_src->getperVertexBlockArr()[indexIndrArray],((u_int16_t) *reinterpret_cast<uint16_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] +1)), (eb_block_size / 2), key);

        /// eb block
        EdgeInfoHal *eih_index = *reinterpret_cast<EdgeInfoHal **>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + ((eb_block_size / 2) * 8) +((indexFixedBlock * 8) - 8));

        if (eih_index->getSTime() < key)
        {
            indexFixedBlock--;
        }

        EdgeInfoHal *setOutOfOrder = *reinterpret_cast<EdgeInfoHal **>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + ((eb_block_size / 2) * 8) +((indexFixedBlock * 8) - 8));
        // EdgeInfoHal *setOutOfOrder = *reinterpret_cast<EdgeInfoHal *>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] +((eb_block_size / 2) * 8) + ((indexFixedBlock * 8) - 8));
        if (setOutOfOrder->getOutOfOrderUpdt() == 0)
        {
            // use art syncornized version for correctness
            stalb_type *stalb = static_cast<stalb_type *>(me->Allocate(32, thread_id));

            double *property = static_cast<double *>(me->Allocate(16, thread_id));

            memset(stalb, 0, 32);
            memset(property, 0, 16);

            *reinterpret_cast<uint8_ptr>(stalb) = 2; // block size

            *reinterpret_cast<uint16_ptr>(stalb + 1) = 2; // cur_index

            // insertion steps
            *reinterpret_cast<uint64_ptr>(stalb + 8) = add_version_ooo(phy_dest_id); // dest node

            /// %%%%%%%% update if consider new ln as duplicate
            if(IsDelete)
            {
                *reinterpret_cast<uint64_ptr>(stalb + 8) = add_version(phy_dest_id);
                if (!HBALStore::isReadActive)
                {
                    *reinterpret_cast<uint64_ptr>(stalb + 8) = add_version_read(phy_dest_id); /// no read-only queries are active
                }
                stalb_ln = stalb;
            }
            //// %%%%%%%
            *reinterpret_cast<uint64_ptr>(stalb +16) = reinterpret_cast<vertex_t>(ln); // edge info hal insertion

            property[1] = weight_w;

            *reinterpret_cast<double **>(stalb + 24) = property; // property insertion

            uint16_t *cur_ind = reinterpret_cast<uint16_ptr>(stalb + 1);

            *cur_ind = 1; // update cur index

            //%%%%%%%%%%%
            setOutOfOrder->setOutOfOrderUpdt(remove_version(reinterpret_cast<vertex_t>(stalb)));

            if(!HBALStore::isReadActive)
            {
                *reinterpret_cast<uint64_ptr>((ptr_per_src->getperVertexBlockArr()[indexIndrArray]) +((indexFixedBlock) * 8)) = add_version_ooo(*reinterpret_cast<uint64_ptr>((ptr_per_src->getperVertexBlockArr()[indexIndrArray]) +((indexFixedBlock) * 8)));
            }

            ln->setWTsEdge(commit_time);
        }
        else
        {

            //////%%%%%%%%%
            if (!check_version(setOutOfOrder->getOutOfOrderUpdt()))
            {
                stalb_type *per_stalb = reinterpret_cast<stalb_type *>(setOutOfOrder->getOutOfOrderUpdt());

                if (*reinterpret_cast<uint16_ptr>(per_stalb + 1) == 1)
                {
                    // first resize stalb
                    // secondly swap for sorting

                    if (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(per_stalb)) < OOOBLOCKSIZE)
                    {
                        UPBlockSizePointer *newblock_pointer = UpEdgeBlock(per_stalb,*reinterpret_cast<uint8_ptr>(per_stalb), me, thread_id);

                        stalb_type *new_ebCur = newblock_pointer->new_stalb;

                        // index[phy_src_id].setEdgePtr(reinterpret_cast<vertex_t>(new_ebCur),true);

                        me->free(newblock_pointer->prev_stalb, newblock_pointer->prev_block_size * 8,thread_id); // free previous block after new block update

                        me->free(newblock_pointer->prev_property_arr,(newblock_pointer->prev_block_size / 2) * 8, thread_id); // free property array

                        uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(new_ebCur + 1);

                        u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(new_ebCur);

                        uint64_t pos = findIndexForBlockArrOOO(new_ebCur,
                                                               *reinterpret_cast<uint16_ptr>(new_ebCur + 1),
                                                               (eb_block_size_ooo / 2), key);
                        // if declare as duplicate
                        if(IsDelete)
                        {
                            StoreKeyInBlock(new_ebCur, pos, phy_dest_id, ln, weight_w, true);
                            stalb_ln = new_ebCur;

                        }
                        else
                        {
                            StoreKeyInBlock(new_ebCur, pos, phy_dest_id, ln, weight_w);
                        }
                        if(!HBALStore::isReadActive)
                        {
                            setOutOfOrder->setOutOfOrderUpdt(remove_version(reinterpret_cast<vertex_t>(new_ebCur)));
                        }
                    }
                    else
                    {
                        // split block and connect through adaptive radix tree
                        // step 1
                        uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(per_stalb + 1);

                        u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(per_stalb);

                        // memmove
                        // int src_index = *new_curPos;
                        // int dest_index = *new_curPos - 1;

                        // int pos = //
                        uint64_t pos = findIndexForBlockArrOOO(per_stalb, *new_curPos, (eb_block_size_ooo / 2),
                                                               key);

                        // %%%%%%% split block
                        uint64_t new_block_size = (eb_block_size_ooo / 2);
                        uint64_t split_main_block_index = new_block_size / 2;
                        if (pos >= split_main_block_index)
                        {
                            // move main block indexes that are < pos considering deletion factor as well for counter

                            stalb_type *new_stalb_block = DownEdgeBlock(*reinterpret_cast<uint8_ptr>(per_stalb + 1), me, thread_id);

                            // split block function call  (per_stalb, new_stalb_block, new_block_size, split_main_block_index, per_stalb_cur_index)
                            SplitBlock(per_stalb, new_stalb_block, new_block_size, split_main_block_index,*new_curPos, 1);

                            //%%%%%%% store current key value in there corresponding order position
                            if(IsDelete)
                            {
                                // consider as duplicate
                                StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w, true);
                                stalb_ln = per_stalb;

                            }
                            else
                            {
                                StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w);
                            }
                            //%%%%%%%%%%% end memmove
                            // get the max source-timestamp of both block and consider it as key for ART
                            //
                            ArtPerIndex *perArt = (ArtPerIndex *) (std::aligned_alloc(PAGE_SIZE,
                                                                                      sizeof(ArtPerIndex)));
                            perArt->initArtTree();
                            StoreBlockInArt(per_stalb, new_stalb_block, eb_block_size_ooo, new_block_size,
                                            setOutOfOrder, perArt);

                            if(!HBALStore::isReadActive)
                            {
                                setOutOfOrder->setOutOfOrderUpdt(add_version(reinterpret_cast<vertex_t>(perArt)));
                            }
                            // create ART and store it the value
                        }
                        else
                        {
                            // when pos < half main
                            stalb_type *new_stalb_block = DownEdgeBlock(*reinterpret_cast<uint8_ptr>(per_stalb + 1), me, thread_id);

                            // split block function call  (per_stalb, new_stalb_block, new_block_size, split_main_block_index, per_stalb_cur_index)
                            SplitBlock(per_stalb, new_stalb_block, new_block_size, (split_main_block_index - 1),*new_curPos, 0);

                            //store current key value in there corresponding order position
                            if(IsDelete)
                            {
                                StoreKeyInBlock(new_stalb_block, pos, phy_dest_id, ln, weight_w, true);
                                stalb_ln = per_stalb;
                            }
                            else
                            {
                                StoreKeyInBlock(new_stalb_block, pos, phy_dest_id, ln, weight_w);
                            }
                            // get the max source-timestamp of both block and consider it as key for ART
                            ArtPerIndex *perArt = (ArtPerIndex *) (std::aligned_alloc(PAGE_SIZE,
                                                                                      sizeof(ArtPerIndex)));
                            perArt->initArtTree();
                            StoreBlockInArt(per_stalb, new_stalb_block, eb_block_size_ooo, new_block_size,setOutOfOrder, perArt);
                            if(!HBALStore::isReadActive)
                            {
                                setOutOfOrder->setOutOfOrderUpdt(add_version(reinterpret_cast<vertex_t>(perArt)));
                            }
                        }

                        // %%%%%%%%

                    }

                }
                else {
                    // arrange stalb block
                    uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(per_stalb + 1);

                    u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(per_stalb);

                    uint64_t pos = findIndexForBlockArrOOO(per_stalb,*reinterpret_cast<uint16_ptr>(per_stalb + 1),(eb_block_size_ooo / 2), key);
                    if(IsDelete)
                    {
                        StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w, true);
                        stalb_ln = per_stalb;
                    }
                    else
                    {
                        StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w);
                    }

                }

                ln->setWTsEdge(commit_time);

                ///%%%%%%%%%%
            }
            else
            {
                //%%%%%%%%%% for Adaptive radix tree
                ArtPerIndex *art_per_index = reinterpret_cast<ArtPerIndex *>(remove_version(setOutOfOrder->getOutOfOrderUpdt()));

                art::art<stalb_type *> *art_val = art_per_index->getArtTree();
                //%%%%%%%%%%%% adaptive radix tree scan start
                // ei->outOfOrderUpdt.scanArtTree();
                auto it = art_val->begin(std::to_string(ln->getSTime()).c_str());
                stalb_type *per_stalb = *it;

                if (*reinterpret_cast<uint16_ptr>(per_stalb + 1) == 1)
                {
                    // first resize stalb
                    // secondly swap for sorting
                    if ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(per_stalb) < OOOBLOCKSIZE)
                    {
                        UPBlockSizePointer *newblock_pointer = UpEdgeBlock(per_stalb,*reinterpret_cast<uint8_ptr>(per_stalb),me, thread_id);

                        stalb_type *new_ebCur = newblock_pointer->new_stalb;


                        me->free(newblock_pointer->prev_stalb, newblock_pointer->prev_block_size * 8,
                                 thread_id); // free previous block after new block update

                        me->free(newblock_pointer->prev_property_arr,(newblock_pointer->prev_block_size / 2) * 8, thread_id); // free property array

                        uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(new_ebCur + 1);

                        u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(new_ebCur);

                        uint64_t pos = findIndexForBlockArrOOO(new_ebCur, *new_curPos, (eb_block_size_ooo / 2),
                                                               key);

                        if(IsDelete)
                        {
                            StoreKeyInBlock(new_ebCur, pos, phy_dest_id, ln, weight_w, true);
                            stalb_ln = new_ebCur;
                        }
                        else
                        {
                            StoreKeyInBlock(new_ebCur, pos, phy_dest_id, ln, weight_w);
                        }

                        LeafNode *leaf_src = *reinterpret_cast<LeafNode **>(new_ebCur + ((eb_block_size_ooo / 2) * 8) + ((*reinterpret_cast<uint16_ptr>(new_ebCur + 1) * 8) - 8));

                        art_val->del(it.key().c_str());

                        art_val->set(std::to_string(leaf_src->getSTime()).c_str(), new_ebCur);
                    }
                    else
                    {
                        // split block and connect through adaptive radix tree
                        // step 1
                        uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(per_stalb + 1);

                        u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(per_stalb);

                        // memmove
                        uint64_t pos = findIndexForBlockArrOOO(per_stalb, *new_curPos, (eb_block_size_ooo / 2), key);

                        // %%%%%%% split block
                        uint64_t new_block_size = (eb_block_size_ooo / 2);
                        uint64_t split_main_block_index = new_block_size / 2;
                        if (pos >= split_main_block_index)
                        {
                            // move main block indexes that are < pos considering deletion factor as well for counter
                            stalb_type *new_stalb_block = DownEdgeBlock(*reinterpret_cast<uint8_ptr>(per_stalb + 1), me, thread_id);

                            // split block function call  (per_stalb, new_stalb_block, new_block_size, split_main_block_index, per_stalb_cur_index)
                            SplitBlock(per_stalb, new_stalb_block, new_block_size, split_main_block_index,
                                       *new_curPos, 1);

                            //%%%%%%% store current key value in there corresponding order position
                            if(IsDelete)
                            {
                                StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w, true);
                                stalb_ln = per_stalb;

                            }
                            else
                            {
                                StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w);
                            }
                            //%%%%%%%%%%% end memmove
                            // get the max source-timestamp of both block and consider it as key for ART
                            StoreBlockInArt(per_stalb, new_stalb_block, eb_block_size_ooo, new_block_size,
                                            setOutOfOrder, art_per_index);
                            // create ART and store it the value
                        }
                        else
                        {
                            // when pos < half main
                            stalb_type *new_stalb_block = DownEdgeBlock(*reinterpret_cast<uint8_ptr>(per_stalb + 1), me, thread_id);

                            // split block function call  (per_stalb, new_stalb_block, new_block_size, split_main_block_index, per_stalb_cur_index)
                            SplitBlock(per_stalb, new_stalb_block, new_block_size, (split_main_block_index - 1),
                                       *new_curPos, 0);

                            //store current key value in there corresponding order position
                            if(IsDelete)
                            {
                                StoreKeyInBlock(new_stalb_block, pos, phy_dest_id, ln, weight_w, true);
                                stalb_ln = per_stalb;

                            }
                            else
                            {
                                StoreKeyInBlock(new_stalb_block, pos, phy_dest_id, ln, weight_w);
                            }
                            // get the max source-timestamp of both block and consider it as key for ART
                            StoreBlockInArt(per_stalb, new_stalb_block, eb_block_size_ooo, new_block_size,setOutOfOrder, art_per_index);

                        }

                        // %%%%%%%%

                    }

                }
                else
                {
                    // arrange stalb block
                    uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(per_stalb + 1);

                    u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(per_stalb);

                    uint64_t pos = findIndexForBlockArrOOO(per_stalb, *new_curPos, (eb_block_size_ooo / 2),key);
                    if(IsDelete)
                    {
                        StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w, true);
                        stalb_ln = per_stalb;

                    }
                    else
                    {
                        StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w);
                    }

                }

                ln->setWTsEdge(commit_time);
            }
        }
        *reinterpret_cast<uint8_ptr>(ptr_per_src->getperVertexBlockArr()[indexIndrArray] + 4) = 1; // out-of-order 1
    }
    else
    {
        //%%%%%%%%%%%%% direct edge block

        //fixed size block binary search
        stalb_type *eb = static_cast<stalb_type *>(index[phy_src_id].getEdgePtr());
        u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

        int indexFixedBlock = findIndexForBlockArr(eb, ((u_int16_t) *reinterpret_cast<uint16_ptr>(eb + 1)),(eb_block_size / 2), key);

        /// eb block
        EdgeInfoHal *eih_index = *reinterpret_cast<EdgeInfoHal **>(eb + ((eb_block_size / 2) * 8) + ((indexFixedBlock * 8) - 8));

        if (eih_index->getSTime() < key)
        {
            indexFixedBlock--;
        }

        EdgeInfoHal *setOutOfOrder = *reinterpret_cast<EdgeInfoHal **>(eb + ((eb_block_size / 2) * 8) + ((indexFixedBlock * 8) - 8));

        if (setOutOfOrder->getOutOfOrderUpdt() == NULL)
        {
            // use art syncornized version for correctness
            stalb_type *stalb = static_cast<stalb_type *>(me->Allocate(32, thread_id));

            double *property = static_cast<double *>(me->Allocate(16, thread_id));


            memset(stalb, 0, 32);
            memset(property, 0, 16);

            *reinterpret_cast<uint8_ptr>(stalb) = 2; // block size

            *reinterpret_cast<uint16_ptr>(stalb + 1) = 2; // cur_index

            // insertion steps

            *reinterpret_cast<uint64_ptr>(stalb + 8) = add_version_ooo(phy_dest_id); // dest node
            /// %%%%%%%% update if consider new ln as duplicate
            if(IsDelete)
            {
                *reinterpret_cast<uint64_ptr>(stalb + 8) = add_version(phy_dest_id);

                if (!HBALStore::isReadActive)
                {
                    *reinterpret_cast<uint64_ptr>(stalb + 8) = add_version_read(phy_dest_id); /// no read-only queries are active
                }
                stalb_ln = stalb;

            }
            //// %%%%%%%
            *reinterpret_cast<uint64_ptr>(stalb +
                                          16) = reinterpret_cast<vertex_t>(ln); // edge info hal insertion

            property[1] = weight_w;

            *reinterpret_cast<double **>(stalb + 24) = property; // property insertion

            uint16_t *cur_ind = reinterpret_cast<uint16_ptr>(stalb + 1);

            *cur_ind = 1; // update cur index

            //%%%%%%%%%%%
            setOutOfOrder->setOutOfOrderUpdt(reinterpret_cast<vertex_t>(stalb));
            if(!HBALStore::isReadActive)
            {
                *reinterpret_cast<uint64_ptr>((eb) + ((indexFixedBlock) * 8)) = add_version_ooo(*reinterpret_cast<uint64_ptr>((eb) + ((indexFixedBlock) * 8)));
            }
            ln->setWTsEdge(commit_time);
        }
        else
        {
            // %%%%%%%%%%
            if (!check_version(setOutOfOrder->getOutOfOrderUpdt()))
            {
                stalb_type *per_stalb = reinterpret_cast<stalb_type *>(setOutOfOrder->getOutOfOrderUpdt());
                if (*reinterpret_cast<uint16_ptr>(per_stalb + 1) == 1)
                {
                    if ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(per_stalb) < OOOBLOCKSIZE)
                    {
                        UPBlockSizePointer *newblock_pointer = UpEdgeBlock(per_stalb,
                                                                           *reinterpret_cast<uint8_ptr>(per_stalb),
                                                                           me, thread_id);

                        stalb_type *new_ebCur = newblock_pointer->new_stalb;

                        // index[phy_src_id].setEdgePtr(reinterpret_cast<vertex_t>(new_ebCur),true);

                        me->free(newblock_pointer->prev_stalb, newblock_pointer->prev_block_size * 8,
                                 thread_id); // free previous block after new block update

                        me->free(newblock_pointer->prev_property_arr,
                                 (newblock_pointer->prev_block_size / 2) * 8, thread_id); // free property array

                        uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(new_ebCur + 1);

                        u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(new_ebCur);

                        uint64_t pos = findIndexForBlockArrOOO(new_ebCur,
                                                               *reinterpret_cast<uint16_ptr>(new_ebCur + 1),
                                                               (eb_block_size_ooo / 2), key);

                        // memmove
                        if(IsDelete)
                        {
                            StoreKeyInBlock(new_ebCur, pos, phy_dest_id, ln, weight_w, true);
                            stalb_ln = new_ebCur;
                        }
                        else
                        {
                            StoreKeyInBlock(new_ebCur, pos, phy_dest_id, ln, weight_w);
                        }

                        setOutOfOrder->setOutOfOrderUpdt(reinterpret_cast<vertex_t>(new_ebCur));

                    }
                    else
                    {
                        // split block and connect through adaptive radix tree
                        // step 1
                        uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(per_stalb + 1);

                        u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(per_stalb);

                        // memmove
                        // int src_index = *new_curPos;
                        // int dest_index = *new_curPos - 1;

                        // int pos = //
                        uint64_t pos = findIndexForBlockArrOOO(per_stalb, *new_curPos, (eb_block_size_ooo / 2),
                                                               key);

                        // %%%%%%% split block
                        uint64_t new_block_size = (eb_block_size_ooo / 2);
                        uint64_t split_main_block_index = new_block_size / 2;
                        if (pos >= split_main_block_index)
                        {
                            // move main block indexes that are < pos considering deletion factor as well for counter

                            stalb_type *new_stalb_block = DownEdgeBlock(*reinterpret_cast<uint8_ptr>(per_stalb + 1), me, thread_id);

                            // split block function call  (per_stalb, new_stalb_block, new_block_size, split_main_block_index, per_stalb_cur_index)
                            SplitBlock(per_stalb, new_stalb_block, new_block_size, split_main_block_index,*new_curPos, 1);

                            //%%%%%%% store current key value in there corresponding order position
                            if(IsDelete)
                            {
                                // consider as duplicate
                                StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w, true);
                                stalb_ln = per_stalb;

                            }
                            else
                            {
                                StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w);
                            }
                            //%%%%%%%%%%% end memmove
                            // get the max source-timestamp of both block and consider it as key for ART
                            //
                            ArtPerIndex *perArt = (ArtPerIndex *) (std::aligned_alloc(PAGE_SIZE,sizeof(ArtPerIndex)));
                            perArt->initArtTree();
                            StoreBlockInArt(per_stalb, new_stalb_block, eb_block_size_ooo, new_block_size,setOutOfOrder, perArt);
                            setOutOfOrder->setOutOfOrderUpdt(add_version(reinterpret_cast<vertex_t>(perArt)));
                            // create ART and store it the value
                        }
                        else
                        {
                            // when pos < half main
                            stalb_type *new_stalb_block = DownEdgeBlock(
                                    *reinterpret_cast<uint8_ptr>(per_stalb + 1), me, thread_id);

                            // split block function call  (per_stalb, new_stalb_block, new_block_size, split_main_block_index, per_stalb_cur_index)
                            SplitBlock(per_stalb, new_stalb_block, new_block_size, (split_main_block_index - 1),*new_curPos, 0);

                            //store current key value in there corresponding order position
                            if(IsDelete)
                            {
                                StoreKeyInBlock(new_stalb_block, pos, phy_dest_id, ln, weight_w, true);
                                stalb_ln = per_stalb;
                            }
                            else
                            {
                                StoreKeyInBlock(new_stalb_block, pos, phy_dest_id, ln, weight_w);
                            }
                            // get the max source-timestamp of both block and consider it as key for ART
                            ArtPerIndex *perArt = (ArtPerIndex *) (std::aligned_alloc(PAGE_SIZE,sizeof(ArtPerIndex)));
                            perArt->initArtTree();
                            StoreBlockInArt(per_stalb, new_stalb_block, eb_block_size_ooo, new_block_size,
                                            setOutOfOrder, perArt);
                            setOutOfOrder->setOutOfOrderUpdt(add_version(reinterpret_cast<vertex_t>(perArt)));

                        }

                        // %%%%%%%%

                        // %%%%%%%%
                    }
                }
                else
                {
                    // arrange stalb block
                    uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(per_stalb + 1);

                    u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(per_stalb);

                    uint64_t pos = findIndexForBlockArrOOO(per_stalb,
                                                           *reinterpret_cast<uint16_ptr>(per_stalb + 1),
                                                           (eb_block_size_ooo / 2), key);
                    if(IsDelete)
                    {
                        StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w, true);
                        stalb_ln = per_stalb;
                    }
                    else
                    {
                        StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w);
                    }

                }
                ln->setWTsEdge(commit_time);
            }
            else
            {
                //%%%%%%%% adaptive radix tree
                //%%%%%%%%%% for Adaptive radix tree
                ArtPerIndex *art_per_index = reinterpret_cast<ArtPerIndex *>(remove_version(
                        setOutOfOrder->getOutOfOrderUpdt()));

                art::art<stalb_type *> *art_val = art_per_index->getArtTree();
                //%%%%%%%%%%%% adaptive radix tree scan start
                // ei->outOfOrderUpdt.scanArtTree();
                auto it = art_val->begin(std::to_string(ln->getSTime()).c_str());
                stalb_type *per_stalb = *it;

                if (*reinterpret_cast<uint16_ptr>(per_stalb + 1) == 1)
                {
                    // first resize stalb
                    // secondly swap for sorting
                    if ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(per_stalb) < OOOBLOCKSIZE) {
                        UPBlockSizePointer *newblock_pointer = UpEdgeBlock(per_stalb,
                                                                           *reinterpret_cast<uint8_ptr>(per_stalb),
                                                                           me, thread_id);

                        stalb_type *new_ebCur = newblock_pointer->new_stalb;


                        me->free(newblock_pointer->prev_stalb, newblock_pointer->prev_block_size * 8,
                                 thread_id); // free previous block after new block update

                        me->free(newblock_pointer->prev_property_arr,
                                 (newblock_pointer->prev_block_size / 2) * 8, thread_id); // free property array

                        uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(new_ebCur + 1);

                        u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(new_ebCur);

                        uint64_t pos = findIndexForBlockArrOOO(new_ebCur, *new_curPos, (eb_block_size_ooo / 2),
                                                               key);

                        if(IsDelete)
                        {
                            StoreKeyInBlock(new_ebCur, pos, phy_dest_id, ln, weight_w, true);
                            stalb_ln = new_ebCur;
                        }
                        else
                        {
                            StoreKeyInBlock(new_ebCur, pos, phy_dest_id, ln, weight_w);
                        }

                        LeafNode *leaf_src = *reinterpret_cast<LeafNode **>(new_ebCur + ((eb_block_size_ooo / 2) * 8) + ((*reinterpret_cast<uint16_ptr>(new_ebCur + 1) * 8) - 8));

                        art_val->del(it.key().c_str());

                        art_val->set(std::to_string(leaf_src->getSTime()).c_str(), new_ebCur);
                    }
                    else
                    {
                        // split block and connect through adaptive radix tree
                        // step 1
                        uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(per_stalb + 1);

                        u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(per_stalb);

                        // memmove
                        uint64_t pos = findIndexForBlockArrOOO(per_stalb, *new_curPos, (eb_block_size_ooo / 2), key);

                        // %%%%%%% split block
                        uint64_t new_block_size = (eb_block_size_ooo / 2);
                        uint64_t split_main_block_index = new_block_size / 2;
                        if (pos >= split_main_block_index)
                        {
                            // move main block indexes that are < pos considering deletion factor as well for counter
                            stalb_type *new_stalb_block = DownEdgeBlock(*reinterpret_cast<uint8_ptr>(per_stalb + 1), me, thread_id);

                            // split block function call  (per_stalb, new_stalb_block, new_block_size, split_main_block_index, per_stalb_cur_index)
                            SplitBlock(per_stalb, new_stalb_block, new_block_size, split_main_block_index,
                                       *new_curPos, 1);

                            //%%%%%%% store current key value in there corresponding order position
                            if(IsDelete)
                            {
                                StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w, true);
                                stalb_ln = per_stalb;

                            }
                            else
                            {
                                StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w);
                            }
                            //%%%%%%%%%%% end memmove
                            // get the max source-timestamp of both block and consider it as key for ART
                            StoreBlockInArt(per_stalb, new_stalb_block, eb_block_size_ooo, new_block_size, setOutOfOrder, art_per_index);
                            // create ART and store it the value
                        }
                        else
                        {
                            // when pos < half main
                            stalb_type *new_stalb_block = DownEdgeBlock(
                                    *reinterpret_cast<uint8_ptr>(per_stalb + 1), me, thread_id);

                            // split block function call  (per_stalb, new_stalb_block, new_block_size, split_main_block_index, per_stalb_cur_index)
                            SplitBlock(per_stalb, new_stalb_block, new_block_size, (split_main_block_index - 1), *new_curPos, 0);

                            //store current key value in there corresponding order position
                            if(IsDelete)
                            {
                                StoreKeyInBlock(new_stalb_block, pos, phy_dest_id, ln, weight_w, true);
                                stalb_ln = per_stalb;

                            }
                            else
                            {
                                StoreKeyInBlock(new_stalb_block, pos, phy_dest_id, ln, weight_w);
                            }
                            // get the max source-timestamp of both block and consider it as key for ART
                            StoreBlockInArt(per_stalb, new_stalb_block, eb_block_size_ooo, new_block_size,setOutOfOrder, art_per_index);

                        }

                        // %%%%%%%%

                    }

                }
                else
                {
                    // arrange stalb block
                    uint16_t *new_curPos = reinterpret_cast<uint16_ptr>(per_stalb + 1);

                    u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(per_stalb);

                    uint64_t pos = findIndexForBlockArrOOO(per_stalb, *new_curPos, (eb_block_size_ooo / 2), key);
                    if(IsDelete)
                    {
                        StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w, true);
                        stalb_ln = per_stalb;

                    }
                    else
                    {
                        StoreKeyInBlock(per_stalb, pos, phy_dest_id, ln, weight_w);
                    }

                }

                ln->setWTsEdge(commit_time);
            }
        }
        *reinterpret_cast<uint8_ptr>(eb + 4) = 1; // out-of-order 1

        // %%%%%%% end
    }

    u_int64_t adjcent_pos_store = 0;
    u_int64_t dest_prefix_ooo = (u_int64_t) phy_dest_id & thread_prefix_OOO;
    adjcent_pos_store = setBitMask(adjcent_pos_store, source_timestamp, t_s_start, t_s_bits);
    adjcent_pos_store = setBitMask(adjcent_pos_store, dest_prefix_ooo, DNPStart_OOO, DNP_ooo_bits);
    // adjcent_pos_store = setBitMask(adjcent_pos_store, 1, IsSTALStart, IsSTALBits);
    adjcent_pos_store = setBitMask(adjcent_pos_store, 1, IsOutOrderStart, IsOutOrderBits);
    return adjcent_pos_store;
}

// Pointer to edge metadata from upi
vertex_t HBALStore::getEdgeMetaData(vertex_t phy_src_id, AdjacentPos *ap, stalb_type *stalb_cur)
{
    if(!check_version_read(ap->edgeBlockIndex))
    {
        // in-order update
        EdgeInfoHal *eih;
        if (!(index[phy_src_id].edgePtr & 0x1))
        {
            //%%%% Step 1: Add ITM block in current inserted position for considering as duplicate + Go to STAL position to store ITM in corresponding inserted position
            u_int64_t index_block = ap->indrIndex;

            PerSourceIndrPointer *ptr_ind = static_cast<PerSourceIndrPointer *>(index[phy_src_id].getEdgePtr());

            stalb_type *eb = ptr_ind->getperVertexBlockArr()[index_block];

            stalb_cur = eb;

            u_int64_t index_edge_block = ap->edgeBlockIndex;

            u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

            eih = *reinterpret_cast<EdgeInfoHal **>(eb + ((eb_block_size / 2) * 8) + ((index_edge_block * 8) - 8));

            return reinterpret_cast<vertex_t>(eih);

        }
        else
        {
            //%%%% Step 1: Add ITM block in current inserted position for considering as duplicate + Go to STAL position to store ITM in corresponding inserted position

            stalb_type *ptr_edge;

            ptr_edge = static_cast<stalb_type *>(index[phy_src_id].getEdgePtr());

            stalb_type *eb = ptr_edge;
            stalb_cur = eb;

            u_int64_t index_edge_block = ap->edgeBlockIndex;

            u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb);

            eih = *reinterpret_cast<EdgeInfoHal **>(eb + ((eb_block_size / 2) * 8) + ((index_edge_block * 8) - 8));
            return reinterpret_cast<vertex_t>(eih);
            // %%% Step 4: Update hashtableindex value to store new and current edge inserted position

        }
    }
    else
    {
        // out-of-order updates
        LeafNode *ln = reinterpret_cast<LeafNode *> (remove_version_read(ap->edgeBlockIndex));
        stalb_cur = reinterpret_cast<stalb_type *>(remove_version(ap->indrIndex));
        return reinterpret_cast<vertex_t>(ln);
    }
}

void HBALStore::setStalbMetadata(vertex_t stalb_cur_version,vertex_t index_pos, bool flag ,EdgeInfoHal *edge_info_smaller)
{

    stalb_type *stalb_cur = NULL;
    if(check_version(stalb_cur_version))
    {
        stalb_cur = reinterpret_cast<stalb_type *>(remove_version(stalb_cur_version));
        *reinterpret_cast<uint64_ptr>(stalb_cur + (index_pos * 8)) = add_version(*reinterpret_cast<uint64_ptr>(stalb_cur + (index_pos * 8)));
    }
    else
    {
        stalb_cur = reinterpret_cast<stalb_type *>(remove_version(stalb_cur_version));
    }

    // set eih empty
    *reinterpret_cast<uint8_ptr>(stalb_cur + 3) = 1; // isdeletion flag

    // if no ooo exist in eih
    if(flag)
    {
        if (edge_info_smaller->getOutOfOrderUpdt() == 0)
        {
            *reinterpret_cast<uint16_ptr>(stalb_cur + 5) = *reinterpret_cast<uint16_ptr>(stalb_cur + 5) + 1; // set empty space
        }
    }
    else
    {
        *reinterpret_cast<uint16_ptr>(stalb_cur + 5) = *reinterpret_cast<uint16_ptr>(stalb_cur + 5) + 1;
    }
}

void HBALStore::setITMBlockDuplicate(vertex_t stalb_cur_version, vertex_t metadatablock_greater, vertex_t metadata_block_smaller, int thread_id, MemoryAllocator *me, bool flag, bool isDelete, vertex_t index_pos)
{
    // in-order update
    EdgeInfoHal *edge_info_greater = NULL;
    EdgeInfoHal *edge_info_smaller = NULL;
    LeafNode *leaf_node_greater    = NULL;
    LeafNode *leaf_node_smaller    = NULL;

    ITM *itm = static_cast<ITM *>(me->Allocate(sizeof(ITM),thread_id));
    timestamp_t greater_source_time = 0;
    // check if greater metadata_block is leaf node or edgeinfohal
    if(!check_version(metadatablock_greater))
    {
        edge_info_greater = reinterpret_cast<EdgeInfoHal *>(remove_version(metadatablock_greater));
        greater_source_time = remove_version(edge_info_greater->getSTime());

        itm->setSrcTime(greater_source_time);

        itm->setWts(edge_info_greater->getWTsEdge());
    }
    else
    {
        leaf_node_greater = reinterpret_cast<LeafNode *>(remove_version(metadatablock_greater));
        greater_source_time = remove_version(leaf_node_greater->getSTime());

        itm->setSrcTime(greater_source_time);

        itm->setWts(leaf_node_greater->getWTsEdge());
    }

    // check if smaller metadata_block is leaf node or edgeinfohal
    if(!check_version(metadata_block_smaller))
    {
        edge_info_smaller = reinterpret_cast<EdgeInfoHal *>(remove_version(metadata_block_smaller));
        if(edge_info_smaller->getInvldTime() == NULL)
        {
            itm->setNext(NULL);
            edge_info_smaller->setInvldTime(itm);
            setStalbMetadata(stalb_cur_version, remove_version_read(remove_version(index_pos)), flag, edge_info_smaller);
        }
        else
        {
            ITM *prev_itm = edge_info_smaller->getInvldTime();
            if(remove_version(prev_itm->getSrcTime()) > greater_source_time)
            {
                itm->setNext(prev_itm);
                edge_info_smaller->setInvldTime(itm);
                if(!check_version(index_pos))
                {
                    setStalbMetadata(stalb_cur_version, remove_version_read(remove_version(index_pos)), flag, edge_info_smaller);
                }
            }
            else
            {
                me->free(itm, sizeof(ITM), thread_id);
            }
        }
    }
    else
    {
        leaf_node_smaller = reinterpret_cast<LeafNode *>(remove_version(metadata_block_smaller));
        if(leaf_node_smaller->getInvalEntry() == NULL)
        {
            itm->setNext(NULL);

            leaf_node_smaller->setInvldTime(itm);

            setStalbMetadata(stalb_cur_version, remove_version_read(remove_version(index_pos)), flag, edge_info_smaller);

        }
        else
        {
            ITM *prev_itm = leaf_node_smaller->getInvalEntry();
            if(remove_version(prev_itm->getSrcTime()) >  greater_source_time)
            {
                itm->setNext(prev_itm);

                leaf_node_smaller->setInvldTime(itm);
                if(!check_version(index_pos))
                {
                    setStalbMetadata(stalb_cur_version, remove_version_read(remove_version(index_pos)), flag, edge_info_smaller);
                }
            }
            else
            {
                me->free(itm, sizeof(ITM), thread_id);
            }
        }
    }
}

//  insertion in the hash table > new insertion && dve->next != NULL
void HBALStore::dupInsertionGreaterNewInsertion(DuplicateUpdate *prev, DuplicateUpdate *dup, DVE *dve, stalb_type *stalb_cur, vertex_t metadatablock_greater, vertex_t metadata_block_smaller, timestamp_t source_time, AdjacentPos *ap_pos_prev, vertex_t stalb_inserted_pos, vertex_t phy_src_id, int thread_id, MemoryAllocator *me)
{

    if(prev == NULL)
    {

        // only > insertion exist as compared to new insertion
        setITMBlockDuplicate(remove_version((reinterpret_cast<vertex_t>(stalb_cur))), metadatablock_greater, metadata_block_smaller, thread_id, me, false, false);

        DuplicateUpdate *dup_ln = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));
        dup_ln->SrcTime   = source_time;
        dup_ln->UpdatePos = stalb_inserted_pos;
        dup_ln->next = dup;
        dve->next = dup_ln;
    }
    else if (check_version(prev->SrcTime))
    {
        // prev is deleted

        // only > insertion exist as compared to new insertion
        setITMBlockDuplicate(remove_version((reinterpret_cast<vertex_t>(stalb_cur))), metadatablock_greater, metadata_block_smaller, thread_id, me, false, false);

        DuplicateUpdate *dup_ln = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));
        dup_ln->SrcTime   = source_time;
        dup_ln->UpdatePos = stalb_inserted_pos;
        dup_ln->next = dup;
        dve->next = dup_ln;
    }
    else
    {
        // prev exist < new value
        uint64_t *pos_ooo_prev = 0;
        ap_pos_prev = index[phy_src_id].getDestBySourceTimeNodeHashTableVal(prev->UpdatePos,ap_pos_prev, pos_ooo_prev);
        stalb_type *stalb_cur_prev = NULL;
        // LeafNode *ln_prev;
        if (!check_version_read(ap_pos_prev->edgeBlockIndex))
        {
            // if prev is in-order
            EdgeInfoHal *eih_prev = reinterpret_cast<EdgeInfoHal *>(getEdgeMetaData(phy_src_id, ap_pos_prev, stalb_cur_prev));

            setITMBlockDuplicate(remove_version((reinterpret_cast<vertex_t>(stalb_cur))), metadatablock_greater,metadata_block_smaller, thread_id, me, false, false);

            setITMBlockDuplicate(remove_version((reinterpret_cast<vertex_t>(stalb_cur_prev))), metadata_block_smaller,remove_version((reinterpret_cast<vertex_t>(eih_prev))), thread_id, me, false, false,add_version(*pos_ooo_prev));

        }
        else
        {
            // if prev is out-of-order
            LeafNode *ln_prev = reinterpret_cast<LeafNode *>(getEdgeMetaData(phy_src_id, ap_pos_prev, stalb_cur_prev));

            setITMBlockDuplicate(remove_version((reinterpret_cast<vertex_t>(stalb_cur))), metadatablock_greater,
                                 metadata_block_smaller, thread_id, me, false, false);

            setITMBlockDuplicate(remove_version((reinterpret_cast<vertex_t>(stalb_cur_prev))), metadatablock_greater,
                                 add_version((reinterpret_cast<vertex_t>(ln_prev))), thread_id, me, false, false,
                                 add_version(*pos_ooo_prev));

        }
        DuplicateUpdate *dup_ln = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));
        dup_ln->SrcTime   = source_time;
        dup_ln->UpdatePos = stalb_inserted_pos;
        prev->next = dup_ln;
        dup_ln->next = dup;
    }

}

void HBALStore::setITMBlockDelete( DuplicateUpdate *dup, vertex_t smaller_metadata_block, int thread_id, MemoryAllocator *me)
{
    ITM *itm_delete = static_cast<ITM *>(me->Allocate(sizeof(ITM),thread_id));
    itm_delete->setSrcTime((remove_version(dup->SrcTime)));
    itm_delete->setWts(getCurrTimeStamp());

    if(!check_version(smaller_metadata_block))
    {
        EdgeInfoHal *ei = reinterpret_cast<EdgeInfoHal *>(remove_version(smaller_metadata_block));
        ei->setInvldTime(itm_delete);
    }
    else
    {
        // std::cout<<smaller_metadata_block<<" "<<check_version(smaller_metadata_block)<<std::endl;

        LeafNode *ln = reinterpret_cast<LeafNode *>(remove_version(smaller_metadata_block));
        ln->setInvldTime(itm_delete);
    }

}
ITM* HBALStore::createITMBlockAndUpdated(ITM *itm, vertex_t srctime, int thread_id, MemoryAllocator *me)
{

    if(itm == NULL)
    {

        itm = static_cast<ITM *>(me->Allocate(sizeof(ITM),thread_id));
        itm->setSrcTime(add_version(srctime));
        itm->setWts(getCurrTimeStamp());
        itm->setNext(NULL);
        return itm;
    }
    else
    {
        ITM *new_itm = static_cast<ITM *>(me->Allocate(sizeof(ITM),thread_id));
        new_itm->setWts(getCurrTimeStamp());
        new_itm->setSrcTime(add_version(srctime));
        new_itm->setNext(itm);
        return new_itm;
    }
}
void HBALStore::dupPrevExistStalDeleted(timestamp_t commit_time, vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info, vertex_t *dup_stalb, vertex_t *pos_dup, int *dup_indr_pos, DVE *dve, vertex_t *prev_stalb, vertex_t *pos_prev, DuplicateUpdate *dup, DuplicateUpdate *prev_f, int thread_id, MemoryAllocator *me, bool flag)
{
    timestamp_t source_time = getTimestampSrc(inputedge_info, IsOutOfOrder);
    EdgeInfoHal *ei_cur;
    LeafNode *ln_ei;
    LeafNode *ln;
    stalb_type *stalb_ln= NULL;
    bool flag_ooo = false;
    bool isDelete = true;
    vertex_t cur_source_time_itm = 0;
    stalb_type *dup_s = NULL;
    /////
    if(!check_version(*dup_stalb))
    {
        // in-order dup_stalb
        dup_s  = reinterpret_cast<stalb_type *>(remove_version(*dup_stalb));
        ei_cur = *reinterpret_cast<EdgeInfoHal **>(dup_s + (((((u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(dup_s)) / 2)) * 8) + ((*pos_dup * 8) - 8));
        if(!check_version(ei_cur->getInvldTime()->getSrcTime()))
        {
            isDelete = false;
        }

        cur_source_time_itm = ei_cur->getInvldTime()->getSrcTime();
    }
    else
    {
        // out-of-order stalb
        // in-order dup_stalb
        dup_s = reinterpret_cast<stalb_type *>(remove_version(*dup_stalb));
        ln_ei = *reinterpret_cast<LeafNode **>(dup_s + (((((u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(dup_s)) / 2)) * 8) + ((*pos_dup * 8) - 8));
        if(!check_version(ln_ei->getInvalEntry()->getSrcTime()))
        {
            //
            isDelete = false;
        }

        cur_source_time_itm = ln_ei->getInvalEntry()->getSrcTime();
        flag_ooo = true;
    }

    ////

    if(isDelete)
    {
        if (cur_source_time_itm > source_time)
        {
            // the entry is deleted in hash table
            // we insert the new entry
            vertex_t stalb_inserted_pos;

            stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info, ln, phy_src_id, phy_dest_id, me, thread_id, true,stalb_ln);

            ITM *new_itm = createITMBlockAndUpdated(ln->getInvalEntry(), cur_source_time_itm, thread_id, me);
            ln->setInvldTime(new_itm);

            if (flag_ooo)
            {
                setITMBlockDuplicate((reinterpret_cast<vertex_t>(stalb_ln)),add_version(reinterpret_cast<vertex_t>(ln)),add_version(reinterpret_cast<vertex_t>(ln_ei)), thread_id, me, true, false, 0);
            }
            else
            {
                setITMBlockDuplicate((reinterpret_cast<vertex_t>(stalb_ln)),add_version(reinterpret_cast<vertex_t>(ln)),remove_version(reinterpret_cast<vertex_t>(ei_cur)), thread_id, me, true, false, 0);
            }

            // ei should need to be inserted in the hashtable

            stalb_inserted_pos = setHashtableUpiInorder(phy_src_id, *pos_dup, *reinterpret_cast<uint8_ptr>(dup_s),*dup_indr_pos);

            DuplicateUpdate *d_new = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate), thread_id));

            if (flag_ooo)
            {
                d_new->SrcTime = ln_ei->getSTime();
            }
            else
            {
                d_new->SrcTime = ei_cur->getSTime();
            }
            d_new->UpdatePos = stalb_inserted_pos;
            dve->next = d_new;
            // todo
        }
        else
        {
            if (!check_version(*prev_stalb))
            {
                stalb_type *dup_prev = reinterpret_cast<stalb_type *>(remove_version(*prev_stalb));

                EdgeInfoHal *ei_prev_del = *reinterpret_cast<EdgeInfoHal **>(dup_prev + (((((u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(dup_prev)) / 2)) * 8) + ((*pos_prev * 8) - 8));
                if (flag)
                {
                    if (ei_prev_del->getSTime() > remove_version(dup->SrcTime))
                    {
                        vertex_t stalb_inserted_pos;

                        stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info, ln, phy_src_id, phy_dest_id, me,thread_id, true, stalb_ln);

                        ITM *new_itm = createITMBlockAndUpdated(
                                ln->getInvalEntry(),
                                remove_version(dup->SrcTime), thread_id, me);
                        ln->setInvldTime(new_itm);

                        setStalbMetadata(add_version(reinterpret_cast<vertex_t>(stalb_ln)),
                                         *reinterpret_cast<uint16_ptr>(stalb_ln + 1), true, NULL);

                        if (prev_f != NULL) {
                            prev_f->next = dup->next;
                            me->free(dup, sizeof(DuplicateUpdate), thread_id);
                        } else {
                            dve->next = dup->next;
                            me->free(dup, sizeof(DuplicateUpdate), thread_id);
                        }
                    }
                }
                else
                {
                    vertex_t stalb_inserted_pos;

                    stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info, ln, phy_src_id, phy_dest_id, me, thread_id,
                                                             true, stalb_ln);

                    setITMBlockDuplicate((reinterpret_cast<vertex_t>(stalb_ln)),
                                         remove_version(reinterpret_cast<vertex_t>(ei_prev_del)),
                                         add_version(reinterpret_cast<vertex_t>(ln)), thread_id, me, true, false, 0);

                    DuplicateUpdate *d_new = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));
                    d_new->SrcTime = ei_prev_del->getSTime();
                    d_new->UpdatePos = stalb_inserted_pos;
                    dve->next = d_new;
                    // todo
                }

            }
            else {
                LeafNode *ln_prev_del = *reinterpret_cast<LeafNode **>(prev_stalb + (((((u_int64_t) 1
                        << *reinterpret_cast<uint8_ptr>(prev_stalb)) / 2)) * 8) + ((*pos_prev * 8) - 8));
                if (flag) {
                    if (ln_prev_del->getSTime() > remove_version(dup->SrcTime)) {
                        vertex_t stalb_inserted_pos;

                        stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info, ln, phy_src_id, phy_dest_id, me,
                                                                 thread_id, true, stalb_ln);

                        ITM *new_itm = createITMBlockAndUpdated(ln->getInvalEntry(), remove_version(dup->SrcTime),
                                                                thread_id, me);
                        ln->setInvldTime(new_itm);

                        setStalbMetadata(add_version(reinterpret_cast<vertex_t>(stalb_ln)),
                                         *reinterpret_cast<uint16_ptr>(stalb_ln + 1), true, NULL);

                        if (prev_f != NULL) {
                            prev_f->next = dup->next;
                            me->free(dup, sizeof(DuplicateUpdate), thread_id);
                        } else {
                            dve->next = dup->next;
                            me->free(dup, sizeof(DuplicateUpdate), thread_id);
                        }
                    }
                } else {
                    vertex_t stalb_inserted_pos;

                    stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info, ln, phy_src_id, phy_dest_id, me, thread_id,
                                                             true, stalb_ln);

                    setITMBlockDuplicate((reinterpret_cast<vertex_t>(stalb_ln)),
                                         add_version(reinterpret_cast<vertex_t>(ln_prev_del)),
                                         add_version(reinterpret_cast<vertex_t>(ln)), thread_id, me, true, false, 0);

                    DuplicateUpdate *d_new = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),
                                                                                         thread_id));
                    d_new->SrcTime = ln_prev_del->getSTime();
                    d_new->UpdatePos = stalb_inserted_pos;
                    dve->next = d_new;
                }

            }

        }
    }
    else
    {
        // duplicate
        EdgeInfoHal *ei_prev_del = NULL;
        LeafNode *ln_prev_del   = NULL;
        vertex_t  source_time_prev = 0;
        bool flag_prev_ooo = false;
        stalb_type *dup_prev;
        if (!check_version(*prev_stalb))
        {
            dup_prev = reinterpret_cast<stalb_type *>(remove_version(*prev_stalb));

            ei_prev_del = *reinterpret_cast<EdgeInfoHal **>(dup_prev + (((((u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(dup_prev)) / 2)) * 8) + ((*pos_prev * 8) - 8));
            source_time_prev = ei_prev_del->getSTime();
        }
        else
        {
            dup_prev = reinterpret_cast<stalb_type *>(remove_version(*prev_stalb));
            ln_prev_del = *reinterpret_cast<LeafNode **>(dup_prev + (((((u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(dup_prev)) / 2)) * 8) + ((*pos_prev * 8) - 8));
            source_time_prev = ln_prev_del->getSTime();
            flag_prev_ooo = true;
        }

        if (source_time_prev > remove_version(dup->SrcTime) && flag)
        {
            vertex_t stalb_inserted_pos;
            stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info, ln, phy_src_id, phy_dest_id, me,thread_id, true, stalb_ln);
            ITM *new_itm = createITMBlockAndUpdated(ln->getInvalEntry(),remove_version(dup->SrcTime), thread_id, me);
            ln->setInvldTime(new_itm);
            setStalbMetadata(add_version(reinterpret_cast<vertex_t>(stalb_ln)),*reinterpret_cast<uint16_ptr>(stalb_ln + 1), true, NULL);

            if(!flag_ooo)
            {
                setITMBlockDuplicate((reinterpret_cast<vertex_t>(dup_stalb)),add_version(reinterpret_cast<vertex_t>(ln)),remove_version(reinterpret_cast<vertex_t>(ei_cur)), thread_id, me, false, false, FLAG_EMPTY_SLOT);
            }
            else
            {
                setITMBlockDuplicate((reinterpret_cast<vertex_t>(dup_stalb)),add_version(reinterpret_cast<vertex_t>(ln)),add_version(reinterpret_cast<vertex_t>(ln_ei)), thread_id, me, false, false, FLAG_EMPTY_SLOT);
            }

            if (prev_f != NULL)
            {
                prev_f->next = dup->next;
                me->free(dup, sizeof(DuplicateUpdate), thread_id);
            }
            else
            {
                dve->next = dup->next;
                me->free(dup, sizeof(DuplicateUpdate), thread_id);
            }

        }
        else
        {
            //
            vertex_t stalb_inserted_pos;
            stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info, ln, phy_src_id, phy_dest_id, me,thread_id, true, stalb_ln);

            // new as duplicate
            if(!flag_prev_ooo)
            {
                setITMBlockDuplicate((reinterpret_cast<vertex_t>(stalb_ln)),remove_version(reinterpret_cast<vertex_t>(ei_prev_del)),add_version(reinterpret_cast<vertex_t>(ln)), thread_id, me, false, false, 0);
            }
            else
            {
                setITMBlockDuplicate((reinterpret_cast<vertex_t>(stalb_ln)),add_version(reinterpret_cast<vertex_t>(ln_prev_del)),add_version(reinterpret_cast<vertex_t>(ln)), thread_id, me, false, false, 0);
            }
            // dup_stalb as duplicate
            if(!flag_ooo)
            {
                setITMBlockDuplicate((reinterpret_cast<vertex_t>(dup_stalb)),add_version(reinterpret_cast<vertex_t>(ln)),remove_version(reinterpret_cast<vertex_t>(ei_cur)), thread_id, me, false, false, FLAG_EMPTY_SLOT);
            }
            else
            {
                setITMBlockDuplicate((reinterpret_cast<vertex_t>(dup_stalb)),add_version(reinterpret_cast<vertex_t>(ln)),add_version(reinterpret_cast<vertex_t>(ln_ei)), thread_id, me, false, false, FLAG_EMPTY_SLOT);
            }

            DuplicateUpdate *d_new = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));
            if(!flag_prev_ooo)
            {
                d_new->SrcTime = ei_prev_del->getSTime();
            }
            else
            {
                d_new->SrcTime = ln_prev_del->getSTime();
            }
            d_new->UpdatePos = stalb_inserted_pos;
            if (prev_f != NULL)
            {
                prev_f->next = d_new;
                d_new->next =  dup;
                //me->free(dup, sizeof(DuplicateUpdate), thread_id);
            }
            else
            {
                dve->next = d_new;
                d_new->next =  dup;
                // me->free(dup, sizeof(DuplicateUpdate), thread_id);
            }
        }

    }

}
void HBALStore::prevExistDupNotExist(timestamp_t commit_time,vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info, vertex_t *prev_stalb, vertex_t *pos_prev, DVE *dve, int thread_id, MemoryAllocator *me, bool flag, DuplicateUpdate *dup, DuplicateUpdate *prev_f)
{
    stalb_type *stalb_ln = NULL;
    LeafNode *ln;
    bool flag_ooo = false;
    vertex_t source_time_prev = 0;
    EdgeInfoHal *ei_prev_del;
    LeafNode *ln_prev_del;
    if(!check_version(*prev_stalb))
    {
        ei_prev_del = *reinterpret_cast<EdgeInfoHal **>(prev_stalb + (((((u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(prev_stalb)) / 2)) * 8) + ((*pos_prev * 8) - 8));
        source_time_prev = ei_prev_del->getSTime();
    }
    else
    {
        ln_prev_del = *reinterpret_cast<LeafNode **>(prev_stalb + (((((u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(prev_stalb)) / 2)) * 8) + ((*pos_prev * 8) - 8));
        flag_ooo = true;
        source_time_prev = ln_prev_del->getSTime();
    }

    if(flag)
    {
        if (source_time_prev > remove_version(dup->SrcTime))
        {
            vertex_t stalb_inserted_pos = 0;

            stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info, ln, phy_src_id, phy_dest_id, me,thread_id, true, stalb_ln);

            ITM *new_itm = createITMBlockAndUpdated(ln->getInvalEntry(),remove_version(dup->SrcTime), thread_id, me);
            ln->setInvldTime(new_itm);

            setStalbMetadata(add_version(reinterpret_cast<vertex_t>(stalb_ln)),*reinterpret_cast<uint16_ptr>(stalb_ln + 1), true, NULL);

            if (prev_f != NULL)
            {
                prev_f->next = dup->next;
                me->free(dup, sizeof(DuplicateUpdate), thread_id);
            }
            else
            {
                dve->next = dup->next;
                me->free(dup, sizeof(DuplicateUpdate), thread_id);
            }
        }
        else
        {
            //
            vertex_t stalb_inserted_pos = 0;

            stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info, ln, phy_src_id, phy_dest_id, me,thread_id, true, stalb_ln);

            if(!flag_ooo)
            {
                setITMBlockDuplicate((reinterpret_cast<vertex_t>(stalb_ln)),remove_version(reinterpret_cast<vertex_t>(ei_prev_del)),add_version(reinterpret_cast<vertex_t>(ln)), thread_id, me, false, false, 0);
            }
            else
            {
                setITMBlockDuplicate((reinterpret_cast<vertex_t>(stalb_ln)),add_version(reinterpret_cast<vertex_t>(ln_prev_del)),add_version(reinterpret_cast<vertex_t>(ln)), thread_id, me, false, false, 0);

            }
            DuplicateUpdate *d_new = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));

            d_new->SrcTime = source_time_prev;

            d_new->UpdatePos = stalb_inserted_pos;
            if (prev_f != NULL)
            {
                prev_f->next = d_new;
                d_new->next =  dup;
                //me->free(dup, sizeof(DuplicateUpdate), thread_id);
            }
            else
            {
                dve->next = d_new;
                d_new->next =  dup;
                // me->free(dup, sizeof(DuplicateUpdate), thread_id);
            }
        }
    }
    else
    {
        vertex_t stalb_inserted_pos;

        stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info, ln, phy_src_id, phy_dest_id, me, thread_id, true,stalb_ln);

        if(!flag_ooo)
        {
            setITMBlockDuplicate((reinterpret_cast<vertex_t>(stalb_ln)),remove_version(reinterpret_cast<vertex_t>(ei_prev_del)),add_version(reinterpret_cast<vertex_t>(ln)), thread_id, me, true, false, 0);
        }
        else
        {
            setITMBlockDuplicate((reinterpret_cast<vertex_t>(stalb_ln)),add_version(reinterpret_cast<vertex_t>(ln_prev_del)),add_version(reinterpret_cast<vertex_t>(ln)), thread_id, me, true, false, 0);

        }
        DuplicateUpdate *d_new = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate), thread_id));
        d_new->SrcTime = source_time_prev;
        d_new->UpdatePos = stalb_inserted_pos;
        dve->next = d_new;
        // todo
    }
}
void HBALStore::DupExistPrevNotExist(timestamp_t commit_time, vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info, vertex_t *dup_stalb,vertex_t *pos_dup,int *dup_indr_pos, DVE *dve, int thread_id, MemoryAllocator *me)
{
    EdgeInfoHal *ei;
    LeafNode *ln;
    stalb_type *stalb_ln;
    vertex_t cur_edge_source_time = 0;
    stalb_type  *stalb_cur_ins = NULL;
    bool flag = false;
    LeafNode *ln_dup;
    EdgeInfoHal *ei_dup;
    stalb_type *dup;
    timestamp_t source_time = getTimestampSrc(inputedge_info, IsOutOfOrder);
    if(dup_stalb != 0)
    {
        // new insertion either duplicate or deleted
        if(!check_version(*dup_stalb))
        {
            // in-order dup_stalb
            dup = reinterpret_cast<stalb_type *>(remove_version(*dup_stalb));
            ei_dup = *reinterpret_cast<EdgeInfoHal **>(dup + (((((u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(dup)) / 2)) * 8) + ((*pos_dup * 8) - 8));
            cur_edge_source_time = ei_dup->getInvldTime()->getSrcTime();
        }
        else
        {
            // out-of-order stalb
            // in-order dup_stalb
            dup = reinterpret_cast<stalb_type *>(remove_version(*dup_stalb));
            ln_dup = *reinterpret_cast<LeafNode **>(dup + (((((u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(dup)) / 2)) * 8) + ((*pos_dup * 8) - 8));
            cur_edge_source_time = ln_dup->getInvalEntry()->getSrcTime();
            flag = true;
        }

        if(cur_edge_source_time > source_time)
        {
            vertex_t  stalb_inserted_pos;
            if(!(CheckOutOfOrder(source_time, phy_src_id)) || (!IsOutOfOrder))
            {
                ei = static_cast<EdgeInfoHal *>(me->Allocate(sizeof(EdgeInfoHal),thread_id));
                stalb_inserted_pos = inOrderInsertion(commit_time, inputedge_info, ei, phy_src_id, phy_dest_id, me,thread_id,false, stalb_cur_ins);

                ITM *new_itm;

                new_itm = createITMBlockAndUpdated(ei->getInvldTime(), cur_edge_source_time,thread_id, me);

                ei->setInvldTime(new_itm);

                setStalbMetadata(add_version(reinterpret_cast<vertex_t>(stalb_cur_ins)), *reinterpret_cast<uint16_ptr>(stalb_cur_ins + 1), false , ei);

                if(!flag)
                {
                    setITMBlockDuplicate((reinterpret_cast<vertex_t>(stalb_ln)),remove_version(reinterpret_cast<vertex_t>(ei)),remove_version(reinterpret_cast<vertex_t>(ei_dup)), thread_id, me, false,false, 0);
                }
                else
                {
                    setITMBlockDuplicate((reinterpret_cast<vertex_t>(stalb_ln)),remove_version(reinterpret_cast<vertex_t>(ei)),add_version(reinterpret_cast<vertex_t>(ln_dup)), thread_id, me, true,false, 0);
                }
                // ei should need to be inserted in the hashtable

                stalb_inserted_pos = setHashtableUpiInorder(phy_src_id, *pos_dup, *reinterpret_cast<uint8_ptr>(dup), *dup_indr_pos);

                DuplicateUpdate *d_new = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));

                if(!flag)
                {
                    d_new->SrcTime = ei_dup->getSTime();
                }
                else
                {
                    d_new->SrcTime = ln_dup->getSTime();
                }

                d_new->UpdatePos = stalb_inserted_pos;
                dve->next = d_new;
            }
            else
            {
                ln = static_cast<LeafNode *>(me->Allocate(sizeof(LeafNode),thread_id));
                stalb_inserted_pos = outOfOrderInsertion(commit_time,inputedge_info,ln, phy_src_id, phy_dest_id, me, thread_id,true, stalb_cur_ins);

                ITM *new_itm = createITMBlockAndUpdated(ln->getInvalEntry(), cur_edge_source_time, thread_id, me);
                ln->setInvldTime(new_itm);

                setStalbMetadata(add_version(reinterpret_cast<vertex_t>(stalb_cur_ins)), *reinterpret_cast<uint16_ptr>(stalb_cur_ins + 1), true , NULL);

                if(!flag)
                {
                    setITMBlockDuplicate((reinterpret_cast<vertex_t>(stalb_ln)),add_version(reinterpret_cast<vertex_t>(ln)),remove_version(reinterpret_cast<vertex_t>(ei_dup)), thread_id, me, false,false, 0);
                }
                else
                {
                    setITMBlockDuplicate((reinterpret_cast<vertex_t>(stalb_ln)),add_version(reinterpret_cast<vertex_t>(ln)), add_version(reinterpret_cast<vertex_t>(ln_dup)), thread_id, me, true,false, 0);
                }
                // ei should need to be inserted in the hashtable

                stalb_inserted_pos = setHashtableUpiInorder(phy_src_id, *pos_dup, *reinterpret_cast<uint8_ptr>(dup), *dup_indr_pos);

                DuplicateUpdate *d_new = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));

                if(!flag)
                {
                    d_new->SrcTime = ei_dup->getSTime();
                }
                else
                {
                    d_new->SrcTime = ln_dup->getSTime();
                }

                d_new->UpdatePos = stalb_inserted_pos;
                dve->next = d_new;
            }
            //
        }
        else
        {
            // insertion simple
            vertex_t  stalb_inserted_pos;
            if((!CheckOutOfOrder(source_time, phy_src_id)) || (!IsOutOfOrder))
            {
                ei = static_cast<EdgeInfoHal *>(me->Allocate(sizeof(EdgeInfoHal),thread_id));
                stalb_inserted_pos = inOrderInsertion(commit_time, inputedge_info, ei, phy_src_id,phy_dest_id, me, thread_id, false,stalb_cur_ins);
                DuplicateUpdate *d_new = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));
                d_new->SrcTime   = ei->getSTime();
                d_new->UpdatePos = stalb_inserted_pos;
                dve->next = d_new;
            }
            else
            {
                ln = static_cast<LeafNode *>(me->Allocate(sizeof(LeafNode),thread_id));
                stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info, ln, phy_src_id, phy_dest_id, me, thread_id, false, stalb_ln);
                DuplicateUpdate *d_new = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));
                d_new->SrcTime   = ln->getSTime();
                d_new->UpdatePos = stalb_inserted_pos;
                dve->next = d_new;
            }
        }
    }
}
vertex_t HBALStore::setHashtableUpiInorder(vertex_t phy_src_id, vertex_t pos, vertex_t edgeblocksize, int dup_indr_pos)
{
    u_int64_t indr_index_bits_val = 0;
    u_int64_t indr_size_bits_val = 0;
    u_int64_t edge_block_index_bits_val = 0;
    u_int64_t edge_block_size_bits_val = 0;
    //u_int64_t adjcent_pos_store = 0;

    vertex_t adjcent_pos_store = 0;
    // for indr array 0,32 for indr size 32 to 5, for edgeblock index 37 to 11 bit, for edge block size 48 to 4, for prefix 52 to 62
    // for either pointer for edge per source is edgeblock or STAL 62 to 1 and for ooo 63 to 1
    if(!(index[phy_src_id].edgePtr & 0x1))
    {
        //
        PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(index[phy_src_id].edgePtr));
        adjcent_pos_store = setBitMask(adjcent_pos_store, dup_indr_pos, L1IndexStart,L1IndexBits);
        adjcent_pos_store = setBitMask(adjcent_pos_store, ptr_ind->getBlockLen(), L1SizeStart,L1SizeBits);
    }
    else
    {
        //
        adjcent_pos_store = setBitMask(adjcent_pos_store, 0, L1IndexStart,L1IndexBits);
        adjcent_pos_store = setBitMask(adjcent_pos_store, 0, L1SizeStart,L1SizeBits);
    }

    adjcent_pos_store = setBitMask(adjcent_pos_store, pos,L2IndexStart, L2IndexBits);
    adjcent_pos_store = setBitMask(adjcent_pos_store, edgeblocksize,L2SizeStart, L2SizeBits);

    adjcent_pos_store = setBitMask(adjcent_pos_store, 0, DNPStart, DNPBits);
    // 0 means edge block 1 means STAL
    adjcent_pos_store = setBitMask(adjcent_pos_store, 0, IsSTALStart, IsSTALBits);
    // for ooo update 1 means no ooo 0 means ooo
    adjcent_pos_store = setBitMask(adjcent_pos_store, 0, IsInOrderStart, IsInOrderBits);

    return adjcent_pos_store;
}
void HBALStore::newInsertionSrctimeGreaterThanLatestDeletion(timestamp_t commit_time, bool *is_degree_add, vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info,DVE *dve,int thread_id, MemoryAllocator *me)
{

    EdgeInfoHal *ei = NULL;
    LeafNode *ln = NULL;
    timestamp_t source_time = getTimestampSrc(inputedge_info, IsOutOfOrder);
    if(dve->next == NULL)
    {
        // the entry is deleted in hash table
        // we insert the new entry
        vertex_t  stalb_inserted_pos = 0;
        if((!CheckOutOfOrder(source_time, phy_src_id))|| (!IsOutOfOrder))
        {
            ei = static_cast<EdgeInfoHal *>(me->Allocate(sizeof(EdgeInfoHal),thread_id));
            stalb_inserted_pos = inOrderInsertion(commit_time, inputedge_info, ei, phy_src_id, phy_dest_id, me,thread_id);
        }
        else
        {
            ln = static_cast<LeafNode *>(me->Allocate(sizeof(LeafNode),thread_id));
            // std::cout<<"oooo"<<std::endl;
            stalb_inserted_pos = outOfOrderInsertion(commit_time,inputedge_info,ln, phy_src_id, phy_dest_id, me, thread_id);
        }

        DuplicateUpdate *d_new = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));
        d_new->SrcTime   = source_time;
        d_new->UpdatePos = stalb_inserted_pos;
        d_new->next = NULL;
        //  dve->oldest_version = 0; //
        dve->next = d_new;
    }
    else
    {
        // std::cout<<"dve->next !=NULL"<<std::endl;
        // dve->next contains entry
        DuplicateUpdate *dup = dve->next;
        DuplicateUpdate *prev = NULL;
        AdjacentPos *ap_pos_dup;
        AdjacentPos *ap_pos_prev;
        while(dup != NULL)
        {
            if(remove_version(dup->SrcTime) >= source_time)
            {
                break;
            }
            prev = dup;
            dup = dup->next;
        }
        // dup is NULL
        if(dup == NULL)
        {
            // it means that the new insertion > all insertions in the hash table hence in-order update
            vertex_t  stalb_inserted_pos;
            vertex_t metadatablock_greater;
            if((!CheckOutOfOrder(source_time, phy_src_id)) || (!IsOutOfOrder))
            {
                ei = static_cast<EdgeInfoHal *>(me->Allocate(sizeof(EdgeInfoHal),thread_id));

                stalb_inserted_pos = inOrderInsertion(commit_time,inputedge_info, ei, phy_src_id, phy_dest_id, me,thread_id);
                metadatablock_greater = remove_version(reinterpret_cast<vertex_t>(ei));
            }
            else
            {
                ln = static_cast<LeafNode *>(me->Allocate(sizeof(LeafNode),thread_id));
                stalb_inserted_pos = outOfOrderInsertion(commit_time,inputedge_info,ln, phy_src_id, phy_dest_id, me, thread_id);
                metadatablock_greater = add_version(reinterpret_cast<vertex_t>(ln));
            }
            // prev exist but dup is NULL
            if(!check_version(prev->SrcTime))
            {
                uint64_t *pos_ooo_prev = 0;
                ap_pos_prev = index[phy_src_id].getDestBySourceTimeNodeHashTableVal(prev->UpdatePos,ap_pos_prev, pos_ooo_prev);

                stalb_type *stalb_cur_prev = NULL;
                // LeafNode *ln_prev;
                if (!check_version_read(ap_pos_prev->edgeBlockIndex))
                {
                    // if prev is in-order
                    EdgeInfoHal *eih_prev = reinterpret_cast<EdgeInfoHal *>(getEdgeMetaData(phy_src_id, ap_pos_prev, stalb_cur_prev));

                    setITMBlockDuplicate(add_version((reinterpret_cast<vertex_t>(stalb_cur_prev))),metadatablock_greater,remove_version(reinterpret_cast<vertex_t>(eih_prev)),thread_id, me, false, false, add_version(*pos_ooo_prev));

                }
                else
                {
                    // if prev is out-of-order
                    LeafNode *ln_prev = reinterpret_cast<LeafNode *>(getEdgeMetaData(phy_src_id, ap_pos_prev, stalb_cur_prev));

                    setITMBlockDuplicate(add_version((reinterpret_cast<vertex_t>(stalb_cur_prev))),
                                         metadatablock_greater,
                                         add_version(reinterpret_cast<vertex_t>(ln_prev)), thread_id,
                                         me, false, false, add_version(*pos_ooo_prev));

                }

            }
            // prev is deletion
            DuplicateUpdate *dup_ln = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));
            dup_ln->SrcTime   = source_time;
            dup_ln->UpdatePos = stalb_inserted_pos;
            prev->next = dup_ln;
        }
        else
        {
            // dup is insertion
            if (!check_version(dup->SrcTime))
            {
                // dup  is duplicate
                uint64_t *pos_ooo = 0;
                ap_pos_dup = index[phy_src_id].getDestBySourceTimeNodeHashTableVal(dup->UpdatePos,ap_pos_dup, pos_ooo);
                stalb_type *stalb_cur = NULL;

                if (!check_version_read(ap_pos_dup->edgeBlockIndex))
                {
                    // dup is in-order
                    EdgeInfoHal *eih = reinterpret_cast<EdgeInfoHal *>(getEdgeMetaData(phy_src_id, ap_pos_dup, stalb_cur));
                    // the condition in while loop true
                    vertex_t stalb_inserted_pos = 0;

                    stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info, ln, phy_src_id,phy_dest_id, me, thread_id);

                    dupInsertionGreaterNewInsertion(prev, dup, dve, stalb_cur,remove_version(reinterpret_cast<vertex_t>(eih)),add_version(reinterpret_cast<vertex_t>(ln)),source_time, ap_pos_prev, stalb_inserted_pos,phy_src_id, thread_id, me);

                }
                else
                {
                    // dup is out-of-order
                    LeafNode *ln_node = reinterpret_cast<LeafNode *>(getEdgeMetaData(phy_src_id, ap_pos_dup, stalb_cur));
                    vertex_t stalb_inserted_pos = 0;
                    stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info, ln, phy_src_id,phy_dest_id, me, thread_id);
                    dupInsertionGreaterNewInsertion(prev, dup, dve, stalb_cur,add_version(reinterpret_cast<vertex_t>(ln_node)),add_version(reinterpret_cast<vertex_t>(ln)),source_time, ap_pos_prev, stalb_inserted_pos,phy_src_id, thread_id, me);

                }
            }
                // dup is deletion
            else
            {

                // dup is delete
                // it means that the new insertion > all insertions in the hash table hence in-order update
                // prev does not have immidiate insertion that arrive before new insertion
                vertex_t  stalb_inserted_pos = 0;
                vertex_t smaller_metadata_block = 0;
                // std::cout<<source_time<<" "<<" "<<index[phy_src_id].getLatestSrcTime()<<std::endl;
                if((!(CheckOutOfOrder(source_time, phy_src_id))) || (!IsOutOfOrder))
                {
                    ei = static_cast<EdgeInfoHal *>(me->Allocate(sizeof(EdgeInfoHal),thread_id));
                    stalb_inserted_pos = inOrderInsertion(commit_time, inputedge_info, ei, phy_src_id, phy_dest_id, me,thread_id);
                    smaller_metadata_block = remove_version(reinterpret_cast<vertex_t>((ei)));
                    //std::cout<<"hello 1"<<" "<<ei<<std::endl;

                }
                else
                {
                    ln = static_cast<LeafNode *>(me->Allocate(sizeof(LeafNode),thread_id));
                    stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info,ln, phy_src_id, phy_dest_id, me, thread_id);
                    smaller_metadata_block = add_version(reinterpret_cast<vertex_t>((ln)));
                    // std::cout<<"hello 2"<<std::endl;
                }


                // std::cout<<"hello 1"<<" "<<ei<<" "<<ln<<std::endl;

                // std::cout<<smaller_metadata_block<<" "<<stalb_inserted_pos<<" "<<std::endl;

                setITMBlockDelete(dup, smaller_metadata_block, thread_id, me);

                if(prev == NULL)
                {
                    if(dup->next == NULL)
                    {
                        dve->next = NULL;
                    }
                    else
                    {
                        dve->next = dup->next;
                    }
                }
                else
                {
                    prev->next = dup->next;
                }
                dve->l_del = remove_version(dup->SrcTime);
                me->free(dup, sizeof(DuplicateUpdate), thread_id);
                *is_degree_add = false;
                // return false;
            }
        }
    }

    // return true;
}
void HBALStore::dveNextNullAndLatestDelGreaterThanInsertion(timestamp_t commit_time, vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info, DVE *dve,int thread_id, MemoryAllocator *me)
{
    // if new insertion < latest edge version deletion source time
    vertex_t  *dup_stalb  = 0;
    vertex_t *prev_stalb  = 0;
    vertex_t *pos_dup     = 0;
    vertex_t *pos_prev    = 0;
    stalb_type *stalb_ln = NULL;
    int *dup_indr_pos = 0;
    int *prev_indr_pos = 0;
    vertex_t latest_Deletion = dve->l_del;
    vertex_t dst_id = phy_dest_id;

    // binary search by  dve->l_del to find dup and prev reference to new_insertion
    findDuplicateInsertionPosInSTALBBinarySearchByLatestDeletion(dup_indr_pos,prev_indr_pos, dup_stalb, prev_stalb, pos_dup, pos_prev, phy_src_id, latest_Deletion, dst_id, inputedge_info);

    if(prev_stalb != 0)
    {
        // prev_stalb exist
        if(dup_stalb != 0)
        {
            // new insertion either duplicate or deleted
            dupPrevExistStalDeleted(commit_time, phy_src_id,  phy_dest_id,  inputedge_info,  dup_stalb,  pos_dup,  dup_indr_pos,  dve, prev_stalb, pos_prev, NULL, NULL, thread_id, me, false);
        }
        else
        {
            // new insertion is duplicate %%%%
            prevExistDupNotExist(commit_time, phy_src_id, phy_dest_id, inputedge_info,  prev_stalb, pos_prev, dve, thread_id, me);
        }
    }
    else
    {
        // prev_stalb does not exist
        DupExistPrevNotExist(commit_time, phy_src_id, phy_dest_id, inputedge_info, dup_stalb,pos_dup, dup_indr_pos, dve, thread_id, me);
    }
}
void HBALStore::dveNextDeletionLessThanLatestDelForInsertion(timestamp_t commit_time,vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info,DVE *dve,int thread_id, MemoryAllocator *me, DuplicateUpdate *dup, DuplicateUpdate *prev)
{
    vertex_t  *dup_stalb  = 0;
    vertex_t *prev_stalb  = 0;
    vertex_t *pos_dup     = 0;
    vertex_t *pos_prev    = 0;
    stalb_type *stalb_ln = NULL;
    int *dup_indr_pos = 0;
    int *prev_indr_pos = 0;
    vertex_t latest_Deletion = remove_version(dup->SrcTime);
    vertex_t dst_id = phy_dest_id;

    // binary search by  dve->l_del to find dup and prev reference to new_insertion
    findDuplicateInsertionPosInSTALBBinarySearchByLatestDeletion(dup_indr_pos,prev_indr_pos, dup_stalb, prev_stalb, pos_dup, pos_prev, phy_src_id, latest_Deletion, dst_id, inputedge_info);

    if(prev_stalb != 0)
    {
        // prev_stalb exist
        if(dup_stalb != 0)
        {
            // new insertion either duplicate or deleted
            dupPrevExistStalDeleted(commit_time, phy_src_id, phy_dest_id, inputedge_info,dup_stalb, pos_dup, dup_indr_pos, dve, prev_stalb,pos_prev, dup, prev, thread_id, me, true);
        }
        else
        {
            // new insertion is duplicate %%%%
            prevExistDupNotExist( commit_time, phy_src_id, phy_dest_id, inputedge_info,  prev_stalb, pos_prev, dve, thread_id, me,true, dup, prev);

        }
    }
}
void HBALStore::dveNextInsertionLessThanLatestDelForInsertion(timestamp_t commit_time,vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info,DVE *dve,int thread_id, MemoryAllocator *me, DuplicateUpdate *dup, DuplicateUpdate *prev)
{
    vertex_t  *dup_stalb  = 0;
    vertex_t *prev_stalb  = 0;
    vertex_t *pos_dup     = 0;
    vertex_t *pos_prev    = 0;
    stalb_type *stalb_ln = NULL;
    int *dup_indr_pos = 0;
    int *prev_indr_pos = 0;
    vertex_t latest_Deletion = remove_version(dup->SrcTime);
    vertex_t dst_id = phy_dest_id;

    // binary search by  dve->l_del to find dup and prev reference to new_insertion
    findDuplicateInsertionPosInSTALBBinarySearchByLatestDeletion(dup_indr_pos,prev_indr_pos, dup_stalb, prev_stalb, pos_dup, pos_prev, phy_src_id, latest_Deletion, dst_id, inputedge_info);

    if(prev_stalb != 0)
    {
        // prev_stalb exist
        if(dup_stalb != 0)
        {
            // new insertion either duplicate or deleted
            dupPrevExistStalDeleted( commit_time,phy_src_id, phy_dest_id, inputedge_info,dup_stalb, pos_dup, dup_indr_pos, dve, prev_stalb,pos_prev, dup, prev, thread_id, me, false);
        }
        else
        {
            // new insertion is duplicate %%%%
            prevExistDupNotExist(commit_time, phy_src_id, phy_dest_id, inputedge_info,  prev_stalb, pos_prev, dve, thread_id, me,false, dup, prev);
        }
    }
}
void HBALStore::dveNextExistInsertion(timestamp_t commit_time, vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info,DVE *dve,int thread_id, MemoryAllocator *me)
{
    timestamp_t source_time = getTimestampSrc(inputedge_info, IsOutOfOrder);
    DuplicateUpdate *dup = dve->next;
    DuplicateUpdate *prev = NULL;
    AdjacentPos *ap_pos_dup;
    AdjacentPos *ap_pos_prev;
    while(dup != NULL)
    {
        if(remove_version(dup->SrcTime) > source_time)
        {
            break;
        }
        prev = dup;
        dup = dup->next;
    }

    if(dup != NULL)
    {
        if(check_version(dup->SrcTime))
        {
            // dup is deletion
            if(remove_version(dup->SrcTime) < dve->l_del)
            {
                // remove_version(dup->SrcTime) < dve->l_del
                dveNextDeletionLessThanLatestDelForInsertion( commit_time, phy_src_id, phy_dest_id, inputedge_info,dve,thread_id, me, dup, prev);
            }
            else
            {
                // similar flow like dve->next = NULL
                // when remove_version(dup->SrcTime) > dve->l_del
                dveNextNullAndLatestDelGreaterThanInsertion(commit_time, phy_src_id, phy_dest_id, inputedge_info, dve,thread_id, me);
            }
        }
        else
        {
            // dup is duplicate
            if(remove_version(dup->SrcTime) < dve->l_del)
            {
                // remove_version(dup->SrcTime) < dve->l_del
                //dveNextDeletionLessThanLatestDelForInsertion(phy_src_id, phy_dest_id, inputedge_info,dve,thread_id, me, dup, prev);
                dveNextInsertionLessThanLatestDelForInsertion(commit_time, phy_src_id, phy_dest_id, inputedge_info, dve, thread_id, me, dup, prev);
            }
            else
            {
                // similar flow like dve->next = NULL
                // when remove_version(dup->SrcTime) > dve->l_del
                dveNextNullAndLatestDelGreaterThanInsertion(commit_time, phy_src_id, phy_dest_id, inputedge_info, dve,thread_id, me);
            }
        }
    }
    else
    {
        // similar flow like dve->next = NULL
        // when remove_version(dup->SrcTime) > dve->l_del
        dveNextNullAndLatestDelGreaterThanInsertion(commit_time, phy_src_id, phy_dest_id, inputedge_info, dve,thread_id, me);
    }
}


void* HBALStore::rollback()
{
    return 0;
}

void HBALStore::upi_zero_insertion_edge(timestamp_t commit_time, vertex_t phy_src_id, vertex_t phy_dest_id,InputEdge inputedge_info,AdjacentPos *ap_pos,vertex_t *pos_ooo, MemoryAllocator *me, int thread_id)
{
    // upi^0 exist
    EdgeInfoHal *ei;
    LeafNode *ln;
    timestamp_t source_time = getTimestampSrc(inputedge_info, IsOutOfOrder);
    if(!check_version_read(ap_pos->edgeBlockIndex))
    {
        // is in-order insertion store in the HT
        stalb_type *stalb_cur;
        stalb_type  *ln_stalb;
        EdgeInfoHal *eih = reinterpret_cast<EdgeInfoHal *>(getEdgeMetaData(phy_src_id, ap_pos, stalb_cur));

        vertex_t src_timestamp = eih->getSTime();

        // in-order flow consider cur-ei as duplicate
        vertex_t  stalb_inserted_pos = 0;

        if(!(CheckOutOfOrder(source_time, phy_src_id)) || (!IsOutOfOrder))
        {
            ei = static_cast<EdgeInfoHal *>(me->Allocate(sizeof(EdgeInfoHal),thread_id));
            stalb_inserted_pos = inOrderInsertion( commit_time,inputedge_info, ei, phy_src_id, phy_dest_id, me, thread_id, false);
            // hence new_ei.src > cur_ei.src
            setITMBlockDuplicate(add_version(reinterpret_cast<vertex_t>(stalb_cur)),remove_version(reinterpret_cast<vertex_t>(ei)),remove_version(reinterpret_cast<vertex_t>(eih)), thread_id, me, true, false,ap_pos->edgeBlockIndex);
        }
        else
        {
            // comparison of cur_ei.src > new_ei.src
            if(src_timestamp > source_time)
            {
                ln = static_cast<LeafNode *>(me->Allocate(sizeof(LeafNode),thread_id));
                stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info,ln, phy_src_id, phy_dest_id, me, thread_id, true, ln_stalb);

                setITMBlockDuplicate(remove_version((reinterpret_cast<vertex_t>(ln_stalb))),remove_version(reinterpret_cast<vertex_t>(eih)),add_version(reinterpret_cast<vertex_t>(ln)), thread_id, me, false, false);
            }
            else
            {
                ln = static_cast<LeafNode *>(me->Allocate(sizeof(LeafNode),thread_id));
                stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info,ln, phy_src_id, phy_dest_id, me, thread_id);

                setITMBlockDuplicate(add_version((reinterpret_cast<vertex_t>(stalb_cur))),add_version(reinterpret_cast<vertex_t>(ln)),remove_version(reinterpret_cast<vertex_t>(eih)), thread_id, me, true,false, ap_pos->edgeBlockIndex);
            }
        }

        DVE *dve = static_cast<DVE *>(me->Allocate(sizeof(DVE),thread_id));

        //dve->Dnode = phy_dest_id;
        dve->oldest_version = 0;
        dve->l_del = 0;

        DuplicateUpdate *d_old = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));
        DuplicateUpdate *d_new = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));
        d_old->SrcTime   = eih->getSTime();
        d_new->SrcTime   = source_time;
        d_old->UpdatePos = ap_pos->edgeBlockIndex;
        d_new->UpdatePos = stalb_inserted_pos;

        index[phy_src_id].hashPtr->hashtable[ap_pos->hashTableIndex] = check_version(reinterpret_cast<vertex_t>(dve));

        if(source_time > eih->getSTime())
        {
            // new > oldest
            d_old->next = d_new;
            dve->next = d_old;
        }
        else
        {
            // new < oldest
            d_new->next = d_old;
            dve->next = d_new;
        }
    }
    else
    {
        stalb_type *stalb_cur;
        stalb_type  *ln_stalb;
        // out-of-order update store in the HT
        LeafNode *ln_node = reinterpret_cast<LeafNode *>(getEdgeMetaData(phy_src_id, ap_pos, stalb_cur));

        vertex_t src_timestamp = ln_node->getSTime();

        // in-order flow consider cur-ei as duplicate
        vertex_t  stalb_inserted_pos = 0;

        if(!(CheckOutOfOrder(source_time, phy_src_id)) || (!IsOutOfOrder))
        {
            ei = static_cast<EdgeInfoHal *>(me->Allocate(sizeof(EdgeInfoHal),thread_id));
            stalb_inserted_pos = inOrderInsertion(commit_time, inputedge_info, ei, phy_src_id, phy_dest_id, me, thread_id, false);

            setITMBlockDuplicate(add_version((reinterpret_cast<vertex_t>(stalb_cur))),
                                 remove_version(reinterpret_cast<vertex_t>(ei)),
                                 add_version(reinterpret_cast<vertex_t>(ln_node)), thread_id, me, false, false,
                                 *pos_ooo);
        }
        else
        {
            // comparison of cur_ei.src > new_ei.src
            if(src_timestamp > source_time)
            {
                ln = static_cast<LeafNode *>(me->Allocate(sizeof(LeafNode),thread_id));
                stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info,ln, phy_src_id, phy_dest_id, me, thread_id, true, ln_stalb);

                setITMBlockDuplicate(remove_version((reinterpret_cast<vertex_t>(ln_stalb))),add_version(reinterpret_cast<vertex_t>(ln_node)),add_version(reinterpret_cast<vertex_t>(ln)), thread_id, me, false, false);

            }
            else
            {
                ln = static_cast<LeafNode *>(me->Allocate(sizeof(LeafNode),thread_id));
                stalb_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info,ln, phy_src_id, phy_dest_id, me, thread_id);

                setITMBlockDuplicate(add_version((reinterpret_cast<vertex_t>(stalb_cur))),
                                     add_version(reinterpret_cast<vertex_t>(ln)),
                                     add_version(reinterpret_cast<vertex_t>(ln_node)), thread_id, me, false,
                                     false, *pos_ooo);

            }
        }

        // insert it into hashtable duplicate insertion
        DVE *dve = static_cast<DVE *>(me->Allocate(sizeof(DVE),thread_id));
        // dve->Dnode = phy_dest_id;
        dve->oldest_version = 0;
        dve->l_del = 0;

        DuplicateUpdate *d_old = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));
        DuplicateUpdate *d_new = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));
        d_old->SrcTime   = ln_node->getSTime();
        d_new->SrcTime   = source_time;
        d_old->UpdatePos = ap_pos->edgeBlockIndex;
        d_new->UpdatePos = stalb_inserted_pos;

        index[phy_src_id].hashPtr->hashtable[ap_pos->hashTableIndex] = check_version(reinterpret_cast<vertex_t>(dve));

        if(source_time > ln_node->getSTime())
        {
            // new > oldest
            d_old->next = d_new;
            dve->next = d_old;
        }
        else
        {
            // new < oldest
            d_new->next = d_old;
            dve->next = d_new;
        }

    }
}

void HBALStore::executeEdgeInsertion(InputEdge inputedge_info, MemoryAllocator *me, int thread_id)
{
    InsertSrcVertex(inputedge_info.getSrcVertexId(), me, thread_id);
    InsertDestVertex(inputedge_info.getDestVertexId(), me, thread_id);

    vertex_t phy_src_id  = physical_id(inputedge_info.getSrcVertexId());
    vertex_t phy_dest_id = physical_id(inputedge_info.getDestVertexId());
    //std::cout<<"Hello 2"<<std::endl;

    index[phy_src_id].perVertexLock.lock();
    index[phy_dest_id].perVertexLock.lock();

    timestamp_t commit_time = getCurrTimeStamp();
    InsertEdge(phy_src_id, phy_dest_id ,inputedge_info, commit_time, me, thread_id);
    //std::cout<<"Hello 5"<<std::endl;

    InsertEdge(phy_dest_id,phy_src_id, {inputedge_info.getDestVertexId(), inputedge_info.getSrcVertexId(), inputedge_info.getUpdtSrcTime(),inputedge_info.getUpdtWeight()}, commit_time, me, thread_id);
    // std::cout<<"Hello 4"<<std::endl;

    index[phy_src_id].perVertexLock.unlock();
    index[phy_dest_id].perVertexLock.unlock();

}

// insert edge
void HBALStore::InsertEdge(vertex_t phy_src_id,vertex_t phy_dest_id, InputEdge inputedge_info,timestamp_t commit_time, MemoryAllocator *me, int thread_id)
{

    bool in_order_update = true;
    bool isAddDegreeVersion = true;
    //  double weight_w = inputedge_info.getUpdtWeight();
    double weight_w = 0;//inputedge_info.getUpdtWeight();

    // least significant 10 bits extract from destnode to store in hash table
    //vertex_t phy_src_id  = physical_id(inputedge_info.getSrcVertexId());
    //vertex_t phy_dest_id = physical_id(inputedge_info.getDestVertexId());

    AdjacentPos ap;
    ap.edgeBlockIndex = FLAG_EMPTY_SLOT_HT;
    ap.indrIndex      = FLAG_EMPTY_SLOT_HT;
    ap.hashTableIndex = FLAG_EMPTY_SLOT_HT;

    AdjacentPos *ap_pos;

    uint64_t  *pos_ooo;
    timestamp_t source_time = getTimestampSrc(inputedge_info, IsOutOfOrder);

    if(index[phy_src_id].getHashTable() != NULL)
    {
        ap_pos = index[phy_src_id].getDestNodeHashTableVal(phy_dest_id, &ap, pos_ooo);
        // ap_pos = &ap;
    }
    else
    {
        ap_pos = &ap;
    }
    // No edge entry exist in the hash table
    if (ap_pos->edgeBlockIndex == FLAG_EMPTY_SLOT_HT)
    {
        // perThreadNumberOfEdges[thread_id]++;
        EdgeInfoHal *ei = NULL;
        LeafNode *ln = NULL;
        if((!(CheckOutOfOrder(source_time, phy_src_id))) || (!IsOutOfOrder))
        {
            ei = static_cast<EdgeInfoHal *>(me->Allocate(sizeof(EdgeInfoHal),thread_id));
            vertex_t  stalb_inserted_pos = inOrderInsertion(commit_time, inputedge_info,ei, phy_src_id,  phy_dest_id, me, thread_id);
            index[phy_src_id].setDestNodeHashTableVal(phy_dest_id, stalb_inserted_pos, me, thread_id);
        }
        else
        {
            // out of order insertion
            ln = static_cast<LeafNode *>(me->Allocate(sizeof(LeafNode),thread_id));
            vertex_t  ooo_inserted_pos = outOfOrderInsertion(commit_time, inputedge_info,ln, phy_src_id, phy_dest_id, me, thread_id);
            index[phy_src_id].setDestNodeHashTableVal(phy_dest_id, ooo_inserted_pos, me, thread_id);
            // add degree version
        }
    }
    else
    {

        // std::cout<<"!(ap_pos->edgeBlockIndex == FLAG_EMPTY_SLOT_HT)"<<std::endl;

        //  ############
        bool isInOrder = false;
        timestamp_t rts = MaxValue;
        bool readFlag = false;

        // due to resize of indr array the block_index size that store in hash table bucket could change there position for that we use formula
        // current block len (indr array) - (indrsize size at the time of insertion - the block index of insertion)
        // check if the deletion dest node entry in order or ooo
        // ############

        if(!check_version(ap_pos->edgeBlockIndex)) // upi^0
        {

            //std::cout<<"!check_version(ap_pos->edgeBlockIndex)"<<std::endl;
            // std::cout << inputedge_info.getSrcVertexId() << " " << inputedge_info.getDestVertexId() << " " << inputedge_info.getUpdtSrcTime()<< std::endl;

            // if upi exist
            upi_zero_insertion_edge(commit_time, phy_src_id, phy_dest_id, inputedge_info,ap_pos, pos_ooo, me,  thread_id);
        }
        else
        {
            // if EdgeversionInfo exist in the HT for specific edge // upi^1
            if(check_version_read(ap_pos->edgeBlockIndex))
            {
                DestGc *dve_gc = reinterpret_cast<DestGc *>(remove_version(remove_version_read(ap_pos->edgeBlockIndex)));
                if(source_time < dve_gc->oldest_version)
                {
                    // std::cout << "rollback 1: " <<std::endl;
                    rollback();
                }
                else
                {
                    vertex_t  stalb_inserted_pos = 0;
                    EdgeInfoHal *ei = NULL;
                    LeafNode *ln = NULL;
                    //  timestamp_t source_time = getTimestampSrc(inputedge_info, IsOutOfOrder);
                    if((!CheckOutOfOrder(source_time, phy_src_id))|| (!IsOutOfOrder))
                    {
                        ei = static_cast<EdgeInfoHal *>(me->Allocate(sizeof(EdgeInfoHal),thread_id));
                        stalb_inserted_pos = inOrderInsertion(commit_time, inputedge_info, ei, phy_src_id, phy_dest_id, me,thread_id);
                    }
                    else
                    {
                        ln = static_cast<LeafNode *>(me->Allocate(sizeof(LeafNode),thread_id));
                        stalb_inserted_pos = outOfOrderInsertion(commit_time,inputedge_info,ln, phy_src_id, phy_dest_id, me, thread_id);
                    }

                    DuplicateUpdate *d_new = static_cast<DuplicateUpdate *>(me->Allocate(sizeof(DuplicateUpdate),thread_id));
                    d_new->SrcTime   = source_time;
                    d_new->UpdatePos = stalb_inserted_pos;
                    d_new->next = NULL;

                    //  dve->oldest_version = 0; //
                    DVE *dve = static_cast<DVE *>(me->Allocate(sizeof(DVE), thread_id));
                    dve->oldest_version = dve_gc->oldest_version;
                    dve->Dnode = remove_version(dve_gc->Dnode);
                    dve->l_del = 0;
                    dve->next = d_new;

                    index[phy_src_id].getHashTable()->hashtable[ap_pos->hashTableIndex] = add_version(reinterpret_cast<vertex_t>(dve));
                    me->free(dve_gc, sizeof(DestGc), thread_id);
                }
            }
            else
            {
                // std::cout<<"insertion dve entr"<<std::endl;
                DVE *dve = reinterpret_cast<DVE *>(remove_version(ap_pos->edgeBlockIndex));
                if (source_time < dve->oldest_version) {
                    //  std::cout<<"dve->oldest_version"<<std::endl;

                    // the new insertion is < than their oldest edge version garbage collection
                    // rollback whole transaction
                    // std::cout << "rollback 1: " << source_time << " " << dve->oldest_version << std::endl;
                    rollback();
                } else {

                    // new insertion > oldest edge version garbage collected
                    if (source_time >= dve->l_del) {
                        //%%%%%% new insertion source time > latest deletion occured in the database .. start
                        // std::cout<<"source_time >= dve->l_del"<<std::endl;

                        newInsertionSrctimeGreaterThanLatestDeletion(commit_time, &isAddDegreeVersion, phy_src_id,phy_dest_id, inputedge_info, dve, thread_id, me);

                        // %%%%%% inputedge_info.getUpdtSrcTime() > dve->l_del end
                    } else {
                        //std::cout << "hello dve->next" << std::endl;
                        if (dve->next == NULL) {
                            // if new insertion < latest edge version deletion source time
                            dveNextNullAndLatestDelGreaterThanInsertion(commit_time, phy_src_id, phy_dest_id,
                                                                        inputedge_info, dve, thread_id, me);
                        } else {
                            // dve->next exist
                            dveNextExistInsertion(commit_time, phy_src_id, phy_dest_id, inputedge_info, dve, thread_id,
                                                  me);
                        }
                    }
                }
            }
        }

    }

    if(isAddDegreeVersion)
    {
        addDegreeVersion(commit_time, phy_src_id, phy_dest_id, me, thread_id);
    }

    // index[phy_src_id].perVertexLock.unlock();

    // todo end
}
template <typename Key, typename Value>
std::size_t estimate_concurrent_hash_map_size(const tbb::concurrent_hash_map<Key, Value>& map) {
    std::size_t estimated_size = 0;

    // Estimate size of keys and values
    for (const auto& item : map) {
        estimated_size += sizeof(item.first) + sizeof(item.second);
    }

    // Estimate size of internal data structures
    estimated_size += sizeof(map);

    return estimated_size;
}

void HBALStore::getMemoryConsumptionBreakDown(MemoryAllocator *ga)
{
    uint64_t total_space = 0;
    uint64_t VA_space = 0;
    uint64_t HT_space = 0;
    uint64_t STAL_space = 0;
    uint64_t num_of_edges_tot = 0;
    uint64_t HT_empty_space = 0;
    uint64_t HT_empty_space_block1 = 0;
    uint64_t HT_empty_space_block2 = 0;
    uint64_t HT_empty_space_block3 = 0;
    uint64_t HT_empty_space_block_double = 0;

    uint64_t HT_space_block1 = 0;
    uint64_t HT_space_block2 = 0;
    uint64_t HT_space_block3 = 0;
    uint64_t HT_space_block_double = 0;

    uint64_t ht_num_block1 = 32;
    uint64_t ht_num_block2 = 65536ul;
    uint64_t ht_num_block3 = 131072ul;


    for(int i=0;i<high_water_mark;i++)
    {
        // get VA size
        total_space += sizeof(PerSourceVertexIndr);
        VA_space += sizeof(PerSourceVertexIndr);
        if(index[i].getEdgePtr() != NULL)
        {
            //index[i].getDegree()
            num_of_edges_tot += perSourceGetDegree(i, 0, NULL);
            // get Hash table size
            HT_space += sizeof(HashTablePerSource);
            HT_space += (index[i].getHashTable()->hashBlockSize * 8);

            if(index[i].getHashTable()->hashBlockSize <= ht_num_block1)
            {
                HT_empty_space_block1 += (index[i].getHashTable()->hashBlockSize - index[i].getHashTable()->entryCounter);
                HT_space_block1 += index[i].getHashTable()->hashBlockSize;
            }
            else if(index[i].getHashTable()->hashBlockSize > ht_num_block1 && index[i].getHashTable()->hashBlockSize <= ht_num_block2)
            {
                HT_empty_space_block2 += (index[i].getHashTable()->hashBlockSize - index[i].getHashTable()->entryCounter);
                HT_space_block2 += index[i].getHashTable()->hashBlockSize;
            }
            else if(index[i].getHashTable()->hashBlockSize > ht_num_block2 && index[i].getHashTable()->hashBlockSize <= ht_num_block3)
            {
                HT_empty_space_block3 += (index[i].getHashTable()->hashBlockSize - index[i].getHashTable()->entryCounter);
                HT_space_block3 += index[i].getHashTable()->hashBlockSize;
            }
            else
            {
                HT_empty_space_block_double += (index[i].getHashTable()->hashBlockSize - index[i].getHashTable()->entryCounter);
                HT_space_block_double += index[i].getHashTable()->hashBlockSize;
            }

            total_space += sizeof(HashTablePerSource);
            total_space += (index[i].getHashTable()->hashBlockSize * 8);

            // get adjacency list size
            if (!(index[i].edgePtr & 0x1)) {
                PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[i].getEdgePtr());
                STAL_space += sizeof(PerSourceIndrPointer);
                total_space += sizeof(PerSourceIndrPointer);
                STAL_space += ((u_int64_t) 1 << ind_ptr->getBlockLen()) * 8;
                total_space += ((u_int64_t) 1 << ind_ptr->getBlockLen()) * 8;

                int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();

                // deletion bit per source-vertex not active direct read L2 vector
                for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                {

                    stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                    int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k))/2; // get block size
                    uint64_t block_entries_size = (l2_len * 8);
                    uint64_t block_property_size = (l2_len * 8) / 2;

                    STAL_space += block_entries_size;
                    STAL_space += block_property_size;
                    total_space += block_entries_size;
                    total_space += block_property_size;
                }

            }
            else
            {

                stalb_type *eb_k = static_cast<stalb_type *>(index[i].getEdgePtr());

                int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k));
                uint64_t block_entries_size = (l2_len * 8);
                uint64_t block_property_size = (l2_len * 8) / 2;
                STAL_space += block_entries_size;
                STAL_space += block_property_size;
                total_space += block_entries_size;
                total_space += block_property_size;
                // #### per source edge reading end
            }
        }
    }

    uint64_t  tot_num_edges = num_of_edges_tot;
    uint64_t entries_size = tot_num_edges * 16;
    entries_size += (tot_num_edges * sizeof(EdgeInfoHal));

    uint64_t with_IEM_SIZE_STAL_space = STAL_space + (tot_num_edges * sizeof(EdgeInfoHal));

    total_space += (tot_num_edges * sizeof(EdgeInfoHal));

    /// phy to logical and logical to physical
    std::size_t size_estimate_ht = estimate_concurrent_hash_map_size(logical_to_physical);

    VA_space += size_estimate_ht;
    VA_space += index.capacity() * 8;
    VA_space += physical_to_logical.capacity() * 8;

    total_space += size_estimate_ht;
    total_space += index.capacity() * 8;
    total_space += physical_to_logical.capacity() * 8;

    //
    uint64_t free_space = 0;
    uint64_t alloc_space = 0;
    uint64_t alloc_space_act_block = 0;
    for(int i=0;i<40;i++)
    {
        free_space += ga->perThreadArr[i]->getPerThreadFreeMemory();
        alloc_space += ga->perThreadArr[i]->getPerThreadAllocMemory();
        alloc_space_act_block += ga->perThreadArr[i]->getPerThreadAllocMemoryBlock();
    }
    //


    // uint64_t number_of_edges =
    testLock.lock();
    std::cout<< " Total space taken by data structure :"<<total_space<<std::endl;
    std::cout<< " Space taken by HT :"<<HT_space<<std::endl;
    //
    std::cout<< " empty Space in HT block1 :"<<HT_empty_space_block1 * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)
    //
    std::cout<< " empty Space in HT block2 :"<<HT_empty_space_block2 * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)
    std::cout<< " empty Space in HT block3 :"<<HT_empty_space_block3 * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)
    std::cout<< " empty Space in HT blockdouble :"<<HT_empty_space_block_double * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)

    std::cout<< " Space in HT block1 :"<<HT_space_block1 * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)
    //
    std::cout<< " Space in HT block2 :"<<HT_space_block2 * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)
    std::cout<< " Space in HT block3 :"<<HT_space_block3 * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)
    std::cout<< " Space in HT blockdouble :"<<HT_space_block_double * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)



    std::cout<< " Space taken by VA :"<<VA_space<<std::endl;
    std::cout<< " Space taken by STAL :"<<STAL_space<<std::endl;
    std::cout<< " Space taken by STAL with IEM :"<<with_IEM_SIZE_STAL_space<<std::endl;
    std::cout<< " Space taken by  VA logical to phy hash table :"<<size_estimate_ht<<std::endl;
    std::cout<< " Space taken by  phy to log vector :"<<physical_to_logical.capacity() * 8<<std::endl;
    std::cout<< " Space taken by  VA index :"<<index.capacity() * 8<<std::endl;
    std::cout<< " Space taken by  entries size edges :"<<entries_size<<std::endl;
    std::cout<< " IEM entry size :"<<tot_num_edges<<" "<<(tot_num_edges * sizeof(EdgeInfoHal))<<std::endl;
    std::cout<< " free space :"<<free_space<<std::endl;
    std::cout<< " Alloc space :"<<alloc_space<<std::endl;
    std::cout<< " Actual Alloc space :"<<alloc_space_act_block<<std::endl;
    testLock.unlock();


}
////
void HBALStore::getMemoryConsumptionBreakDownUpdates(MemoryAllocator *ga)
{

    uint64_t total_entry_stored_in_ht = 0;
    uint64_t  num_of_duplicates = 0;
    uint64_t total_entry_stored_in_ht_with_dve = 0;
    uint64_t total_entry_stored_in_ht_with_dve_insertion = 0;

    uint64_t total_entry_stored_in_ht_with_dve_space = 0;

    uint64_t total_space = 0;
    uint64_t VA_space = 0;
    uint64_t HT_space_tot = 0;
    uint64_t HT_space_arr = 0;
    uint64_t STAL_space = 0;
    uint64_t max_hal_entries = 0;
    uint64_t invalid_block_space = 0;
    uint64_t STAL_iem_entries_space = 0;
    uint64_t iem_deleted_not_garbage_collect = 0;
    uint64_t HT_empty_space_block1 = 0;
    uint64_t HT_empty_space_block2 = 0;
    uint64_t HT_empty_space_block3 = 0;
    uint64_t HT_empty_space_block_double = 0;

    uint64_t HT_space_block1 = 0;
    uint64_t HT_space_block2 = 0;
    uint64_t HT_space_block3 = 0;
    uint64_t HT_space_block_double = 0;

    uint64_t ht_num_block1 = 32;
    uint64_t ht_num_block2 = 16384ul;
    uint64_t ht_num_block3 = 131072ul;
    uint64_t total_entry_stored_in_ht_with_gc_dest_space = 0;
    uint64_t total_entry_stored_in_ht_with_gc_dest = 0;
    uint64_t  max_hal_space = 0;
    uint64_t num_of_edges_tot = 0;
    uint64_t  upi_count = 0;
    uint64_t total_dest_ids = 0;

    for(int i=0;i<high_water_mark;i++)
    {
        // get VA size
        total_space += sizeof(PerSourceVertexIndr);
        VA_space += sizeof(PerSourceVertexIndr);
        if(index[i].getEdgePtr() != NULL)
        {
            //index[i].getDegree()
            num_of_edges_tot += perSourceGetDegree(i, 0, NULL);

            // **** invalid block
            // inputedge_info
            //  InvalidLinkListNode *newInval = (InvalidLinkListNode *) (me->Allocate(sizeof(InvalidLinkListNode), thread_id));
            InvalidListVal *inv_per_src = reinterpret_cast<InvalidListVal *>(index[i].invalidBlock);
            if(inv_per_src != NULL)
            {
                invalid_block_space += sizeof(InvalidListVal);
                total_space += sizeof(InvalidListVal);
                InvalidLinkListNode *newInval = inv_per_src->head;
                while(newInval != NULL)
                {
                    invalid_block_space += sizeof(InvalidLinkListNode);
                    total_space         += sizeof(InvalidLinkListNode);
                    newInval = newInval->next;
                }
            }
            // ***** invalid block memory calculation

            // ********** get Hash table size //
            HT_space_tot += sizeof(HashTablePerSource);
            HT_space_tot += (index[i].getHashTable()->hashBlockSize * 8);
            HT_space_arr += sizeof(HashTablePerSource);
            HT_space_arr += (index[i].getHashTable()->hashBlockSize * 8);
            total_space += sizeof(HashTablePerSource);
            total_space += (index[i].getHashTable()->hashBlockSize * 8);
            if(max_hal_space < index[i].getHashTable()->hashBlockSize)
            {
                max_hal_space = index[i].getHashTable()->hashBlockSize;
                max_hal_entries = index[i].getHashTable()->entryCounter;
            }

            vertex_t half = (u_int32_t) index[i].getHashTable()->hashBlockSize >> 1;
            vertex_t  fifty_perc__hash_table_size = half;
            vertex_t  twnty_five_hash_table_size = half + (half/2);
            vertex_t  tweleve_five_hash_table_size = twnty_five_hash_table_size + (half/4) ;

            if(index[i].getHashTable()->hashBlockSize <= ht_num_block1)
            {
                HT_empty_space_block1 += (index[i].getHashTable()->hashBlockSize - index[i].getHashTable()->entryCounter);
                HT_space_block1 += (index[i].getHashTable()->hashBlockSize);

            }
            else if(index[i].getHashTable()->hashBlockSize > ht_num_block1 && index[i].getHashTable()->hashBlockSize <= ht_num_block2)
            {
                HT_empty_space_block2 += (index[i].getHashTable()->hashBlockSize - index[i].getHashTable()->entryCounter) - (half/4);
                HT_space_block2 += (index[i].getHashTable()->hashBlockSize);
            }
            else if(index[i].getHashTable()->hashBlockSize > ht_num_block2 && index[i].getHashTable()->hashBlockSize <= ht_num_block3)
            {
                HT_empty_space_block3 += (index[i].getHashTable()->hashBlockSize - index[i].getHashTable()->entryCounter) - ((half/2));
                HT_space_block3 += (index[i].getHashTable()->hashBlockSize);
            }
            else
            {
                HT_empty_space_block_double += (index[i].getHashTable()->hashBlockSize - index[i].getHashTable()->entryCounter) -(half);
                HT_space_block_double += (index[i].getHashTable()->hashBlockSize );

            }
            // ###########
            std::set<vertex_t> numbers;
            for(int h=0;h<index[i].getHashTable()->hashBlockSize;h++)
            {
                u_int64_t pos_adj = index[i].getHashTable()->hashtable[h];
                // get pos_adj bit position to compare
                if(pos_adj != FLAG_EMPTY_SLOT_HT)
                {
                    if(pos_adj == 0)
                    {
                        std::cout<<"adj_pos is 0"<<std::endl;
                    }
                    total_entry_stored_in_ht++;
                    if (check_version(pos_adj))
                    {
                        if(!check_version_read(pos_adj))
                        {
                            total_entry_stored_in_ht_with_dve++;
                            DVE *dve_dest = reinterpret_cast<DVE *>(remove_version(pos_adj));
                            numbers.insert(remove_version(dve_dest->Dnode));
                            HT_space_tot += sizeof(DVE);
                            total_space += sizeof(DVE);
                            // numbers.push_back(remove_version(dve_dest->Dnode));
                            total_entry_stored_in_ht_with_dve_space += sizeof(DVE);
                            if (dve_dest->next != NULL)
                            {
                                total_entry_stored_in_ht_with_dve_insertion++;
                                HT_space_tot += sizeof(DuplicateUpdate);
                                total_space += sizeof(DuplicateUpdate);
                                total_entry_stored_in_ht_with_dve_space += sizeof(DuplicateUpdate);
                            }
                        }
                        else
                        {
                            DestGc *dve_dest = reinterpret_cast<DestGc *>(remove_version_read(remove_version(pos_adj)));
                            numbers.insert(remove_version(dve_dest->Dnode));
                            //
                            // total_entry_stored_in_ht_with_gc_dest_space
                            // total_entry_stored_in_ht_with_gc_dest
                            total_entry_stored_in_ht_with_gc_dest++;
                            //  DestGc *dve_dest = reinterpret_cast<DestGc *>(remove_version_read(remove_version(pos_adj)));
                            HT_space_tot += sizeof(DestGc);
                            total_space += sizeof(DestGc);
                            // numbers.push_back(remove_version(dve_dest->Dnode));
                            total_entry_stored_in_ht_with_gc_dest_space += sizeof(DestGc);

                        }
                    }
                    else
                    {
                        //std::cout<<"UPI"<<std::endl;
                        upi_count++;
                        /* u_int64_t is_ooo = getBitMask(pos_adj, IsOutOrderStart, IsOutOrderBits);
                         if (!is_ooo)
                         {

                             // issue is here degree > ((uint32_t) 1 << StartFixedBlockThreshold)-1)
                             if (!(index[i].edgePtr & 0x1))
                             {
                                 // should see the degree part issue is upward
                                 PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(index[i].edgePtr));

                                 u_int64_t indr_index = getBitMask(pos_adj, L1IndexStart, L1IndexBits);
                                 u_int64_t indr_size = (u_int64_t) 1 << getBitMask(pos_adj, L1SizeStart, L1SizeBits);

                                 // lock on per source vertex to get the index_block edge block
                                 u_int64_t index_block = (((u_int64_t) 1 << ptr_ind->getBlockLen()) - (indr_size - indr_index));

                                 u_int64_t edge_block_index = getBitMask(pos_adj, L2IndexStart, L2IndexBits);

                                 u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(pos_adj, L2SizeStart, L2SizeBits);
                                 // per block deletion lock

                                 u_int64_t cur_edge_block_size = (u_int64_t) 1
                                         << *reinterpret_cast<uint8_ptr>(ptr_ind->getperVertexBlockArr()[index_block]); //->getBlockSize();

                                 u_int64_t index_edge_block = ((cur_edge_block_size / 2) - ((edge_block_size / 2) - edge_block_index));


                                 ap->indrIndex = index_block;
                                 ap->edgeBlockIndex = index_edge_block;
                                 ap->hashTableIndex = FLAG_EMPTY_SLOT;
                                 return ap;
                             }
                             else
                             {
                                 // edge block entry
                                 // edge block index remove
                                 stalb_type *ptr_edge;

                                 ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(index[i].edgePtr));
                                 //   flagp = 0;
                                 // }

                                 u_int64_t edge_block_index = getBitMask(pos_adj, L2IndexStart, L2IndexBits);

                                 u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(pos_adj, L2SizeStart, L2SizeBits);

                                 // per block deletion lock
                                 u_int64_t cur_edge_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_edge);

                                 u_int64_t index_edge_block = ((cur_edge_block_size / 2) - ((edge_block_size / 2) - edge_block_index));

                                 ap->edgeBlockIndex = index_edge_block;
                                 ap->hashTableIndex = FLAG_EMPTY_SLOT;
                                 return ap;

                             }
                         }
                         else
                         {
                             if (!(edgePtr & 0x1))
                             {

                                 PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

                                 timestamp_t src_timestamp = getBitMask(pos_adj, t_s_start, t_s_bits);

                                 stalb_type *perArt;

                                 vertex_t ooo_field = (getOutOfOrderDestNode(src_timestamp, 1, ptr_ind));

                                 if (!check_version(ooo_field))
                                 {
                                     perArt = reinterpret_cast<stalb_type *>(remove_version(ooo_field));
                                 }

                                 /// binary search to find the key
                                 u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(perArt);

                                 uint64_t pos = findIndexForBlockArrOOO(perArt, *reinterpret_cast<uint16_ptr>(perArt + 1),
                                                                        (eb_block_size_ooo / 2), src_timestamp);

                                 LeafNode *leaf = *reinterpret_cast<LeafNode **>((perArt) +
                                                                                 ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(perArt)) /
                                                                                   2) * 8) + ((pos * 8) - 8));

                                 *pos_ooo = pos;
                                 ap->indrIndex = add_version(reinterpret_cast<vertex_t>(perArt));
                                 ap->edgeBlockIndex = add_version_read(reinterpret_cast<vertex_t>(leaf));;
                                 ap->hashTableIndex = FLAG_EMPTY_SLOT;
                                 return ap;
                             }
                             else
                             {

                                 stalb_type *ptr_edge;

                                 ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(edgePtr));

                                 timestamp_t src_timestamp = getBitMask(pos_adj, t_s_start, t_s_bits);

                                 stalb_type *perArt;
                                 vertex_t ooo_field = (getOutOfOrderDestNode(src_timestamp, 0, ptr_edge));
                                 if (!check_version(ooo_field)) {
                                     perArt = reinterpret_cast<stalb_type *>(remove_version(ooo_field));
                                 }
                                 /// binary search to find the key
                                 u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(perArt);
                                 uint64_t pos = findIndexForBlockArrOOO(perArt, *reinterpret_cast<uint16_ptr>(perArt + 1),
                                                                        (eb_block_size_ooo / 2), src_timestamp);
                                 LeafNode *leaf = *reinterpret_cast<LeafNode **>((perArt) +
                                                                                 ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(perArt)) /
                                                                                   2) * 8) + ((pos * 8) - 8));
                                 // per block deletion lock

                                 *pos_ooo = pos;
                                 ap->indrIndex = add_version(reinterpret_cast<vertex_t>(perArt));
                                 ap->edgeBlockIndex = add_version_read(reinterpret_cast<vertex_t>(leaf));;
                                 ap->hashTableIndex = FLAG_EMPTY_SLOT;
                                 return ap;

                             }
                         }*/
                    }
                }
            }

            // ********** get Hash table size end //

            // ##########


            // get adjacency list size
            if (!(index[i].edgePtr & 0x1))
            {
                PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[i].getEdgePtr());
                STAL_space += sizeof(PerSourceIndrPointer);
                total_space += sizeof(PerSourceIndrPointer);
                STAL_space += ((u_int64_t) 1 << ind_ptr->getBlockLen()) * 8;
                total_space += ((u_int64_t) 1 << ind_ptr->getBlockLen()) * 8;

                int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();

                // deletion bit per source-vertex not active direct read L2 vector
                for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                {

                    stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                    int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k))/2; // get block size
                    uint64_t block_entries_size = (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8);
                    uint64_t block_property_size = (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) / 2;
                    STAL_space += block_entries_size;
                    STAL_space += block_property_size;

                    total_space += block_entries_size;
                    total_space += block_property_size;
                    // ***************** stalb entries size *********
                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                    {
                        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                        uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                        if (!check_version_read(dst))
                        {
                            STAL_iem_entries_space += sizeof(EdgeInfoHal);
                            total_space += sizeof(EdgeInfoHal);
                            if(check_version(dst))
                            {
                                STAL_iem_entries_space += sizeof(ITM);
                                iem_deleted_not_garbage_collect += sizeof(ITM);
                                total_space += sizeof(ITM);
                            }
                        }
                        /// %%%%%%%%%%% end %%%%%%%%%%
                    }

                    // ****************


                }

            }
            else
            {

                stalb_type *eb_k = static_cast<stalb_type *>(index[i].getEdgePtr());

                int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                uint64_t block_entries_size = (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8);
                uint64_t block_property_size = (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) / 2;
                STAL_space += block_entries_size;
                STAL_space += block_property_size;
                total_space += block_entries_size;
                total_space += block_property_size;
                // #### iem entries record calculation
                for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                {
                    /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                    uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                    if (!check_version_read(dst))
                    {
                        STAL_iem_entries_space += sizeof(EdgeInfoHal);
                        total_space += sizeof(EdgeInfoHal);
                        if(check_version(dst))
                        {
                            STAL_iem_entries_space += sizeof(ITM);
                            iem_deleted_not_garbage_collect += sizeof(ITM);
                            total_space += sizeof(ITM);
                        }
                    }
                    /// %%%%%%%%%%% end %%%%%%%%%%
                }
                // ########### end
            }

            // std::unordered_map<int, int> frequencyMap;

            // Count the frequency of each element
            // for (const auto& num : numbers) {
            //     frequencyMap[num]++;
            // }

            // Display the counts of duplicate entries
            // for (const auto& entry : frequencyMap)
            // {
            // if (entry.second > 1)
            // {
            //   num_of_duplicates++;
            // }
            //  }
            total_dest_ids +=numbers.size();
            numbers.clear();
        }

    }

    uint64_t  tot_num_edges = num_of_edges_tot;

    /// phy to logical and logical to physical
    std::size_t size_estimate_ht = estimate_concurrent_hash_map_size(logical_to_physical);

    VA_space += size_estimate_ht;
    VA_space += index.capacity() * 8;
    VA_space += physical_to_logical.capacity() * 8;

    total_space += size_estimate_ht;
    total_space += index.capacity() * 8;
    total_space += physical_to_logical.capacity() * 8;

    //
    uint64_t free_space = 0;
    uint64_t alloc_space = 0;
    uint64_t alloc_space_act_block = 0;
    uint64_t  insertion_count = 0;
    for(int i=0;i<40;i++)
    {
        free_space += ga->perThreadArr[i]->getPerThreadFreeMemory();
        alloc_space += ga->perThreadArr[i]->getPerThreadAllocMemory();
        alloc_space_act_block += ga->perThreadArr[i]->getPerThreadAllocMemoryBlock();
        insertion_count += perThreadNumberOfEdges[i];
    }
    //


    // uint64_t number_of_edges =
    testLock.lock();
    std::cout<< " Total space taken by data structure :"<<total_space<<std::endl;
    std::cout<< " Space taken by HT array size :"<<HT_space_arr<<std::endl;
    std::cout<< " Space taken by HT total size :"<<HT_space_tot<<std::endl;
    std::cout<< " Count taken by HT with dve space and Duplicate Node :"<<total_entry_stored_in_ht_with_dve<<std::endl;
    std::cout<< " Space taken by HT with dve space and Duplicate Node:"<<total_entry_stored_in_ht_with_dve_space<<std::endl;
    std::cout<< " Count taken by HT with GC Dest :"<<total_entry_stored_in_ht_with_gc_dest<<std::endl;
    std::cout<< " Space taken by HT with GC Dest :"<<total_entry_stored_in_ht_with_gc_dest_space<<std::endl;

    std::cout<< " empty Space in HT block1 :"<<HT_empty_space_block1 * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)
    //
    std::cout<< " empty Space in HT block2 :"<<HT_empty_space_block2 * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)
    std::cout<< " empty Space in HT block3 :"<<HT_empty_space_block3 * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)
    std::cout<< " empty Space in HT blockdouble :"<<HT_empty_space_block_double * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)

    std::cout<< " mand Space in HT block1 :"<<HT_space_block1 * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)
    //
    std::cout<< " mand Space in HT block2 :"<<HT_space_block2 * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)
    std::cout<< " mand Space in HT block3 :"<<HT_space_block3 * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)
    std::cout<< " mand Space in HT blockdouble :"<<HT_space_block_double * 8<<std::endl; // number of empty spaces (entrycounter - blocksize)

    std::cout<< " Total insertion in HT :"<<total_entry_stored_in_ht<<std::endl;
    std::cout<< " HT with dve :"<<total_entry_stored_in_ht_with_dve<<std::endl;
    std::cout<< " HT with dve and insertion :"<<total_entry_stored_in_ht_with_dve_insertion<<std::endl;

    std::cout<< " Duplicate entries in HT :"<<num_of_duplicates<<std::endl;
    std::cout<< " Insertion entries :"<<insertion_count<<std::endl;


    std::cout<< " Space taken by VA :"<<VA_space<<std::endl;
    std::cout<< " Space taken by Invalid block :"<<invalid_block_space<<std::endl;

    std::cout<< " Space taken by STAL :"<<STAL_space<<std::endl;
    std::cout<< " Space taken by STAL with IEM :"<<STAL_iem_entries_space<<std::endl;

    std::cout<< " Space taken by  VA logical to phy hash table :"<<size_estimate_ht<<std::endl;
    std::cout<< " Space taken by  phy to log vector :"<<physical_to_logical.capacity() * 8<<std::endl;
    std::cout<< " Space taken by  VA index :"<<index.capacity() * 8<<std::endl;
    std::cout<< " Space taken by  IEM entries deleted but not garbage collect :"<<iem_deleted_not_garbage_collect<<std::endl;
    std::cout<< " IEM entry size :"<<tot_num_edges<<" "<<(tot_num_edges * sizeof(EdgeInfoHal))<<std::endl;
    std::cout<< " free space :"<<free_space<<std::endl;
    std::cout<< " Alloc space :"<<alloc_space<<std::endl;
    std::cout<< " Actual Alloc space :"<<alloc_space_act_block<<std::endl;
    std::cout<< " Max HAL space :"<<max_hal_space<<std::endl;
    std::cout<< " Max HAL entries :"<<max_hal_entries<<std::endl;

    std::cout<<"UPI count :"<<upi_count<<std::endl;
    std::cout<<"Total number of unique dest ids: "<<total_dest_ids<<std::endl;
    //std::cout<< " Total number of updates operations :"<<traverse_count_t.load(std::memory_order_relaxed)<<std::endl;

/// #########
    do_scanedges(0, 0, 0, 0, ga);
    testLock.unlock();


}
////
void HBALStore::initVertexIndrList(MemoryAllocator *la)
{
    /*
      vertex_t curVertexSize = (vertex_t) 1 << vertexArraySize;
      PerSourceVertexIndr **vertexArray = reinterpret_cast<PerSourceVertexIndr **>((PerSourceVertexIndr *) malloc((curVertexSize * sizeof(PerSourceVertexIndr))));

      for (int i = 0; i < curVertexSize; i++)
      {
          PerSourceVertexIndr *perS = (PerSourceVertexIndr *) (malloc(sizeof(PerSourceVertexIndr)));

          vertexArray[i] = perS;//(PerSourceVertexIndr *) (std::aligned_alloc(PAGE_SIZE,sizeof(PerSourceVertexIndr)));
      }

      curIndVertexSize = VertexArrayIndr;
      vertexIndrList = reinterpret_cast<PerSourceVertexIndr ***>(reinterpret_cast<PerSourceVertexIndr **>((PerSourceVertexIndr *) malloc(((vertex_t) 1 << curIndVertexSize) * 8)));
      vertexIndrList[0] = vertexArray;
      */

}

void HBALStore::resizeVertexArrayHal(vertex_t devVertexIndex, MemoryAllocator *ptr, int thread_id) {
    /* if (devVertexIndex > (((vertex_t) 1 << curIndVertexSize) - 1)) {
         // todo resize the indirection array (done)
         curIndVertexSize++;
         PerSourceVertexIndr ***tempArray = reinterpret_cast<PerSourceVertexIndr ***>(reinterpret_cast<PerSourceVertexIndr **>((PerSourceVertexIndr *) ptr->Allocate(
                 ((vertex_t) 1 << curIndVertexSize) * 8, thread_id)));
         for (int i = 0; i < ((vertex_t) 1 << curIndVertexSize); i++) {
             tempArray[i] = NULL;
         }
         // curIndVertexSize decremented becuase of neutralizing the increament effect few lines before
         std::copy(vertexIndrList, vertexIndrList + ((vertex_t) 1 << (curIndVertexSize - 1)), tempArray);
         //   delete [] vertexIndrList;
         vertexIndrList = tempArray;
     }

     // todo specfic vertex array block assignment (done)
     //  BlockProvider *ptr;
     vertex_t curVertexSize = (vertex_t) 1 << vertexArraySize;
     //static vertex_t  curs = (vertex_t) 1 << vertexArraySize;
     PerSourceVertexIndr **vertexArray = reinterpret_cast<PerSourceVertexIndr **>((PerSourceVertexIndr *) ptr->Allocate(
             curVertexSize * sizeof(PerSourceVertexIndr), thread_id));
     for (int i = 0; i < curVertexSize; i++) {
         PerSourceVertexIndr *perS = (PerSourceVertexIndr *) (ptr->Allocate(sizeof(PerSourceVertexIndr), thread_id));
         //  perS->perVertex = (PerSourceVertexIndr *) (std::aligned_alloc(PAGE_SIZE,sizeof(PerSourceVertexIndr)));
         //perS->initVal();
         //   perS->initLock();

         vertexArray[i] = perS;//(PerSourceVertexIndr *) (std::aligned_alloc(PAGE_SIZE,sizeof(PerSourceVertexIndr)));
         // vertexArray[i]->initVal();

     }
     // todo initialize new vertex array block(done)
     vertexIndrList[devVertexIndex] = vertexArray;*/
}

void HBALStore::initArt() {
    //m = new KeyIndexStore();
}

PerSourceVertexIndr ***HBALStore::getVertexIndrList() {
    return HBALStore::vertexIndrList;
}

vertex_t HBALStore::getMaxSourceVertex() {
    return maxSourceVertex;
}

vertex_t HBALStore::getNumEdges() {
    return numEdges;
}

void HBALStore::setNumEdges(vertex_t c) {
    numEdges = 0;
}

vertex_t HBALStore::gettraverse_count_t() {
    return traverse_count_t;
}

void HBALStore::settraverse_count_t(vertex_t c) {
    traverse_count_t = 0;
}

void HBALStore::setLoadingTime(double loadTime) {
    loadingTime = loadTime;
}

double HBALStore::getLoadingTime() {
    return loadingTime;
}



void HBALStore::getLogClose() {
    HBALStore::foutput.close();
}

bool HBALStore::has_vertex(vertex_t v) {
    l_t_p_table::const_accessor a;
    return logical_to_physical.find(a, v);
}
/* common analytics functions */

// calculate number of vertices entries
u_int64_t HBALStore::CalculateNumberOfVertices() {
    u_int64_t total_vertices = 0;

    for (int i = 0; i < numberOfThread; i++) {
        total_vertices += perThreadNumberOfvertices[i];
    }
    return total_vertices;
}

// calculate number of edges
uint64_t HBALStore::CalculateNumberOfEdges()
{
    uint64_t total_edge = 0;
    for (int i = 0; i < numberOfThread; i++) {
        total_edge += perThreadNumberOfEdges[i];
    }
    return total_edge;
}

// get per source vertex pointer to get adjacency list
PerSourceVertexIndr* HBALStore::getSourceVertexPointer(vertex_t src) {
    return nullptr; //index[src];
}

//

/* Algorithm specfic analytics function for present query */
//// %%%%%%%%
// get degree
// bool Dummy::remove_edge_v2(gfe::graph::WeightedEdge e, int thread_id)

/// %%%%%%%
// get degree
vertex_t HBALStore::perSourceGetDegree(vertex_t v, int read_version, MemoryAllocator *me)
{
    //  std::cout<<me<<std::endl;
    //

    if (check_version(index[v].degree))
    {
        // DegreeNode
        DegreeNode *cur_degree = reinterpret_cast<DegreeNode *>(remove_version(index[v].degree));
        DegreeNode *prev = NULL;

        while (cur_degree != NULL)
        {
            // std::cout<<v<<std::endl;
            if (cur_degree->wts < min_read_degree_version.load())
            {
                // Add degree in garbage collection list
                // if(cur)
                if (cur_degree->next != NULL)
                {
                    // InvalidDegree *new_invl_node = new InvalidDegree(); // replace with own memory allocator

                    prev = cur_degree->next;
                    cur_degree->next = NULL;

                    InvalidDegree *new_invl_node = static_cast<InvalidDegree *>(malloc(sizeof(InvalidDegree)));//static_cast<InvalidDegree *>(me->Allocate(sizeof(InvalidDegree), (v % numberOfThread)));//static_cast<InvalidDegree *>(malloc(sizeof(InvalidDegree))); // InvalidDegree();
                    new_invl_node->ptr = prev;

                    this->invalidListDegree[v % THRESHOLDINVALID]->lock.lock();

                    if (this->invalidListDegree[v % THRESHOLDINVALID]->ptr == NULL)
                    {
                        this->invalidListDegree[v % THRESHOLDINVALID]->ptr = new_invl_node;
                        new_invl_node->next = NULL;
                    }
                    else
                    {
                        new_invl_node->next = static_cast<InvalidDegree *>(this->invalidListDegree[v % THRESHOLDINVALID]->ptr);
                        this->invalidListDegree[v % THRESHOLDINVALID]->ptr = new_invl_node;
                    }

                    this->invalidListDegree[v % THRESHOLDINVALID]->lock.unlock();

                    ///////// %%%%%%%%%%%%%%

                }
                // end degree garbage collection list
                return cur_degree->degree;
            }
            cur_degree = cur_degree->next;
        }
        //std::cout<<"No degree"<<std::endl;
        return 0;
    }
    else
    {
        return remove_version(index[v].degree);
    }

    // index[v].indirectionLock.unlock();

    // return count_edge;*/
}
// BFS start

int64_t HBALStore::do_bfs_BUStep(uint64_t max_vertex_id, int64_t *distances, int64_t distance, gapbs::Bitmap &front, gapbs::Bitmap &next, int read_version)
{
    timestamp_t min_read_versions = min_read_version;
    int64_t awake_count = 0;
    next.reset();
    // std::cout<<"do_bfs_BUStep 0 :"<<std::endl;
    if (IsReadWrite)
    {
#pragma omp parallel for schedule(dynamic, 1024) reduction(+ : awake_count)
        for (uint64_t u = 0; u < max_vertex_id; u++)
        {

            // if (distances[u] == std::numeric_limits<int64_t>::max()) continue; // the vertex does not exist


            // COUT_DEBUG_BFS("explore: " << u << ", distance: " << distances[u]);
            if (distances[u] < 0)
            { // the node has not been visited yet

                // #### per source edge reading
                bool IsWTS = false;
                index[u].indirectionLock.lock();
                //  index[u].perVertexLock.lock();

                if (!(index[u].edgePtr & 0x1))
                {

                    PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[u].getEdgePtr());
                    __builtin_prefetch(index[u + PREFETCH_AHEAD].getEdgePtr(), 0, 3);
                    __builtin_prefetch((char *) index[u + PREFETCH_AHEAD + 1].getEdgePtr(), 0, 3);
                    __builtin_prefetch((char *) index[u + PREFETCH_AHEAD + 2].getEdgePtr(), 0, 3);
                    __builtin_prefetch((char *) index[u + PREFETCH_AHEAD - 1].getEdgePtr(), 0, 3);

                    int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                    bool flag_l1_delete = false;
                    flag_l1_delete = ind_ptr->isDeletionBlock();
                    //  std::cout<<"do_bfs_BUStep 0.2 :"<<u<<std::endl;

                    if (!flag_l1_delete)
                    {


                        // deletion bit per source-vertex not active direct read L2 vector
                        for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                        {

                            stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                            int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size


                            //// %%%%%%%%%% stalb contains deletions %%%%%%%%%%
                            //get the latest deletion wts


                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                            {

                                /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                if (!IsWTS)
                                {
                                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                    if (ei != NULL)
                                    {
                                        if (ei->getWTsEdge() < min_read_versions)
                                        {
                                            IsWTS = true;
                                            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                            if (!check_version(dst))
                                            {
                                                if (front.get_bit(dst))
                                                {
                                                    distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                    awake_count++;
                                                    next.set_bit(u);
                                                    break;
                                                }
                                            }
                                            else
                                            {

                                                if (!check_version_read(dst))
                                                {


                                                    if (min_read_versions < ei->getInvldTime()->getWts())
                                                    {
                                                        if (front.get_bit(remove_version(dst)))
                                                        {
                                                            distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                            awake_count++;
                                                            next.set_bit(u);
                                                            break;
                                                        }
                                                    }
                                                    else
                                                    {

                                                        *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                        // }
                                                    }
                                                    // }
                                                }



                                                //
                                            }

                                        }
                                    }
                                }
                                else
                                {
                                    uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                    if (!check_version(dst))
                                    {
                                        if (front.get_bit(dst))
                                        {
                                            distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                            awake_count++;
                                            next.set_bit(u);
                                            break;
                                        }
                                    }
                                    else
                                    {

                                        if (!check_version_read(dst))
                                        {


                                            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                            if (min_read_versions < ei->getInvldTime()->getWts())
                                            {
                                                if (front.get_bit(remove_version(dst)))
                                                {
                                                    distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                    awake_count++;
                                                    next.set_bit(u);
                                                    break;
                                                }
                                            }
                                            else
                                            {

                                                *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                // }
                                                //

                                            }
                                            // }
                                        }

                                    }

                                }
                                /// %%%%%%%%%%% end %%%%%%%%%%
                            }
                            /*  }*/

                            // %%%%%%%%%% stalb block end %%%%%%%%%%%



                        }


                    }
                    else
                    {
                        /// %%%%%%%%%%%%%%%%% if indirection array have deletions STAL %%%%%%%%%%%%

                        for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                        {
                            if (ind_ptr->getperVertexBlockArr()[ind_l1] != ((stalb_type *) FLAG_EMPTY_SLOT))
                            {
                                stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                //// %%%%%%%%%% stalb contains deletions %%%%%%%%%%
                                //get the latest deletion wts

                                //  std::cout<<"u 1.2 start"<<std::endl;

                                for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                {

                                    /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                    if (!IsWTS)
                                    {
                                        EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                        if (ei != NULL)
                                        {
                                            if (ei->getWTsEdge() < min_read_versions)
                                            {
                                                IsWTS = true;
                                                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                                if (!check_version(dst))
                                                {
                                                    if (front.get_bit(dst))
                                                    {
                                                        distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                        awake_count++;
                                                        next.set_bit(u);
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    if (!check_version_read(dst))
                                                    {


                                                        if (min_read_versions < ei->getInvldTime()->getWts())
                                                        {
                                                            if (front.get_bit(remove_version(dst)))
                                                            {
                                                                distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                                awake_count++;
                                                                next.set_bit(u);
                                                                break;
                                                            }
                                                        }
                                                        else
                                                        {

                                                            *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                            // }
                                                            //

                                                        }
                                                        // }
                                                    }
                                                    //
                                                }

                                            }
                                        }
                                    }
                                    else
                                    {
                                        uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                        if (!check_version(dst))
                                        {
                                            if (front.get_bit(dst))
                                            {
                                                distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                awake_count++;
                                                next.set_bit(u);
                                                break;
                                            }
                                        }
                                        else
                                        {
                                            if (!check_version_read(dst))
                                            {


                                                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                if (min_read_versions < ei->getInvldTime()->getWts())
                                                {
                                                    if (front.get_bit(remove_version(dst)))
                                                    {
                                                        distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                        awake_count++;
                                                        next.set_bit(u);
                                                        break;
                                                    }
                                                }
                                                else
                                                {

                                                    *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                    // }
                                                    //

                                                }
                                                // }
                                            }

                                        }

                                    }
                                    /// %%%%%%%%%%% end %%%%%%%%%%
                                }
                                /* }*/

                                // %%%%%%%%%% stalb block end %%%%%%%%%%%



                            }

                        }


                        /// %%%%%%%%%%%%%%%%% end %%%%%%%%%%%%%%%%%%%%%%
                    }

                }
                else
                {

                    stalb_type *eb_k = static_cast<stalb_type *>(index[u].getEdgePtr());
                    __builtin_prefetch(index[u + PREFETCH_AHEAD].getEdgePtr(), 0, 3);
                    __builtin_prefetch((char *) index[u + PREFETCH_AHEAD + 1].getEdgePtr(), 0, 3);
                    __builtin_prefetch((char *) index[u + PREFETCH_AHEAD + 2].getEdgePtr(), 0, 3);
                    __builtin_prefetch((char *) index[u + PREFETCH_AHEAD - 1].getEdgePtr(), 0, 3);

                    int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;

                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                    {

                        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                        if (!IsWTS)
                        {
                            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                            if (ei != NULL)
                            {
                                if (ei->getWTsEdge() < min_read_versions)
                                {
                                    IsWTS = true;
                                    uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                    if (!check_version(dst))
                                    {
                                        if (front.get_bit(dst))
                                        {
                                            distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                            awake_count++;
                                            next.set_bit(u);
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        if (!check_version_read(dst))
                                        {


                                            if (min_read_versions < ei->getInvldTime()->getWts())
                                            {
                                                if (front.get_bit(remove_version(dst)))
                                                {
                                                    distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                    awake_count++;
                                                    next.set_bit(u);
                                                    break;
                                                }
                                            }
                                            else
                                            {

                                                *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                // }
                                                //

                                            }
                                            // }
                                        }
                                        //
                                    }

                                }
                            }
                        }
                        else
                        {
                            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                            if (!check_version(dst))
                            {
                                if (front.get_bit(dst))
                                {
                                    distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                    awake_count++;
                                    next.set_bit(u);
                                    break;
                                }
                            }
                            else
                            {

                                if (!check_version_read(dst))
                                {


                                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                    if (min_read_versions < ei->getInvldTime()->getWts())
                                    {
                                        if (front.get_bit(remove_version(dst)))
                                        {
                                            distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                            awake_count++;
                                            next.set_bit(u);
                                            break;
                                        }
                                    }
                                    else
                                    {

                                        *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                        // }
                                        //

                                    }
                                    // }
                                }


                            }

                        }
                        /// %%%%%%%%%%% end %%%%%%%%%%
                    }
                    /*   } */

                    // %%%%%%%%%% stalb block end %%%%%%%%%%%


                    // #### per source edge reading end


                }

                index[u].indirectionLock.unlock();
                // index[u].perVertexLock.unlock();

            }
        }
    }
    else
    {
        if (!NODELETEOOO)
        {
#pragma omp parallel for schedule(dynamic, 1024) reduction(+ : awake_count)
            for (uint64_t u = 0; u < max_vertex_id; u++)
            {

                // if (distances[u] == std::numeric_limits<int64_t>::max()) continue; // the vertex does not exist


                // COUT_DEBUG_BFS("explore: " << u << ", distance: " << distances[u]);
                if (distances[u] < 0)
                { // the node has not been visited yet

                    // #### per source edge reading

                    index[u].indirectionLock.lock();
                    //  index[u].perVertexLock.lock();
                    if (index[u].edgePtr != 0)
                    {

                        if (!(index[u].edgePtr & 0x1))
                        {
                            //  std::cout<<"do_bfs_BUStep 0.1 :"<<u<<std::endl;
                            PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[u].getEdgePtr());
                            int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                            bool flag_l1_delete = false;
                            flag_l1_delete = ind_ptr->isDeletionBlock();

                            if (!flag_l1_delete)
                            {


                                // deletion bit per source-vertex not active direct read L2 vector
                                for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                                {

                                    stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                    int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size


                                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                    {

                                        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time

                                        uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                        if (!check_version(dst))
                                        {
                                            if (front.get_bit(dst))
                                            {
                                                distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                awake_count++;
                                                next.set_bit(u);
                                                break;
                                            }
                                        }
                                        else
                                        {

                                            if (!check_version_read(dst))
                                            {


                                                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                if (min_read_versions < ei->getInvldTime()->getWts())
                                                {
                                                    if (front.get_bit(remove_version(dst)))
                                                    {
                                                        distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                        awake_count++;
                                                        next.set_bit(u);
                                                        break;
                                                    }
                                                }
                                                else
                                                {

                                                    *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                    // }
                                                    //

                                                }
                                                // }
                                            }

                                        }

                                        /// %%%%%%%%%%% end %%%%%%%%%%
                                    }
                                    /*  }*/

                                    // %%%%%%%%%% stalb block end %%%%%%%%%%%



                                }


                            }
                            else
                            {
                                /// %%%%%%%%%%%%%%%%% if indirection array have deletions STAL %%%%%%%%%%%%

                                for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                                {
                                    if (ind_ptr->getperVertexBlockArr()[ind_l1] != ((stalb_type *) FLAG_EMPTY_SLOT))
                                    {
                                        stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                        //// %%%%%%%%%% stalb contains deletions %%%%%%%%%%

                                        for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                        {

                                            /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                            if (!check_version(dst))
                                            {
                                                if (front.get_bit(dst))
                                                {
                                                    distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                    awake_count++;
                                                    next.set_bit(u);
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                if (!check_version_read(dst))
                                                {


                                                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                    if (min_read_versions < ei->getInvldTime()->getWts())
                                                    {
                                                        if (front.get_bit(remove_version(dst)))
                                                        {
                                                            distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                            awake_count++;
                                                            next.set_bit(u);
                                                            break;
                                                        }
                                                    }
                                                    else
                                                    {

                                                        *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                        // }
                                                        //

                                                    }
                                                    // }
                                                }

                                            }

                                            /// %%%%%%%%%%% end %%%%%%%%%%
                                        }
                                        /* }*/

                                        // %%%%%%%%%% stalb block end %%%%%%%%%%%



                                    }

                                }


                                /// %%%%%%%%%%%%%%%%% end %%%%%%%%%%%%%%%%%%%%%%
                            }

                        }
                        else
                        {

                            stalb_type *eb_k = static_cast<stalb_type *>(index[u].getEdgePtr());

                            int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;


                            //// %%%%%%%%%% stalb contains deletions %%%%%%%%%%
                            //get the latest deletion wts

                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                            {

                                /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                if (!check_version(dst))
                                {
                                    if (front.get_bit(dst))
                                    {
                                        distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                        awake_count++;
                                        next.set_bit(u);
                                        break;
                                    }
                                }
                                else
                                {

                                    if (!check_version_read(dst))
                                    {


                                        EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                        if (min_read_versions < ei->getInvldTime()->getWts())
                                        {
                                            if (front.get_bit(remove_version(dst)))
                                            {
                                                distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                                awake_count++;
                                                next.set_bit(u);
                                                break;
                                            }
                                        }
                                        else
                                        {

                                            *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                            // }
                                            //

                                        }
                                        // }
                                    }


                                }
                                /// %%%%%%%%%%% end %%%%%%%%%%
                            }
                            /*   } */

                            // %%%%%%%%%% stalb block end %%%%%%%%%%%


                            // #### per source edge reading end


                        }

                    }
                    index[u].indirectionLock.unlock();
                    // index[u].perVertexLock.unlock();

                }
            }
        }
        else
        {
#pragma omp parallel for schedule(dynamic, 1024) reduction(+ : awake_count)
            for (uint64_t u = 0; u < max_vertex_id; u++)
            {
                if (distances[u] < 0)
                {
                    if (!(index[u].edgePtr & 0x1))
                    {
                        //  std::cout<<"do_bfs_BUStep 0.1 :"<<u<<std::endl;
                        PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[u].getEdgePtr());
                        __builtin_prefetch(index[u + PREFETCH_AHEAD].getEdgePtr(), 0, 3);
                        __builtin_prefetch((char *) index[u + PREFETCH_AHEAD + 1].getEdgePtr(), 0, 3);
                        __builtin_prefetch((char *) index[u + PREFETCH_AHEAD + 2].getEdgePtr(), 0, 3);
                        __builtin_prefetch((char *) index[u + PREFETCH_AHEAD - 1].getEdgePtr(), 0, 3);

                        int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();

                        for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                        {

                            stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                            int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size


                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                            {

                                /// %%%%%%%%% transaction time comparison to read updates that are < RTS time

                                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                if (front.get_bit(dst))
                                {
                                    distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                    awake_count++;
                                    next.set_bit(u);
                                    break;
                                }

                                /// %%%%%%%%%%% end %%%%%%%%%%
                            }

                            // %%%%%%%%%% stalb block end %%%%%%%%%%%

                        }

                    }
                    else
                    {

                        stalb_type *eb_k = static_cast<stalb_type *>(index[u].getEdgePtr());
                        __builtin_prefetch(index[u + PREFETCH_AHEAD].getEdgePtr(), 0, 3);
                        __builtin_prefetch((char *) index[u + PREFETCH_AHEAD + 1].getEdgePtr(), 0, 3);
                        __builtin_prefetch((char *) index[u + PREFETCH_AHEAD + 2].getEdgePtr(), 0, 3);
                        __builtin_prefetch((char *) index[u + PREFETCH_AHEAD - 1].getEdgePtr(), 0, 3);

                        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;


                        //// %%%%%%%%%% stalb contains deletions %%%%%%%%%%
                        //get the latest deletion wts

                        for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                        {

                            /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                            if (front.get_bit(dst))
                            {
                                distances[u] = distance; // on each BUStep, all nodes will have the same distance
                                awake_count++;
                                next.set_bit(u);
                                break;
                            }

                            /// %%%%%%%%%%% end %%%%%%%%%%
                        }

                        // %%%%%%%%%% stalb block end %%%%%%%%%%%


                        // #### per source edge reading end


                    }
                }
            }
        }
    }
    return awake_count;
}
////////////////

//////////////
int64_t HBALStore::do_bfs_TDStep(uint64_t max_vertex_id, int64_t *distances, int64_t distance, gapbs::SlidingQueue<int64_t> &queue, int read_version)
{
    timestamp_t min_read_versions = min_read_version;
    int64_t scout_count = 0;
    if (IsReadWrite)
    {
#pragma omp parallel reduction(+ : scout_count)
        {
            gapbs::QueueBuffer<int64_t> lqueue(queue);

#pragma omp for schedule(dynamic, 64)
            for (auto q_iter = queue.begin(); q_iter < queue.end(); q_iter++)
            {
                int64_t u = *q_iter;

                // stal
                bool IsWTS = false;

                index[u].indirectionLock.lock();
                if (index[u].edgePtr != 0)
                {
                    if (!(index[u].edgePtr & 0x1))
                    {
                        PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[u].getEdgePtr());
                        int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                        bool flag_l1_delete = false;
                        flag_l1_delete = ind_ptr->isDeletionBlock();
                        if (!flag_l1_delete)
                        {
                            // deletion bit per source-vertex not active direct read L2 vector
                            for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                            {

                                stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                {

                                    /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                    if (!IsWTS)
                                    {
                                        EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                        if (ei != NULL)
                                        {
                                            if (ei->getWTsEdge() < min_read_versions)
                                            {
                                                IsWTS = true;
                                                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                                if (!check_version(dst))
                                                {
                                                    int64_t curr_val = distances[dst];
                                                    if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                                    {

                                                        lqueue.push_back(dst);
                                                        scout_count += -curr_val;
                                                    }
                                                }
                                                else
                                                {


                                                    if (!check_version_read(dst))
                                                    {


                                                        EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                        if (min_read_versions < ei->getInvldTime()->getWts())
                                                        {
                                                            int64_t curr_val = distances[remove_version(dst)];
                                                            if (curr_val < 0 && gapbs::compare_and_swap(distances[remove_version(dst)], curr_val, distance))
                                                            {

                                                                lqueue.push_back(remove_version(dst));
                                                                scout_count += -curr_val;
                                                            }
                                                        }
                                                        else
                                                        {

                                                            *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                            // }
                                                            //

                                                        }
                                                        // }
                                                    }

                                                    //
                                                }

                                            }
                                        }
                                    }
                                    else
                                    {
                                        uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                        if (!check_version(dst))
                                        {
                                            int64_t curr_val = distances[dst];
                                            if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                            {

                                                lqueue.push_back(dst);
                                                scout_count += -curr_val;
                                            }
                                        }
                                        else
                                        {
                                            if (!check_version_read(dst))
                                            {


                                                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                if (min_read_versions < ei->getInvldTime()->getWts())
                                                {
                                                    int64_t curr_val = distances[remove_version(dst)];
                                                    if (curr_val < 0 && gapbs::compare_and_swap(distances[remove_version(dst)], curr_val, distance))
                                                    {

                                                        lqueue.push_back(remove_version(dst));
                                                        scout_count += -curr_val;
                                                    }
                                                }
                                                else
                                                {

                                                    *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                    // }
                                                    //

                                                }
                                                // }
                                            }

                                        }

                                    }
                                    /// %%%%%%%%%%% end %%%%%%%%%%
                                }
                                // %%%%%%%%%% stalb block end %%%%%%%%%%%
                            }
                        }
                        else
                        {
                            /// %%%%%%%%%%%%%%%%% if indirection array have deletions STAL %%%%%%%%%%%%

                            for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                            {
                                if (ind_ptr->getperVertexBlockArr()[ind_l1] != ((stalb_type *) FLAG_EMPTY_SLOT))
                                {
                                    stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer
                                    int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size
                                    //// %%%%%%%%%% stalb contains deletions %%%%%%%%%%
                                    //get the latest deletion wts
                                    // timestamp_t latest_deletion = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1<< *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8))[0];//
                                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                    {
                                        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                        if (!IsWTS)
                                        {
                                            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                            if (ei != NULL)
                                            {
                                                if (ei->getWTsEdge() < min_read_versions)
                                                {
                                                    IsWTS = true;
                                                    uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                                    if (!check_version(dst))
                                                    {
                                                        int64_t curr_val = distances[dst];
                                                        if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                                        {

                                                            lqueue.push_back(dst);
                                                            scout_count += -curr_val;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        if (!check_version_read(dst))
                                                        {


                                                            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                            if (min_read_versions < ei->getInvldTime()->getWts())
                                                            {
                                                                int64_t curr_val = distances[remove_version(dst)];
                                                                if (curr_val < 0 && gapbs::compare_and_swap(distances[remove_version(dst)], curr_val, distance))
                                                                {

                                                                    lqueue.push_back(remove_version(dst));
                                                                    scout_count += -curr_val;
                                                                }
                                                            }
                                                            else
                                                            {

                                                                *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                                // }
                                                                //

                                                            }
                                                            // }
                                                        }
                                                        //
                                                    }

                                                }
                                            }
                                        }
                                        else
                                        {
                                            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                            if (!check_version(dst))
                                            {
                                                int64_t curr_val = distances[dst];
                                                if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                                {

                                                    lqueue.push_back(dst);
                                                    scout_count += -curr_val;
                                                }
                                            }
                                            else
                                            {
                                                if (!check_version_read(dst))
                                                {


                                                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                    if (min_read_versions < ei->getInvldTime()->getWts())
                                                    {
                                                        int64_t curr_val = distances[remove_version(dst)];
                                                        if (curr_val < 0 && gapbs::compare_and_swap(distances[remove_version(dst)], curr_val, distance))
                                                        {

                                                            lqueue.push_back(remove_version(dst));
                                                            scout_count += -curr_val;
                                                        }
                                                    }
                                                    else
                                                    {

                                                        *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                        // }
                                                        //

                                                    }
                                                    // }
                                                }

                                            }
                                        }
                                        /// %%%%%%%%%%% end %%%%%%%%%%
                                    }
                                    /*  }*/
                                    // %%%%%%%%%% stalb block end %%%%%%%%%%%



                                }

                            }

                            /// %%%%%%%%%%%%%%%%% end %%%%%%%%%%%%%%%%%%%%%%
                        }
                    }
                    else
                    {

                        stalb_type *eb_k = static_cast<stalb_type *>(index[u].getEdgePtr());

                        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;

                        //// %%%%%%%%%% stalb contains deletions %%%%%%%%%%

                        //get the latest deletion wts
                        for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                        {
                            /// %%%%%%%%% transaction time comparison to read updates that are < RTS time

                            if (!IsWTS)
                            {
                                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                if (ei != NULL)
                                {
                                    if (ei->getWTsEdge() < min_read_versions)
                                    {
                                        IsWTS = true;
                                        uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                        if (!check_version(dst))
                                        {
                                            int64_t curr_val = distances[dst];
                                            if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                            {

                                                lqueue.push_back(dst);
                                                scout_count += -curr_val;
                                            }
                                        }
                                        else
                                        {
                                            if (!check_version_read(dst))
                                            {


                                                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                if (min_read_versions < ei->getInvldTime()->getWts())
                                                {
                                                    int64_t curr_val = distances[remove_version(dst)];
                                                    if (curr_val < 0 && gapbs::compare_and_swap(distances[remove_version(dst)], curr_val, distance))
                                                    {

                                                        lqueue.push_back(remove_version(dst));
                                                        scout_count += -curr_val;
                                                    }
                                                }
                                                else
                                                {

                                                    *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                    // }
                                                    //

                                                }
                                                // }
                                            }
                                            //
                                        }
                                    }
                                }
                            }
                            else
                            {
                                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                if (!check_version(dst))
                                {
                                    int64_t curr_val = distances[dst];
                                    if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                    {

                                        lqueue.push_back(dst);
                                        scout_count += -curr_val;
                                    }
                                }
                                else
                                {
                                    if (!check_version_read(dst))
                                    {


                                        EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                        if (min_read_versions < ei->getInvldTime()->getWts())
                                        {
                                            int64_t curr_val = distances[remove_version(dst)];
                                            if (curr_val < 0 && gapbs::compare_and_swap(distances[remove_version(dst)], curr_val, distance))
                                            {

                                                lqueue.push_back(remove_version(dst));
                                                scout_count += -curr_val;
                                            }
                                        }
                                        else
                                        {
                                            // if(readTable->readQuery[read_version] == getMinReadVersion())
                                            // {
                                            *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                            // }
                                            //

                                        }
                                        // }
                                    }

                                }

                            }

                            /// %%%%%%%%%%% end %%%%%%%%%%
                        }

                        // %%%%%%%%%% stalb block end %%%%%%%%%%%
                    }
                }
                index[u].indirectionLock.unlock();
            }
            lqueue.flush();
        }
    }
    else
    {
        if (!NODELETEOOO)
        {
#pragma omp parallel reduction(+ : scout_count)
            {
                gapbs::QueueBuffer<int64_t> lqueue(queue);

#pragma omp for schedule(dynamic, 64)
                for (auto q_iter = queue.begin(); q_iter < queue.end(); q_iter++)
                {
                    int64_t u = *q_iter;

                    index[u].indirectionLock.lock();
                    if (index[u].edgePtr != 0)
                    {
                        if (!(index[u].edgePtr & 0x1))
                        {
                            PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[u].getEdgePtr());
                            int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                            bool flag_l1_delete = false;
                            flag_l1_delete = ind_ptr->isDeletionBlock();
                            if (!flag_l1_delete)
                            {
                                // deletion bit per source-vertex not active direct read L2 vector
                                for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                                {

                                    stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                    int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                    {

                                        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                        uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                        if (!check_version(dst))
                                        {
                                            int64_t curr_val = distances[dst];
                                            if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                            {

                                                lqueue.push_back(dst);
                                                scout_count += -curr_val;
                                            }
                                        }
                                        else
                                        {
                                            if (!check_version_read(dst))
                                            {


                                                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                if (min_read_versions < ei->getInvldTime()->getWts())
                                                {
                                                    int64_t curr_val = distances[remove_version(dst)];
                                                    if (curr_val < 0 && gapbs::compare_and_swap(distances[remove_version(dst)], curr_val, distance))
                                                    {

                                                        lqueue.push_back(remove_version(dst));
                                                        scout_count += -curr_val;
                                                    }
                                                }
                                                else
                                                {

                                                    *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                    // }
                                                    //

                                                }
                                                // }
                                            }

                                        }


                                        /// %%%%%%%%%%% end %%%%%%%%%%
                                    }


                                    // %%%%%%%%%% stalb block end %%%%%%%%%%%



                                }
                            }
                            else
                            {
                                /// %%%%%%%%%%%%%%%%% if indirection array have deletions STAL %%%%%%%%%%%%

                                for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                                {
                                    if (ind_ptr->getperVertexBlockArr()[ind_l1] != ((stalb_type *) FLAG_EMPTY_SLOT))
                                    {
                                        stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer
                                        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                        for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                        {

                                            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                            if (!check_version(dst))
                                            {
                                                int64_t curr_val = distances[dst];
                                                if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                                {

                                                    lqueue.push_back(dst);
                                                    scout_count += -curr_val;
                                                }
                                            }
                                            else
                                            {
                                                if (!check_version_read(dst))
                                                {


                                                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                    if (min_read_versions < ei->getInvldTime()->getWts())
                                                    {
                                                        int64_t curr_val = distances[remove_version(dst)];
                                                        if (curr_val < 0 && gapbs::compare_and_swap(distances[remove_version(dst)], curr_val, distance))
                                                        {

                                                            lqueue.push_back(remove_version(dst));
                                                            scout_count += -curr_val;
                                                        }
                                                    }
                                                    else
                                                    {

                                                        *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                        // }
                                                        //

                                                    }
                                                    // }
                                                }

                                            }

                                            /// %%%%%%%%%%% end %%%%%%%%%%
                                        }
                                        /*  }*/
                                        // %%%%%%%%%% stalb block end %%%%%%%%%%%



                                    }

                                }

                                /// %%%%%%%%%%%%%%%%% end %%%%%%%%%%%%%%%%%%%%%%
                            }
                        }
                        else
                        {

                            stalb_type *eb_k = static_cast<stalb_type *>(index[u].getEdgePtr());

                            int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;

                            //// %%%%%%%%%% stalb contains deletions %%%%%%%%%%

                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                            {

                                /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                if (!check_version(dst))
                                {
                                    int64_t curr_val = distances[dst];
                                    if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                    {

                                        lqueue.push_back(dst);
                                        scout_count += -curr_val;
                                    }
                                }
                                else
                                {
                                    if (!check_version_read(dst))
                                    {


                                        EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                        if (min_read_versions < ei->getInvldTime()->getWts())
                                        {
                                            int64_t curr_val = distances[remove_version(dst)];
                                            if (curr_val < 0 && gapbs::compare_and_swap(distances[remove_version(dst)], curr_val, distance))
                                            {

                                                lqueue.push_back(remove_version(dst));
                                                scout_count += -curr_val;
                                            }
                                        }
                                        else
                                        {
                                            // if(readTable->readQuery[read_version] == getMinReadVersion())
                                            // {
                                            *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                            // }
                                            //

                                        }
                                        // }
                                    }

                                }


                                /// %%%%%%%%%%% end %%%%%%%%%%
                            }
                            // %%%%%%%%%% stalb block end %%%%%%%%%%%


                            // #### per source edge reading end


                        }
                    }
                    index[u].indirectionLock.unlock();
                }
                lqueue.flush();
            }
        }
        else
        {
#pragma omp parallel reduction(+ : scout_count)
            {
                gapbs::QueueBuffer<int64_t> lqueue(queue);

#pragma omp for schedule(dynamic, 64)
                for (auto q_iter = queue.begin(); q_iter < queue.end(); q_iter++)
                {
                    int64_t u = *q_iter;
                    if (!(index[u].edgePtr & 0x1))
                    {
                        PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[u].getEdgePtr());
                        int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                        for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                        {

                            stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                            int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                            {

                                /// %%%%%%%%% transaction time comparison to read updates that are < RTS time

                                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                int64_t curr_val = distances[dst];
                                if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                                {

                                    lqueue.push_back(dst);
                                    scout_count += -curr_val;
                                }


                                /// %%%%%%%%%%% end %%%%%%%%%%
                            }
                        }


                    }
                    else
                    {

                        stalb_type *eb_k = static_cast<stalb_type *>(index[u].getEdgePtr());

                        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;

                        for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                        {

                            /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                            int64_t curr_val = distances[dst];
                            if (curr_val < 0 && gapbs::compare_and_swap(distances[dst], curr_val, distance))
                            {

                                lqueue.push_back(dst);
                                scout_count += -curr_val;
                            }

                            /// %%%%%%%%%%% end %%%%%%%%%%
                        }
                        // %%%%%%%%%% stalb block end %%%%%%%%%%%
                    }
                }
                lqueue.flush();
            }
        }
    }
    return scout_count;
}

void HBALStore::do_bfs_QueueToBitmap(uint64_t max_vertex_id, const gapbs::SlidingQueue<int64_t> &queue, gapbs::Bitmap &bm)
{
#pragma omp parallel for
    for (auto q_iter = queue.begin(); q_iter < queue.end(); q_iter++)
    {
        int64_t u = *q_iter;
        bm.set_bit_atomic(u);

    }
}

void HBALStore::do_bfs_BitmapToQueue(uint64_t max_vertex_id, const gapbs::Bitmap &bm, gapbs::SlidingQueue<int64_t> &queue)
{
#pragma omp parallel
    {
        gapbs::QueueBuffer<int64_t> lqueue(queue);
#pragma omp for
        for (uint64_t n = 0; n < max_vertex_id; n++)
            if (bm.get_bit(n))
            {

                lqueue.push_back(n);
            }
        lqueue.flush();
    }
    queue.slide_window();
}


// taken from gfe_driver livegraph part
std::unique_ptr<int64_t[]> HBALStore::do_bfs_init_distances(uint64_t max_vertex_id, int read_version)
{

    std::unique_ptr<int64_t[]> distances{new int64_t[max_vertex_id]};
    // int64_t sumedges = 0;
#pragma omp parallel for
    for (uint64_t n = 0; n < max_vertex_id; n++)
    {
        index[n].perVertexLock.lock();

        int64_t out_degree = perSourceGetDegree(n, read_version, NULL);//index[n].degree;

        index[n].perVertexLock.unlock();

        distances[n] = distances[n] = out_degree != 0 ? (-1 * out_degree) : -1;

    }
    return distances;
}


std::unique_ptr<int64_t[]> HBALStore::do_bfs(uint64_t num_vertices, uint64_t max_vertex_id, uint64_t root, int alpha, int beta, int read_version)
{

    //std::cout<<"read version :"<<read_version<<std::endl;
    //std::cout<<"read version timestamp :"<<readTable->readQuery[read_version]<<std::endl;

    max_vertex_id = get_high_water_mark();
    num_vertices = get_vertex_count(0);

    // std::cout<<"read version distance 0:"<<readTable->readQuery[read_version]<<std::endl;

    std::unique_ptr<int64_t[]> ptr_distances = do_bfs_init_distances(max_vertex_id, read_version);
    min_read_degree_version = MaxValue;

    //std::cout<<"read version distance 1:"<<readTable->readQuery[read_version]<<std::endl;
    uint64_t num_edges = 0; // = CalculateNumberOfEdges();

    // #pragma omp parallel for reduction(+:num_edges)
    for (uint64_t n = 0; n < max_vertex_id; n++)
    {
        num_edges += (ptr_distances[n] * -1);
    }


    int64_t *__restrict distances = ptr_distances.get();


    int64_t root_degree = 0;

    if (distances[root] != -1)
    {
        // std::cout<<"distance :"<<distances[root]<<std::endl;

        root_degree = (distances[root] * -1);
    }
    else
    {
        root_degree = 0;
    }

    distances[root] = 0;


    gapbs::SlidingQueue<int64_t> queue(max_vertex_id);
    queue.push_back(root);
    queue.slide_window();
    gapbs::Bitmap curr(max_vertex_id);
    curr.reset();
    gapbs::Bitmap front(max_vertex_id);
    front.reset();

    int64_t edges_to_check = num_edges / 2; //g.num_edges_directed();

    int64_t scout_count = root_degree;


    int64_t distance = 1; // current distance

    while (!queue.empty())
    {

        if (scout_count > edges_to_check / alpha)
        {
            int64_t awake_count, old_awake_count;

            //   std::cout<<"read version distance 3:"<<std::endl;

            do_bfs_QueueToBitmap(max_vertex_id, queue, front);

            awake_count = queue.size();
            queue.slide_window();

            do
            {
                old_awake_count = awake_count;

                awake_count = do_bfs_BUStep(max_vertex_id, distances, distance, front, curr, read_version);

                front.swap(curr);
                distance++;
            } while ((awake_count >= old_awake_count) || (awake_count > (int64_t) num_vertices / beta));

            do_bfs_BitmapToQueue(max_vertex_id, front, queue);
            scout_count = 1;
        }
        else
        {

            edges_to_check -= scout_count;
            scout_count = do_bfs_TDStep(max_vertex_id, distances, distance, queue, read_version);
            queue.slide_window();
            distance++;
        }
    }
    return ptr_distances;
}

// BFS end

// %%%%%%%%%%%%%%%%%%% pagerank algorithm start %%%%%%%%%%%%%%%%%%%%%%%%%%

// STALB scan no deletion and no out-of-order update
void HBALStore::pagerank_no_deletion_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total)
{
    //double stalb_incomming_total = 0.0;
    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
    {

        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!(*IsWTS))
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei->getWTsEdge() < min_read_versions)
            {
                *IsWTS = 1;
                *stalb_incomming_total += outgoing_contrib->start_[remove_version_read(remove_version(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8)))];
            }
        }
        else
        {
            *stalb_incomming_total += outgoing_contrib->start_[remove_version_read(remove_version(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8)))];
        }
        /// %%%%%%%%%%% end %%%%%%%%%%

    }
    // return stalb_incomming_total;
}

// STALB scan deletion and no out-of-order update scan
void HBALStore::pagerank_deletion_no_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total)
{
    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
    {
        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!*IsWTS)
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei != NULL)
            {
                if (ei->getWTsEdge() < min_read_versions)
                {
                    *IsWTS = 1;
                    uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                    if (!check_version(dst))
                    {
                        *stalb_incomming_total += outgoing_contrib->start_[remove_version_read(remove_version(dst))];
                    }
                    else
                    {
                        if (!check_version_read(dst))
                        {
                            //if(ei->getInvldTime() != NULL)
                            // {
                            if (min_read_versions < ei->getInvldTime()->getWts())
                            {
                                *stalb_incomming_total += outgoing_contrib->start_[remove_version_read(remove_version(dst))];
                            }
                            else
                            {
                                *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                            }
                        }

                    }

                }
            }
        }
        else
        {

            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
            if (!check_version(dst))
            {
                *stalb_incomming_total += outgoing_contrib->start_[remove_version_read(remove_version(dst))];
            }
            else
            {
                if (!check_version_read(dst))
                {

                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                    if (ei->getInvldTime() != NULL)
                    {
                        if (min_read_versions < ei->getInvldTime()->getWts())
                        {
                            *stalb_incomming_total += outgoing_contrib->start_[remove_version_read(remove_version(dst))];
                        }
                        else
                        {
                            *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                        }
                    }
                    else
                    {
                        *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                    }

                }

            }

        }
        /// %%%%%%%%%%% end %%%%%%%%%%
    }
}

// STALB no deletion and out-of-order update
void HBALStore::pagerank_no_deletion_ooo_exist_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total)
{
    // std::cout<<"ooo"<<std::endl;
    //double stalb_incomming_total = 0.0;
    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
    {

        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!(*IsWTS))
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei->getWTsEdge() < min_read_versions)
            {
                *IsWTS = 1;
                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                *stalb_incomming_total += outgoing_contrib->start_[remove_version_ooo(dst)];

                if (check_version_ooo(dst))
                {

                    // ooo updates exist
                    //   ArtPerIndex *art_read = ei->getOutOfOrderUpdt();
                    vertex_t art_node = (ei->getOutOfOrderUpdt());
                    if (!check_version(art_node))
                    {
                        stalb_type *art_val = reinterpret_cast<stalb_type *>(remove_version(art_node));
                        // %%%%%%%%%%%% adaptive radix tree scan start
                        //  art_val->greater_equal(node<T> *root, const char *key)
                        int l2_len_ooo = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2; // get block size

                        // uint8_t is_delete_l2_flag = 0;
                        // uint8_t is_ooo_l2_flag = 0;

                        //is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                        //%%%%%%%%%%%%%%%%%%% out-of-order block reading %%%%%%%%%%%
                        for (int ind_l2_ooo = *reinterpret_cast<uint16_ptr>(art_val + 1); ind_l2_ooo < l2_len_ooo; ind_l2_ooo++)
                        {
                            //
                            uint64_t dst_ooo = *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8);
                            if (check_version_ooo(dst_ooo))
                            {
                                *stalb_incomming_total += outgoing_contrib->start_[remove_version_ooo(dst_ooo)];
                            }
                            else
                            {
                                LeafNode *ln = *reinterpret_cast<LeafNode **>(art_val + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2)) * 8) + ((ind_l2_ooo * 8) - 8));
                                if (ln->getWTsEdge() < min_read_versions)
                                {
                                    *stalb_incomming_total += outgoing_contrib->start_[dst_ooo];
                                    *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8) = add_version_ooo(dst_ooo);
                                }
                            }
                        }
                    }

                    // %%%%%%%%%%%%%%%%%% end out-of-order block %%%%%%%%%%%%%%%
                    // *stalb_incomming_total += outgoing_contrib->start_[ln_cur->getDNode()];


                    //%%%%%%% end adaptive radix tree scan
                }
            }
        }
        else
        {

            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
            *stalb_incomming_total += outgoing_contrib->start_[remove_version_ooo(dst)];

            if (check_version_ooo(dst))
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

                // ooo updates exist

                //// %%%%%%%%% out-of-order update part
                vertex_t art_node = (ei->getOutOfOrderUpdt());

                if (!check_version(art_node))
                {
                    stalb_type *art_val = reinterpret_cast<stalb_type *>(art_node);

                    int l2_len_ooo = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2; // get block size
                    for (int ind_l2_ooo = *reinterpret_cast<uint16_ptr>(art_val + 1); ind_l2_ooo < l2_len_ooo; ind_l2_ooo++)
                    {
                        //
                        uint64_t dst_ooo = *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8);
                        if (check_version_ooo(dst_ooo))
                        {
                            *stalb_incomming_total += outgoing_contrib->start_[remove_version_ooo(dst_ooo)];
                        }
                        else
                        {
                            LeafNode *ln = *reinterpret_cast<LeafNode **>(art_val + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2)) * 8) + ((ind_l2_ooo * 8) - 8));
                            if (ln->getWTsEdge() < min_read_versions)
                            {
                                *stalb_incomming_total += outgoing_contrib->start_[dst_ooo];
                                *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8) = add_version_ooo(dst_ooo);
                            }
                        }
                    }
                }
                /// %%%%%%% out-of-order update part end
                // ooo updates part end

            }
        }
        /// %%%%%%%%%%% end %%%%%%%%%%

    }
    // return stalb_incomming_total;
}

// STALB deletion and out-of-order update scan
void HBALStore::pagerank_deletion_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total)
{
    // std::cout<<"ooo"<<std::endl;

    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
    {
        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        if (!*IsWTS)
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
            if (ei->getWTsEdge() < min_read_versions)
            {
                *IsWTS = 1;
                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                if (!check_version(dst))
                {
                    *stalb_incomming_total += outgoing_contrib->start_[dst];
                    // ooo existence check
                    // ooo exist check

                }
                else
                {
                    if (!check_version_read(dst))
                    {
                        if (min_read_versions < ei->getInvldTime()->getWts())
                        {
                            *stalb_incomming_total += outgoing_contrib->start_[remove_version(dst)];
                        }
                        else
                        {
                            *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                        }
                    }
                }

                // check if dst contains ooo updates
                if (check_version_ooo(dst))
                {
                    // ooo updates exist
                    // ooo updates exist

                    stalb_type *art_val = reinterpret_cast<stalb_type *>(ei->getOutOfOrderUpdt());
                    int l2_len_ooo = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2; // get block size
                    for (int ind_l2_ooo = *reinterpret_cast<uint16_ptr>(art_val + 1); ind_l2_ooo < l2_len_ooo; ind_l2_ooo++)
                    {
                        //
                        uint64_t dst_ooo = *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8);
                        if (check_version_ooo(dst_ooo))
                        {
                            *stalb_incomming_total += outgoing_contrib->start_[remove_version_ooo(dst_ooo)];
                        }
                        else
                        {
                            LeafNode *ln = *reinterpret_cast<LeafNode **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2_ooo * 8) - 8));
                            if (ln->getWTsEdge() < min_read_versions)
                            {
                                *stalb_incomming_total += outgoing_contrib->start_[dst_ooo];
                                *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8) = add_version_ooo(dst_ooo);
                            }
                        }
                    }


                    // ooo updates part end
                }

            }
        }
        else
        {

            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
            if (!check_version(dst))
            {
                *stalb_incomming_total += outgoing_contrib->start_[dst];
            }
            else
            {
                if (!check_version_read(dst))
                {
                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                    if (min_read_versions < ei->getInvldTime()->getWts())
                    {
                        *stalb_incomming_total += outgoing_contrib->start_[remove_version(dst)];
                    }
                    else
                    {

                        *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                    }

                }

            }

            // check if dst contains ooo updates
            if (check_version_ooo(dst))
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

                // ooo updates exist
                stalb_type *art_val = reinterpret_cast<stalb_type *>(ei->getOutOfOrderUpdt());
                int l2_len_ooo = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2; // get block size
                for (int ind_l2_ooo = *reinterpret_cast<uint16_ptr>(art_val + 1); ind_l2_ooo < l2_len_ooo; ind_l2_ooo++)
                {
                    //
                    uint64_t dst_ooo = *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8);
                    if (check_version_ooo(dst_ooo))
                    {
                        *stalb_incomming_total += outgoing_contrib->start_[remove_version_ooo(dst_ooo)];
                    }
                    else
                    {
                        LeafNode *ln = *reinterpret_cast<LeafNode **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2_ooo * 8) - 8));
                        if (ln->getWTsEdge() < min_read_versions)
                        {
                            *stalb_incomming_total += outgoing_contrib->start_[dst_ooo];
                            *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8) = add_version_ooo(dst_ooo);
                        }
                    }
                }
                // ooo updates part end
            }

        }
        /// %%%%%%%%%%% end %%%%%%%%%%
    }
}

// STALB scan no deletion and no out-of-order update
void HBALStore::pagerank_no_deletion_ooo_entries_stalb_no_readwrite(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total)
{
    //double stalb_incomming_total = 0.0;
    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
    {

        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time

        *stalb_incomming_total += outgoing_contrib->start_[remove_version_read(remove_version(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8)))];

        /// %%%%%%%%%%% end %%%%%%%%%%

    }
    // return stalb_incomming_total;
}

// STALB scan deletion and no out-of-order update scan
void HBALStore::pagerank_deletion_no_ooo_entries_stalb_no_readwrite(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total)
{
    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
    {
        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time


        uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
        if (!check_version(dst))
        {
            *stalb_incomming_total += outgoing_contrib->start_[remove_version_read(remove_version(dst))];
        }
        else
        {
            if (!check_version_read(dst))
            {

                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                if (ei->getInvldTime() != NULL)
                {
                    if (min_read_versions < ei->getInvldTime()->getWts())
                    {
                        *stalb_incomming_total += outgoing_contrib->start_[remove_version_read(remove_version(dst))];
                    }
                    else
                    {
                        *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                    }
                }
                else
                {
                    *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                }

            }

        }


        /// %%%%%%%%%%% end %%%%%%%%%%
    }
}

// STALB no deletion and out-of-order update
void HBALStore::pagerank_no_deletion_ooo_exist_entries_stalb_no_readwrite(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total)
{
    // std::cout<<"ooo"<<std::endl;
    //double stalb_incomming_total = 0.0;
    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
    {

        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
        *stalb_incomming_total += outgoing_contrib->start_[remove_version_ooo(dst)];

        if (check_version_ooo(dst))
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

            // ooo updates exist

            //// %%%%%%%%% out-of-order update part
            vertex_t art_node = (ei->getOutOfOrderUpdt());

            if (!check_version(art_node))
            {
                stalb_type *art_val = reinterpret_cast<stalb_type *>(art_node);

                int l2_len_ooo = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2; // get block size
                for (int ind_l2_ooo = *reinterpret_cast<uint16_ptr>(art_val + 1); ind_l2_ooo < l2_len_ooo; ind_l2_ooo++)
                {
                    //
                    uint64_t dst_ooo = *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8);
                    if (check_version_ooo(dst_ooo))
                    {
                        *stalb_incomming_total += outgoing_contrib->start_[remove_version_ooo(dst_ooo)];
                    }
                    else
                    {
                        LeafNode *ln = *reinterpret_cast<LeafNode **>(art_val + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2)) * 8) + ((ind_l2_ooo * 8) - 8));
                        if (ln->getWTsEdge() < min_read_versions)
                        {
                            *stalb_incomming_total += outgoing_contrib->start_[dst_ooo];
                            *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8) = add_version_ooo(dst_ooo);
                        }
                    }
                }
            }
            /// %%%%%%% out-of-order update part end
            // ooo updates part end

        }

        /// %%%%%%%%%%% end %%%%%%%%%%

    }
    // return stalb_incomming_total;
}

// STALB deletion and out-of-order update scan
void HBALStore::pagerank_deletion_ooo_entries_stalb_no_readwrite(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total)
{
    // std::cout<<"ooo"<<std::endl;

    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
    {
        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
        uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
        if (!check_version(dst))
        {
            *stalb_incomming_total += outgoing_contrib->start_[dst];
        }
        else
        {
            if (!check_version_read(dst))
            {
                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                if (min_read_versions < ei->getInvldTime()->getWts())
                {
                    *stalb_incomming_total += outgoing_contrib->start_[remove_version(dst)];
                }
                else
                {

                    *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                }

            }

        }

        // check if dst contains ooo updates
        if (check_version_ooo(dst))
        {
            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

            // ooo updates exist
            stalb_type *art_val = reinterpret_cast<stalb_type *>(ei->getOutOfOrderUpdt());
            int l2_len_ooo = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2; // get block size
            for (int ind_l2_ooo = *reinterpret_cast<uint16_ptr>(art_val + 1); ind_l2_ooo < l2_len_ooo; ind_l2_ooo++)
            {
                //
                uint64_t dst_ooo = *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8);
                if (check_version_ooo(dst_ooo))
                {
                    *stalb_incomming_total += outgoing_contrib->start_[remove_version_ooo(dst_ooo)];
                }
                else
                {
                    LeafNode *ln = *reinterpret_cast<LeafNode **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2_ooo * 8) - 8));
                    if (ln->getWTsEdge() < min_read_versions)
                    {
                        *stalb_incomming_total += outgoing_contrib->start_[dst_ooo];
                        *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8) = add_version_ooo(dst_ooo);
                    }
                }
            }
            // ooo updates part end
        }


        /// %%%%%%%%%%% end %%%%%%%%%%
    }
}


//implementation in gfe_driver
std::unique_ptr<double[]> HBALStore::do_pagerank(uint64_t max_vertex_id, uint64_t num_iterations, double damping_factor, int read_version, MemoryAllocator *me)
{
    //std::cout<<"start pagerank execution"<<std::endl;
    u_int64_t num_vertices = get_vertex_count(0);
    max_vertex_id = get_high_water_mark();

    const double init_score = 1.0 / num_vertices;
    const double base_score = (1.0 - damping_factor) / num_vertices;
    std::unique_ptr<double[]> ptr_scores{new double[max_vertex_id]()}; // avoid memory leaks
    std::unique_ptr<uint64_t[]> ptr_degrees{new uint64_t[max_vertex_id]()}; // avoid memory leaks
    double *scores = ptr_scores.get();
    uint64_t *__restrict degrees = ptr_degrees.get();

#pragma omp parallel for
    for (uint64_t v = 0; v < max_vertex_id; v++)
    {
        scores[v] = init_score;
        index[v].perVertexLock.lock(); ///////
        degrees[v] = perSourceGetDegree(v, read_version, me);//index[v].degree; //perSourceGetDegree(v, read_version);
        index[v].perVertexLock.unlock();
    }

    min_read_degree_version.store(MaxValue);
    gapbs::pvector<double> outgoing_contrib(max_vertex_id, 0.0);

    // pagerank iterations
    timestamp_t min_read_versions = min_read_version.load();
    // pagerank iterations
    if (min_read_versions == getMinReadVersion())
    {
        if (IsReadWrite)
        {
            for (uint64_t iteration = 0; iteration < num_iterations; iteration++)
            {
                // std::cout<<"iteration # "<<iteration<<std::endl;
                double dangling_sum = 0.0;

                // for each node, precompute its contribution to all of its outgoing neighbours and, if it's a sink,
                // add its rank to the `dangling sum' (to be added to all nodes).

#pragma omp parallel for reduction(+:dangling_sum)
                for (uint64_t v = 0; v < max_vertex_id; v++)
                {
                    uint64_t out_degree = degrees[v];
                    if (out_degree == 0)
                    { // this is a sink
                        dangling_sum += scores[v];
                    }
                    else
                    {
                        outgoing_contrib[v] = scores[v] / out_degree;
                        //  std::cout<<outgoing_contrib[v]<<" "<<outgoing_contrib.start_[v]<<std::endl;
                    }
                }
//
                dangling_sum /= num_vertices;

                // compute the new score for each node in the graph
#pragma omp parallel for schedule(dynamic, 64)
                for (uint64_t v = 0; v < max_vertex_id; v++)
                {

                    //if(degrees[v] == std::numeric_limits<uint64_t>::max()){ continue; } // the vertex does not exist
                    if (degrees[v] == 0)
                    {
                        continue;
                    }

                    index[v].indirectionLock.lock();

                    double incoming_total = 0;

                    // per source STAL access

                    // PerSourceVertexIndr *ps_stal = getSourceVertexPointer(v);//transaction.get_edges(u,   0); // fixme: incoming edges for directed graphs


                    //
                    uint8_t IsWTS = 0;
                    if (!IsReadWrite)
                    {
                        IsWTS = 1;
                    }
                    // *IsWTS = 0;
                    if (!(index[v].edgePtr & 0x1))
                    {
                        PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[v].getEdgePtr());

                        int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                        bool flag_l1_delete = false;
                        flag_l1_delete = ind_ptr->isDeletionBlock();

                        if (!flag_l1_delete)
                        {
                            // deletion bit per source-vertex not active direct read L2 vector
                            for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                            {

                                stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                uint8_t is_delete_l2_flag = 0;
                                uint8_t is_ooo_l2_flag = 0;

                                is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                is_ooo_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 4);

                                if (!is_delete_l2_flag && !is_ooo_l2_flag)
                                {
                                    // no deletion and ooo entries
                                    //if(Read)

                                    pagerank_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);

                                }
                                else if (is_delete_l2_flag && !is_ooo_l2_flag)
                                {

                                    pagerank_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                }
                                else if (!is_delete_l2_flag && is_ooo_l2_flag)
                                {

                                    pagerank_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                    //double stalb_incomming_total = 0.0;

                                    //// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% out of order update exist


                                }
                                else
                                {

                                    pagerank_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                }


                            }
                        }
                        else
                        {
                            /// %%%%%%%%%%%%%%%%% if indirection array have deletions STAL %%%%%%%%%%%%

                            for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                            {
                                if (reinterpret_cast<vertex_t>(ind_ptr->getperVertexBlockArr()[ind_l1]) != FLAG_EMPTY_SLOT)
                                {
                                    stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                    int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                    uint8_t is_delete_l2_flag = 0;
                                    uint8_t is_ooo_l2_flag = 0;

                                    is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                    is_ooo_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 4);


                                    if (!is_delete_l2_flag && !is_ooo_l2_flag)
                                    {

                                        // no deletion and ooo entries
                                        pagerank_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);

                                    }
                                    else if (is_delete_l2_flag && !is_ooo_l2_flag)
                                    {

                                        pagerank_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                    }
                                    else if (!is_delete_l2_flag && is_ooo_l2_flag)
                                    {

                                        pagerank_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                    }
                                    else
                                    {

                                        pagerank_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                    }
                                }

                            }

                            /// %%%%%%%%%%%%%%%%% end %%%%%%%%%%%%%%%%%%%%%%
                        }
                    }
                    else
                    {

                        stalb_type *eb_k = static_cast<stalb_type *>(index[v].getEdgePtr());

                        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                        uint8_t is_delete_l2_flag = 0;
                        uint8_t is_ooo_l2_flag = 0;

                        is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                        is_ooo_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 4);


                        if (!is_delete_l2_flag && !is_ooo_l2_flag)
                        {

                            // no deletion and ooo entries
                            pagerank_no_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);

                        }
                        else if (is_delete_l2_flag && !is_ooo_l2_flag)
                        {

                            pagerank_deletion_no_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                        }
                        else if (!is_delete_l2_flag && is_ooo_l2_flag)
                        {

                            pagerank_no_deletion_ooo_exist_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                            //// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% out of order update exist

                        }
                        else
                        {

                            pagerank_deletion_ooo_entries_stalb(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                        }
                        // #### per source edge reading end
                    }

                    // STAL end
                    // update the score
                    // std::cout<<"incoming_total "<<incoming_total<<std::endl;
                    scores[v] = base_score + damping_factor * (incoming_total + dangling_sum);

                    index[v].indirectionLock.unlock();

                }

            }
        }
        else
        {
            // if no parallel read write
            if (HBALStore::NODELETEOOO)
            {
                for (uint64_t iteration = 0; iteration < num_iterations; iteration++)
                {
                    // std::cout<<"iteration # "<<iteration<<std::endl;
                    double dangling_sum = 0.0;

                    // for each node, precompute its contribution to all of its outgoing neighbours and, if it's a sink,
                    // add its rank to the `dangling sum' (to be added to all nodes).

#pragma omp parallel for reduction(+:dangling_sum)
                    for (uint64_t v = 0; v < max_vertex_id; v++)
                    {
                        uint64_t out_degree = degrees[v];
                        if (out_degree == 0)
                        { // this is a sink
                            dangling_sum += scores[v];
                        }
                        else
                        {
                            outgoing_contrib[v] = scores[v] / out_degree;
                            //  std::cout<<outgoing_contrib[v]<<" "<<outgoing_contrib.start_[v]<<std::endl;
                        }
                    }
//
                    dangling_sum /= num_vertices;

                    // compute the new score for each node in the graph
#pragma omp parallel for schedule(dynamic, 64)
                    for (uint64_t v = 0; v < max_vertex_id; v++)
                    {

                        //if(degrees[v] == std::numeric_limits<uint64_t>::max()){ continue; } // the vertex does not exist
                        if (degrees[v] == 0)
                        {
                            continue;
                        }

                        index[v].indirectionLock.lock();

                        double incoming_total = 0;

                        //
                        uint8_t IsWTS = 0;

                        // *IsWTS = 0;
                        if (!(index[v].edgePtr & 0x1))
                        {
                            PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[v].getEdgePtr());

                            int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                            // deletion bit per source-vertex not active direct read L2 vector
                            for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                            {
                                stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                pagerank_no_deletion_ooo_entries_stalb_no_readwrite(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                            }

                        }
                        else
                        {

                            stalb_type *eb_k = static_cast<stalb_type *>(index[v].getEdgePtr());

                            int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;

                            // no deletion and ooo entries
                            pagerank_no_deletion_ooo_entries_stalb_no_readwrite(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);

                            // #### per source edge reading end
                        }

                        // STAL end
                        // update the score
                        // std::cout<<"incoming_total "<<incoming_total<<std::endl;
                        scores[v] = base_score + damping_factor * (incoming_total + dangling_sum);

                        index[v].indirectionLock.unlock();

                    }

                }
            }
            else
            {
                for (uint64_t iteration = 0; iteration < num_iterations; iteration++)
                {
                    // std::cout<<"iteration # "<<iteration<<std::endl;
                    double dangling_sum = 0.0;

                    // for each node, precompute its contribution to all of its outgoing neighbours and, if it's a sink,
                    // add its rank to the `dangling sum' (to be added to all nodes).

#pragma omp parallel for reduction(+:dangling_sum)
                    for (uint64_t v = 0; v < max_vertex_id; v++)
                    {
                        uint64_t out_degree = degrees[v];
                        if (out_degree == 0)
                        { // this is a sink
                            dangling_sum += scores[v];
                        }
                        else
                        {
                            outgoing_contrib[v] = scores[v] / out_degree;
                            //  std::cout<<outgoing_contrib[v]<<" "<<outgoing_contrib.start_[v]<<std::endl;
                        }
                    }
//
                    dangling_sum /= num_vertices;

                    // compute the new score for each node in the graph
#pragma omp parallel for schedule(dynamic, 64)
                    for (uint64_t v = 0; v < max_vertex_id; v++)
                    {

                        //if(degrees[v] == std::numeric_limits<uint64_t>::max()){ continue; } // the vertex does not exist
                        if (degrees[v] == 0)
                        {
                            continue;
                        }

                        index[v].indirectionLock.lock();

                        double incoming_total = 0;

                        // per source STAL access

                        // PerSourceVertexIndr *ps_stal = getSourceVertexPointer(v);//transaction.get_edges(u,   0); // fixme: incoming edges for directed graphs


                        //
                        uint8_t IsWTS = 0;

                        // *IsWTS = 0;
                        if (!(index[v].edgePtr & 0x1))
                        {
                            PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[v].getEdgePtr());

                            int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                            bool flag_l1_delete = false;
                            flag_l1_delete = ind_ptr->isDeletionBlock();

                            if (!flag_l1_delete)
                            {
                                // deletion bit per source-vertex not active direct read L2 vector
                                for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                                {

                                    stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                    int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                    uint8_t is_delete_l2_flag = 0;
                                    uint8_t is_ooo_l2_flag = 0;

                                    is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                    is_ooo_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 4);

                                    if (!is_delete_l2_flag && !is_ooo_l2_flag)
                                    {
                                        // no deletion and ooo entries
                                        //if(Read)

                                        pagerank_no_deletion_ooo_entries_stalb_no_readwrite(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);

                                    }
                                    else if (is_delete_l2_flag && !is_ooo_l2_flag)
                                    {

                                        pagerank_deletion_no_ooo_entries_stalb_no_readwrite(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                    }
                                    else if (!is_delete_l2_flag && is_ooo_l2_flag)
                                    {

                                        pagerank_no_deletion_ooo_exist_entries_stalb_no_readwrite(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                        //double stalb_incomming_total = 0.0;

                                        //// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% out of order update exist


                                    }
                                    else
                                    {

                                        pagerank_deletion_ooo_entries_stalb_no_readwrite(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                    }


                                }
                            }
                            else
                            {
                                /// %%%%%%%%%%%%%%%%% if indirection array have deletions STAL %%%%%%%%%%%%

                                for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                                {
                                    if (reinterpret_cast<vertex_t>(ind_ptr->getperVertexBlockArr()[ind_l1]) != FLAG_EMPTY_SLOT)
                                    {
                                        stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                        uint8_t is_delete_l2_flag = 0;
                                        uint8_t is_ooo_l2_flag = 0;

                                        is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                        is_ooo_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 4);


                                        if (!is_delete_l2_flag && !is_ooo_l2_flag)
                                        {

                                            // no deletion and ooo entries
                                            pagerank_no_deletion_ooo_entries_stalb_no_readwrite(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);

                                        }
                                        else if (is_delete_l2_flag && !is_ooo_l2_flag)
                                        {

                                            pagerank_deletion_no_ooo_entries_stalb_no_readwrite(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                        }
                                        else if (!is_delete_l2_flag && is_ooo_l2_flag)
                                        {

                                            pagerank_no_deletion_ooo_exist_entries_stalb_no_readwrite(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                        }
                                        else
                                        {

                                            pagerank_deletion_ooo_entries_stalb_no_readwrite(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                        }
                                    }

                                }

                                /// %%%%%%%%%%%%%%%%% end %%%%%%%%%%%%%%%%%%%%%%
                            }
                        }
                        else
                        {

                            stalb_type *eb_k = static_cast<stalb_type *>(index[v].getEdgePtr());

                            int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                            uint8_t is_delete_l2_flag = 0;
                            uint8_t is_ooo_l2_flag = 0;

                            is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                            is_ooo_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 4);


                            if (!is_delete_l2_flag && !is_ooo_l2_flag)
                            {

                                // no deletion and ooo entries
                                pagerank_no_deletion_ooo_entries_stalb_no_readwrite(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);

                            }
                            else if (is_delete_l2_flag && !is_ooo_l2_flag)
                            {

                                pagerank_deletion_no_ooo_entries_stalb_no_readwrite(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                            }
                            else if (!is_delete_l2_flag && is_ooo_l2_flag)
                            {

                                pagerank_no_deletion_ooo_exist_entries_stalb_no_readwrite(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                                //// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% out of order update exist

                            }
                            else
                            {

                                pagerank_deletion_ooo_entries_stalb_no_readwrite(eb_k, l2_len, &IsWTS, min_read_versions, &outgoing_contrib, &incoming_total);
                            }
                            // #### per source edge reading end
                        }

                        // STAL end
                        // update the score
                        // std::cout<<"incoming_total "<<incoming_total<<std::endl;
                        scores[v] = base_score + damping_factor * (incoming_total + dangling_sum);

                        index[v].indirectionLock.unlock();

                    }

                }
            }
        }
    }

    return ptr_scores;
}

// %%%%%%%%%%%%%%%%%%% pagerank algorithm end %%%%%%%%%%%%%%%%%%%%%%%%%%

// wcc
std::unique_ptr<uint64_t[]> HBALStore::do_wcc(uint64_t max_vertex_id)
{
    // init
    std::unique_ptr<uint64_t[]> ptr_components{new uint64_t[max_vertex_id]};
    uint64_t *comp = ptr_components.get();

#pragma omp parallel for
    for (uint64_t n = 0; n < max_vertex_id; n++)
    {
        comp[n] = n;
    }
    bool change = true;
    if (!IsReadWrite)
    {
        if (!NODELETEOOO)
        {
            while (change)
            {
                change = false;

#pragma omp parallel for schedule(dynamic, 64)
                for (uint64_t u = 0; u < max_vertex_id; u++)
                {
                    if (!(index[u].edgePtr & 0x1))
                    {

                        PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[u].getEdgePtr());

                        bool flag_l1_delete = false;
                        flag_l1_delete = ind_ptr->isDeletionBlock();

                        if (!flag_l1_delete)
                        {
                            // deletion bit per source-vertex not active direct read L2 vector
                            for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < (u_int64_t) 1 << ind_ptr->getBlockLen(); ind_l1++)
                            {

                                stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];

                                // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                                uint8_t is_delete_l2_flag = 0;
                                is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                if (!is_delete_l2_flag)
                                {

                                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                                    {
                                        // uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                        uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);

                                        uint64_t comp_u = comp[u];
                                        uint64_t comp_v = comp[v];
                                        if (comp_u != comp_v)
                                        {
                                            // Hooking condition so lower component ID wins independent of direction
                                            uint64_t high_comp = std::max(comp_u, comp_v);
                                            uint64_t low_comp = std::min(comp_u, comp_v);
                                            if (high_comp == comp[high_comp])
                                            {
                                                change = true;
                                                //   COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
                                                comp[high_comp] = low_comp;
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    /* for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++) {
                                         if (eb_k->getEdgeUpdate()[ind_l2] != NULL) {
                                             if (eb_k->getEdgeUpdate()[ind_l2]->getInvldTime() != NULL)
                                             {
                                                 uint64_t v = eb_k->getDestIdArr()[ind_l2];

                                                 uint64_t comp_u = comp[u];
                                                 uint64_t comp_v = comp[v];
                                                 if (comp_u != comp_v)
                                                 {
                                                     // Hooking condition so lower component ID wins independent of direction
                                                     uint64_t high_comp = std::max(comp_u, comp_v);
                                                     uint64_t low_comp = std::min(comp_u, comp_v);
                                                     if (high_comp == comp[high_comp])
                                                     {
                                                         change = true;
                                                         //     COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
                                                         comp[high_comp] = low_comp;
                                                     }
                                                 }
                                                 //incoming_total += outgoing_contrib[dst];
                                             }
                                         }
                                     }*/
                                }
                            }
                            //
                        }
                        else
                        {

// deletion bit per source-vertex not active direct read L2 vector (when deletion in indr_array)
                            /* for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                             {
                                 stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];
                                 if (eb_k != NULL)
                                 {
                                     int l2_len = (u_int64_t) 1 << eb_k->getBlockSize();
                                     bool is_delete_l2_flag = false;
                                     is_delete_l2_flag = eb_k->getIsDeletion();
                                     if (!is_delete_l2_flag)
                                     {

                                         for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++)
                                         {
                                             uint64_t v = eb_k->getDestIdArr()[ind_l2];

                                             uint64_t comp_u = comp[u];
                                             uint64_t comp_v = comp[v];
                                             if (comp_u != comp_v)
                                             {
                                                 // Hooking condition so lower component ID wins independent of direction
                                                 uint64_t high_comp = std::max(comp_u, comp_v);
                                                 uint64_t low_comp = std::min(comp_u, comp_v);
                                                 if (high_comp == comp[high_comp])
                                                 {
                                                     change = true;
                                                     //  COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
                                                     comp[high_comp] = low_comp;
                                                 }
                                             }
                                         }

                                     }
                                     else
                                     {*/
                            /*  for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++) {
                                  if (eb_k->getEdgeUpdate()[ind_l2] != NULL) {
                                      if (eb_k->getEdgeUpdate()[ind_l2]->getInvldTime() != NULL)
                                      {
                                          uint64_t v = eb_k->getDestIdArr()[ind_l2];

                                          uint64_t comp_u = comp[u];
                                          uint64_t comp_v = comp[v];
                                          if (comp_u != comp_v)
                                          {
                                              // Hooking condition so lower component ID wins independent of direction
                                              uint64_t high_comp = std::max(comp_u, comp_v);
                                              uint64_t low_comp = std::min(comp_u, comp_v);
                                              if (high_comp == comp[high_comp])
                                              {
                                                  change = true;
                                                  //    COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
                                                  comp[high_comp] = low_comp;
                                              }
                                          }
                                      }
                                  }
                              }*/
                            /*    }
                            }
                        }*/
                        }
                    }
                    else
                    {
                        // directly read from edge block
                        stalb_type *eb_k = static_cast<stalb_type *>(index[u].getEdgePtr());

                        // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                        uint8_t is_delete_l2_flag = 0;
                        is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                        if (!is_delete_l2_flag)
                        {

                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                            {
                                // uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);

                                uint64_t comp_u = comp[u];
                                uint64_t comp_v = comp[v];
                                if (comp_u != comp_v)
                                {
                                    // Hooking condition so lower component ID wins independent of direction
                                    uint64_t high_comp = std::max(comp_u, comp_v);
                                    uint64_t low_comp = std::min(comp_u, comp_v);
                                    if (high_comp == comp[high_comp])
                                    {
                                        change = true;
                                        // COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
                                        comp[high_comp] = low_comp;
                                    }
                                }
                            }

                        }
                        else
                        {
                            /* for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++)
                             {
                                 if (eb_k->getEdgeUpdate()[ind_l2] != NULL)
                                 {
                                     if (eb_k->getEdgeUpdate()[ind_l2]->getInvldTime() != NULL)
                                     {
                                         uint64_t v = eb_k->getDestIdArr()[ind_l2];

                                         uint64_t comp_u = comp[u];
                                         uint64_t comp_v = comp[v];
                                         if (comp_u != comp_v)
                                         {
                                             // Hooking condition so lower component ID wins independent of direction
                                             uint64_t high_comp = std::max(comp_u, comp_v);
                                             uint64_t low_comp = std::min(comp_u, comp_v);
                                             if (high_comp == comp[high_comp])
                                             {
                                                 change = true;
                                                 //     COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
                                                 comp[high_comp] = low_comp;
                                             }
                                         }
                                     }
                                 }
                             }*/
                        }
                    }
//
// STAL end
//
// STAL end

//

/* while(iterator.valid()){
    uint64_t v = iterator.dst_id();

    uint64_t comp_u = comp[u];
    uint64_t comp_v = comp[v];
    if (comp_u != comp_v) {
        // Hooking condition so lower component ID wins independent of direction
        uint64_t high_comp = std::max(comp_u, comp_v);
        uint64_t low_comp = std::min(comp_u, comp_v);
        if (high_comp == comp[high_comp]) {
            change = true;
            COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
            comp[high_comp] = low_comp;
        }
    }

    iterator.next();
}*/
                }

#pragma omp parallel for schedule(dynamic, 64)
                for (uint64_t n = 0; n < max_vertex_id; n++)
                {
                    // if(comp[n] == std::numeric_limits<uint64_t>::max()) continue; // the vertex does not exist

                    while (comp[n] != comp[comp[n]])
                    {
                        comp[n] = comp[comp[n]];
                    }
                }

            }
        }
        else
        {
            while (change)
            {
                change = false;

#pragma omp parallel for schedule(dynamic, 64)
                for (uint64_t u = 0; u < max_vertex_id; u++)
                {
                    if (!(index[u].edgePtr & 0x1))
                    {

                        PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[u].getEdgePtr());
                        // deletion bit per source-vertex not active direct read L2 vector
                        for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < (u_int64_t) 1 << ind_ptr->getBlockLen(); ind_l1++)
                        {

                            stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];


                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                            {
                                // uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);

                                uint64_t comp_u = comp[u];
                                uint64_t comp_v = comp[v];
                                if (comp_u != comp_v)
                                {
                                    // Hooking condition so lower component ID wins independent of direction
                                    uint64_t high_comp = std::max(comp_u, comp_v);
                                    uint64_t low_comp = std::min(comp_u, comp_v);
                                    if (high_comp == comp[high_comp])
                                    {
                                        change = true;
                                        //   COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
                                        comp[high_comp] = low_comp;
                                    }
                                }
                            }
                        }
                        //

                    }
                    else
                    {
                        // directly read from edge block
                        stalb_type *eb_k = static_cast<stalb_type *>(index[u].getEdgePtr());

                        // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                        for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                        {
                            // uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                            uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);

                            uint64_t comp_u = comp[u];
                            uint64_t comp_v = comp[v];
                            if (comp_u != comp_v)
                            {
                                // Hooking condition so lower component ID wins independent of direction
                                uint64_t high_comp = std::max(comp_u, comp_v);
                                uint64_t low_comp = std::min(comp_u, comp_v);
                                if (high_comp == comp[high_comp])
                                {
                                    change = true;
                                    // COUT_DEBUG_WCC("comp[" << high_comp << "] = " << low_comp);
                                    comp[high_comp] = low_comp;
                                }
                            }
                        }

                    }
                }

#pragma omp parallel for schedule(dynamic, 64)
                for (uint64_t n = 0; n < max_vertex_id; n++)
                {
                    // if(comp[n] == std::numeric_limits<uint64_t>::max()) continue; // the vertex does not exist

                    while (comp[n] != comp[comp[n]])
                    {
                        comp[n] = comp[comp[n]];
                    }
                }

            }
        }
    }
    return ptr_components;
}

// cdlp
std::unique_ptr<uint64_t[]> HBALStore::do_cdlp(uint64_t max_vertex_id, bool is_graph_directed, uint64_t max_iterations)
{

    std::unique_ptr<uint64_t[]> ptr_labels0{new uint64_t[max_vertex_id]};
    std::unique_ptr<uint64_t[]> ptr_labels1{new uint64_t[max_vertex_id]};
    uint64_t *labels0 = ptr_labels0.get(); // current labels
    uint64_t *labels1 = ptr_labels1.get(); // labels for the next iteration

    // initialisation
#pragma omp parallel for
    for (uint64_t v = 0; v < max_vertex_id; v++)
    {
        labels0[v] = v;
    }

    // algorithm pass
    bool change = true;
    uint64_t current_iteration = 0;
    if (!IsReadWrite)
    {
        if (!NODELETEOOO)
        {
            while (current_iteration < max_iterations && change)
            {
                change = false; // reset the flag

#pragma omp parallel for schedule(dynamic, 64) shared(change)
                for (uint64_t v = 0; v < max_vertex_id; v++)
                {

                    std::unordered_map<uint64_t, uint64_t> histogram;

                    /// get dest node entries start


                    //
                    if (!(index[v].edgePtr & 0x1))
                    {

                        PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[v].getEdgePtr());

                        int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                        bool flag_l1_delete = false;
                        flag_l1_delete = ind_ptr->isDeletionBlock();

                        if (!flag_l1_delete)
                        {
                            // deletion bit per source-vertex not active direct read L2 vector
                            for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                            {

                                stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];

                                // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                                uint8_t is_delete_l2_flag = 0;
                                is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                if (!is_delete_l2_flag)
                                {

                                    for (uint16_t ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                                    {
                                        uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                        histogram[labels0[dst]]++;
                                    }

                                }
                                else
                                {
                                    /* for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++)
                                     {
                                         if (eb_k->getEdgeUpdate()[ind_l2] != NULL)
                                         {
                                             if (eb_k->getEdgeUpdate()[ind_l2]->getInvldTime() != NULL)
                                             {
                                                 uint64_t dst = eb_k->getDestIdArr()[ind_l2];
                                                 histogram[labels0[dst]]++;
                                             }
                                         }
                                     }*/
                                }
                            }
                        }
                        else
                        {
                            /*
                                // deletion bit per source-vertex not active direct read L2 vector (when deletion in indr_array)
                                for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                                {
                                    EdgeBlock *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];
                                    if (eb_k != NULL)
                                    {
                                        int l2_len = (u_int64_t) 1 << eb_k->getBlockSize();
                                        bool is_delete_l2_flag = false;
                                        is_delete_l2_flag = eb_k->getIsDeletion();
                                        if (!is_delete_l2_flag)
                                        {

                                            for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++)
                                            {
                                                uint64_t dst =  eb_k->getDestIdArr()[ind_l2];
                                                histogram[labels0[dst]]++;
                                            }

                                        } else {
                                            for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++)
                                            {
                                                if (eb_k->getEdgeUpdate()[ind_l2] != NULL)
                                                {
                                                    if (eb_k->getEdgeUpdate()[ind_l2]->getInvldTime() != NULL)
                                                    {
                                                        uint64_t dst =  eb_k->getDestIdArr()[ind_l2];
                                                        histogram[labels0[dst]]++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }*/
                        }
                    }
                    else
                    {
                        // directly read from edge block
                        stalb_type *eb_k = static_cast<stalb_type *>(index[v].getEdgePtr());

                        // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                        uint8_t is_delete_l2_flag = 0;
                        is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                        if (!is_delete_l2_flag)
                        {

                            for (uint16_t ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                            {
                                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                histogram[labels0[dst]]++;
                            }

                        }
                        else
                        {
                            /*for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++)
                            {
                                if (eb_k->getEdgeUpdate()[ind_l2] != NULL)
                                {
                                    if (eb_k->getEdgeUpdate()[ind_l2]->getInvldTime() != NULL)
                                    {
                                        uint64_t dst =  eb_k->getDestIdArr()[ind_l2];
                                        histogram[labels0[dst]]++;
                                    }
                                }
                            }*/
                        }
                    }

                    // dest node entry end

                    uint64_t label_max = std::numeric_limits<int64_t>::max();
                    uint64_t count_max = 0;
                    for (const auto pair: histogram)
                    {
                        if (pair.second > count_max || (pair.second == count_max && pair.first < label_max))
                        {
                            label_max = pair.first;
                            count_max = pair.second;
                        }
                    }

                    labels1[v] = label_max;
                    change |= (labels0[v] != labels1[v]);
                }

                std::swap(labels0, labels1); // next iteration
                current_iteration++;
            }
        }
        else
        {
            //
            while (current_iteration < max_iterations && change)
            {
                change = false; // reset the flag

#pragma omp parallel for schedule(dynamic, 64) shared(change)
                for (uint64_t v = 0; v < max_vertex_id; v++)
                {

                    std::unordered_map<uint64_t, uint64_t> histogram;

                    if (!(index[v].edgePtr & 0x1))
                    {

                        PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[v].getEdgePtr());

                        int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();

                        // deletion bit per source-vertex not active direct read L2 vector
                        for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                        {

                            stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];
                            for (uint16_t ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                            {
                                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                histogram[labels0[dst]]++;
                            }

                        }
                    }
                    else
                    {
                        // directly read from edge block
                        stalb_type *eb_k = static_cast<stalb_type *>(index[v].getEdgePtr());

                        for (uint16_t ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                        {
                            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                            histogram[labels0[dst]]++;
                        }

                    }

                    // dest node entry end
                    uint64_t label_max = std::numeric_limits<int64_t>::max();
                    uint64_t count_max = 0;
                    for (const auto pair: histogram)
                    {
                        if (pair.second > count_max || (pair.second == count_max && pair.first < label_max))
                        {
                            label_max = pair.first;
                            count_max = pair.second;
                        }
                    }

                    labels1[v] = label_max;
                    change |= (labels0[v] != labels1[v]);
                }

                std::swap(labels0, labels1); // next iteration
                current_iteration++;
            }
        }
    }

    if (labels0 == ptr_labels0.get())
    {
        return ptr_labels0;
    }
    else
    {
        return ptr_labels1;
    }
}

std::vector<WeightT> HBALStore::do_sssp(uint64_t num_edges, uint64_t max_vertex_id, uint64_t source, double delta, int read_version, MemoryAllocator *me)
{
    // Init
    std::vector<WeightT> dist(max_vertex_id, std::numeric_limits<WeightT>::infinity());
    dist[source] = 0;
    std::unique_ptr<uint64_t[]> ptr_edges_count_per_vertex{new uint64_t[max_vertex_id]()}; // avoid memory leaks
    uint64_t *__restrict edges_count_per_vertex = ptr_edges_count_per_vertex.get();
#pragma omp parallel for
    for (uint64_t v = 0; v < max_vertex_id; v++)
    {
        //scores[v] = init_score;
        index[v].perVertexLock.lock(); ///////
        edges_count_per_vertex[v] = perSourceGetDegree(v, 0, me);//index[v].degree; //perSourceGetDegree(v, read_version);
        index[v].perVertexLock.unlock();
    }
    num_edges = 0; // = CalculateNumberOfEdges();

    //#pragma omp parallel for reduction(+:num_edges)
    for (uint64_t n = 0; n < max_vertex_id; n++)
    {
        num_edges += (edges_count_per_vertex[n]);
    }

    min_read_degree_version = MaxValue;
    // std::cout<<"end min version"<<std::endl;

    // pagerank iterations
    // readTable->readQuery[read_version]
    timestamp_t min_read_versions = min_read_version.load();
    gapbs::pvector<NodeID> frontier(num_edges);
    // two element arrays for double buffering curr=iter&1, next=(iter+1)&1
    size_t shared_indexes[2] = {0, kMaxBin};
    size_t frontier_tails[2] = {1, 0};
    frontier[0] = source;
    if (IsReadWrite)
    {
#pragma omp parallel
        {
            std::vector<std::vector<NodeID> > local_bins(0);
            size_t iter = 0;

            while (shared_indexes[iter & 1] != kMaxBin)
            {
                size_t &curr_bin_index = shared_indexes[iter & 1];
                size_t &next_bin_index = shared_indexes[(iter + 1) & 1];
                size_t &curr_frontier_tail = frontier_tails[iter & 1];
                size_t &next_frontier_tail = frontier_tails[(iter + 1) & 1];
#pragma omp for nowait schedule(dynamic, 64)
                for (size_t i = 0; i < curr_frontier_tail; i++)
                {
                    NodeID u = frontier[i];
                    if (dist[u] >= delta * static_cast<WeightT>(curr_bin_index))
                    {

                        // stal start
                        //    PerSourceVertexIndr *ps = ds->getSourceVertexPointer(u);//transaction.get_edges(u,   0); // fixme: incoming edges for directed graphs

                        //
                        bool IsWTS = false;
                        if (!HBALStore::IsReadWrite)
                        {
                            IsWTS = true;
                        }
                        //  if(HBALStore.Read)
                        if (!(index[u].edgePtr & 0x1))
                        {

                            PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[u].getEdgePtr());

                            int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                            bool flag_l1_delete = false;
                            flag_l1_delete = ind_ptr->isDeletionBlock();

                            if (!flag_l1_delete)
                            {
                                // deletion bit per source-vertex not active direct read L2 vector
                                for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                                {

                                    stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];

                                    // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                                    int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                    uint8_t is_delete_l2_flag = 0;
                                    is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                    if (!is_delete_l2_flag)
                                    {

                                        double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                        for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                        {
                                            //////////
                                            if (!IsWTS)
                                            {
                                                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                if (ei->getWTsEdge() < min_read_versions)
                                                {
                                                    IsWTS = true;
                                                    uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                                    // get dest
                                                    double w = property[ind_l2];
                                                    WeightT old_dist = dist[v];
                                                    WeightT new_dist = dist[u] + w;
                                                    if (new_dist < old_dist)
                                                    {
                                                        bool changed_dist = true;
                                                        while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                        {
                                                            old_dist = dist[v];
                                                            if (old_dist <= new_dist)
                                                            {
                                                                changed_dist = false;
                                                                break;
                                                            }
                                                        }
                                                        if (changed_dist)
                                                        {
                                                            size_t dest_bin = new_dist / delta;
                                                            if (dest_bin >= local_bins.size())
                                                            {
                                                                local_bins.resize(dest_bin + 1);
                                                            }
                                                            local_bins[dest_bin].push_back(v);
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                                // get dest
                                                double w = property[ind_l2];
                                                WeightT old_dist = dist[v];
                                                WeightT new_dist = dist[u] + w;
                                                if (new_dist < old_dist)
                                                {
                                                    bool changed_dist = true;
                                                    while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                    {
                                                        old_dist = dist[v];
                                                        if (old_dist <= new_dist)
                                                        {
                                                            changed_dist = false;
                                                            break;
                                                        }
                                                    }
                                                    if (changed_dist)
                                                    {
                                                        size_t dest_bin = new_dist / delta;
                                                        if (dest_bin >= local_bins.size())
                                                        {
                                                            local_bins.resize(dest_bin + 1);
                                                        }
                                                        local_bins[dest_bin].push_back(v);
                                                    }
                                                }

                                            }
                                        }

                                    }
                                    else
                                    {
                                        // %%%%%%%%%%%%%% if stalb contains deletion start %%%%%%%%

                                        //get the latest deletion wts
                                        // timestamp_t latest_deletion = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1<< *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8))[0]; //
                                        for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                        {

                                            /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                            if (!IsWTS)
                                            {
                                                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                if (ei->getWTsEdge() < min_read_versions)
                                                {
                                                    IsWTS = true;
                                                    uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                                    if (!check_version(v))
                                                    {
                                                        // incoming_total += outgoing_contrib[dst];
                                                        double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                        double w = property[ind_l2];
                                                        WeightT old_dist = dist[v];
                                                        WeightT new_dist = dist[u] + w;
                                                        if (new_dist < old_dist)
                                                        {
                                                            bool changed_dist = true;
                                                            while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                            {
                                                                old_dist = dist[v];
                                                                if (old_dist <= new_dist)
                                                                {
                                                                    changed_dist = false;
                                                                    break;
                                                                }
                                                            }
                                                            if (changed_dist)
                                                            {
                                                                size_t dest_bin = new_dist / delta;
                                                                if (dest_bin >= local_bins.size())
                                                                {
                                                                    local_bins.resize(dest_bin + 1);
                                                                }
                                                                local_bins[dest_bin].push_back(v);
                                                            }
                                                        }

                                                    }
                                                    else
                                                    {
                                                        if (!check_version_read(v))
                                                        {

                                                            // if ((readTable->readQuery[read_version] < latest_deletion))
                                                            //{
                                                            if (min_read_versions < ei->getInvldTime()->getWts())
                                                            {

                                                                double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                                double w = property[ind_l2];
                                                                WeightT old_dist = dist[v];
                                                                WeightT new_dist = dist[u] + w;
                                                                if (new_dist < old_dist)
                                                                {
                                                                    bool changed_dist = true;
                                                                    while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                                    {
                                                                        old_dist = dist[v];
                                                                        if (old_dist <= new_dist)
                                                                        {
                                                                            changed_dist = false;
                                                                            break;
                                                                        }
                                                                    }
                                                                    if (changed_dist)
                                                                    {
                                                                        size_t dest_bin = new_dist / delta;
                                                                        if (dest_bin >= local_bins.size())
                                                                        {
                                                                            local_bins.resize(dest_bin + 1);
                                                                        }
                                                                        local_bins[dest_bin].push_back(v);
                                                                    }
                                                                }


                                                                //incoming_total += outgoing_contrib[dst];
                                                            }
                                                            else
                                                            {
                                                                //   if(readTable->readQuery[read_version] == getMinReadVersion())
                                                                //  {
                                                                *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                                // }
                                                            }
                                                            // }
                                                        }
                                                        //
                                                    }

                                                }
                                            }
                                            else
                                            {

                                                uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                                if (!check_version(v))
                                                {
                                                    double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                    double w = property[ind_l2];
                                                    WeightT old_dist = dist[v];
                                                    WeightT new_dist = dist[u] + w;
                                                    if (new_dist < old_dist)
                                                    {
                                                        bool changed_dist = true;
                                                        while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                        {
                                                            old_dist = dist[v];
                                                            if (old_dist <= new_dist)
                                                            {
                                                                changed_dist = false;
                                                                break;
                                                            }
                                                        }
                                                        if (changed_dist)
                                                        {
                                                            size_t dest_bin = new_dist / delta;
                                                            if (dest_bin >= local_bins.size())
                                                            {
                                                                local_bins.resize(dest_bin + 1);
                                                            }
                                                            local_bins[dest_bin].push_back(v);
                                                        }
                                                    }

                                                }
                                                else
                                                {
                                                    if (!check_version_read(v))
                                                    {

                                                        // if ((readTable->readQuery[read_version] < latest_deletion))
                                                        //{

                                                        EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                        if (min_read_versions < ei->getInvldTime()->getWts())
                                                        {
                                                            double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                            double w = property[ind_l2];
                                                            WeightT old_dist = dist[v];
                                                            WeightT new_dist = dist[u] + w;
                                                            if (new_dist < old_dist)
                                                            {
                                                                bool changed_dist = true;
                                                                while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                                {
                                                                    old_dist = dist[v];
                                                                    if (old_dist <= new_dist)
                                                                    {
                                                                        changed_dist = false;
                                                                        break;
                                                                    }
                                                                }
                                                                if (changed_dist)
                                                                {
                                                                    size_t dest_bin = new_dist / delta;
                                                                    if (dest_bin >= local_bins.size())
                                                                    {
                                                                        local_bins.resize(dest_bin + 1);
                                                                    }
                                                                    local_bins[dest_bin].push_back(v);
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            // if(readTable->readQuery[read_version] == getMinReadVersion())
                                                            // {
                                                            *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                            // }
                                                            //

                                                        }
                                                        // }
                                                    }

                                                }

                                            }
                                            /// %%%%%%%%%%% end %%%%%%%%%%
                                        }



                                        // %%%%%%%%%%%%%% if stalb contains deletion end %%%%%%%%

                                    }
                                }
                            }
                            else
                            {

                                ////////////////
                                for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                                {
                                    if (ind_ptr->getperVertexBlockArr()[ind_l1] != ((stalb_type *) FLAG_EMPTY_SLOT))
                                    {
                                        stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];

                                        // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                                        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                        uint8_t is_delete_l2_flag = 0;
                                        is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                        if (!is_delete_l2_flag)
                                        {

                                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                            {

                                                //////////
                                                if (!IsWTS)
                                                {
                                                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                    if (ei->getWTsEdge() < min_read_versions)
                                                    {
                                                        IsWTS = true;
                                                        uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                                        // get dest
                                                        double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                        double w = property[ind_l2];
                                                        WeightT old_dist = dist[v];
                                                        WeightT new_dist = dist[u] + w;
                                                        if (new_dist < old_dist)
                                                        {
                                                            bool changed_dist = true;
                                                            while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                            {
                                                                old_dist = dist[v];
                                                                if (old_dist <= new_dist)
                                                                {
                                                                    changed_dist = false;
                                                                    break;
                                                                }
                                                            }
                                                            if (changed_dist)
                                                            {
                                                                size_t dest_bin = new_dist / delta;
                                                                if (dest_bin >= local_bins.size())
                                                                {
                                                                    local_bins.resize(dest_bin + 1);
                                                                }
                                                                local_bins[dest_bin].push_back(v);
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                                    // get dest
                                                    double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                    double w = property[ind_l2];
                                                    WeightT old_dist = dist[v];
                                                    WeightT new_dist = dist[u] + w;
                                                    if (new_dist < old_dist)
                                                    {
                                                        bool changed_dist = true;
                                                        while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                        {
                                                            old_dist = dist[v];
                                                            if (old_dist <= new_dist)
                                                            {
                                                                changed_dist = false;
                                                                break;
                                                            }
                                                        }
                                                        if (changed_dist)
                                                        {
                                                            size_t dest_bin = new_dist / delta;
                                                            if (dest_bin >= local_bins.size())
                                                            {
                                                                local_bins.resize(dest_bin + 1);
                                                            }
                                                            local_bins[dest_bin].push_back(v);
                                                        }
                                                    }

                                                }


                                                /////////

                                                // dest end
                                                // incoming_total += outgoing_contrib[dst];
                                            }

                                        }
                                        else
                                        {
                                            // %%%%%%%%%%%%%% if stalb contains deletion start %%%%%%%%

                                            //get the latest deletion wts
                                            // timestamp_t latest_deletion = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1<< *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8))[0]; //
                                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                            {

                                                /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                                if (!IsWTS)
                                                {
                                                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                    if (ei->getWTsEdge() < min_read_versions)
                                                    {
                                                        IsWTS = true;
                                                        uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                                        if (!check_version(v))
                                                        {
                                                            // incoming_total += outgoing_contrib[dst];
                                                            double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                            double w = property[ind_l2];
                                                            WeightT old_dist = dist[v];
                                                            WeightT new_dist = dist[u] + w;
                                                            if (new_dist < old_dist)
                                                            {
                                                                bool changed_dist = true;
                                                                while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                                {
                                                                    old_dist = dist[v];
                                                                    if (old_dist <= new_dist)
                                                                    {
                                                                        changed_dist = false;
                                                                        break;
                                                                    }
                                                                }
                                                                if (changed_dist)
                                                                {
                                                                    size_t dest_bin = new_dist / delta;
                                                                    if (dest_bin >= local_bins.size())
                                                                    {
                                                                        local_bins.resize(dest_bin + 1);
                                                                    }
                                                                    local_bins[dest_bin].push_back(v);
                                                                }
                                                            }

                                                        }
                                                        else
                                                        {
                                                            if (!check_version_read(v))
                                                            {

                                                                // if ((readTable->readQuery[read_version] < latest_deletion))
                                                                //{
                                                                if (min_read_versions < ei->getInvldTime()->getWts())
                                                                {

                                                                    double *property = *reinterpret_cast<double **>(
                                                                            eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                                    double w = property[ind_l2];
                                                                    WeightT old_dist = dist[v];
                                                                    WeightT new_dist = dist[u] + w;
                                                                    if (new_dist < old_dist)
                                                                    {
                                                                        bool changed_dist = true;
                                                                        while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                                        {
                                                                            old_dist = dist[v];
                                                                            if (old_dist <= new_dist)
                                                                            {
                                                                                changed_dist = false;
                                                                                break;
                                                                            }
                                                                        }
                                                                        if (changed_dist)
                                                                        {
                                                                            size_t dest_bin = new_dist / delta;
                                                                            if (dest_bin >= local_bins.size())
                                                                            {
                                                                                local_bins.resize(dest_bin + 1);
                                                                            }
                                                                            local_bins[dest_bin].push_back(v);
                                                                        }
                                                                    }


                                                                    //incoming_total += outgoing_contrib[dst];
                                                                }
                                                                else
                                                                {
                                                                    //   if(readTable->readQuery[read_version] == getMinReadVersion())
                                                                    //  {
                                                                    *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                                    // }
                                                                }
                                                                // }
                                                            }
                                                            //
                                                        }

                                                    }
                                                }
                                                else
                                                {

                                                    uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                                    if (!check_version(v))
                                                    {
                                                        double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                        double w = property[ind_l2];
                                                        WeightT old_dist = dist[v];
                                                        WeightT new_dist = dist[u] + w;
                                                        if (new_dist < old_dist)
                                                        {
                                                            bool changed_dist = true;
                                                            while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                            {
                                                                old_dist = dist[v];
                                                                if (old_dist <= new_dist)
                                                                {
                                                                    changed_dist = false;
                                                                    break;
                                                                }
                                                            }
                                                            if (changed_dist)
                                                            {
                                                                size_t dest_bin = new_dist / delta;
                                                                if (dest_bin >= local_bins.size())
                                                                {
                                                                    local_bins.resize(dest_bin + 1);
                                                                }
                                                                local_bins[dest_bin].push_back(v);
                                                            }
                                                        }

                                                    }
                                                    else
                                                    {
                                                        if (!check_version_read(v))
                                                        {

                                                            // if ((readTable->readQuery[read_version] < latest_deletion))
                                                            //{

                                                            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                            if (min_read_versions < ei->getInvldTime()->getWts())
                                                            {
                                                                double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                                double w = property[ind_l2];
                                                                WeightT old_dist = dist[v];
                                                                WeightT new_dist = dist[u] + w;
                                                                if (new_dist < old_dist)
                                                                {
                                                                    bool changed_dist = true;
                                                                    while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                                    {
                                                                        old_dist = dist[v];
                                                                        if (old_dist <= new_dist)
                                                                        {
                                                                            changed_dist = false;
                                                                            break;
                                                                        }
                                                                    }
                                                                    if (changed_dist)
                                                                    {
                                                                        size_t dest_bin = new_dist / delta;
                                                                        if (dest_bin >= local_bins.size())
                                                                        {
                                                                            local_bins.resize(dest_bin + 1);
                                                                        }
                                                                        local_bins[dest_bin].push_back(v);
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                // if(readTable->readQuery[read_version] == getMinReadVersion())
                                                                // {
                                                                *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                                // }
                                                                //

                                                            }
                                                            // }
                                                        }

                                                    }

                                                }
                                                /// %%%%%%%%%%% end %%%%%%%%%%
                                            }



                                            // %%%%%%%%%%%%%% if stalb contains deletion end %%%%%%%%

                                        }


                                    }


                                    //////////////


                                }
                            }
                        }
                        else
                        {
                            // directly read from edge block
                            stalb_type *eb_k = static_cast<stalb_type *>(index[u].getEdgePtr());

                            // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                            uint8_t is_delete_l2_flag = 0;
                            is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                            if (!is_delete_l2_flag)
                            {

                                double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));

                                for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                                {

                                    uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                    // get dest
                                    double w = property[ind_l2];
                                    WeightT old_dist = dist[v];
                                    WeightT new_dist = dist[u] + w;
                                    if (new_dist < old_dist)
                                    {
                                        bool changed_dist = true;
                                        while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                        {
                                            old_dist = dist[v];
                                            if (old_dist <= new_dist)
                                            {
                                                changed_dist = false;
                                                break;
                                            }
                                        }
                                        if (changed_dist)
                                        {
                                            size_t dest_bin = new_dist / delta;
                                            if (dest_bin >= local_bins.size())
                                            {
                                                local_bins.resize(dest_bin + 1);
                                            }
                                            local_bins[dest_bin].push_back(v);
                                        }
                                    }
                                    // get dest end
                                    // incoming_total += outgoing_contrib[dst];
                                }

                            }
                            else
                            {
                                /*   for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++) {
                                   if (eb_k->getEdgeUpdate()[ind_l2] != NULL) {
                                       if (eb_k->getEdgeUpdate()[ind_l2]->getInvldTime() != NULL) {

                                           // get dest
                                           uint64_t v   =  eb_k->getDestIdArr()[ind_l2];
                                           double w     =  eb_k->getpropertyIdArr()[ind_l2];
                                           WeightT old_dist = dist[v];
                                           WeightT new_dist = dist[u] + w;
                                           if (new_dist < old_dist)
                                           {
                                               bool changed_dist = true;
                                               while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                               {
                                                   old_dist = dist[v];
                                                   if (old_dist <= new_dist)
                                                   {
                                                       changed_dist = false;
                                                       break;
                                                   }
                                               }
                                               if (changed_dist)
                                               {
                                                   size_t dest_bin = new_dist/delta;
                                                   if (dest_bin >= local_bins.size())
                                                   {
                                                       local_bins.resize(dest_bin+1);
                                                   }
                                                   local_bins[dest_bin].push_back(v);
                                               }
                                           }
                                           // get dest end
                                       }
                                   }
                               }*/
                            }
                        }
                        // stal end
                    }
                }

                for (size_t i = curr_bin_index; i < local_bins.size(); i++)
                {
                    if (!local_bins[i].empty())
                    {
#pragma omp critical
                        next_bin_index = std::min(next_bin_index, i);
                        break;
                    }
                }

#pragma omp barrier
#pragma omp single nowait
                {
                    curr_bin_index = kMaxBin;
                    curr_frontier_tail = 0;
                }

                if (next_bin_index < local_bins.size())
                {
                    size_t copy_start = gapbs::fetch_and_add(next_frontier_tail, local_bins[next_bin_index].size());
                    std::copy(local_bins[next_bin_index].begin(), local_bins[next_bin_index].end(), frontier.data() + copy_start);
                    local_bins[next_bin_index].resize(0);
                }

                iter++;
#pragma omp barrier
            }

#if defined(DEBUG)
            #pragma omp single
        COUT_DEBUG("took " << iter << " iterations");
#endif
        }
    }
    else
    {
        // No read write
        if (!NODELETEOOO)
        {
#pragma omp parallel
            {
                std::vector<std::vector<NodeID> > local_bins(0);
                size_t iter = 0;

                while (shared_indexes[iter & 1] != kMaxBin)
                {
                    size_t &curr_bin_index = shared_indexes[iter & 1];
                    size_t &next_bin_index = shared_indexes[(iter + 1) & 1];
                    size_t &curr_frontier_tail = frontier_tails[iter & 1];
                    size_t &next_frontier_tail = frontier_tails[(iter + 1) & 1];
#pragma omp for nowait schedule(dynamic, 64)
                    for (size_t i = 0; i < curr_frontier_tail; i++)
                    {
                        NodeID u = frontier[i];
                        if (dist[u] >= delta * static_cast<WeightT>(curr_bin_index))
                        {

                            // stal start
                            //    PerSourceVertexIndr *ps = ds->getSourceVertexPointer(u);//transaction.get_edges(u,   0); // fixme: incoming edges for directed graphs

                            //
                            bool IsWTS = false;

                            //  if(HBALStore.Read)
                            if (!(index[u].edgePtr & 0x1))
                            {

                                PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[u].getEdgePtr());

                                int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                                bool flag_l1_delete = false;
                                flag_l1_delete = ind_ptr->isDeletionBlock();

                                if (!flag_l1_delete)
                                {
                                    // deletion bit per source-vertex not active direct read L2 vector
                                    for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                                    {

                                        stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];

                                        // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                                        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                        uint8_t is_delete_l2_flag = 0;
                                        is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                        if (!is_delete_l2_flag)
                                        {

                                            double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                            {
                                                //////////
                                                if (!IsWTS)
                                                {
                                                    EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                    if (ei->getWTsEdge() < min_read_versions)
                                                    {
                                                        IsWTS = true;
                                                        uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                                        // get dest
                                                        double w = property[ind_l2];
                                                        WeightT old_dist = dist[v];
                                                        WeightT new_dist = dist[u] + w;
                                                        if (new_dist < old_dist)
                                                        {
                                                            bool changed_dist = true;
                                                            while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                            {
                                                                old_dist = dist[v];
                                                                if (old_dist <= new_dist)
                                                                {
                                                                    changed_dist = false;
                                                                    break;
                                                                }
                                                            }
                                                            if (changed_dist)
                                                            {
                                                                size_t dest_bin = new_dist / delta;
                                                                if (dest_bin >= local_bins.size())
                                                                {
                                                                    local_bins.resize(dest_bin + 1);
                                                                }
                                                                local_bins[dest_bin].push_back(v);
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                                    // get dest
                                                    double w = property[ind_l2];
                                                    WeightT old_dist = dist[v];
                                                    WeightT new_dist = dist[u] + w;
                                                    if (new_dist < old_dist)
                                                    {
                                                        bool changed_dist = true;
                                                        while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                        {
                                                            old_dist = dist[v];
                                                            if (old_dist <= new_dist)
                                                            {
                                                                changed_dist = false;
                                                                break;
                                                            }
                                                        }
                                                        if (changed_dist)
                                                        {
                                                            size_t dest_bin = new_dist / delta;
                                                            if (dest_bin >= local_bins.size())
                                                            {
                                                                local_bins.resize(dest_bin + 1);
                                                            }
                                                            local_bins[dest_bin].push_back(v);
                                                        }
                                                    }

                                                }
                                            }

                                        }
                                        else
                                        {
                                            // %%%%%%%%%%%%%% if stalb contains deletion start %%%%%%%%

                                            //get the latest deletion wts
                                            // timestamp_t latest_deletion = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1<< *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8))[0]; //
                                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                            {

                                                /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                                uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                                if (!check_version(v))
                                                {
                                                    double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                    double w = property[ind_l2];
                                                    WeightT old_dist = dist[v];
                                                    WeightT new_dist = dist[u] + w;
                                                    if (new_dist < old_dist)
                                                    {
                                                        bool changed_dist = true;
                                                        while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                        {
                                                            old_dist = dist[v];
                                                            if (old_dist <= new_dist)
                                                            {
                                                                changed_dist = false;
                                                                break;
                                                            }
                                                        }
                                                        if (changed_dist)
                                                        {
                                                            size_t dest_bin = new_dist / delta;
                                                            if (dest_bin >= local_bins.size())
                                                            {
                                                                local_bins.resize(dest_bin + 1);
                                                            }
                                                            local_bins[dest_bin].push_back(v);
                                                        }
                                                    }

                                                }
                                                else
                                                {
                                                    if (!check_version_read(v))
                                                    {

                                                        // if ((readTable->readQuery[read_version] < latest_deletion))
                                                        //{

                                                        EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                        if (min_read_versions < ei->getInvldTime()->getWts())
                                                        {
                                                            double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                            double w = property[ind_l2];
                                                            WeightT old_dist = dist[v];
                                                            WeightT new_dist = dist[u] + w;
                                                            if (new_dist < old_dist)
                                                            {
                                                                bool changed_dist = true;
                                                                while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                                {
                                                                    old_dist = dist[v];
                                                                    if (old_dist <= new_dist)
                                                                    {
                                                                        changed_dist = false;
                                                                        break;
                                                                    }
                                                                }
                                                                if (changed_dist)
                                                                {
                                                                    size_t dest_bin = new_dist / delta;
                                                                    if (dest_bin >= local_bins.size())
                                                                    {
                                                                        local_bins.resize(dest_bin + 1);
                                                                    }
                                                                    local_bins[dest_bin].push_back(v);
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            // if(readTable->readQuery[read_version] == getMinReadVersion())
                                                            // {
                                                            *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                            // }
                                                            //

                                                        }
                                                        // }
                                                    }

                                                }

                                                /// %%%%%%%%%%% end %%%%%%%%%%
                                            }



                                            // %%%%%%%%%%%%%% if stalb contains deletion end %%%%%%%%

                                        }
                                    }
                                }
                                else
                                {

                                    ////////////////
                                    for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                                    {
                                        if (ind_ptr->getperVertexBlockArr()[ind_l1] != ((stalb_type *) FLAG_EMPTY_SLOT))
                                        {
                                            stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];

                                            // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                                            int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                            uint8_t is_delete_l2_flag = 0;
                                            is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                            if (!is_delete_l2_flag)
                                            {

                                                for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                                {

                                                    //////////
                                                    uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                                    // get dest
                                                    double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                    double w = property[ind_l2];
                                                    WeightT old_dist = dist[v];
                                                    WeightT new_dist = dist[u] + w;
                                                    if (new_dist < old_dist)
                                                    {
                                                        bool changed_dist = true;
                                                        while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                        {
                                                            old_dist = dist[v];
                                                            if (old_dist <= new_dist)
                                                            {
                                                                changed_dist = false;
                                                                break;
                                                            }
                                                        }
                                                        if (changed_dist)
                                                        {
                                                            size_t dest_bin = new_dist / delta;
                                                            if (dest_bin >= local_bins.size())
                                                            {
                                                                local_bins.resize(dest_bin + 1);
                                                            }
                                                            local_bins[dest_bin].push_back(v);
                                                        }
                                                    }




                                                    /////////

                                                    // dest end
                                                    // incoming_total += outgoing_contrib[dst];
                                                }

                                            }
                                            else
                                            {
                                                // %%%%%%%%%%%%%% if stalb contains deletion start %%%%%%%%

                                                //get the latest deletion wts
                                                // timestamp_t latest_deletion = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1<< *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8))[0]; //
                                                for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                                {

                                                    /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                                    uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);//eb_k->getDestIdArr()[ind_l2];   //getEdgeUpdate()[ind_l2]->getDestId();
                                                    if (!check_version(v))
                                                    {
                                                        double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                        double w = property[ind_l2];
                                                        WeightT old_dist = dist[v];
                                                        WeightT new_dist = dist[u] + w;
                                                        if (new_dist < old_dist)
                                                        {
                                                            bool changed_dist = true;
                                                            while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                            {
                                                                old_dist = dist[v];
                                                                if (old_dist <= new_dist)
                                                                {
                                                                    changed_dist = false;
                                                                    break;
                                                                }
                                                            }
                                                            if (changed_dist)
                                                            {
                                                                size_t dest_bin = new_dist / delta;
                                                                if (dest_bin >= local_bins.size())
                                                                {
                                                                    local_bins.resize(dest_bin + 1);
                                                                }
                                                                local_bins[dest_bin].push_back(v);
                                                            }
                                                        }

                                                    }
                                                    else
                                                    {
                                                        if (!check_version_read(v))
                                                        {

                                                            // if ((readTable->readQuery[read_version] < latest_deletion))
                                                            //{

                                                            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                                            if (min_read_versions < ei->getInvldTime()->getWts())
                                                            {
                                                                double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                                                double w = property[ind_l2];
                                                                WeightT old_dist = dist[v];
                                                                WeightT new_dist = dist[u] + w;
                                                                if (new_dist < old_dist)
                                                                {
                                                                    bool changed_dist = true;
                                                                    while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                                    {
                                                                        old_dist = dist[v];
                                                                        if (old_dist <= new_dist)
                                                                        {
                                                                            changed_dist = false;
                                                                            break;
                                                                        }
                                                                    }
                                                                    if (changed_dist)
                                                                    {
                                                                        size_t dest_bin = new_dist / delta;
                                                                        if (dest_bin >= local_bins.size())
                                                                        {
                                                                            local_bins.resize(dest_bin + 1);
                                                                        }
                                                                        local_bins[dest_bin].push_back(v);
                                                                    }
                                                                }
                                                            }
                                                            else
                                                            {
                                                                // if(readTable->readQuery[read_version] == getMinReadVersion())
                                                                // {
                                                                *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8) = add_version_read(*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8));
                                                                // }
                                                                //

                                                            }
                                                            // }
                                                        }

                                                    }

                                                    /// %%%%%%%%%%% end %%%%%%%%%%
                                                }



                                                // %%%%%%%%%%%%%% if stalb contains deletion end %%%%%%%%

                                            }


                                        }


                                        //////////////


                                    }
                                }
                            }
                            else
                            {
                                // directly read from edge block
                                stalb_type *eb_k = static_cast<stalb_type *>(index[u].getEdgePtr());

                                // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                                uint8_t is_delete_l2_flag = 0;
                                is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                if (!is_delete_l2_flag)
                                {

                                    double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));

                                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                                    {

                                        uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                        // get dest
                                        double w = property[ind_l2];
                                        WeightT old_dist = dist[v];
                                        WeightT new_dist = dist[u] + w;
                                        if (new_dist < old_dist)
                                        {
                                            bool changed_dist = true;
                                            while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                            {
                                                old_dist = dist[v];
                                                if (old_dist <= new_dist)
                                                {
                                                    changed_dist = false;
                                                    break;
                                                }
                                            }
                                            if (changed_dist)
                                            {
                                                size_t dest_bin = new_dist / delta;
                                                if (dest_bin >= local_bins.size())
                                                {
                                                    local_bins.resize(dest_bin + 1);
                                                }
                                                local_bins[dest_bin].push_back(v);
                                            }
                                        }
                                        // get dest end
                                        // incoming_total += outgoing_contrib[dst];
                                    }

                                }
                                else
                                {
                                    /*   for (int ind_l2 = eb_k->getCurIndex(); ind_l2 < l2_len; ind_l2++) {
                                           if (eb_k->getEdgeUpdate()[ind_l2] != NULL) {
                                               if (eb_k->getEdgeUpdate()[ind_l2]->getInvldTime() != NULL) {

                                                   // get dest
                                                   uint64_t v   =  eb_k->getDestIdArr()[ind_l2];
                                                   double w     =  eb_k->getpropertyIdArr()[ind_l2];
                                                   WeightT old_dist = dist[v];
                                                   WeightT new_dist = dist[u] + w;
                                                   if (new_dist < old_dist)
                                                   {
                                                       bool changed_dist = true;
                                                       while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                                       {
                                                           old_dist = dist[v];
                                                           if (old_dist <= new_dist)
                                                           {
                                                               changed_dist = false;
                                                               break;
                                                           }
                                                       }
                                                       if (changed_dist)
                                                       {
                                                           size_t dest_bin = new_dist/delta;
                                                           if (dest_bin >= local_bins.size())
                                                           {
                                                               local_bins.resize(dest_bin+1);
                                                           }
                                                           local_bins[dest_bin].push_back(v);
                                                       }
                                                   }
                                                   // get dest end
                                               }
                                           }
                                       }*/
                                }
                            }
                            // stal end
                        }
                    }

                    for (size_t i = curr_bin_index; i < local_bins.size(); i++)
                    {
                        if (!local_bins[i].empty())
                        {
#pragma omp critical
                            next_bin_index = std::min(next_bin_index, i);
                            break;
                        }
                    }

#pragma omp barrier
#pragma omp single nowait
                    {
                        curr_bin_index = kMaxBin;
                        curr_frontier_tail = 0;
                    }

                    if (next_bin_index < local_bins.size())
                    {
                        size_t copy_start = gapbs::fetch_and_add(next_frontier_tail, local_bins[next_bin_index].size());
                        std::copy(local_bins[next_bin_index].begin(), local_bins[next_bin_index].end(), frontier.data() + copy_start);
                        local_bins[next_bin_index].resize(0);
                    }

                    iter++;
#pragma omp barrier
                }

#if defined(DEBUG)
                #pragma omp single
                COUT_DEBUG("took " << iter << " iterations");
#endif
            }
        }
        else
        {
#pragma omp parallel
            {
                std::vector<std::vector<NodeID> > local_bins(0);
                size_t iter = 0;

                while (shared_indexes[iter & 1] != kMaxBin)
                {
                    size_t &curr_bin_index = shared_indexes[iter & 1];
                    size_t &next_bin_index = shared_indexes[(iter + 1) & 1];
                    size_t &curr_frontier_tail = frontier_tails[iter & 1];
                    size_t &next_frontier_tail = frontier_tails[(iter + 1) & 1];
#pragma omp for nowait schedule(dynamic, 64)
                    for (size_t i = 0; i < curr_frontier_tail; i++)
                    {
                        NodeID u = frontier[i];
                        if (dist[u] >= delta * static_cast<WeightT>(curr_bin_index))
                        {

                            // stal start
                            bool IsWTS = false;

                            //  if(HBALStore.Read)
                            if (!(index[u].edgePtr & 0x1))
                            {

                                PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[u].getEdgePtr());

                                int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();

                                // deletion bit per source-vertex not active direct read L2 vector
                                for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                                {

                                    stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];

                                    int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                    double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));
                                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                    {
                                        //////////

                                        uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                        // get dest
                                        double w = property[ind_l2];
                                        WeightT old_dist = dist[v];
                                        WeightT new_dist = dist[u] + w;
                                        if (new_dist < old_dist)
                                        {
                                            bool changed_dist = true;
                                            while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                            {
                                                old_dist = dist[v];
                                                if (old_dist <= new_dist)
                                                {
                                                    changed_dist = false;
                                                    break;
                                                }
                                            }
                                            if (changed_dist)
                                            {
                                                size_t dest_bin = new_dist / delta;
                                                if (dest_bin >= local_bins.size())
                                                {
                                                    local_bins.resize(dest_bin + 1);
                                                }
                                                local_bins[dest_bin].push_back(v);
                                            }
                                        }


                                    }

                                }
                            }
                            else
                            {
                                // directly read from edge block
                                stalb_type *eb_k = static_cast<stalb_type *>(index[u].getEdgePtr());

                                double *property = *reinterpret_cast<double **>( eb_k + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) * 8) - 8));

                                for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                                {

                                    uint64_t v = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                    // get dest
                                    double w = property[ind_l2];
                                    WeightT old_dist = dist[v];
                                    WeightT new_dist = dist[u] + w;
                                    if (new_dist < old_dist)
                                    {
                                        bool changed_dist = true;
                                        while (!gapbs::compare_and_swap(dist[v], old_dist, new_dist))
                                        {
                                            old_dist = dist[v];
                                            if (old_dist <= new_dist)
                                            {
                                                changed_dist = false;
                                                break;
                                            }
                                        }
                                        if (changed_dist)
                                        {
                                            size_t dest_bin = new_dist / delta;
                                            if (dest_bin >= local_bins.size())
                                            {
                                                local_bins.resize(dest_bin + 1);
                                            }
                                            local_bins[dest_bin].push_back(v);
                                        }
                                    }
                                    // get dest end
                                    // incoming_total += outgoing_contrib[dst];
                                }

                            }
                            // stal end
                        }
                    }

                    for (size_t i = curr_bin_index; i < local_bins.size(); i++)
                    {
                        if (!local_bins[i].empty())
                        {
#pragma omp critical
                            next_bin_index = std::min(next_bin_index, i);
                            break;
                        }
                    }

#pragma omp barrier
#pragma omp single nowait
                    {
                        curr_bin_index = kMaxBin;
                        curr_frontier_tail = 0;
                    }

                    if (next_bin_index < local_bins.size())
                    {
                        size_t copy_start = gapbs::fetch_and_add(next_frontier_tail, local_bins[next_bin_index].size());
                        std::copy(local_bins[next_bin_index].begin(), local_bins[next_bin_index].end(), frontier.data() + copy_start);
                        local_bins[next_bin_index].resize(0);
                    }

                    iter++;
#pragma omp barrier
                }


            }
        }
    }
    return dist;
}

// sssp end
std::unique_ptr<double[]> HBALStore::do_scanedges(uint64_t max_vertex_id, uint64_t num_iterations, double damping_factor, int read_version, MemoryAllocator *me)
{
    max_vertex_id = get_high_water_mark();
    int64_t min_read_versions = 0;
    uint64_t countedges = 0;

    if (0 == 0)
    {
        for (uint64_t iteration = 0; iteration < 1; iteration++)
        {
            // std::cout<<"iteration # "<<iteration<<std::endl;

            // compute the new score for each node in the graph
            for (uint64_t v = 0; v < max_vertex_id; v++)
            {

                //  if(degrees[v] == std::numeric_limits<uint64_t>::max()){ continue; } // the vertex does not exist

                index[v].indirectionLock.lock();

                double incoming_total = 0;

                // per source STAL access

                // PerSourceVertexIndr *ps_stal = getSourceVertexPointer(v);//transaction.get_edges(u,   0); // fixme: incoming edges for directed graphs


                //
                bool IsWTS = false;
                if (index[v].edgePtr != 0)
                {

                    if (!(index[v].edgePtr & 0x1))
                    {
                        PerSourceIndrPointer *ind_ptr = static_cast<PerSourceIndrPointer *>(index[v].getEdgePtr());

                        int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                        bool flag_l1_delete = false;
                        flag_l1_delete = ind_ptr->isDeletionBlock();

                        if (1)
                        {
                            // deletion bit per source-vertex not active direct read L2 vector
                            for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < l1_len; ind_l1++)
                            {

                                stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1]; // stalb pointer

                                int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2; // get block size

                                uint8_t is_delete_l2_flag = 0;
                                is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                                if (1)
                                {

                                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                                    {

                                        /// %%%%%%%%% transaction time comparison to read updates that are < RTS time
                                        if (1)
                                        {
                                            uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                            EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));
                                            countedges++;
                                            // check if dst contains ooo updates
                                            if (check_version_ooo(dst))
                                            {
                                                // ooo updates exist
                                                // ooo updates exist
                                                stalb_type *art_val = reinterpret_cast<stalb_type *>(ei->getOutOfOrderUpdt());
                                                int l2_len_ooo = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2; // get block size
                                                for (int ind_l2_ooo = *reinterpret_cast<uint16_ptr>(art_val + 1); ind_l2_ooo < l2_len_ooo; ind_l2_ooo++)
                                                {
                                                    //
                                                    uint64_t dst_ooo = *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8);
                                                    if (check_version_ooo(dst_ooo))
                                                    {
                                                        countedges++;
                                                        //*stalb_incomming_total += outgoing_contrib->start_[remove_version_ooo(dst_ooo)]; counter
                                                    }
                                                    else
                                                    {
                                                        LeafNode *ln = *reinterpret_cast<LeafNode **>(art_val + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2)) * 8) + ((ind_l2_ooo * 8) - 8));
                                                        if (ln->getWTsEdge() < min_read_versions)
                                                        {
                                                            countedges++;
                                                            //  *stalb_incomming_total += outgoing_contrib->start_[dst_ooo];
                                                            *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8) = add_version_ooo(dst_ooo);
                                                        }
                                                    }
                                                }
                                                // ooo updates part end
                                            }
                                            // std::cout<<ei->getSTime() << "    "<<ei->getWTsEdge()<<std::endl;
                                        }
                                        /// %%%%%%%%%%% end %%%%%%%%%%
                                    }

                                }


                            }
                        }

                    }
                    else
                    {
                        stalb_type *eb_k = static_cast<stalb_type *>(index[v].getEdgePtr());

                        int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                        uint8_t is_delete_l2_flag = 0;
                        is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                        if (1)
                        {

                            for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < l2_len; ind_l2++)
                            {

                                /// %%%%%%%%% transaction time comparison to read updates that are < RTS time

                                EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) + ((ind_l2 * 8) - 8));

                                // std::cout<<ei->getSTime() << "    "<<ei->getWTsEdge()<<std::endl;
                                uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                // EdgeInfoHal *ei = *reinterpret_cast<EdgeInfoHal **>(eb_k + (((((u_int64_t) 1<< *reinterpret_cast<uint8_ptr>(eb_k)) / 2)) * 8) +((ind_l2 * 8) - 8));
                                countedges++;
                                // check if dst contains ooo updates
                                if (check_version_ooo(dst))
                                {

                                    // ooo updates exist
                                    // ooo updates exist
                                    stalb_type *art_val = reinterpret_cast<stalb_type *>(ei->getOutOfOrderUpdt());
                                    int l2_len_ooo = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2; // get block size
                                    for (int ind_l2_ooo = *reinterpret_cast<uint16_ptr>(art_val + 1); ind_l2_ooo < l2_len_ooo; ind_l2_ooo++)
                                    {
                                        //
                                        uint64_t dst_ooo = *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8);
                                        if (check_version_ooo(dst_ooo))
                                        {
                                            countedges++;
                                            //  *stalb_incomming_total += outgoing_contrib->start_[remove_version_ooo(dst_ooo)];
                                        }
                                        else
                                        {
                                            LeafNode *ln = *reinterpret_cast<LeafNode **>(art_val + (((((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(art_val)) / 2)) * 8) + ((ind_l2_ooo * 8) - 8));
                                            if (ln->getWTsEdge() < min_read_versions)
                                            {
                                                countedges++;
                                                // *stalb_incomming_total += outgoing_contrib->start_[dst_ooo];
                                                *reinterpret_cast<uint64_ptr>(art_val + ind_l2_ooo * 8) = add_version_ooo(dst_ooo);
                                            }
                                        }
                                    }
                                    // ooo updates part end
                                }
                                /// %%%%%%%%%%% end %%%%%%%%%%
                            }

                        }
                    }
                }

                index[v].indirectionLock.unlock();

            }
        }
    }

    // std::cout<<"total edge count : "<<countedges<<std::endl;
    return 0;
}

// lcc algorithm
std::unique_ptr<double[]> HBALStore::do_lcc_undirected(uint64_t max_vertex_id, MemoryAllocator *me)
{
    max_vertex_id = get_high_water_mark();

    std::unique_ptr<double[]> ptr_lcc{new double[max_vertex_id]};
    double *lcc = ptr_lcc.get();
    std::unique_ptr<uint32_t[]> ptr_degrees_out{new uint32_t[max_vertex_id]};
    uint32_t *__restrict degrees_out = ptr_degrees_out.get();
    // spinlock lcc_lock[max_vertex_id];
    std::unique_ptr<std::atomic<uint32_t>[]> p(new std::atomic<uint32_t>[max_vertex_id]());
    auto triangles_per_vertex = p.get();
    if (!IsReadWrite)
    {
        if (!NODELETEOOO)
        {
#pragma omp parallel for schedule(dynamic, 64)
            for (uint64_t v = 0; v < max_vertex_id; v++)
            {

                if (!(index[v].edgePtr & 0x1))
                {

                    PerSourceIndrPointer *ind_ptr = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(index[v].edgePtr));

                    //int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                    bool flag_l1_delete = false;
                    flag_l1_delete = ind_ptr->isDeletionBlock();

                    if (!flag_l1_delete)
                    {
                        // deletion bit per source-vertex not active direct read L2 vector
                        for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < (u_int64_t) 1 << ind_ptr->getBlockLen(); ind_l1++)
                        {

                            stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];

                            // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                            uint8_t is_delete_l2_flag = 0;
                            is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                            if (!is_delete_l2_flag)
                            {

                                for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                                {
                                    // uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                    uint64_t u = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8); // dest id

                                    ///////////////////////////// Level 1 start /////////////////////////////////////////////////////////////
                                    if (v > u)
                                    {
                                        continue;
                                    }

                                    if (!(index[u].edgePtr & 0x1))
                                    {

                                        PerSourceIndrPointer *ind_ptr_u = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(index[u].edgePtr));

                                        //int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                                        bool flag_l1_delete_u = false;
                                        flag_l1_delete_u = ind_ptr_u->isDeletionBlock();

                                        if (!flag_l1_delete_u)
                                        {
                                            // deletion bit per source-vertex not active direct read L2 vector
                                            for (int ind_l1_u = ind_ptr_u->getCurPos(); ind_l1_u < (u_int64_t) 1 << ind_ptr_u->getBlockLen(); ind_l1_u++)
                                            {
                                                stalb_type *eb_k_u = ind_ptr_u->getperVertexBlockArr()[ind_l1_u];
                                                // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                                                uint8_t is_delete_l2_flag_u = 0;
                                                is_delete_l2_flag_u = *reinterpret_cast<uint8_ptr>(eb_k_u + 3);
                                                if (!is_delete_l2_flag_u)
                                                {

                                                    for (int ind_l2_u = *reinterpret_cast<uint16_ptr>(eb_k_u + 1); ind_l2_u < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k_u)) / 2); ind_l2_u++)
                                                    {
                                                        // uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                                        uint64_t w = *reinterpret_cast<uint64_ptr>(eb_k_u + ind_l2_u * 8); // dest id

                                                        if (u > w)
                                                        {
                                                            continue;
                                                        }
                                                        //// hash table lookup start ////
                                                        AdjacentPos ap;
                                                        ap.edgeBlockIndex = NOTFOUNDKEY;
                                                        ap.indrIndex = NOTFOUNDKEY;
                                                        ap.hashTableIndex = NOTFOUNDKEY;
                                                        /// now check based on edge block indr index
                                                        AdjacentPos *ap_pos = index[v].getDestNodeHashTableVal(w, &ap);
                                                        if (ap_pos->edgeBlockIndex != NOTFOUNDKEY)
                                                        {
                                                            triangles_per_vertex[v] += 2;
                                                            triangles_per_vertex[u] += 2;
                                                            triangles_per_vertex[w] += 2;

                                                        }

                                                        ///// hash table lookup end /////
                                                    }

                                                }
                                            }
                                        }
                                        ////////// Level 0 end /////////////////
                                    }
                                    else
                                    {
                                        //// Level 0 start ////
                                        // directly read from edge block
                                        stalb_type *eb_k_u = reinterpret_cast<stalb_type *>(get_pointer(index[u].edgePtr));

                                        // int l2_len = (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2);
                                        uint8_t is_delete_l2_flag_u = 0;
                                        is_delete_l2_flag_u = *reinterpret_cast<uint8_ptr>(eb_k_u + 3);
                                        if (!is_delete_l2_flag_u)
                                        {

                                            for (int ind_l2_u = *reinterpret_cast<uint16_ptr>(eb_k_u + 1); ind_l2_u < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k_u)) / 2); ind_l2_u++)
                                            {

                                                uint64_t w = *reinterpret_cast<uint64_ptr>(eb_k_u + ind_l2_u * 8); // dest id

                                                //// hash table lookup start ////
                                                if (u > w)
                                                {
                                                    continue;
                                                }

                                                AdjacentPos ap;
                                                ap.edgeBlockIndex = NOTFOUNDKEY;
                                                ap.indrIndex = NOTFOUNDKEY;
                                                ap.hashTableIndex = NOTFOUNDKEY;
                                                /// now check based on edge block indr index
                                                AdjacentPos *ap_pos = index[v].getDestNodeHashTableVal(w, &ap);
                                                if (ap_pos->edgeBlockIndex != NOTFOUNDKEY)
                                                {
                                                    // triangle count
                                                    //num_triangles =num_triangles + 3;
                                                    triangles_per_vertex[v] += 2;
                                                    triangles_per_vertex[u] += 2;
                                                    triangles_per_vertex[w] += 2;

                                                }
                                                ///// hash table lookup end /////
                                            }
                                        }

                                        //// level 0 end ////


                                    }

                                    ///////////////////////////// Level 1 end /////////////////////////////////////////////////////////////
                                }

                            }
                        }
                    }
                    ////////// Level 0 end /////////////////

                }
                else
                {
                    //// Level 0 start ////
                    // directly read from edge block
                    stalb_type *eb_k = reinterpret_cast<stalb_type *>(get_pointer(index[v].edgePtr));

                    // int l2_len = (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2);
                    uint8_t is_delete_l2_flag = 0;
                    is_delete_l2_flag = *reinterpret_cast<uint8_ptr>(eb_k + 3);
                    if (!is_delete_l2_flag)
                    {

                        for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                        {
                            uint64_t u = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);

                            //    incoming_total += outgoing_contrib[*reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8)];

                            ///////////////////////////// Level 1 start /////////////////////////////////////////////////////////////
                            if (v >= u)
                            {
                                continue;
                            }

                            if (!(index[u].edgePtr & 0x1))
                            {

                                PerSourceIndrPointer *ind_ptr_u = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(index[u].edgePtr));

                                //int l1_len = (u_int64_t) 1 << ind_ptr->getBlockLen();
                                bool flag_l1_delete_u = false;
                                flag_l1_delete_u = ind_ptr_u->isDeletionBlock();

                                if (!flag_l1_delete_u)
                                {
                                    // deletion bit per source-vertex not active direct read L2 vector
                                    for (int ind_l1_u = ind_ptr_u->getCurPos(); ind_l1_u < (u_int64_t) 1 << ind_ptr_u->getBlockLen(); ind_l1_u++)
                                    {

                                        stalb_type *eb_k_u = ind_ptr_u->getperVertexBlockArr()[ind_l1_u];

                                        // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;
                                        uint8_t is_delete_l2_flag_u = 0;
                                        is_delete_l2_flag_u = *reinterpret_cast<uint8_ptr>(eb_k_u + 3);
                                        if (!is_delete_l2_flag_u)
                                        {

                                            for (int ind_l2_u = *reinterpret_cast<uint16_ptr>(eb_k_u + 1); ind_l2_u < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k_u)) / 2); ind_l2_u++)
                                            {
                                                // uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                                uint64_t w = *reinterpret_cast<uint64_ptr>(eb_k_u + ind_l2_u * 8); // dest id

                                                //// hash table lookup start ////

                                                if (u > w)
                                                {
                                                    continue;
                                                }

                                                AdjacentPos ap;
                                                ap.edgeBlockIndex = NOTFOUNDKEY;
                                                ap.indrIndex = NOTFOUNDKEY;
                                                ap.hashTableIndex = NOTFOUNDKEY;
                                                /// now check based on edge block indr index
                                                AdjacentPos *ap_pos = index[v].getDestNodeHashTableVal(w, &ap);
                                                if (ap_pos->edgeBlockIndex != NOTFOUNDKEY)
                                                {
                                                    // triangle count
                                                    // num_triangles =num_triangles + 3;
                                                    triangles_per_vertex[v] += 2;
                                                    triangles_per_vertex[u] += 2;
                                                    triangles_per_vertex[w] += 2;
                                                }

                                                ///// hash table lookup end /////


                                            }

                                        }
                                    }
                                }
                                ////////// Level 0 end /////////////////
                            }
                            else
                            {
                                //// Level 0 start ////
                                // directly read from edge block
                                stalb_type *eb_k_u = reinterpret_cast<stalb_type *>(get_pointer(index[u].edgePtr));

                                // int l2_len = (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2);
                                uint8_t is_delete_l2_flag_u = 0;
                                is_delete_l2_flag_u = *reinterpret_cast<uint8_ptr>(eb_k_u + 3);
                                if (!is_delete_l2_flag_u)
                                {

                                    for (int ind_l2_u = *reinterpret_cast<uint16_ptr>(eb_k_u + 1); ind_l2_u < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k_u)) / 2); ind_l2_u++)
                                    {

                                        uint64_t w = *reinterpret_cast<uint64_ptr>(eb_k_u + ind_l2_u * 8); // dest id

                                        //// hash table lookup start ////

                                        if (u > w)
                                        {
                                            continue;
                                        }

                                        AdjacentPos ap;
                                        ap.edgeBlockIndex = NOTFOUNDKEY;
                                        ap.indrIndex = NOTFOUNDKEY;
                                        ap.hashTableIndex = NOTFOUNDKEY;
                                        /// now check based on edge block indr index
                                        AdjacentPos *ap_pos = index[v].getDestNodeHashTableVal(w, &ap);
                                        if (ap_pos->edgeBlockIndex != NOTFOUNDKEY)
                                        {
                                            // triangle count
                                            // num_triangles =num_triangles + 3;
                                            triangles_per_vertex[v] += 2;
                                            triangles_per_vertex[u] += 2;
                                            triangles_per_vertex[w] += 2;
                                        }
                                        ///// hash table lookup end /////
                                    }
                                }

                                //// level 0 end ////

                            }

                            ///////////////////////////// Level 1 end /////////////////////////////////////////////////////////////
                        }
                    }

                    //// level 0 end ////

                }

                // register the final score

                //  testLock.lock();
                //  std::cout<<"Score computed: " << triangles_per_vertex[v] << "/" << max_num_edges<< " = " << lcc[v]<<std::endl;
                //  testLock.unlock();
                //  COUT_DEBUG_LCC("Score computed: " << num_tr << "/" << max_num_edges<< " = " << lcc[v]);

            }
        }
        else
        {
#pragma omp parallel for schedule(dynamic, 64)
            for (uint64_t v = 0; v < max_vertex_id; v++)
            {

                if (!(index[v].edgePtr & 0x1))
                {

                    PerSourceIndrPointer *ind_ptr = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(index[v].edgePtr));

                    // deletion bit per source-vertex not active direct read L2 vector
                    for (int ind_l1 = ind_ptr->getCurPos(); ind_l1 < (u_int64_t) 1 << ind_ptr->getBlockLen(); ind_l1++)
                    {

                        stalb_type *eb_k = ind_ptr->getperVertexBlockArr()[ind_l1];

                        // int l2_len = ((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2;

                        for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                        {

                            uint64_t u = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8); // dest id
                            ///////////////////////////// Level 1 start /////////////////////////////////////////////////////////////
                            if (v > u)
                            {
                                continue;
                            }

                            if (!(index[u].edgePtr & 0x1))
                            {

                                PerSourceIndrPointer *ind_ptr_u = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(index[u].edgePtr));

                                // deletion bit per source-vertex not active direct read L2 vector
                                for (int ind_l1_u = ind_ptr_u->getCurPos(); ind_l1_u < (u_int64_t) 1 << ind_ptr_u->getBlockLen(); ind_l1_u++)
                                {
                                    stalb_type *eb_k_u = ind_ptr_u->getperVertexBlockArr()[ind_l1_u];

                                    for (int ind_l2_u = *reinterpret_cast<uint16_ptr>(eb_k_u + 1); ind_l2_u < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k_u)) / 2); ind_l2_u++)
                                    {
                                        uint64_t w = *reinterpret_cast<uint64_ptr>(eb_k_u + ind_l2_u * 8); // dest id

                                        if (u > w)
                                        {
                                            continue;
                                        }
                                        //// hash table lookup start ////
                                        AdjacentPos ap;
                                        ap.edgeBlockIndex = NOTFOUNDKEY;
                                        ap.indrIndex = NOTFOUNDKEY;
                                        ap.hashTableIndex = NOTFOUNDKEY;
                                        /// now check based on edge block indr index
                                        AdjacentPos *ap_pos = index[v].getDestNodeHashTableVal(w, &ap);
                                        if (ap_pos->edgeBlockIndex != NOTFOUNDKEY)
                                        {
                                            triangles_per_vertex[v] += 2;
                                            triangles_per_vertex[u] += 2;
                                            triangles_per_vertex[w] += 2;

                                        }

                                        ///// hash table lookup end /////
                                    }

                                }
                                ////////// Level 0 end /////////////////
                            }
                            else
                            {
                                //// Level 0 start ////
                                // directly read from edge block
                                stalb_type *eb_k_u = reinterpret_cast<stalb_type *>(get_pointer(index[u].edgePtr));

                                for (int ind_l2_u = *reinterpret_cast<uint16_ptr>(eb_k_u + 1); ind_l2_u < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k_u)) / 2); ind_l2_u++)
                                {

                                    uint64_t w = *reinterpret_cast<uint64_ptr>(eb_k_u + ind_l2_u * 8); // dest id

                                    //// hash table lookup start ////
                                    if (u > w)
                                    {
                                        continue;
                                    }

                                    AdjacentPos ap;
                                    ap.edgeBlockIndex = NOTFOUNDKEY;
                                    ap.indrIndex = NOTFOUNDKEY;
                                    ap.hashTableIndex = NOTFOUNDKEY;
                                    /// now check based on edge block indr index
                                    AdjacentPos *ap_pos = index[v].getDestNodeHashTableVal(w, &ap);
                                    if (ap_pos->edgeBlockIndex != NOTFOUNDKEY)
                                    {
                                        // triangle count
                                        //num_triangles =num_triangles + 3;
                                        triangles_per_vertex[v] += 2;
                                        triangles_per_vertex[u] += 2;
                                        triangles_per_vertex[w] += 2;

                                    }
                                    ///// hash table lookup end /////
                                }


                                //// level 0 end ////


                            }

                            ///////////////////////////// Level 1 end /////////////////////////////////////////////////////////////
                        }

                    }
                }
                else
                {
                    //// Level 0 start ////
                    // directly read from edge block
                    stalb_type *eb_k = reinterpret_cast<stalb_type *>(get_pointer(index[v].edgePtr));

                    for (int ind_l2 = *reinterpret_cast<uint16_ptr>(eb_k + 1); ind_l2 < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k)) / 2); ind_l2++)
                    {
                        uint64_t u = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);

                        ///////////////////////////// Level 1 start /////////////////////////////////////////////////////////////
                        if (v >= u)
                        {
                            continue;
                        }

                        if (!(index[u].edgePtr & 0x1))
                        {

                            PerSourceIndrPointer *ind_ptr_u = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(index[u].edgePtr));
                            // deletion bit per source-vertex not active direct read L2 vector
                            for (int ind_l1_u = ind_ptr_u->getCurPos(); ind_l1_u < (u_int64_t) 1 << ind_ptr_u->getBlockLen(); ind_l1_u++)
                            {

                                stalb_type *eb_k_u = ind_ptr_u->getperVertexBlockArr()[ind_l1_u];
                                for (int ind_l2_u = *reinterpret_cast<uint16_ptr>(eb_k_u + 1); ind_l2_u < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k_u)) / 2); ind_l2_u++)
                                {
                                    // uint64_t dst = *reinterpret_cast<uint64_ptr>(eb_k + ind_l2 * 8);
                                    uint64_t w = *reinterpret_cast<uint64_ptr>(eb_k_u + ind_l2_u * 8); // dest id

                                    //// hash table lookup start ////

                                    if (u > w)
                                    {
                                        continue;
                                    }

                                    AdjacentPos ap;
                                    ap.edgeBlockIndex = NOTFOUNDKEY;
                                    ap.indrIndex = NOTFOUNDKEY;
                                    ap.hashTableIndex = NOTFOUNDKEY;
                                    /// now check based on edge block indr index
                                    AdjacentPos *ap_pos = index[v].getDestNodeHashTableVal(w, &ap);
                                    if (ap_pos->edgeBlockIndex != NOTFOUNDKEY)
                                    {
                                        // triangle count
                                        // num_triangles =num_triangles + 3;
                                        triangles_per_vertex[v] += 2;
                                        triangles_per_vertex[u] += 2;
                                        triangles_per_vertex[w] += 2;
                                    }

                                    ///// hash table lookup end /////


                                }

                            }

                            ////////// Level 0 end /////////////////
                        }
                        else
                        {
                            //// Level 0 start ////
                            // directly read from edge block
                            stalb_type *eb_k_u = reinterpret_cast<stalb_type *>(get_pointer(index[u].edgePtr));

                            for (int ind_l2_u = *reinterpret_cast<uint16_ptr>(eb_k_u + 1); ind_l2_u < (((u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(eb_k_u)) / 2); ind_l2_u++)
                            {

                                uint64_t w = *reinterpret_cast<uint64_ptr>(eb_k_u + ind_l2_u * 8); // dest id

                                //// hash table lookup start ////

                                if (u > w)
                                {
                                    continue;
                                }

                                AdjacentPos ap;
                                ap.edgeBlockIndex = NOTFOUNDKEY;
                                ap.indrIndex = NOTFOUNDKEY;
                                ap.hashTableIndex = NOTFOUNDKEY;
                                /// now check based on edge block indr index
                                AdjacentPos *ap_pos = index[v].getDestNodeHashTableVal(w, &ap);
                                if (ap_pos->edgeBlockIndex != NOTFOUNDKEY)
                                {
                                    // triangle count
                                    // num_triangles =num_triangles + 3;
                                    triangles_per_vertex[v] += 2;
                                    triangles_per_vertex[u] += 2;
                                    triangles_per_vertex[w] += 2;
                                }
                                ///// hash table lookup end /////
                            }


                            //// level 0 end ////

                        }

                        ///////////////////////////// Level 1 end /////////////////////////////////////////////////////////////
                    }


                    //// level 0 end ////

                }

                // register the final score



            }
        }
    }
    // std::vector<double> lcc_values(max_vertex_id);

#pragma omp parallel for
    for (uint64_t v = 0; v < max_vertex_id; v++)
    {
        lcc[v] = 0.0;
        index[v].perVertexLock.lock(); ///////
        uint64_t degree = perSourceGetDegree(v, 0, me);//index[v].degree; //perSourceGetDegree(v, read_version);
        index[v].perVertexLock.unlock();

        uint64_t max_num_edges = degree * (degree - 1);
        if (max_num_edges != 0)
        {
            lcc[v] = ((double) triangles_per_vertex[v]) / max_num_edges;
        }
        else
        {
            lcc[v] = 0.0;
        }
    }

//std::cout<<num_triangles<<std::endl;
    return ptr_lcc;
}
