/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 ******************************************************************************/
#include "VertexBlock.h"
#include <iostream>
/* PerSourceIndrPointer*/
void PerSourceIndrPointer::setBlockLen(uint8_t blen)
{
    blockLen = blen;
}

int PerSourceIndrPointer::addNewBlock(stalb_type *tb)
{
    blockArr[--curPos] = tb;
    return curPos;
    //    std::cout<<"hello";
}

void PerSourceIndrPointer::setCurPos(int curPos)
{
    this->curPos = curPos;
}

int PerSourceIndrPointer::getCurPos() const
{
    return curPos;
}

void PerSourceIndrPointer::initBlockSize()
{
    blockLen = log2(1);
}

void PerSourceIndrPointer::setPerVertexBlockArr(stalb_type **newArr)
{
    blockArr = newArr;
}

stalb_type **PerSourceIndrPointer::getperVertexBlockArr()
{
    return blockArr;
}

uint8_t PerSourceIndrPointer::getBlockLen()
{
    return blockLen;
}

void PerSourceIndrPointer::setEmptySpace()
{
    emptySpace++;
}

int PerSourceIndrPointer::getEmptySpace()
{
    return emptySpace;
}

void PerSourceIndrPointer::setDeletionBlock(bool d)
{
    is_deletion_block = d;
}

bool PerSourceIndrPointer::isDeletionBlock()
{
    return is_deletion_block;
}

void PerSourceIndrPointer::setIndexList(MemoryAllocator *me, int thread_id)
{
    if (blockArr == NULL)
    {
        // blockLen = 1;
        u_int64_t block_l = 2; //((u_int64_t) 1 << blockLen);
        curPos = fixedSize * block_l;
        blockArr = static_cast<stalb_type **>(me->Allocate(8 * (block_l * fixedSize), thread_id));///static_cast<EdgeBlock **>(aligned_alloc(8,8 * (blockLen * fixedSize)));//static_cast<EdgeBlock **>(malloc(8 * (blockLen * fixedSize)));
        memset(blockArr, 0, 8 * (block_l * fixedSize));
        blockLen = 1;
        /*
          for(int i=0;i<(blockLen * fixedSize);i++)
          {
              blockArr[i] = NULL;
          }

          */
    }
    else
    {
        // downsize_lock.lock();

        u_int64_t blenprev = ((u_int64_t) 1 << blockLen);
        u_int64_t b_len_new = 2 * blenprev;

        stalb_type **temp = static_cast<stalb_type **>(me->Allocate(8 * ((b_len_new) * fixedSize), thread_id));
        memset(temp, 0, 8 * ((b_len_new) * fixedSize));

        //  FixedTimeIndexBlock **temp2;
        // todo deletion mechanism ()create node in globle linklist and add the address of deleted index array and with time to replace
        // the garbage collector will deallocate the memory than
        int k = 0;
        int tempC = (blenprev) * fixedSize;

        memcpy(temp + tempC, blockArr, tempC * 8);
        curPos = tempC;
        void *ptr = blockArr;
        blockArr = temp;

        me->free(ptr, (blenprev * fixedSize * 8), thread_id);
        //  free(ptr);
        // blenprev++;
        blockLen = log2(b_len_new);

        //  downsize_lock.unlock();

    }
}

/* PerSourceVertexIndr class defination */
void PerSourceVertexIndr::setEdgePtr(vertex_t ptr, bool flag)
{
    if (flag)
    {
        edgePtr = reinterpret_cast<vertex_t>(ptr |= 0x1);
    }
    else
    {
        edgePtr = reinterpret_cast<vertex_t>(ptr &= ~0x1);
    }
}

void *PerSourceVertexIndr::getEdgePtr()
{
    return reinterpret_cast<void *>(get_pointer(edgePtr));
}

/* Degree class */
void PerSourceVertexIndr::setDegreePtr(vertex_t ptr, bool flag)
{
    if (flag)
    {
        degree = reinterpret_cast<vertex_t>(ptr |= 0x1);
    }
    else
    {
        degree = reinterpret_cast<vertex_t>(ptr &= ~0x1);
    }
}

void *PerSourceVertexIndr::getDegreePtr()
{
    return reinterpret_cast<void *>(get_pointer(degree));
}

/* */


void PerSourceVertexIndr::setPerVertexLock()
{

}

void PerSourceVertexIndr::unsetPerVertexLock()
{

    // lock.unlock();
}


u_int64_t PerSourceVertexIndr::hash(u_int64_t key, u_int64_t size)
{
    return key % size;
}

//%%% for out-of-order updates
inline int PerSourceVertexIndr::findIndexForBlockArrOOO(stalb_type *arr, int start, int eb_block_size, timestamp_t K)
{
    // Lower and upper bounds
    int end = eb_block_size - 1;
    // Traverse the search space
    while (start <= end)
    {
        int mid = (start + end) / 2;
        // If K is found
        LeafNode *eih_cur = *reinterpret_cast<LeafNode **>(arr + ((eb_block_size) * 8) + ((mid * 8) - 8)); // %%%% start here

        if (eih_cur->getSTime() == K)
            return mid;
        else if (eih_cur->getSTime() > K)
            start = mid + 1;
        else
            end = mid - 1;
    }
    // Return insert position
    //std::cout<<"Hello ghufran 1"<<std::endl;

    return end;
}

//
inline int PerSourceVertexIndr::findIndexForIndr(stalb_type **arr, int start, int n, timestamp_t K)
{
    // Lower and upper bounds
    int end = n - 1;
    // Traverse the search space
    timestamp_t eih_cur_time = 0;
    timestamp_t eih_end_time = 0;
    while (start <= end)
    {
        int mid = (start + end) / 2;
        if (!check_version(reinterpret_cast<vertex_t>(arr[mid])))
        {
            u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(arr[mid]);
            EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(arr[mid] + ((eb_block_size / 2) * 8) + (((*reinterpret_cast<uint16_ptr>(arr[mid] + 1)) * 8) - 8));
            EdgeInfoHal *eih_end = *reinterpret_cast<EdgeInfoHal **>(arr[mid] + ((eb_block_size / 2) * 8) + ((((eb_block_size / 2) - 1) * 8) - 8));
            eih_cur_time = eih_cur->getSTime();
            eih_end_time = eih_end->getSTime();
        }
        else
        {
            //
            eih_cur_time = remove_version(reinterpret_cast<vertex_t>(arr[mid]));
            eih_end_time = eih_cur_time;
        }
        if (eih_cur_time > K && eih_end_time < K)
            return mid;
        else if (eih_cur_time > K)
            start = mid + 1;
        else
            end = mid - 1;
    }
    // Return insert position
    return end;
}

inline int PerSourceVertexIndr::findIndexForBlockArr(stalb_type *arr, int start, int eb_block_size, timestamp_t K)
{
    // Lower and upper bounds
    int end = eb_block_size - 1;
    // Traverse the search space
    while (start <= end)
    {
        int mid = (start + end) / 2;
        // If K is found
        EdgeInfoHal *eih_cur = *reinterpret_cast<EdgeInfoHal **>(arr + ((eb_block_size) * 8) + ((mid * 8) - 8)); // %%%% start here

        if (eih_cur->getSTime() == K)
            return mid;
        else if (eih_cur->getSTime() > K)
            start = mid + 1;
        else
            end = mid - 1;
    }
    // Return insert position
    return end;
}

vertex_t PerSourceVertexIndr::getOutOfOrderDestNode(timestamp_t srctimestamp, bool isIndrArray, void *edgePtr)
{

    if (isIndrArray)
    {
        PerSourceIndrPointer *ptr_edge = static_cast<PerSourceIndrPointer *>(edgePtr);
        int startPos = ptr_edge->getCurPos();
        int endPos = ((u_int64_t) 1 << ptr_edge->getBlockLen());

        int indexIndrArray = findIndexForIndr(ptr_edge->getperVertexBlockArr(), startPos, endPos, srctimestamp);
        u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_edge->getperVertexBlockArr()[indexIndrArray]);

        int indexFixedBlock = findIndexForBlockArr(ptr_edge->getperVertexBlockArr()[indexIndrArray], ((u_int16_t) *reinterpret_cast<uint16_ptr>(ptr_edge->getperVertexBlockArr()[indexIndrArray] + 1)), (eb_block_size / 2), srctimestamp);

        /// eb block
        EdgeInfoHal *eih_index = *reinterpret_cast<EdgeInfoHal **>(ptr_edge->getperVertexBlockArr()[indexIndrArray] + ((eb_block_size / 2) * 8) + ((indexFixedBlock * 8) - 8));
        // ArtPerIndex *perArt = eih_index->getOutOfOrderUpdt();

        // LeafNode *leaf = perArt->getLeaf(srctimestamp);
        return eih_index->getOutOfOrderUpdt();
        // findIndexForBlockArr(stalb_type *arr, int start, int eb_block_size, timestamp_t K)
    }
    else
    {

        stalb_type *ptr_edge = static_cast<stalb_type *>(edgePtr);

        u_int64_t eb_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_edge);

        int indexFixedBlock = findIndexForBlockArr(ptr_edge, ((u_int16_t) *reinterpret_cast<uint16_ptr>(ptr_edge + 1)), (eb_block_size / 2), srctimestamp);

        /// eb block
        EdgeInfoHal *eih_index = *reinterpret_cast<EdgeInfoHal **>(ptr_edge + ((eb_block_size / 2) * 8) + ((indexFixedBlock * 8) - 8));

        // ArtPerIndex *perArt = eih_index->getOutOfOrderUpdt();

        // LeafNode *leaf = perArt->getLeaf(srctimestamp);

        return eih_index->getOutOfOrderUpdt();

    }

}

void PerSourceVertexIndr::resizeHashTable(int requested_block, MemoryAllocator *la, int thread_id)
{

    // HashTablePerSource* oldArr = hashPtr;

    u_int32_t oldCap = hashPtr->hashBlockSize;

    // capacity =requested_block;
    // u_int64_t* newHashTable = static_cast<u_int64_t *>(la->Allocate(sizeof(HashTablePerSource), thread_id));

    u_int64_t *newHashTable = static_cast<u_int64_t *>(la->Allocate(8 * requested_block, thread_id));

    for (int i = 0; i < requested_block; i++)
    {
        newHashTable[i] = FLAG_EMPTY_SLOT_HT;
    }

    // memset(newHashTable, FLAG_EMPTY_SLOT_HT, requested_block * 8);	//reset new array

    for (u_int32_t i = 0; i < oldCap; i++)
    {
        u_int64_t adj_p = hashPtr->hashtable[i];

        if (adj_p != FLAG_EMPTY_SLOT_HT)
        {
            if (check_version(adj_p))
            {
                size_t index = 0;
                if (check_version_read(adj_p))
                {
                    DestGc *D_gc = reinterpret_cast<DestGc *>(remove_version(remove_version_read(adj_p)));
                    index = hash(remove_version(D_gc->Dnode), requested_block);
                }
                else
                {
                    // contain destination edge version (dve)
                    DVE *dve = reinterpret_cast<DVE *>(remove_version(adj_p));
                    index = hash(remove_version(dve->Dnode), requested_block);
                }
                while (newHashTable[index] != FLAG_EMPTY_SLOT_HT)
                {
                    index++;
                    if (index >= requested_block)
                    {
                        index = 0;
                    }
                }
                newHashTable[index] = adj_p;
            }
            else
            {
                //oldArr->hashtable[i]
                u_int64_t is_ooo = getBitMask(adj_p, IsOutOrderStart, IsOutOrderBits);
                if (!is_ooo)
                {
                    if (!(edgePtr & 0x1))
                    {

                        PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

                        u_int64_t indr_index = getBitMask(adj_p, L1IndexStart, L1IndexBits);
                        u_int64_t indr_size = (u_int64_t) 1 << getBitMask(adj_p, L1SizeStart, L1SizeBits);

                        // lock on per source vertex to get the index_block edge block
                        u_int64_t index_block = (((u_int64_t) 1 << ptr_ind->getBlockLen()) - (indr_size - indr_index));

                        u_int64_t edge_block_index = getBitMask(adj_p, L2IndexStart, L2IndexBits);

                        u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(adj_p, L2SizeStart, L2SizeBits);
                        // per block deletion lock

                        u_int64_t cur_edge_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_ind->getperVertexBlockArr()[index_block]); //->getBlockSize();

                        u_int64_t index_edge_block = ((cur_edge_block_size / 2) - ((edge_block_size / 2) - edge_block_index));

                        //
                        //
                        size_t index = hash(remove_version_ooo(*reinterpret_cast<uint64_ptr>(ptr_ind->getperVertexBlockArr()[index_block] + ((index_edge_block) * 8))), requested_block);

                        while (newHashTable[index] != FLAG_EMPTY_SLOT_HT)
                        {
                            index++;
                            if (index >= requested_block)
                            {
                                index = 0;
                            }

                        }
                        newHashTable[index] = adj_p;
                    }
                    else
                    {
                        // only edge block ... degree < 1024
                        stalb_type *ptr_edge;

                        ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(edgePtr));

                        u_int64_t edge_block_index = getBitMask(adj_p, L2IndexStart, L2IndexBits);

                        u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(adj_p, L2SizeStart, L2SizeBits);
                        // per block deletion lock

                        u_int64_t cur_edge_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_edge);

                        u_int64_t index_edge_block = ((cur_edge_block_size / 2) - ((edge_block_size / 2) - edge_block_index));

                        size_t index = hash(remove_version_ooo(*reinterpret_cast<uint64_ptr>(ptr_edge + ((index_edge_block) * 8))), requested_block); //   /->getEdgeUpdate()[index_edge_block]->getDestId(),requested_block);

                        while (newHashTable[index] != FLAG_EMPTY_SLOT_HT)
                        {
                            index++;
                            if (index >= requested_block)
                            {
                                index = 0;
                            }

                        }
                        newHashTable[index] = adj_p;
                    }
                }
                else
                {
                    // out-of-order update
                    if (!(edgePtr & 0x1))
                    {

                        PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

                        timestamp_t src_timestamp = getBitMask(adj_p, t_s_start, t_s_bits);

                        stalb_type *perArt = reinterpret_cast<stalb_type *>(getOutOfOrderDestNode(src_timestamp, 1, ptr_ind));
                        /// binary search to find the key
                        u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(perArt);

                        uint64_t pos = findIndexForBlockArrOOO(perArt, *reinterpret_cast<uint16_ptr>(perArt + 1), (eb_block_size_ooo / 2), src_timestamp);
                        // LeafNode *leaf = *reinterpret_cast<LeafNode**>((perArt) + ((((uint64_t) 1<< *reinterpret_cast<uint8_ptr>(perArt)) / 2) * 8) + ((pos * 8) -8));

                        size_t index = hash(remove_version_ooo(*reinterpret_cast<uint64_ptr>(perArt + (pos * 8))), requested_block);

                        while (newHashTable[index] != FLAG_EMPTY_SLOT_HT)
                        {
                            index++;
                            if (index >= requested_block)
                            {
                                index = 0;
                            }
                        }
                        newHashTable[index] = adj_p;
                    }
                    else
                    {
                        // only edge block ... degree < 1024
                        stalb_type *ptr_edge;

                        ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(edgePtr));

                        timestamp_t src_timestamp = getBitMask(adj_p, t_s_start, t_s_bits);


                        /////

                        stalb_type *perArt = reinterpret_cast<stalb_type *>(getOutOfOrderDestNode(src_timestamp, 0, ptr_edge));
                        /// binary search to find the key
                        u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(perArt);

                        uint64_t pos = findIndexForBlockArrOOO(perArt, *reinterpret_cast<uint16_ptr>(perArt + 1), (eb_block_size_ooo / 2), src_timestamp);
                        // LeafNode *leaf = *reinterpret_cast<LeafNode**>((perArt) + ((((uint64_t) 1<< *reinterpret_cast<uint8_ptr>(perArt)) / 2) * 8) + ((pos * 8) -8));
                        size_t index = hash(remove_version_ooo(*reinterpret_cast<uint64_ptr>(perArt + (pos * 8))), requested_block);
                        while (newHashTable[index] != FLAG_EMPTY_SLOT_HT)
                        {
                            index++;
                            if (index >= requested_block)
                            {
                                index = 0;
                            }

                        }
                        newHashTable[index] = adj_p;
                    }

                }

            }
        }
    }

    u_int64_t *oldArr = hashPtr->hashtable;

    hashPtr->hashBlockSize = requested_block;
    hashPtr->hashtable = newHashTable;

    la->free(oldArr, oldCap * 8, thread_id);
    // for resize everytime you need to move to the STAL to get dest node and store it in the array

}

void PerSourceVertexIndr::setDestNodeHashTableVal(vertex_t dest_node, timestamp_t adj_pos, MemoryAllocator *la, int thread_id)
{
   // std::cout<<"set"<<std::endl;
    vertex_t d_node = remove_version(dest_node);

    uint64_t block_size_equal = 32;
    if (hashPtr->entryCounter < block_size_equal)
    {
        if (hashPtr->entryCounter == (hashPtr->hashBlockSize))
        {
            resizeHashTable(hashPtr->hashBlockSize * 2, la, thread_id);
        }
    }
    else if (hashPtr->entryCounter == block_size_equal)
    {
        resizeHashTable(hashPtr->hashBlockSize * 2, la, thread_id);
    }
    else
    {

        vertex_t half = (u_int32_t) hashPtr->hashBlockSize >> 1;
        vertex_t fifty_perc__hash_table_size = half;
        vertex_t twnty_five_hash_table_size = half + (half / 2);
        vertex_t tweleve_five_hash_table_size = twnty_five_hash_table_size + (half / 4);
        uint64_t block_size_1 = 16384ul;
        uint64_t block_size_2 = 131072ul;

        if (hashPtr->hashBlockSize > block_size_equal && hashPtr->hashBlockSize <= block_size_1)
        {
            if (hashPtr->entryCounter >= tweleve_five_hash_table_size)
            {
                resizeHashTable(hashPtr->hashBlockSize * 2, la, thread_id);
            }
        }
        else if (hashPtr->hashBlockSize > block_size_1 && hashPtr->hashBlockSize <= block_size_2)
        {
            if (hashPtr->entryCounter >= twnty_five_hash_table_size)
            {
                resizeHashTable(hashPtr->hashBlockSize * 2, la, thread_id);
            }
        }
        else
        {
            if (hashPtr->entryCounter >= half)
            {
                resizeHashTable(hashPtr->hashBlockSize * 2, la, thread_id);
            }
        }

    }


    hashPtr->entryCounter++;

    size_t index = hash(d_node, hashPtr->hashBlockSize);
//int count =0;
    while (hashPtr->hashtable[index] != FLAG_EMPTY_SLOT_HT && hashPtr->hashtable[index] != FLAG_TOMB_STONE)
    {

        index++;
        if (index >= hashPtr->hashBlockSize)
        {
            index = 0;
        }

    }
    if (check_version(dest_node))
    {
        // std::cout<<"check_version(dest_node)"<<std::endl;
        DVE *dve = static_cast<DVE *>(la->Allocate(sizeof(DVE), thread_id));
        dve->Dnode = dest_node;
        dve->l_del = 0;
        dve->oldest_version = 0;
        DuplicateUpdate *dup = static_cast<DuplicateUpdate *>(la->Allocate(sizeof(DuplicateUpdate), thread_id));
        dup->SrcTime = add_version(adj_pos);
        dup->next = NULL;
        dve->next = dup;
        hashPtr->hashtable[index] = add_version(reinterpret_cast<vertex_t>(dve));
    }
    else
    {
        hashPtr->hashtable[index] = adj_pos;
    }
}

void PerSourceVertexIndr::removeDestNodeHashTableVal(vertex_t dest_node, vertex_t hashtable_index, timestamp_t source_timestamp, MemoryAllocator *la, int thread_id)
{
    if (!check_version(hashPtr->hashtable[hashtable_index]))
    {
        // dve does not exist
        DVE *dve = static_cast<DVE *>(la->Allocate(sizeof(DVE), thread_id));
        dve->Dnode = add_version(dest_node);
        dve->l_del = source_timestamp;
        dve->next = NULL;
        dve->oldest_version = 0;
        hashPtr->hashtable[hashtable_index] = add_version(reinterpret_cast<vertex_t>(dve));
    }

    /*
    // get the dest node
    u_int64_t index = hash(dest_node,hashPtr->hashBlockSize);
    bool isEntryExist = false;
    // int count = 0;
    while (hashPtr->hashtable[index] != FLAG_EMPTY_SLOT_HT)
    {
        // count++;
        if (hashPtr->hashtable[index] != FLAG_TOMB_STONE) {

            u_int64_t pos_adj = hashPtr->hashtable[index];
            // get pos_adj bit position to compare
            //
            u_int64_t is_ooo = getBitMask(pos_adj, IsOutOrderStart, IsOutOrderBits);
            if (!is_ooo)
            {
                if (((u_int64_t) (dest_node & thread_prefix)) == getBitMask(pos_adj, DNPStart, DNPBits)) {

                    //u_int64_t is_stal = getBitMask(pos_adj, IsSTALStart, IsSTALBits);

                    if (!(edgePtr & 0x1))
                    {

                        PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

                        u_int64_t indr_index = getBitMask(pos_adj, L1IndexStart, L1IndexBits);
                        u_int64_t indr_size = (u_int64_t) 1 << getBitMask(pos_adj, L1SizeStart, L1SizeBits);

                        // lock on per source vertex to get the index_block edge block
                        u_int64_t index_block = (((u_int64_t) 1 << ptr_ind->getBlockLen()) - (indr_size - indr_index));

                        u_int64_t edge_block_index = getBitMask(pos_adj, L2IndexStart, L2IndexBits);

                        u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(pos_adj, L2SizeStart, L2SizeBits);
                        // per block deletion lock

                        u_int64_t cur_edge_block_size = (u_int64_t) 1
                                << *reinterpret_cast<uint8_ptr>(ptr_ind->getperVertexBlockArr()[index_block]); //->getBlockSize();

                        u_int64_t index_edge_block = ((cur_edge_block_size / 2) -
                                                      ((edge_block_size / 2) - edge_block_index));


                        // per block deletion lock
                        if (*reinterpret_cast<uint64_ptr>(ptr_ind->getperVertexBlockArr()[index_block] +
                                                          ((index_edge_block) * 8)) != FLAG_TOMB_STONE) {

                            if (*reinterpret_cast<uint64_ptr>(ptr_ind->getperVertexBlockArr()[index_block] +((index_edge_block) * 8)) == dest_node)
                            {
                                hashPtr->hashtable[index] = FLAG_TOMB_STONE;
                                isEntryExist = true;
                                hashPtr->entryCounter--;
                                break;
                            }
                        }

                    } else {
                        // edge block index remove
                        stalb_type *ptr_edge;

                        ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(edgePtr));

                        u_int64_t edge_block_index = getBitMask(pos_adj, L2IndexStart, L2IndexBits);

                        u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(pos_adj, L2SizeStart, L2SizeBits);

                        // per block deletion lock
                        u_int64_t cur_edge_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_edge);

                        u_int64_t index_edge_block = ((cur_edge_block_size / 2) -
                                                      ((edge_block_size / 2) - edge_block_index));

                        if (*reinterpret_cast<uint64_ptr>(ptr_edge + ((index_edge_block) * 8)) != FLAG_TOMB_STONE) {
                            if (*reinterpret_cast<uint64_ptr>(ptr_edge + ((index_edge_block) * 8)) == dest_node) {
                                hashPtr->hashtable[index] = FLAG_TOMB_STONE;
                                isEntryExist = true;
                                hashPtr->entryCounter--;
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                // ooo part
                // out-of-order update part
                if (((u_int64_t) (dest_node & thread_prefix_OOO)) == getBitMask(pos_adj, DNPStart_OOO, DNP_ooo_bits))
                {

                    //u_int64_t is_stal = getBitMask(pos_adj, IsSTALStart, IsSTALBits);

                    // issue is here degree > ((uint32_t) 1 << StartFixedBlockThreshold)-1)
                    if (!(edgePtr & 0x1))
                    {
                        // should see the degree part issue is upward

                        //

                        PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

                        timestamp_t src_timestamp          = getBitMask(pos_adj, t_s_start, t_s_bits);


///

                        stalb_type *perArt = reinterpret_cast<stalb_type *>(getOutOfOrderDestNode(src_timestamp, 1,ptr_ind));
                        /// binary search to find the key
                        u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(perArt);

                        uint64_t pos =  findIndexForBlockArrOOO(perArt, *reinterpret_cast<uint16_ptr>(perArt + 1), (eb_block_size_ooo / 2),src_timestamp);


                        // per block deletion lock
                        if (*reinterpret_cast<uint64_ptr>(perArt + (pos * 8)) == dest_node)
                        {
                            hashPtr->hashtable[index] = FLAG_TOMB_STONE;
                            isEntryExist = true;
                            hashPtr->entryCounter--;
                            break;
                        }

                    } else {
                        // edge block entry
                        // edge block index remove


                        // only edge block ... degree < 1024
                        stalb_type *ptr_edge;

                        ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(edgePtr));

                        timestamp_t src_timestamp          = getBitMask(pos_adj, t_s_start, t_s_bits);

                        stalb_type *perArt = reinterpret_cast<stalb_type *>(getOutOfOrderDestNode(src_timestamp, 0,
                                                                                                  ptr_edge));
                        /// binary search to find the key
                        u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(perArt);

                        uint64_t pos =  findIndexForBlockArrOOO(perArt, *reinterpret_cast<uint16_ptr>(perArt + 1), (eb_block_size_ooo / 2),src_timestamp);


                        // per block deletion lock
                        if (*reinterpret_cast<uint64_ptr>(perArt + (pos * 8)) == dest_node)
                        {
                            hashPtr->hashtable[index] = FLAG_TOMB_STONE;
                            isEntryExist = true;
                            hashPtr->entryCounter--;
                            break;
                        }
                    }
                }
            }
        }
        index = index + 1;
        if (index >= hashPtr->hashBlockSize) {
            index = 0;
        }


    }
    if(!isEntryExist)
    {
        std::cout<<"Entry does not exist: "<<std::endl;
    }*/

}

AdjacentPos *PerSourceVertexIndr::getDestBySourceTimeNodeHashTableVal(vertex_t pos_adj, AdjacentPos *ap, uint64_t *pos_ooo)
{
    u_int64_t is_ooo = getBitMask(pos_adj, IsOutOrderStart, IsOutOrderBits);
    if (!is_ooo)
    {

        // issue is here degree > ((uint32_t) 1 << StartFixedBlockThreshold)-1)
        if (!(edgePtr & 0x1))
        {
            // should see the degree part issue is upward
            PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

            u_int64_t indr_index = getBitMask(pos_adj, L1IndexStart, L1IndexBits);
            u_int64_t indr_size = (u_int64_t) 1 << getBitMask(pos_adj, L1SizeStart, L1SizeBits);

            // lock on per source vertex to get the index_block edge block
            u_int64_t index_block = (((u_int64_t) 1 << ptr_ind->getBlockLen()) - (indr_size - indr_index));

            u_int64_t edge_block_index = getBitMask(pos_adj, L2IndexStart, L2IndexBits);

            u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(pos_adj, L2SizeStart, L2SizeBits);
            // per block deletion lock

            u_int64_t cur_edge_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_ind->getperVertexBlockArr()[index_block]); //->getBlockSize();

            u_int64_t index_edge_block = ((cur_edge_block_size / 2) - ((edge_block_size / 2) - edge_block_index));


            ap->indrIndex = index_block;
            ap->edgeBlockIndex = index_edge_block;
            ap->hashTableIndex = FLAG_EMPTY_SLOT;
            return ap;
        }
        else
        {
            // edge block entry
            // edge block index remove
            stalb_type *ptr_edge;

            ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(edgePtr));
            //   flagp = 0;
            // }

            u_int64_t edge_block_index = getBitMask(pos_adj, L2IndexStart, L2IndexBits);

            u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(pos_adj, L2SizeStart, L2SizeBits);

            // per block deletion lock
            u_int64_t cur_edge_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_edge);

            u_int64_t index_edge_block = ((cur_edge_block_size / 2) - ((edge_block_size / 2) - edge_block_index));

            ap->edgeBlockIndex = index_edge_block;
            ap->hashTableIndex = FLAG_EMPTY_SLOT;
            return ap;

        }
    }
    else
    {
        if (!(edgePtr & 0x1))
        {

            PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

            timestamp_t src_timestamp = getBitMask(pos_adj, t_s_start, t_s_bits);

            stalb_type *perArt;

            vertex_t ooo_field = (getOutOfOrderDestNode(src_timestamp, 1, ptr_ind));

            if (!check_version(ooo_field))
            {
                perArt = reinterpret_cast<stalb_type *>(remove_version(ooo_field));
            }

            /// binary search to find the key
            u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(perArt);

            uint64_t pos = findIndexForBlockArrOOO(perArt, *reinterpret_cast<uint16_ptr>(perArt + 1), (eb_block_size_ooo / 2), src_timestamp);

            LeafNode *leaf = *reinterpret_cast<LeafNode **>((perArt) + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(perArt)) / 2) * 8) + ((pos * 8) - 8));

            *pos_ooo = pos;
            ap->indrIndex = add_version(reinterpret_cast<vertex_t>(perArt));
            ap->edgeBlockIndex = add_version_read(reinterpret_cast<vertex_t>(leaf));;
            ap->hashTableIndex = FLAG_EMPTY_SLOT;
            return ap;
        }
        else
        {

            stalb_type *ptr_edge;

            ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(edgePtr));

            timestamp_t src_timestamp = getBitMask(pos_adj, t_s_start, t_s_bits);

            stalb_type *perArt;
            vertex_t ooo_field = (getOutOfOrderDestNode(src_timestamp, 0, ptr_edge));
            if (!check_version(ooo_field))
            {
                perArt = reinterpret_cast<stalb_type *>(remove_version(ooo_field));
            }
            /// binary search to find the key
            u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(perArt);
            uint64_t pos = findIndexForBlockArrOOO(perArt, *reinterpret_cast<uint16_ptr>(perArt + 1), (eb_block_size_ooo / 2), src_timestamp);
            LeafNode *leaf = *reinterpret_cast<LeafNode **>((perArt) + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(perArt)) / 2) * 8) + ((pos * 8) - 8));
            // per block deletion lock

            *pos_ooo = pos;
            ap->indrIndex = add_version(reinterpret_cast<vertex_t>(perArt));
            ap->edgeBlockIndex = add_version_read(reinterpret_cast<vertex_t>(leaf));;
            ap->hashTableIndex = FLAG_EMPTY_SLOT;
            return ap;

        }
    }

    return ap;
}

AdjacentPos *PerSourceVertexIndr::getDestNodeHashTableVal(vertex_t dest_node, AdjacentPos *ap, uint64_t *pos_ooo)
{
    // get the dest node
    size_t index = hash(dest_node, hashPtr->hashBlockSize);
    bool isEntryExist = false;
    int count = 0;
    while (hashPtr->hashtable[index] != FLAG_EMPTY_SLOT_HT)
    {
       // std::cout<<"hello "<<count<<std::endl;
        count++;
        if (hashPtr->hashtable[index] != FLAG_TOMB_STONE)
        {

            u_int64_t pos_adj = hashPtr->hashtable[index];
            //std::cout<<hashPtr->hashtable[index]<<std::endl;

            // get pos_adj bit position to compare
            if (check_version(pos_adj))
            {
                DestGc *D_gc;
                DVE *dve_dest;
                vertex_t dest_id_d = 0;
                if (check_version_read(pos_adj))
                {
                    D_gc = reinterpret_cast<DestGc *>(remove_version(remove_version_read(pos_adj)));
                    dest_id_d = remove_version(D_gc->Dnode);
                }
                else
                {
                    dve_dest = reinterpret_cast<DVE *>(remove_version(pos_adj));
                    dest_id_d = remove_version(dve_dest->Dnode);
                }
                //DVE *index_dvp = reinterpret_cast<DVE *>(remove_version(pos_adj));
                if (dest_id_d == dest_node)
                {
                    ap->edgeBlockIndex = pos_adj;
                    ap->hashTableIndex = index;
                    return ap;
                }
            }
            else
            {
                u_int64_t is_ooo = getBitMask(pos_adj, IsOutOrderStart, IsOutOrderBits);
                if (!is_ooo)
                {
                    if (((u_int64_t) (dest_node & thread_prefix)) == getBitMask(pos_adj, DNPStart, DNPBits))
                    {
                        // issue is here degree > ((uint32_t) 1 << StartFixedBlockThreshold)-1)
                        if (!(edgePtr & 0x1))
                        {
                            // should see the degree part issue is upward
                            PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

                            u_int64_t indr_index = getBitMask(pos_adj, L1IndexStart, L1IndexBits);
                            u_int64_t indr_size = (u_int64_t) 1 << getBitMask(pos_adj, L1SizeStart, L1SizeBits);

                            // lock on per source vertex to get the index_block edge block
                            u_int64_t index_block = (((u_int64_t) 1 << ptr_ind->getBlockLen()) - (indr_size - indr_index));

                            u_int64_t edge_block_index = getBitMask(pos_adj, L2IndexStart, L2IndexBits);

                            u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(pos_adj, L2SizeStart, L2SizeBits);
                            // per block deletion lock

                            u_int64_t cur_edge_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_ind->getperVertexBlockArr()[index_block]); //->getBlockSize();

                            u_int64_t index_edge_block = ((cur_edge_block_size / 2) - ((edge_block_size / 2) - edge_block_index));


                            // per block deletion lock
                            if (*reinterpret_cast<uint64_ptr>(ptr_ind->getperVertexBlockArr()[index_block] + ((index_edge_block) * 8)) != FLAG_TOMB_STONE)
                            {

                                if (remove_version_ooo(*reinterpret_cast<uint64_ptr>(ptr_ind->getperVertexBlockArr()[index_block] + ((index_edge_block) * 8))) == dest_node)
                                {
                                    ap->indrIndex = index_block;
                                    ap->edgeBlockIndex = index_edge_block;
                                    ap->hashTableIndex = index;
                                    return ap;
                                }
                            }
                        }
                        else
                        {
                            // edge block entry
                            // edge block index remove
                            stalb_type *ptr_edge;
                            PerSourceIndrPointer *ptr_ind;

                            ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(edgePtr));

                            u_int64_t edge_block_index = getBitMask(pos_adj, L2IndexStart, L2IndexBits);

                            u_int64_t edge_block_size = (u_int64_t) 1 << getBitMask(pos_adj, L2SizeStart, L2SizeBits);

                            // per block deletion lock
                            u_int64_t cur_edge_block_size = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(ptr_edge);

                            u_int64_t index_edge_block = ((cur_edge_block_size / 2) - ((edge_block_size / 2) - edge_block_index));

                            if (*reinterpret_cast<uint64_ptr>(ptr_edge + ((index_edge_block) * 8)) != FLAG_TOMB_STONE)
                            {
                                if (remove_version_ooo(*reinterpret_cast<uint64_ptr>(ptr_edge + ((index_edge_block) * 8))) == dest_node)
                                {

                                    ap->edgeBlockIndex = index_edge_block;
                                    ap->hashTableIndex = index;
                                    return ap;
                                }
                            }
                        }
                    }
                }
                else
                {
                    // out-of-order update part
                    if (((u_int64_t) (dest_node & thread_prefix_OOO)) == getBitMask(pos_adj, DNPStart_OOO, DNP_ooo_bits))
                    {

                        //u_int64_t is_stal = getBitMask(pos_adj, IsSTALStart, IsSTALBits);

                        // issue is here degree > ((uint32_t) 1 << StartFixedBlockThreshold)-1)
                        if (!(edgePtr & 0x1))
                        {

                            PerSourceIndrPointer *ptr_ind = reinterpret_cast<PerSourceIndrPointer *>(get_pointer(edgePtr));

                            timestamp_t src_timestamp = getBitMask(pos_adj, t_s_start, t_s_bits);

                            //ArtPerIndex *dest_id_ooo = getOutOfOrderDestNode(src_timestamp, 1,  ptr_ind);
                            stalb_type *stalb_block_art;
                            ArtPerIndex *art_per_index;
                            vertex_t ooo_field = (getOutOfOrderDestNode(src_timestamp, 1, ptr_ind));

                            if (!check_version(ooo_field))
                            {
                                stalb_block_art = reinterpret_cast<stalb_type *>(remove_version(ooo_field));
                            }
                            else
                            {
                                // ART return
                                art_per_index = reinterpret_cast<ArtPerIndex *>(remove_version(ooo_field));

                                art::art < stalb_type * > *art_val = art_per_index->getArtTree();
                                //%%%%%%%%%%%% adaptive radix tree scan start
                                // ei->outOfOrderUpdt.scanArtTree();
                                auto it = art_val->begin(std::to_string(src_timestamp).c_str());
                                stalb_block_art = *it;
                            }
                            /// binary search to find the key
                            u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(stalb_block_art);

                            uint64_t pos = findIndexForBlockArrOOO(stalb_block_art, *reinterpret_cast<uint16_ptr>(stalb_block_art + 1), (eb_block_size_ooo / 2), src_timestamp);

                            LeafNode *leaf = *reinterpret_cast<LeafNode **>((stalb_block_art) + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(stalb_block_art)) / 2) * 8) + ((pos * 8) - 8));

                            // per block deletion lock
                            if (remove_version_ooo(*reinterpret_cast<uint64_ptr>(stalb_block_art + (pos * 8))) == dest_node)
                            {
                                *pos_ooo = pos;
                                if (!check_version(ooo_field))
                                {
                                    ap->indrIndex = add_version(reinterpret_cast<vertex_t>(stalb_block_art));
                                }
                                else
                                {
                                    ap->indrIndex = add_version_read(add_version(reinterpret_cast<vertex_t>(stalb_block_art)));
                                }
                                if (!check_version(ooo_field))
                                {
                                    ap->edgeBlockIndex = add_version(reinterpret_cast<vertex_t>(leaf));;
                                }
                                else
                                {
                                    ap->edgeBlockIndex = add_version_read(reinterpret_cast<vertex_t>(stalb_block_art));;
                                }
                                ap->hashTableIndex = index;
                                return ap;
                            }
                        }
                        else
                        {
                            // only edge block ... degree < 1024
                            stalb_type *ptr_edge;

                            ptr_edge = reinterpret_cast<stalb_type *>(get_pointer(edgePtr));

                            timestamp_t src_timestamp = getBitMask(pos_adj, t_s_start, t_s_bits);

                            //ArtPerIndex *dest_id_ooo = getOutOfOrderDestNode(src_timestamp, 1,  ptr_ind);
                            // ########### ART
                            stalb_type *stalb_block_art;
                            ArtPerIndex *art_per_index;
                            vertex_t ooo_field = (getOutOfOrderDestNode(src_timestamp, 0, ptr_edge));

                            if (!check_version(ooo_field))
                            {
                                stalb_block_art = reinterpret_cast<stalb_type *>(remove_version(ooo_field));
                            }
                            else
                            {
                                // ART return
                                art_per_index = reinterpret_cast<ArtPerIndex *>(remove_version(ooo_field));

                                art::art < stalb_type * > *art_val = art_per_index->getArtTree();
                                //%%%%%%%%%%%% adaptive radix tree scan start
                                auto it = art_val->begin(std::to_string(src_timestamp).c_str());
                                stalb_block_art = *it;
                            }

                            // ##########

                            /// binary search to find the key
                            u_int64_t eb_block_size_ooo = (u_int64_t) 1 << *reinterpret_cast<uint8_ptr>(stalb_block_art);

                            uint64_t pos = findIndexForBlockArrOOO(stalb_block_art, *reinterpret_cast<uint16_ptr>(stalb_block_art + 1), (eb_block_size_ooo / 2), src_timestamp);

                            LeafNode *leaf = *reinterpret_cast<LeafNode **>((stalb_block_art) + ((((uint64_t) 1 << *reinterpret_cast<uint8_ptr>(stalb_block_art)) / 2) * 8) + ((pos * 8) - 8));

                            // per block deletion lock
                            if (remove_version_ooo(*reinterpret_cast<uint64_ptr>(stalb_block_art + (pos * 8))) == dest_node)
                            {
                                *pos_ooo = pos;
                                if (!check_version(ooo_field))
                                {
                                    ap->indrIndex = add_version(reinterpret_cast<vertex_t>(stalb_block_art));
                                }
                                else
                                {
                                    ap->indrIndex = add_version_read(add_version(reinterpret_cast<vertex_t>(art_per_index)));
                                }
                                if (!check_version(ooo_field))
                                {
                                    ap->edgeBlockIndex = add_version(reinterpret_cast<vertex_t>(leaf));;
                                }
                                else
                                {
                                    // return stalb instead of leaf
                                    ap->edgeBlockIndex = add_version_read(reinterpret_cast<vertex_t>(stalb_block_art));;
                                }
                                ap->hashTableIndex = index;
                                return ap;
                            }
                        }
                    }
                }
            }
        }

        index = index + 1;
        if (index >= hashPtr->hashBlockSize)
        {
            index = 0;
        }
        if (count == hashPtr->hashBlockSize)
        {
            break;
        }
    }

    return ap;

}

void PerSourceVertexIndr::initHashTable(MemoryAllocator *la, int thread_id)
{
    hashPtr = static_cast<HashTablePerSource *>(la->Allocate(sizeof(HashTablePerSource), thread_id));
    hashPtr->hashBlockSize = 2;
    hashPtr->hashtable = static_cast<u_int64_t *>(la->Allocate(16, thread_id));
    for (int i = 0; i < 2; i++)
    {
        hashPtr->hashtable[i] = FLAG_EMPTY_SLOT_HT;
    }
    hashPtr->entryCounter = 0;
}

void PerSourceVertexIndr::setLatestSrcTime(timestamp_t lct)
{
    latestSrcTime = lct;
}

timestamp_t PerSourceVertexIndr::getLatestSrcTime()
{
    return latestSrcTime;
}

HashTablePerSource *PerSourceVertexIndr::getHashTable()
{
    return hashPtr;
}
