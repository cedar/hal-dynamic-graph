/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This C++ header file is used to manipulate binary dataset files.
 ******************************************************************************/


#include "BlockAdjacencyList.h"

#include <iostream>


/* ArtPerIndex class defination */


// insert Adjacency Edge Metadata
void ArtPerIndex::insertArtLeaf(timestamp_t source_key, stalb_type *block)
{
    //  Key key;
    // loadKey(ln->getSrcTime(), key);


    artTree->set(std::to_string(source_key).c_str(), block);

}
// initialize art of specfic index for per source adjacency list
void ArtPerIndex::initArtTree()
{
    artTree = new art::art<stalb_type *>();
    //count =0;
}
// get the Adjacent Edge Metadata in other word leaf
stalb_type * ArtPerIndex::getLeaf(timestamp_t src_time)
{
    return artTree->get(std::to_string(src_time).c_str());
}
void ArtPerIndex::removeArtLeaf(timestamp_t src_time)
{
    artTree->del(std::to_string(src_time).c_str());
}

art::art<stalb_type *>* ArtPerIndex::getArtTree() const
{
    return artTree;
}

/* EdgeInfoHal class defination */

EdgeInfoHal::EdgeInfoHal()
{
    outOfOrderUpdt = NULL;
}
void EdgeInfoHal::setSTime(timestamp_t stime)
{
    sTime = stime;
}

void EdgeInfoHal::setOutOfOrderUpdt(vertex_t outOfOrderUpdt)
{
    this->outOfOrderUpdt = outOfOrderUpdt;
}

vertex_t EdgeInfoHal::getOutOfOrderUpdt()
{
    return outOfOrderUpdt;
}
void EdgeInfoHal::setInvldTime(ITM* de)
{
    invld_time = de;
}
void EdgeInfoHal::setWTsEdge(timestamp_t wtsE)
{
    this->wTs_edge = wtsE;
}
ITM*  EdgeInfoHal::getInvldTime()
{
    return invld_time;
}
timestamp_t EdgeInfoHal::getWTsEdge()
{
    return wTs_edge;
}
timestamp_t EdgeInfoHal::getSTime() const
{
    return sTime;
}


/* EdgeBlock class defination */

void EdgeBlock::lock()
{
    perBlockLock.lock();
}
void EdgeBlock::unlock()
{
    perBlockLock.unlock();
}
void EdgeBlock::resizeEdgeBlock(MemoryAllocator *me,int thread_id,uint8_t block_size)
{
    int16_t  block_s = (int16_t) 1 << block_size;
    edgeUpdt     = reinterpret_cast<EdgeInfoHal **>(me->Allocate(8 * block_s,thread_id));//reinterpret_cast<EdgeInfoHal **>(aligned_alloc(8,8 * START_BLOCK_UPDATE_SIZE));
    memset(edgeUpdt, 0, 8 * block_s);	//reset new array

    isDeletion   = false;
    isOutOfOrder = false;
    blockSize    = block_size;
    curIndx      = block_s;
    emptySpace   = 0;

}
void EdgeBlock::UpEdgeBlock(MemoryAllocator *me, int thread_id, uint8_t cur_block_size)
{
    uint8_t prev_size = cur_block_size;
    uint8_t new_size  = ++cur_block_size;
    int16_t new_size_power = ((int16_t) 1 << new_size);

    EdgeInfoHal **temp =  static_cast<EdgeInfoHal **>(me->Allocate(8 * new_size_power,thread_id));
    memset(temp, 0, 8 * new_size_power);
    vertex_t  *temp_destId   =  static_cast<vertex_t*>(me->Allocate(8 * new_size_power,thread_id));
    memset(temp_destId, 0, 8 * new_size_power);

    double  *temp_p   =  static_cast<double *>(me->Allocate(8 * new_size_power,thread_id));
    memset(temp_p, 0, 8 * new_size_power);
//  FixedTimeIndexBlock **temp2;
// todo deletion mechanism ()create node in globle linklist and add the address of deleted index array and with time to replace
// the garbage collector will deallocate the memory than
    u_int64_t k = 0;
    int16_t tempC = ((int16_t) 1 << prev_size);

    memcpy(temp+tempC,edgeUpdt,tempC*8);
    memcpy(temp_destId+tempC,destId,tempC*8);
    memcpy(temp_p+tempC,property,tempC*8);

    blockSize = new_size;

    curIndx   = tempC;
    void *ptr = edgeUpdt;
    void *dest_ptr = destId;
    void *tmp_ptr = property;

    edgeUpdt = temp;
    destId   = temp_destId;
    property = temp_p;
    me->free(ptr,8 * tempC,thread_id);
    me->free(dest_ptr,8 * tempC,thread_id);
    me->free(tmp_ptr,8 * tempC,thread_id);

//  free(ptr);
// blenprev++;

}
void EdgeBlock::initEdgeBlock(MemoryAllocator *me,int thread_id,int16_t prev_block_size)
{
    int16_t block_size = prev_block_size;
    edgeUpdt = reinterpret_cast<EdgeInfoHal **>(me->Allocate(8 * block_size, thread_id));
    destId   =  reinterpret_cast<vertex_t *>(me->Allocate(8 * block_size, thread_id));
    property   =  static_cast<double *>(me->Allocate(8 * block_size,thread_id));
    memset(property, 0, 8 * block_size);
    memset(edgeUpdt, 0, 8 * block_size);
    memset(destId, 0, 8 * block_size);

    isDeletion = false;
    isOutOfOrder = false;
    blockSize    = log2(block_size);
    curIndx      = block_size;
    emptySpace   = 0;
}
void EdgeBlock::setEdgeUpdt(EdgeInfoHal *e, int64_t dest_node, double  w)
{

    edgeUpdt[--curIndx] = e;
    destId[curIndx] = dest_node;
    property[curIndx] = w;
}

void EdgeBlock::setCurIndex(int16_t b)
{
    curIndx = b;
}
void EdgeBlock::setIsDeletion(bool del)
{
    isDeletion = del;
}
void EdgeBlock::setIsOutOfOrder(bool ooo)
{
    isOutOfOrder = ooo;
}
void EdgeBlock::setBlockSize(uint8_t bsize)
{
    blockSize = bsize;
}
void EdgeBlock::setEmptySpace()
{
    emptySpace++;
}
EdgeInfoHal** EdgeBlock::getEdgeUpdate()
{
    return edgeUpdt;
}
vertex_t* EdgeBlock::getDestIdArr()
{
    return destId;
}
uint8_t EdgeBlock::getBlockSize()
{
    return blockSize;
}
int16_t EdgeBlock::getEmptySpace()
{
    return emptySpace;
}
int16_t EdgeBlock::getCurIndex() const
{
    return curIndx;
}
bool EdgeBlock::getIsDeletion()
{
    return isDeletion;
}
bool EdgeBlock::getIsOutOfOrder()
{
    return isOutOfOrder;
}
double* EdgeBlock::getpropertyIdArr()
{
    return property;
}