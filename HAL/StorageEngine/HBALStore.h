/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This C++ header file is used to manipulate binary dataset files.
 ******************************************************************************/


#ifndef OUR_IDEA_READSTORE_H
#define OUR_IDEA_READSTORE_H
#include <fstream>
#include <iostream>
#include <map>
#include <random>
#include <string>
#include <unordered_map>
#include <chrono>
#include <thread>
#include <memory>
#include <ctime>
#include <sys/sysinfo.h> // linux specific
#include <unistd.h> // chdir, gethostname

#include "BlockAdjacencyList.h"
#include "VertexBlock.h"
#include "../MemoryPool/BlockAllocator.hpp"
#include "../DataLoad/InputEdge.h"
#include "../ThirdParty/gapbs/gapbs.hpp"
#include <set>

using NodeID = uint64_t;
using WeightT = double;
static const size_t kMaxBin = std::numeric_limits<size_t>::max()/2;
struct UPBlockSizePointer
{
    stalb_type *prev_stalb;

    stalb_type *new_stalb;

    double *prev_property_arr;

    uint64_t prev_block_size;
};
struct ReadTable
{

    RWSpinLock lock;
   // std::mutex lock;
    timestamp_t readQuery[40];
    int len;
    ReadTable()
    {
        lock.init();
        for(int i =0;i<40;i++)
        {
            readQuery[i] = std::numeric_limits<timestamp_t>::max();
        }
        len = 40;
    }
    void initReadTable()
    {
       // lock.initLock();
        for(int i =0;i<40;i++)
        {
            readQuery[i] = std::numeric_limits<timestamp_t>::max();
        }
        len = 40;
    }
};
struct EdgeInfoPtr
{
    EdgeInfoHal *ptr;
    EdgeInfoPtr *next;
};
struct LeafNodePtr
{
    LeafNode *ptr;
    LeafNodePtr *next;
};
// in case the read only transaction wts < ITM (WTS)
struct InvalidList
{
    EdgeInfoPtr *edgeInfo;
    LeafNodePtr *leafInfo;
};

struct InvalidLinkListNode
{
    //stalb_type* ptr;
    uint64_t edge_block_size;
    timestamp_t latest_Del_Time;
    uint64_t block_index;
    uint64_t indr_block_size;
    InvalidLinkListNode* next;
};
struct  DegreeNode
{
    vertex_t degree;
    timestamp_t wts;
    DegreeNode *next;
};
struct InvalidListVal
{
    InvalidLinkListNode *head;
    InvalidLinkListNode *tail;
};
struct InvalidDegree
{
    void *ptr;
    InvalidDegree *next;
};
struct InvalidNodeAddress{
    void *ptr = NULL;
    spinlock lock{0};
};
struct InvalidEdgeInfoAddress{
    void *ptr = NULL;
    spinlock lock{0};
};

class HBALStore
{
public:
  //  HBALStore();
    void release_vertex_lock(vertex_t v);
    void aquire_vertex_lock(const vertex_t v);
    void executeEdgeInsertion(InputEdge inputedge_info, MemoryAllocator *me, int thread_id);
    void getMemoryConsumptionBreakDownUpdates(MemoryAllocator *ga);
    void getMemoryConsumptionBreakDown(MemoryAllocator *ga);
    void edgeBlockGC(vertex_t phy_src_id,bool is_single_block,  MemoryAllocator *me, int thread_id);
    inline int findIndexForBlockArrOOO(stalb_type *arr, int start, int eb_block_size, timestamp_t K);
    inline int findIndexForIndr(stalb_type **arr, int start, int n, timestamp_t K);
    inline int findIndexForBlockArr(stalb_type *arr, int start, int n, timestamp_t K);
    void InsertEdge(vertex_t phy_src_id,vertex_t phy_dest_id, InputEdge inputedge_info, timestamp_t commit_time, MemoryAllocator *me, int thread_id);
    vertex_t outOfOrderInsertion(timestamp_t commit_time, InputEdge inputedge_info, LeafNode *ln , vertex_t phy_src_id, vertex_t phy_dest_id, MemoryAllocator *me, int thread_id, bool IsDelete = false, stalb_type *stalb_ln = NULL);
    void* rollback();
    void executeEdgeDeletion(InputEdge inputedge_info, MemoryAllocator *me, int thread_id);
    void addDegreeVersion(timestamp_t  commit_time, vertex_t phy_src_id, vertex_t phy_dest_id, MemoryAllocator *me, int thread_id);
    void setITMBlockDuplicate(vertex_t stalb_cur_version, vertex_t metadatablock_greater, vertex_t metadata_block_smaller, int thread_id, MemoryAllocator *me, bool flag, bool isDelete = false, vertex_t index_pos = 0);
    vertex_t inOrderInsertion(timestamp_t commit_time, InputEdge inputedge_info, EdgeInfoHal *ei, vertex_t phy_src_id, vertex_t phy_dest_id, MemoryAllocator *me, int thread_id, bool IsDelete = false, stalb_type *stalb_cur = NULL);
    timestamp_t getTimestampSrc(InputEdge inputedge_info, bool is_in_order, MemoryAllocator *la =NULL);
    vertex_t getEdgeMetaData(vertex_t phy_src_id, AdjacentPos *ap, stalb_type *stalb_cur);
    void setStalbMetadata(vertex_t stalb_cur_version,vertex_t index_pos= 0, bool flag = false,EdgeInfoHal *edge_info_smaller = NULL);
    void dupInsertionGreaterNewInsertion(DuplicateUpdate *prev, DuplicateUpdate *dup, DVE *dve, stalb_type *stalb_cur, vertex_t metadatablock_greater, vertex_t metadata_block_smaller, timestamp_t source_time, AdjacentPos *ap_pos_prev, vertex_t stalb_inserted_pos, vertex_t phy_src_id, int thread_id, MemoryAllocator *me);
    void setITMBlockDelete( DuplicateUpdate *dup, vertex_t smaller_metadata_block, int thread_id, MemoryAllocator *me);
    int findPosInSTALToStartLinearSearch(stalb_type **arr, int start, int n, timestamp_t K);
    int findPosInSTALBToStartLinearSearch(stalb_type *arr, int start, int eb_block_size, timestamp_t K, vertex_t *ooo_pos_start , vertex_t *stalb_addr_ooo);
    int findPosInSTALBBToStarLinearSearchInOOO(stalb_type *arr, int start, int eb_block_size, timestamp_t K);
    void findDuplicateInsertionPosInSTALBBinarySearchByLatestDeletion(int *dup_indr_pos, int *prev_indr_pos,vertex_t *stalb_dup, vertex_t *stalb_prev, vertex_t *pos_dup, vertex_t *pos_prev, vertex_t phy_src_id, timestamp_t latest_Deletion, vertex_t dst_id, InputEdge inputedge_info);
    void findNewInsertionPrevAndNextPos(int *dup_indr_pos, int *prev_indr_pos,bool isIndr,int indexIndrArray,int indexFixedBlock,vertex_t *ooo_pos_start,bool flag_ooo, vertex_t *stalb_dup, vertex_t *stalb_prev, vertex_t *pos_dup, vertex_t *pos_prev, vertex_t phy_src_id, timestamp_t latest_Deletion, vertex_t dst_id, InputEdge inputedge_info);
    ITM* createITMBlockAndUpdated(ITM *itm, vertex_t srctime, int thread_id, MemoryAllocator *me);
    vertex_t setHashtableUpiInorder(vertex_t phy_src_id, vertex_t pos, vertex_t edgeblocksize, int dup_indr_pos);
    void dupPrevExistStalDeleted(timestamp_t commit_time, vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info, vertex_t *dup_stalb, vertex_t *pos_dup, int *dup_indr_pos, DVE *dve, vertex_t *prev_stalb, vertex_t *pos_prev, DuplicateUpdate *dup, DuplicateUpdate *prev_f, int thread_id, MemoryAllocator *me, bool flag);
    void prevExistDupNotExist(timestamp_t commit_time, vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info, vertex_t *prev_stalb, vertex_t *pos_prev, DVE *dve, int thread_id, MemoryAllocator *me, bool flag = false, DuplicateUpdate *dup = NULL, DuplicateUpdate *prev_f = NULL);
    void DupExistPrevNotExist(timestamp_t commit_time,vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info, vertex_t *dup_stalb,vertex_t *pos_dup,int *dup_indr_pos, DVE *dve, int thread_id, MemoryAllocator *me);
    void newInsertionSrctimeGreaterThanLatestDeletion(timestamp_t commit_time,bool *is_degree_add, vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info,DVE *dve,int thread_id, MemoryAllocator *me);
    void dveNextNullAndLatestDelGreaterThanInsertion(timestamp_t commit_time,vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info, DVE *dve,int thread_id, MemoryAllocator *me);
    void dveNextDeletionLessThanLatestDelForInsertion(timestamp_t commit_time,vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info,DVE *dve,int thread_id, MemoryAllocator *me, DuplicateUpdate *dup, DuplicateUpdate *prev);
    void dveNextInsertionLessThanLatestDelForInsertion(timestamp_t commit_time, vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info,DVE *dve,int thread_id, MemoryAllocator *me, DuplicateUpdate *dup, DuplicateUpdate *prev);
    void dveNextExistInsertion(timestamp_t commit_time, vertex_t phy_src_id, vertex_t phy_dest_id, InputEdge inputedge_info,DVE *dve,int thread_id, MemoryAllocator *me);

    // %%%%%%%% deletion part %%%%%%%%%%

    void removeEdge(vertex_t phy_src_id, vertex_t phy_dest_id, timestamp_t commit_time, InputEdge inputedge_info, MemoryAllocator *me, int thread_id);
    void inOrderDeletion(timestamp_t commit_time,vertex_t phy_src_id, vertex_t phy_dest_id, AdjacentPos *ap_pos, InputEdge inputedge_info, MemoryAllocator *me, int thread_id, AdjacentPos *old_adjc_pos);
    void outOfOrderDeletion(vertex_t phy_src_id, vertex_t phy_dest_id, AdjacentPos *ap_pos, uint64_t *pos_ooo, InputEdge inputedge_info, MemoryAllocator *me, int thread_id, vertex_t adjc_pos);
    // %%%%%%%% deletion part end %%%%%%


    static void initVertexIndrList(MemoryAllocator *la);
    static void resizeVertexArrayHal(vertex_t devVertexIndex, MemoryAllocator *ptr, int thread_id);
    static void initArt();
    static PerSourceVertexIndr*** getVertexIndrList();
    void gcDegreeList(vertex_t phy_src_id, MemoryAllocator *me, int thread_id);
    timestamp_t getMinReadDegreeVersion();
    bool checkIsDuplicateExist(vertex_t phy_src_id, vertex_t phy_dest_id);
    static vertex_t getMaxSourceVertex();
    bool InsertSrcVertex(vertex_t  vertex_id, MemoryAllocator *me,int thread_id);
    bool InsertDestVertex(vertex_t  vertex_id,MemoryAllocator *me, int thread_id);
    bool CheckOutOfOrder(timestamp_t src_timestamp, vertex_t phy_src);
    stalb_type* DownEdgeBlock(uint8_t cur_block_size, MemoryAllocator *me, int thread_id);
    void SplitBlock(stalb_type* per_stalb, stalb_type* new_stalb_block, uint64_t new_block_size, uint64_t split_main_block_index, uint64_t per_stalb_cur_index, bool flag);
    void StoreKeyInBlock(stalb_type* stalb_block,uint64_t pos, vertex_t phy_dest_id, LeafNode* ln, double weight_w, bool IsDelete = false);

    void StoreBlockInArt(stalb_type* current_block, stalb_type* new_block, uint64_t current_block_size, uint64_t new_block_size, EdgeInfoHal *setOutOfOrder,ArtPerIndex *perArt);
    void degreeDecrement(timestamp_t commit_time,vertex_t phy_src_id, MemoryAllocator *me, int thread_id);
    void addDuplicateNodeBaseOnSourceTimeInDVE(DuplicateUpdate *val, DVE *dve);
    void removeDuplicateNodeBaseOnSourceTimeInDVE(DuplicateUpdate *val, DVE *dve, MemoryAllocator *me, int thread_id);
    void findNewInsertionNextPos(int *dup_indr_pos, bool isIndr,int indexIndrArray,int indexFixedBlock,vertex_t *ooo_pos_start,bool flag_ooo, vertex_t *stalb_dup, vertex_t *pos_dup, vertex_t phy_src_id, timestamp_t latest_Deletion, vertex_t dst_id, InputEdge inputedge_info);
    void findDeletionInsertionPosInSTALBBinarySearchByNewDeletion(int *dup_indr_pos,vertex_t *stalb_dup, vertex_t *pos_dup, vertex_t phy_src_id, timestamp_t latest_Deletion, vertex_t dst_id, InputEdge inputedge_info);
    void updateNewSTALB(vertex_d cur_edge_b, vertex_t new_edge_b, vertex_t phy_src_id, stalb_type* old_eb, stalb_type* new_eb, property_type* old_property, MemoryAllocator *me, int thread_id, bool ooo_flag = false);
    EdgeInfoHal* getOutOfOrderFieldFromSTAL(vertex_t phy_src_id,timestamp_t key, MemoryAllocator *me, int thread_id);
    std::unique_ptr<double[]>  do_scanedges(uint64_t max_vertex_id, uint64_t num_iterations, double damping_factor, int read_version, MemoryAllocator *me);
    void removeBaseOnSourceTimeInDVE(timestamp_t src_time, DVE *dve, MemoryAllocator *me, int thread_id);
    void upi_zero_insertion_edge(timestamp_t commit_time,vertex_t phy_src_id, vertex_t phy_dest_id,InputEdge inputedge_info,AdjacentPos *ap_pos,vertex_t *pos_ooo, MemoryAllocator *me, int thread_id);
    ////
    vertex_t logical_id(vertex_t v);
    unsigned long physical_id(vertex_t v);

    PerSourceVertexIndr const &operator[](size_t index) const;
    PerSourceVertexIndr &operator[](size_t v);
    size_t get_vertex_count(vertex_t version);

    size_t get_high_water_mark();
    /////


    std::unique_ptr<int64_t[]> do_bfs(uint64_t num_vertices, uint64_t num_edges, uint64_t  max_vertex_id, uint64_t root,int alpha = 15, int beta = 18);
    /*  static KeyIndexStore* getArt()
    {
        return m;
    }*/
    static vertex_t getNumEdges();
    static void setNumEdges(vertex_t c);
    static vertex_t gettraverse_count_t();
    static void settraverse_count_t(vertex_t c);
    static void setLoadingTime(double loadTime);
    static double getLoadingTime();
    static std::mutex vertexTableResizeLock;
    static vertex_t outOfOrderCout;
    ReadTable *readTable;
    InvalidListVal **invalList;
    spinlock testLock;
    u_int64_t  *perThreadNumberOfEdges;
    u_int64_t  *perThreadNumberOfvertices;
    InvalidNodeAddress  **invalidListDegree;
    InvalidEdgeInfoAddress  **InvalidEdgeInfo;

    int numberOfThread;
    std::ofstream foutput;
    void getLogClose();

    UPBlockSizePointer* UpEdgeBlock(stalb_type* stal, uint8_t cur_block_size, MemoryAllocator *me, int thread_id);

    /* Analytics functions */
    u_int64_t CalculateNumberOfEdges();
    u_int64_t CalculateNumberOfVertices();
    bool has_vertex(vertex_t v);
    PerSourceVertexIndr*  getSourceVertexPointer(vertex_t src);
    vertex_t perSourceGetDegree(vertex_t v, int read_version);
    timestamp_t getMinReadVersion();

    std::unique_ptr<int64_t[]> do_bfs_init_distances(uint64_t max_vertex_id, int read_version);

    std::unique_ptr<int64_t[]> do_bfs(uint64_t num_vertices, uint64_t max_vertex_id, uint64_t root, int alpha = 15, int beta = 18, int read_version = 0);

    void do_bfs_BitmapToQueue(uint64_t max_vertex_id, const gapbs::Bitmap &bm, gapbs::SlidingQueue<int64_t> &queue);

    void do_bfs_QueueToBitmap(uint64_t max_vertex_id, const gapbs::SlidingQueue<int64_t> &queue, gapbs::Bitmap &bm);

    int64_t do_bfs_BUStep(uint64_t max_vertex_id, int64_t *distances, int64_t distance, gapbs::Bitmap &front, gapbs::Bitmap &next, int read_version);

    int64_t do_bfs_TDStep(uint64_t max_vertex_id, int64_t *distances, int64_t distance, gapbs::SlidingQueue<int64_t> &queue, int read_version);

    //%%%%%%%%%%%%%%% pagerank functions declaration start %%%%%%%%%%%%%%%%%%%%

    std::unique_ptr<double[]> do_pagerank(uint64_t max_vertex_id, uint64_t num_iterations, double damping_factor, int read_version, MemoryAllocator *me);

    void pagerank_no_deletion_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total);

    void pagerank_deletion_no_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total);

    void pagerank_no_deletion_ooo_exist_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total);

    void pagerank_deletion_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total);

    // No read write page rank
    void pagerank_no_deletion_ooo_entries_stalb_no_readwrite(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total);

    void pagerank_deletion_no_ooo_entries_stalb_no_readwrite(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total);

    void pagerank_no_deletion_ooo_exist_entries_stalb_no_readwrite(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total);

    void pagerank_deletion_ooo_entries_stalb_no_readwrite(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total);


    // %%%%%%%%%%%%% pagerank functions end declaration %%%%%%%%%%%%%%%%%%%%%%%

    // %%%%%%%%%%%% historical queries per source degree %%%%%%%%%%%%%%%%%%%%%%

    void history_degree_deletion_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, int index_start, vertex_t *per_source_degree, timestamp_t src_time);
    void history_degree_no_deletion_ooo_exist_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, int index_start, vertex_t *per_source_degree, timestamp_t src_time);
    void history_degree_deletion_no_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, int index_start, vertex_t *per_source_degree, timestamp_t src_time);
    void history_degree_no_deletion_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, int index_start, vertex_t *per_source_degree);
    uint64_t perSourceHistoryDegreeList(uint64_t max_vertex_id, uint64_t num_iterations, double damping_factor, int read_version, MemoryAllocator *me);

    // %%%%%%%%%%%% historical queries per source degree end %%%%%%%%%%%%%%%%%%%

    // %%%%%%%%%%%% historical pagerank %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    void history_pagerank_no_deletion_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total, int index_start, timestamp_t src_time);
    void history_pagerank_deletion_no_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total, int index_start, timestamp_t src_time);
    void history_pagerank_no_deletion_ooo_exist_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total, int index_start, timestamp_t src_time);
    void history_pagerank_deletion_ooo_entries_stalb(stalb_type *eb_k, int l2_len, uint8_t *IsWTS, timestamp_t min_read_versions, gapbs::pvector<double> *outgoing_contrib, double *stalb_incomming_total, int index_start, timestamp_t src_time);

    std::unique_ptr<double[]> history_pagerank(uint64_t max_vertex_id, uint64_t timestamp, uint64_t num_iterations, double damping_factor, int read_version, MemoryAllocator *me);

    // %%%%%%%%%%%% historical pagerank end %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    // lcc
    std::unique_ptr<double[]> do_lcc_undirected(vertex_t max_vertex_id, MemoryAllocator *me);
    // wcc
    std::unique_ptr<uint64_t[]> do_wcc(uint64_t max_vertex_id);
// cdlp
    std::unique_ptr<uint64_t[]> do_cdlp(uint64_t max_vertex_id, bool is_graph_directed, uint64_t max_iterations);
// sssp
    std::vector<WeightT> do_sssp(uint64_t num_edges, uint64_t max_vertex_id, uint64_t source, double delta, int read_version, MemoryAllocator *me);
    vertex_t perSourceGetDegree(vertex_t v, int read_version, MemoryAllocator *me);

    /* Analytics function end */
    static bool isReadActive;
    static bool isDegreeActive;
    static bool IsReadWrite;
    InvalidLinkListNode *invalidBlock;
    spinlock invalLock;
    std::atomic<timestamp_t>  min_read_version = 0;
   // std::atomic<timestamp_t>  min_read_degree_version = 0;
   std::atomic<timestamp_t>  min_read_degree_version = 0;
    static bool NODELETEOOO;

    std::atomic<timestamp_t>  version = 0;

   // static spinlock querylock;
   //vertex_t version = 0;
  double analytics_time=0;
  bool IsOutOfOrder = 0;
  bool IsDuplicateSupport = 1;

    std::atomic<uint64_t> high_water_mark{0u};  // The next physical vertex id, not yet in use.
    std::atomic<uint64_t> vertex_count{0u};
    std::mutex growing_vector_mutex;
    static PerSourceVertexIndr*** vertexIndrList;
    static vertex_t  curIndVertexSize;
    static std::atomic<vertex_t>  maxSourceVertex;
    static int64_t  numEdges;
    tbb::concurrent_vector<PerSourceVertexIndr> index{INITIAL_VECTOR_SIZE, PerSourceVertexIndr()};

    l_t_p_table logical_to_physical{INITIAL_VECTOR_SIZE}; // took from sortledton
    tbb::concurrent_vector<vertex_t> physical_to_logical{INITIAL_VECTOR_SIZE,FLAG_EMPTY_SLOT};
    template<typename T>
    void grow_vector_if_smaller(tbb::concurrent_vector<T> &v, size_t s, T init_value) {
        if (v.capacity() <= s) {  // Only synchronize with other threads if potentially necessary
            std::scoped_lock<std::mutex> l(growing_vector_mutex);
            if (v.capacity() <= s)
            {
                v.grow_to_at_least(v.capacity() * 2,init_value);
            }
        }
    }
    /////// taken from sortledton


    static std::atomic<vertex_t>  traverse_count_t;
    static double  loadingTime;


};


//KeyIndexStore* HBALStore::m;

#endif //OUR_IDEA_READSTORE_H
