/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This C++ header file is used to manipulate binary dataset files.
 ******************************************************************************/


#ifndef OUR_IDEA_VERTEXBLOCK_H
#define OUR_IDEA_VERTEXBLOCK_H
#include "BlockAdjacencyList.h"
#include "../ThirdParty/oneTBB-2020.2/include/tbb/concurrent_hash_map.h"
#include "../ThirdParty/HashTable-Custom/HashTableCustom.h"
#include "../ThirdParty/RWSpinLock.h"
struct DuplicateUpdate
{
    vertex_t SrcTime;
    vertex_t UpdatePos;
    DuplicateUpdate *next;
};
struct DVE    // destination version entry
{
    vertex_t Dnode;
    vertex_t oldest_version;
    vertex_t l_del;
    DuplicateUpdate *next;
};
struct DestGc
{
    vertex_t Dnode;
    timestamp_t oldest_version;
};
struct HashTablePerSource
{
    u_int64_t  *hashtable;
    u_int32_t  entryCounter;
    u_int32_t  hashBlockSize;
};
struct AdjacentPos
{
    u_int64_t indrIndex;
    u_int64_t edgeBlockIndex;
    u_int64_t hashTableIndex;
};

struct PerSourceVertexIndr
{

    //  void *edgePtr; // if degree > threshold than STAL (two level vector implementation) otherwise single edge block array
    vertex_t edgePtr;
    HashTablePerSource *hashPtr;
    timestamp_t latestSrcTime;
    vertex_t degree;
    void*  invalidBlock;
   // std::mutex perVertexLock;
    spinlock perVertexLock{0};
    spinlock indirectionLock{0};
    bool degreeBit;

    // RWSpinLock lock{};

    // RWSpinLock perVertexLock{};


    PerSourceVertexIndr()
    {
       // lock_ = 0;
        hashPtr = NULL;

        edgePtr = 0;

        latestSrcTime = 0;

        degree = 0;
        degreeBit = false;
        // lock.init();
       // perVertexLock.initLock();
       // initLock();
    }
    PerSourceVertexIndr(const PerSourceVertexIndr& other) {
        edgePtr = other.edgePtr;
        degree = other.degree;
        hashPtr = other.hashPtr;
        latestSrcTime = other.latestSrcTime;
        // init lock
    }
    vertex_t getOutOfOrderDestNode(timestamp_t srctimestamp,bool isIndrArray, void *edgePtr);
    inline int findIndexForIndr(stalb_type **arr, int start, int n, timestamp_t K);
    inline int findIndexForBlockArr(stalb_type *arr, int start, int eb_block_size, timestamp_t K);
    inline int findIndexForBlockArrOOO(stalb_type *arr, int start, int eb_block_size, timestamp_t K);
    void initVal()
    {
        //hashPtr = NULL;

        edgePtr = 0;

      //  latestSrcTime = 0;

        degree = 0;
        degreeBit = false;

        //lock.init();

    }
    void incrDegree()
    {
        degree++;
    }
    void decDegree()
    {
        degree--;
    }
    uint32_t getDegree()
    {
        return degree;
    }
    void initLock()
    {
         //perVertexLock.init();
    }
    void setEdgePtr(vertex_t ptr,bool flag);

    void* getEdgePtr();

    void setPerVertexLock();

    void unsetPerVertexLock();

    void  setDegreePtr(vertex_t ptr, bool flag);
    void* getDegreePtr();

    // source time metadata
    void setLatestSrcTime(timestamp_t lct);
    timestamp_t getLatestSrcTime();

    // hash table metadata
    void initHashTable(MemoryAllocator *la,int thread_id);
    HashTablePerSource* getHashTable();
    void resizeHashTable(int requested_block, MemoryAllocator *la, int thread_id);
    u_int64_t hash(u_int64_t key, u_int64_t size);
    void removeDestNodeHashTableVal(vertex_t dest_node, vertex_t hashtable_index, timestamp_t source_timestamp, MemoryAllocator *la, int thread_id);

    void setDestNodeHashTableVal(vertex_t dest_node, timestamp_t adj_pos, MemoryAllocator *la, int thread_id);
    AdjacentPos* getDestNodeHashTableVal(vertex_t dest_node, AdjacentPos *ap, uint64_t *pos_ooo =0);
    AdjacentPos* getDestBySourceTimeNodeHashTableVal(vertex_t pos_adj, AdjacentPos *ap, uint64_t *pos_ooo);

};

// In case of two level vector
class PerSourceIndrPointer
{
public:
    stalb_type **blockArr;
    int curPos;
    int emptySpace;
    uint8_t blockLen;
    spinlock downsize_lock{0};
    bool is_deletion_block;
    void initVal()
    {
        blockArr = NULL;
        curPos = -1;
        blockLen = log2(1);
        emptySpace = 0;
        is_deletion_block = false;
    }
    void initBlockSize();
    void insertionLock();
    void insertionUnlock();


    void setIndexList(MemoryAllocator *me, int thread_id);

//setter

    void setEmptySpace();
    void setDeletionBlock(bool del);
    void setBlockLen(uint8_t blen);
    void setCurPos(int curPos);
    int addNewBlock(stalb_type *tb);
    void setPerVertexBlockArr(stalb_type**);
    stalb_type** getperVertexBlockArr();


    //getter
    int getEmptySpace();
    bool isDeletionBlock();
    uint8_t getBlockLen();
    int getCurPos() const;
};


#endif //OUR_IDEA_VERTEXBLOCK_H
