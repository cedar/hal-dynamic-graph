/*******************************************************************************
 * Copyright @ INRIA 2024
 * This file is part of [HAL dynamic graph anlaytics].
 * This C++ header file is used to manipulate binary dataset files.
 ******************************************************************************/

#ifndef OUR_IDEA_TRANSACTIONMANAGER_H
#define OUR_IDEA_TRANSACTIONMANAGER_H
#include "StorageEngine/HBALStore.h"
#include <stdio.h>
#include <stdlib.h>
class TransactionManager {
public:
    TransactionManager(int initial_space, int no_of_thread, int ooo, bool duplicate_support);
    static HBALStore* getGraphDataStore(bool isRead, int version_read);
    static MemoryAllocator* getMemory();
    void addReadThread(int id);
    void finishTransaction();
private:
    static HBALStore *graph;
    static MemoryAllocator *memAllocator;
    static thread_local size_t thread_id;

};


#endif //OUR_IDEA_TRANSACTIONMANAGER_H
