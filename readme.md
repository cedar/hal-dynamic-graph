# HAL Repository

This repository contains implementations related to our VLDB 2024 paper:  
**"Dynamic Graph Databases with Out-of-Order Updates."**

## Repository Structure:

- **HAL**  
  Original implementation as published in VLDB 2024.  
  Commit reference: `e6bc147e`

- **HAL-V**  
  Variant for experiments involving out-of-order insertions (as illustrated in Figure 9).

- **HAL-RocksDB** *(Enhanced Version)*  
  Enhanced version of HAL supporting vertex and edge properties storage using **RocksDB**. Includes new features beyond original VLDB publication.

---

##  Important Commit References:

- **HAL**: Original Implementation, VLDB'24  
  - Commit: `e6bc147e` (Stable)

- **HAL-V**: Out-of-order insertion variant (Fig. 9 VLDB) - Stable 

- **HAL-RocksDB**: Current enhanced implementation with RocksDB support (Actively maintained 🚧)

